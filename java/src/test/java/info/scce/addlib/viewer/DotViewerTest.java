package info.scce.addlib.viewer;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.add.ADDManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DotViewerTest extends DDManagerTest {

    private ADDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new ADDManager();
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testDotViewer() {

        /* Get some predicates */
        ADD predA = ddManager.namedVar("predA");
        ADD predB = ddManager.namedVar("predB");
        ADD predC = ddManager.namedVar("predC");

        /* Build a + b */
        ADD sumOfPredicatesAB = predA.plus(predB);
        predA.recursiveDeref();
        predB.recursiveDeref();

        /* Build a + b + c */
        ADD sumOfPredicatesABC = sumOfPredicatesAB.plus(predC);
        sumOfPredicatesAB.recursiveDeref();
        predC.recursiveDeref();

        /* Open and close viewer */
        DotViewer<ADD> viewer = new DotViewer<>();
        viewer.view(sumOfPredicatesABC, "ABC");
        viewer.closeAll();
        sumOfPredicatesABC.recursiveDeref();
    }
}
