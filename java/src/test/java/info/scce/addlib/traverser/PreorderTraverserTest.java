package info.scce.addlib.traverser;

import info.scce.addlib.dd.add.ADD;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PreorderTraverserTest extends TraverserTest {

    @Test
    public void testPreorder() {

        /*
         *       xor
         *          \
         *           x0
         *          /  \
         *         :    \
         *   xor0  |    |  xor1
         *       \ :    | /
         *        x1    x1
         *        | \  / |
         *        :  \/  :
         * const0 |  /\  | const1
         *       \: /  \ :/
         *        c0    c1
         */
        List<ADD> roots = Arrays.asList(xor(), xor0());
        List<ADD> actualSequence = new ArrayList<>();
        for (ADD f : new PreorderTraverser<>(roots))
            actualSequence.add(f);
        List<ADD> expectedSequence = Arrays.asList(xor(), xor0(), const0(), const1(), xor1());
        assertEquals(expectedSequence, actualSequence);
    }

    @Test
    public void testPreorder2() {

        /*
         *        or
         *          \
         *           x0
         *          /  \
         *         :    \
         *    or0  |     \
         *       \ :     |
         *        x1     |
         *        | \    |
         *        :  \   |
         * const0 |   \  | const1
         *       \:    \ |/
         *        c0    c1
         */
        List<ADD> roots = Arrays.asList(or(), or0());
        List<ADD> actualSequence = new ArrayList<>();
        for (ADD f : new PreorderTraverser<>(roots))
            actualSequence.add(f);
        List<ADD> expectedSequence = Arrays.asList(or(), or0(), const0(), const1());
        assertEquals(expectedSequence, actualSequence);
    }
}
