package info.scce.addlib.traverser;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.add.ADDManager;
import org.junit.After;
import org.junit.Before;

public class TraverserTest extends DDManagerTest {

    private ADDManager ddManager = null;
    private ADD xor = null;
    private ADD or = null;
    private ADD varOrderDependant = null;
    private ADD const0 = null;
    private ADD const1 = null;

    @Before
    public void setUp() {
        ddManager = new ADDManager();
    }

    @After
    public void tearDown() {
        if (xor != null) {
            xor.recursiveDeref();
            xor = null;
        }
        if (or != null) {
            or.recursiveDeref();
            or = null;
        }
        if (varOrderDependant != null) {
            varOrderDependant.recursiveDeref();
            varOrderDependant = null;
        }
        if (const0 != null) {
            const0.recursiveDeref();
            const0 = null;
        }
        if (const1 != null) {
            const1.recursiveDeref();
            const1 = null;
        }
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    protected ADD const0() {
        if (const0 == null)
            const0 = ddManager.constant(0);
        return const0;
    }

    protected ADD const1() {
        if (const1 == null)
            const1 = ddManager.readOne();
        return const1;
    }

    protected ADD xor() {
        if (xor == null) {

            /*
             *       xor
             *          \
             *           x0
             *          /  \
             *         :    \
             *   xor0  |    |  xor1
             *       \ :    | /
             *        x1    x1
             *        | \  / |
             *        :  \/  :
             * const0 |  /\  | const1
             *       \: /  \ :/
             *        c0    c1
             */
            ADD x0 = ddManager.ithVar(0);
            ADD x1 = ddManager.ithVar(1);
            xor = x0.xor(x1);
            x0.recursiveDeref();
            x1.recursiveDeref();
        }
        return xor;
    }

    protected ADD xor0() {
        return xor().e();
    }

    protected ADD xor1() {
        return xor().t();
    }

    protected ADD or() {
        if (or == null) {

            /*
             *        or
             *          \
             *           x0
             *          /  \
             *         :    \
             *    or0  |     \
             *       \ :     |
             *        x1     |
             *        | \    |
             *        :  \   |
             * const0 |   \  | const1
             *       \:    \ |/
             *        c0    c1
             */
            ADD x0 = ddManager.ithVar(0);
            ADD x1 = ddManager.ithVar(1);
            or = x0.or(x1);
            x0.recursiveDeref();
            x1.recursiveDeref();
        }
        return or;
    }

    protected ADD or0() {
        return or().e();
    }
}
