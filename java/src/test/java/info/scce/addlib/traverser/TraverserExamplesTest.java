package info.scce.addlib.traverser;

import info.scce.addlib.dd.add.ADD;
import org.junit.Test;

import static org.junit.Assert.*;

public class TraverserExamplesTest extends TraverserTest {

    @Test
    public void testXorExample() {

        /* Assert non-constants */
        assertFalse(xor().isConstant());
        assertFalse(xor().t().isConstant());
        assertFalse(xor().e().isConstant());
        assertFalse(xor0().isConstant());
        assertFalse(xor1().isConstant());

        /* Assert constants */
        assertTrue(xor().e().e().isConstant());
        assertTrue(xor().e().t().isConstant());
        assertTrue(xor().t().e().isConstant());
        assertTrue(xor().t().t().isConstant());
        assertTrue(const0().isConstant());
        assertTrue(const1().isConstant());

        /* Assert variable names */
        assertEquals("x0", xor().readName());
        assertEquals("x1", xor().t().readName());
        assertEquals("x1", xor().e().readName());
        assertEquals("x1", xor0().readName());
        assertEquals("x1", xor1().readName());

        /* Assert constant values */
        double eps = 0;
        assertEquals(0, xor().e().e().v(), eps);
        assertEquals(1, xor().e().t().v(), eps);
        assertEquals(1, xor().t().e().v(), eps);
        assertEquals(0, xor().t().t().v(), eps);
        assertEquals(0, const0().v(), eps);
        assertEquals(1, const1().v(), eps);
    }

    @Test
    public void testOrExample() {

        /* Assert non-constants */
        assertFalse(or().isConstant());
        assertFalse(or().e().isConstant());
        assertFalse(or0().isConstant());

        /* Assert constants */
        assertTrue(or().e().e().isConstant());
        assertTrue(or().e().t().isConstant());
        assertTrue(or().t().isConstant());
        assertTrue(const0().isConstant());
        assertTrue(const1().isConstant());

        /* Assert variable names */
        assertEquals("x0", or().readName());
        assertEquals("x1", or().e().readName());
        assertEquals("x1", or0().readName());

        /* Assert constant values */
        double eps = 0;
        assertEquals(0, or().e().e().v(), eps);
        assertEquals(1, or().e().t().v(), eps);
        assertEquals(1, or().t().v(), eps);
        assertEquals(0, const0().v(), eps);
        assertEquals(1, const1().v(), eps);
    }

    @Test
    public void testExamplesShareTerminals() {

        /* Assert shared 1 terminal */
        ADD orConst1 = or().t();
        ADD xorConst1 = xor().t().e();
        assertNotSame(orConst1, xorConst1);
        assertEquals(orConst1, xorConst1);
        assertEquals(xorConst1, const1());

        /* Assert shared 0 terminal */
        ADD orConst0 = or().e().e();
        ADD xorConst0 = xor().t().t();
        assertNotSame(orConst0, xorConst0);
        assertEquals(orConst0, xorConst0);
        assertEquals(xorConst0, const0());
    }
}
