package info.scce.addlib.layouter;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.add.ADDManager;
import info.scce.addlib.traverser.PreorderTraverser;
import org.junit.After;
import org.junit.Before;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class LayouterTest extends DDManagerTest {

    protected ADDManager ddManager = null;
    private ADD sumOfPredicatesABC = null;

    @Before
    public void setUp() {
        ddManager = new ADDManager();
    }

    @After
    public void tearDown() {
        if (sumOfPredicatesABC != null) {
            sumOfPredicatesABC.recursiveDeref();
            sumOfPredicatesABC = null;
        }
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    protected ADD sumOfPredicatesABC() {
        if (sumOfPredicatesABC == null) {

            /* Get variables */
            ADD predA = ddManager.namedVar("predA");
            ADD predB = ddManager.namedVar("predB");
            ADD predC = ddManager.namedVar("predC");

            /* a + b */
            ADD sumOfPredicatesAB = predA.plus(predB);
            predA.recursiveDeref();
            predB.recursiveDeref();

            /* a + b + c */
            sumOfPredicatesABC = sumOfPredicatesAB.plus(predC);
            sumOfPredicatesAB.recursiveDeref();
            predC.recursiveDeref();
        }
        return sumOfPredicatesABC;
    }

    /* Additional assertions */

    protected void assertNonOverlappingBoundingBoxes(Layouter<ADD> layouter) {
        Set<BoundingBox> bboxes = new HashSet<>();
        for (ADD f : new PreorderTraverser<>(layouter.roots())) {
            BoundingBox b = layouter.bbox(f);
            for (BoundingBox b2 : bboxes)
                assertFalse(b.overlaps(b2));
            bboxes.add(b);
        }
        assertFalse(bboxes.isEmpty());
    }

    protected void assertSizeGreaterThanZero(Layouter<ADD> layouter) {
        assertTrue(layouter.w() > 0);
        assertTrue(layouter.h() > 0);
        for (ADD f : new PreorderTraverser<>(layouter.roots())) {
            assertTrue(layouter.w(f) > 0);
            assertTrue(layouter.h(f) > 0);
        }
    }

    protected void assertInBoundingBox(Layouter<ADD> layouter) {
        for (ADD f : new PreorderTraverser<>(layouter.roots())) {

            /* Check coordinates */
            assertTrue(layouter.x() <= layouter.x(f));
            assertTrue(layouter.y() <= layouter.y(f));
            assertTrue(layouter.x(f) + layouter.w(f) <= layouter.x() + layouter.w());
            assertTrue(layouter.y(f) + layouter.h(f) <= layouter.y() + layouter.h());

            /* Check bounding boxes */
            assertTrue(layouter.bbox().contains(layouter.bbox(f)));
        }
    }

    protected void assertMirroringYieldsPositiveSizes(Layouter<ADD> layouter) {

        /* Derive expectations */
        double expectedMirroredX = -layouter.x() - layouter.w();
        double expectedMirroredY = -layouter.y() - layouter.h();
        ArrayList<Double> expectedMirroredXs = new ArrayList<>();
        ArrayList<Double> expectedMirroredYs = new ArrayList<>();
        for (ADD f : new PreorderTraverser<>(layouter.roots())) {
            expectedMirroredXs.add(-layouter.x(f) - layouter.w(f));
            expectedMirroredYs.add(-layouter.y(f) - layouter.h(f));
        }

        /* Mirrored coordinates */
        double oldFactorX = layouter.transformationFactorX();
        double oldOffsetX = layouter.transformationOffsetX();
        double oldFactorY = layouter.transformationFactorY();
        double oldOffsetY = layouter.transformationOffsetY();
        layouter.setTransformationX(-1, 0);
        layouter.setTransformationY(-1, 0);

        /* Assert mirrored coordinates */
        double actualMirroredX = layouter.x();
        double actualMirroredY = layouter.y();
        ArrayList<Double> actualMirroredXs = new ArrayList<>();
        ArrayList<Double> actualMirroredYs = new ArrayList<>();
        for (ADD f : new PreorderTraverser<>(layouter.roots())) {
            actualMirroredXs.add(layouter.x(f));
            actualMirroredYs.add(layouter.y(f));
            assertTrue(layouter.w(f) > 0);
            assertTrue(layouter.h(f) > 0);
        }
        assertEquals(expectedMirroredX, actualMirroredX, 0);
        assertEquals(expectedMirroredY, actualMirroredY, 0);
        assertEquals(expectedMirroredXs, actualMirroredXs);
        assertEquals(expectedMirroredYs, actualMirroredYs);

        /* Restore transformation */
        layouter.setTransformation(oldFactorX, oldOffsetX, oldFactorY, oldOffsetY);
    }
}
