package info.scce.addlib.layouter;

import org.junit.Test;

import static org.junit.Assert.*;

public class BoundingBoxTest {

    @Test
    public void testContainment() {
        BoundingBox outer = new BoundingBox(-12, 34, 253, 8374);
        BoundingBox inner = new BoundingBox(-3, 123, 123, 6374);

        /* Contained */
        assertTrue(outer.contains(outer));
        assertTrue(inner.contains(inner));
        assertTrue(outer.contains(inner));

        /* Not contained */
        assertFalse(inner.contains(outer));
    }

    @Test
    public void testContainment2() {
        BoundingBox upper = new BoundingBox(5, -1, 2, 4);
        BoundingBox lower = new BoundingBox(1, 2, 9, 3);

        /* Contained */
        assertTrue(upper.contains(upper));
        assertTrue(lower.contains(lower));

        /* Not contained */
        assertFalse(lower.contains(upper));
        assertFalse(upper.contains(lower));
    }

    @Test
    public void testContainment3() {
        BoundingBox upper = new BoundingBox(-2.75, -0.25, 5.0, 5.0);
        BoundingBox lower = new BoundingBox(2.0, 4.5, 0.5, 0.5);

        /* Contained */
        assertTrue(upper.contains(upper));
        assertTrue(lower.contains(lower));

        /* Not contained */
        assertFalse(lower.contains(upper));
        assertFalse(upper.contains(lower));
    }

    @Test
    public void testContainment4() {
        BoundingBox upper = new BoundingBox(1, 2, 3, 4);
        BoundingBox lower = new BoundingBox(4, 6, 8, 10);

        /* Contained */
        assertTrue(upper.contains(upper));
        assertTrue(lower.contains(lower));

        /* Not contained */
        assertFalse(lower.contains(upper));
        assertFalse(upper.contains(lower));
    }

    @Test
    public void testCompleteOverlap() {
        BoundingBox outer = new BoundingBox(-12, 34, 253, 8374);
        BoundingBox inner = new BoundingBox(-3, 123, 123, 6374);

        /* Compare overlap */
        double eps = 0.000001;
        double expectedOverlap = inner.area();
        assertEquals(expectedOverlap, outer.overlap(inner), eps);
        assertEquals(expectedOverlap, inner.overlap(outer), eps);

        /* Assert overlap */
        assertTrue(inner.overlaps(outer));
        assertTrue(outer.overlaps(inner));
    }

    @Test
    public void testPartialOverlap2() {
        BoundingBox upper = new BoundingBox(5, -1, 2, 4);
        BoundingBox lower = new BoundingBox(1, 2, 9, 3);

        /* Compare overlap */
        double eps = 0.000001;
        double expectedOverlap = 2.0 * 1.0;
        assertEquals(expectedOverlap, upper.overlap(lower), eps);
        assertEquals(expectedOverlap, lower.overlap(upper), eps);

        /* Assert overlap */
        assertTrue(upper.overlaps(lower));
        assertTrue(lower.overlaps(upper));
    }

    @Test
    public void testPartialOverlap3() {
        BoundingBox upper = new BoundingBox(-2.75, -0.25, 5.0, 5.0);
        BoundingBox lower = new BoundingBox(2.0, 4.5, 0.5, 0.5);

        /* Compare overlap */
        double eps = 0.000001;
        double expectedOverlap = 0.25 * 0.25;
        assertEquals(expectedOverlap, upper.overlap(lower), eps);
        assertEquals(expectedOverlap, lower.overlap(upper), eps);

        /* Assert overlap */
        assertTrue(upper.overlaps(lower));
        assertTrue(lower.overlaps(upper));
    }

    @Test
    public void testNoOverlap4() {
        BoundingBox upper = new BoundingBox(1, 2, 3, 4);
        BoundingBox lower = new BoundingBox(4, 6, 8, 10);

        /* Compare overlap */
        double eps = 0.000001;
        double expectedOverlap = 0.0;
        assertEquals(expectedOverlap, upper.overlap(lower), eps);
        assertEquals(expectedOverlap, lower.overlap(upper), eps);

        /* Assert no overlap */
        assertFalse(upper.overlaps(lower));
        assertFalse(lower.overlaps(upper));
    }
}
