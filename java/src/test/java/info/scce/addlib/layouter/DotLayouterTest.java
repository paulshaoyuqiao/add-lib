package info.scce.addlib.layouter;

import info.scce.addlib.dd.add.ADD;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DotLayouterTest extends LayouterTest {

    @Test
    public void testDotAvailable() {
        DotLayouter<ADD> dl = new DotLayouter<>();
        assertTrue(dl.isAvailable());
    }

    @Test
    public void testDotLayouter() {

        /* Layout */
        DotLayouter<ADD> dl = new DotLayouter<>();
        dl.layout(sumOfPredicatesABC());

        /* Assert result */
        assertNonOverlappingBoundingBoxes(dl);
        assertSizeGreaterThanZero(dl);
        assertInBoundingBox(dl);
        assertMirroringYieldsPositiveSizes(dl);
    }
}
