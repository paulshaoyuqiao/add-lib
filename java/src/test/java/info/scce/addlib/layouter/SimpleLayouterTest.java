package info.scce.addlib.layouter;

import info.scce.addlib.dd.add.ADD;
import org.junit.Test;

public class SimpleLayouterTest extends LayouterTest {

    @Test
    public void testSimpleLayouter() {

        /* Layout */
        SimpleLayouter<ADD> sl = new SimpleLayouter<>();
        sl.layout(sumOfPredicatesABC());

        /* Assert result */
        assertNonOverlappingBoundingBoxes(sl);
        assertSizeGreaterThanZero(sl);
        assertInBoundingBox(sl);
        assertMirroringYieldsPositiveSizes(sl);
    }
}
