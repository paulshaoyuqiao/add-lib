package info.scce.addlib.dd.xdd.grouplikedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CountDDManagerTest extends DDManagerTest {

    private CountDDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new CountDDManager();
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testNeutralElement() {

        /* Get constants */
        XDD<Integer> neutral = ddManager.neutral();
        XDD<Integer> someCount = ddManager.constant(53);

        /* Join with neutral element */
        XDD<Integer> neutral_join_someCount = neutral.join(someCount);
        XDD<Integer> someCount_join_neutral = someCount.join(neutral);
        XDD<Integer> neutral_join_neutral = neutral.join(neutral);

        /* Assert results */
        assertEquals(someCount, neutral_join_someCount);
        assertEquals(someCount, someCount_join_neutral);
        assertEquals(neutral, neutral_join_neutral);

        /* Release memory */
        neutral.recursiveDeref();
        someCount.recursiveDeref();
        neutral_join_someCount.recursiveDeref();
        someCount_join_neutral.recursiveDeref();
        neutral_join_neutral.recursiveDeref();
    }

    @Test
    public void testJoin() {

        /* Get constants */
        XDD<Integer> a = ddManager.constant(53);
        XDD<Integer> b = ddManager.constant(99);
        XDD<Integer> sum = ddManager.constant(53 + 99);

        /* Assert join */
        XDD<Integer> a_join_b = a.join(b);
        assertEquals(sum, a_join_b);

        /* Release memory */
        a.recursiveDeref();
        b.recursiveDeref();
        sum.recursiveDeref();
        a_join_b.recursiveDeref();
    }

    @Test
    public void testParseElement() {

        /* Repeat test a few times with varying values */
        for (int x = 123; x < 4567; x += 89) {

            /* Assert parseElement is inverse of toString for the value of x */
            XDD<Integer> xddCount = ddManager.constant(x);
            String str = xddCount.toString();
            XDD<Integer> xddCountReproduced = ddManager.constant(ddManager.parseElement(str));
            assertEquals(xddCount, xddCountReproduced);

            /* Release memory */
            xddCount.recursiveDeref();
            xddCountReproduced.recursiveDeref();
        }
    }
}
