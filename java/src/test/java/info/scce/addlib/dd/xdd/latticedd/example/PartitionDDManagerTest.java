package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PartitionDDManagerTest extends DDManagerTest {

    private PartitionLatticeDDManager<Double> ddManager = null;

    @Before
    public void setUp() {
        ddManager = new PartitionLatticeDDManager<Double>() {
            @Override
            protected Double parsePartitionElement(String str) {
                return Double.parseDouble(str);
            }
        };
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testParseElement() {

        /* Get some constants */
        XDD<Partition<Double>> a = ddManager.constant(new Partition(5.0, 7.3, 5.2));
        XDD<Partition<Double>> b = ddManager.constant(new Partition(4.0, 2.4, 5.2));
        XDD<Partition<Double>> a_meet_b = a.meet(b);

        /* Reconstruct the constants */
        XDD<Partition<Double>> reconstructed_a = ddManager.constant(ddManager.parseElement(a.toString()));
        XDD<Partition<Double>> reconstructed_b = ddManager.constant(ddManager.parseElement(b.toString()));
        XDD<Partition<Double>> reconstructed_a_meet_b = ddManager.constant(ddManager.parseElement(a_meet_b.toString()));
        assertEquals(a, reconstructed_a);
        assertEquals(b, reconstructed_b);
        assertEquals(a_meet_b, reconstructed_a_meet_b);

        /* Release memory */
        a.recursiveDeref();
        b.recursiveDeref();
        a_meet_b.recursiveDeref();
        reconstructed_a.recursiveDeref();
        reconstructed_b.recursiveDeref();
        reconstructed_a_meet_b.recursiveDeref();
    }
}
