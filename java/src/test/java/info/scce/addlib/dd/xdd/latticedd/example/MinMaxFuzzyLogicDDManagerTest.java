package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MinMaxFuzzyLogicDDManagerTest extends DDManagerTest {

    private static final double EPS = 0.00001;

    private MinMaxFuzzyLogicDDManager ddManager = null;
    private XDD<Double> a;
    private XDD<Double> b;

    @Before
    public void setUp() {
        ddManager = new MinMaxFuzzyLogicDDManager();
        a = ddManager.constant(0.3);
        b = ddManager.constant(0.8);
    }

    @After
    public void tearDown() {
        a.recursiveDeref();
        b.recursiveDeref();
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testAnd() {
        XDD<Double> a_and_b = a.and(b);
        assertEquals(a.v().doubleValue(), a_and_b.v().doubleValue(), EPS);
        a_and_b.recursiveDeref();
    }

    @Test
    public void testOr() {
        XDD<Double> a_or_b = a.or(b);
        assertEquals(b.v().doubleValue(), a_or_b.v().doubleValue(), EPS);
        a_or_b.recursiveDeref();
    }

    @Test
    public void testNot() {
        XDD<Double> not_a = a.not();
        assertEquals(0.7, not_a.v().doubleValue(), EPS);
        not_a.recursiveDeref();
    }

    @Test
    public void testBot() {
        XDD<Double> zero = ddManager.bot();
        assertEquals(0, zero.v().doubleValue(), EPS);
        zero.recursiveDeref();
    }

    @Test
    public void testTop() {
        XDD<Double> one = ddManager.top();
        assertEquals(1, one.v().doubleValue(), EPS);
        one.recursiveDeref();
    }
}
