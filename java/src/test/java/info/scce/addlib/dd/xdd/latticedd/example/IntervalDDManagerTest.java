package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IntervalDDManagerTest extends DDManagerTest {

    private IntervalDDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new IntervalDDManager();
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testParseElement() {

        /* Assert parseElement is iverse of toString */
        XDD<Interval> c = ddManager.constant(new Interval(3, true,5, false));
        Interval str = ddManager.parseElement(c.toString());
        XDD<Interval> cReproduced = ddManager.constant(str);
        assertEquals(c,cReproduced);

        /* Release memory */
        c.recursiveDeref();
        cReproduced.recursiveDeref();
    }
}