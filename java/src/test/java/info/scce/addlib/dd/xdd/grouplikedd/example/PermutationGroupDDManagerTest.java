package info.scce.addlib.dd.xdd.grouplikedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PermutationGroupDDManagerTest extends DDManagerTest {

    private static final int N = 5;

    private PermutationGroupDDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new PermutationGroupDDManager(N);
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testConstantDDs() {

        /* Assert inverse */
        XDD<Permutation> f = ddManager.constant(new Permutation(2, 4, 3, 0, 1));
        XDD<Permutation> actualInverse = f.inverse();
        XDD<Permutation> expectedInverse = ddManager.constant(new Permutation(3, 4, 0, 2, 1));
        assertEquals(expectedInverse, actualInverse);
        assertEquals(expectedInverse.v(), actualInverse.v());
        actualInverse.recursiveDeref();
        expectedInverse.recursiveDeref();

        /* Assert composition */
        XDD<Permutation> g = ddManager.constant(new Permutation(3, 1, 2, 0, 4));
        XDD<Permutation> actualComposition = f.join(g);
        g.recursiveDeref();
        f.recursiveDeref();
        XDD<Permutation> expectedComposition = ddManager.constant(new Permutation(0, 4, 3, 2, 1));
        assertEquals(expectedComposition, actualComposition);
        assertEquals(expectedComposition.v(), actualComposition.v());
        expectedComposition.recursiveDeref();
        actualComposition.recursiveDeref();
    }

    @Test
    public void testDDs() {

        /* Get some DDs */
        XDD<Permutation> identity = ddManager.neutral();
        XDD<Permutation> f = ddManager.constant(new Permutation(2, 4, 3, 0, 1));
        XDD<Permutation> g = ddManager.constant(new Permutation(3, 1, 2, 0, 4));

        /* Assert composition */
        XDD<Permutation> f_join_g = f.join(g);
        XDD<Permutation> expected_f_join_g = ddManager.constant(new Permutation(0, 4, 3, 2, 1));
        assertEquals(expected_f_join_g, f_join_g);
        assertEquals(expected_f_join_g.v(), f_join_g.v());
        expected_f_join_g.recursiveDeref();

        /* Get small conditional DDs */
        XDD<Permutation> f_if_var0 = ddManager.ithVar(0, f, identity);
        XDD<Permutation> g_if_var1 = ddManager.ithVar(1, g, identity);

        /* Compose small DDs */
        XDD<Permutation> composition = f_if_var0.join(g_if_var1);
        f_if_var0.recursiveDeref();
        g_if_var1.recursiveDeref();
        assertFalse(composition.isConstant());

        /* Get first layer */
        XDD<Permutation> composition_then = composition.t();
        assertFalse(composition_then.isConstant());
        XDD<Permutation> composition_else = composition.e();
        composition.recursiveDeref();
        assertFalse(composition_else.isConstant());

        /* Get second layer */
        XDD<Permutation> composition_then_then = composition_then.t();
        assertTrue(composition_then_then.isConstant());
        XDD<Permutation> composition_then_else = composition_then.e();
        assertTrue(composition_then_else.isConstant());
        XDD<Permutation> composition_else_then = composition_else.t();
        assertTrue(composition_else_then.isConstant());
        XDD<Permutation> composition_else_else = composition_else.e();
        assertTrue(composition_else_else.isConstant());

        /* Assert composition */
        XDD<Permutation> expectedComposition_then_then = f_join_g;
        XDD<Permutation> expectedComposition_then_else = f;
        XDD<Permutation> expectedComposition_else_then = g;
        XDD<Permutation> expectedComposition_else_else = identity;
        f_join_g.recursiveDeref();
        f.recursiveDeref();
        g.recursiveDeref();
        identity.recursiveDeref();
        assertEquals(expectedComposition_then_then, composition_then_then);
        assertEquals(expectedComposition_then_else, composition_then_else);
        assertEquals(expectedComposition_else_then, composition_else_then);
        assertEquals(expectedComposition_else_else, composition_else_else);
    }

    @Test
    public void testParseElement() {
        int[] p = new int[]{0, 1, 2, 3, 4};
        int n = 99;
        for (int i = 0; i < n; i++) {
            swapRandomIdx(p);

            /* Assert parseElement is inverse of toString for permutation p */
            XDD<Permutation> xddPermutation = ddManager.constant(new Permutation(p));
            String str = xddPermutation.toString();
            XDD<Permutation> xddPermutationReproduced = ddManager.parse("\"" + str + "\"");//ddManager.constant(ddManager.parseElement(str));
            assertEquals(xddPermutation, xddPermutationReproduced);

            /* Release memory */
            xddPermutation.recursiveDeref();
            xddPermutationReproduced.recursiveDeref();
        }
    }

    private void swapRandomIdx(int[] p) {
        int i0 = (int) (Math.random() * p.length);
        int i1 = (int) (Math.random() * p.length);
        int tmp = p[i0];
        p[i0] = p[i1];
        p[i1] = tmp;
    }
}
