package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BooleanLogicDDManagerTest extends DDManagerTest {

    private BooleanLogicDDManager ddManager = null;
    private XDD<Boolean> one;
    private XDD<Boolean> zero;

    @Before
    public void setUp() {
        ddManager = new BooleanLogicDDManager();
        one = ddManager.one();
        zero = ddManager.zero();
    }

    @After
    public void tearDown() {
        one.recursiveDeref();
        zero.recursiveDeref();
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testAnd() {
        XDD<Boolean> one_and_one = one.and(one);
        XDD<Boolean> one_and_zero = one.and(zero);
        XDD<Boolean> zero_and_one = zero.and(one);
        XDD<Boolean> zero_and_zero = zero.and(zero);
        assertTrue(one_and_one.v());
        assertFalse(one_and_zero.v());
        assertFalse(zero_and_one.v());
        assertFalse(zero_and_zero.v());
        one_and_one.recursiveDeref();
        one_and_zero.recursiveDeref();
        zero_and_one.recursiveDeref();
        zero_and_zero.recursiveDeref();
    }

    @Test
    public void testOr() {
        XDD<Boolean> one_or_one = one.or(one);
        XDD<Boolean> one_or_zero = one.or(zero);
        XDD<Boolean> zero_or_one = zero.or(one);
        XDD<Boolean> zero_or_zero = zero.or(zero);
        assertTrue(one_or_one.v());
        assertTrue(one_or_zero.v());
        assertTrue(zero_or_one.v());
        assertFalse(zero_or_zero.v());
        one_or_one.recursiveDeref();
        one_or_zero.recursiveDeref();
        zero_or_one.recursiveDeref();
        zero_or_zero.recursiveDeref();
    }

    @Test
    public void testNot() {
        XDD<Boolean> not_one = one.not();
        XDD<Boolean> not_zero = zero.not();
        assertFalse(not_one.v());
        assertTrue(not_zero.v());
        not_one.recursiveDeref();
        not_zero.recursiveDeref();
    }

    @Test
    public void testParseElement() {

        /* Repeat test for both Boolean values */
        for (int i = 0; i < 2; i++) {
            boolean b = i % 2 == 0;

            /* Assert parseElement is inverse of toString for the value of b */
            XDD<Boolean> xddBoolean = ddManager.constant(b);
            String str = xddBoolean.toString();
            XDD<Boolean> xddBooleanReproduced = ddManager.constant(ddManager.parseElement(str));
            assertEquals(xddBoolean, xddBooleanReproduced);

            /* Release memory */
            xddBoolean.recursiveDeref();
            xddBooleanReproduced.recursiveDeref();
        }
    }
}
