package info.scce.addlib.dd;

import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class LabelledRegularDDTest {

    @Test
    public void testValidLabel() {
        String lbl = "ABCabc019_";
        try {
            new LabelledRegularDD(null, lbl);
        } catch (DDManagerException e) {
            fail("Valid label '" + lbl + "' was not accepted.");
        }
    }

    @Test
    public void testInvalidLabel() {
        String lbl = "Invalid Label";
        try {
            new LabelledRegularDD(null, lbl);
            fail("Invalid label '" + lbl + "' was accepted.");
        } catch (DDManagerException e) {
            assertTrue(e.getMessage().contains(lbl));
        }
    }
}
