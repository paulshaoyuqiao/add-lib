package info.scce.addlib.dd.xdd.latticedd.example;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MultisetTest {

    private Multiset<Integer> a;
    private Multiset<Integer> b;
    private Multiset<Integer> c;

    @Before
    public void setUp() {
        a = new Multiset<>(new Integer[]{1, 2, 3});
        b = new Multiset<>(new Integer[]{1, 2, 2, 3, 3});
        c = new Multiset<>(new Integer[]{2, 3, 3, 3});
    }

    @Test
    public void testIntersect() {
        Multiset<Integer> expected_a_meets_c = new Multiset<>(new Integer[]{2, 3});
        assertEquals(expected_a_meets_c, a.intersect(c));
        assertEquals(expected_a_meets_c, c.intersect(a));
        Multiset<Integer> expected_b_meets_c = new Multiset<>(new Integer[]{2, 3, 3});
        assertEquals(expected_b_meets_c, b.intersect(c));
        assertEquals(expected_b_meets_c, c.intersect(b));
    }

    @Test
    public void testUnion() {
        Multiset<Integer> expected_a_union_c = new Multiset<>(new Integer[]{1, 2, 3, 3, 3});
        assertEquals(expected_a_union_c, a.union(c));
        assertEquals(expected_a_union_c, c.union(a));
        Multiset<Integer> expected_b_union_c = new Multiset<>(new Integer[]{1, 2, 2, 3, 3, 3});
        assertEquals(expected_b_union_c, b.union(c));
        assertEquals(expected_b_union_c, c.union(b));
    }

    @Test
    public void testIncludes() {
        boolean expected_b_includes_a = true;
        assertEquals(expected_b_includes_a, b.includes(a));
        boolean expected_b_includes_c = false;
        assertEquals(expected_b_includes_c, b.includes(c));
        boolean expected_c_includes_b = false;
        assertEquals(expected_c_includes_b, c.includes(b));
    }

    @Test
    public void testPlus() {
        Multiset<Integer> expected_a_sum_c = new Multiset<>(new Integer[]{1, 2, 2, 3, 3, 3, 3});
        assertEquals(expected_a_sum_c, a.plus(c));
        assertEquals(expected_a_sum_c, c.plus(a));
        Multiset<Integer> expected_b_sum_c = new Multiset<>(new Integer[]{1, 2, 2, 2, 3, 3, 3, 3, 3});
        assertEquals(expected_b_sum_c, b.plus(c));
        assertEquals(expected_b_sum_c, c.plus(b));
    }

    @Test
    public void testSize() {
        int expected_size_a = 3;
        assertEquals(expected_size_a, a.size());
        int expected_size_b = 5;
        assertEquals(expected_size_b, b.size());
        int expected_size_b_sum_c = 9;
        assertEquals(expected_size_b_sum_c, b.plus(c).size());
    }

    @Test
    public void testEqualsReflexivity() {
        assertTrue(a.equals(a));
        assertTrue(b.equals(b));
    }

    @Test
    public void testParseMultisetEmpty() {
        testParseMultiset(Multiset.emptySet());
    }

    @Test
    public void testParseMultiset() {
        Multiset<String> multiset = new Multiset<>("Hans", "Jan", "Alex", "Alex", "Jan", "Alex", "Alex");
        testParseMultiset(multiset);
    }

    @Test
    public void testParseMultisetSingletons() {
        Multiset<String> multiset = new Multiset<>("Hans", "Jan", "Alex");
        testParseMultiset(multiset);
    }

    public void testParseMultiset(Multiset<String> original) {
        String str = original.toString();
        Multiset<String> reconstructed = Multiset.parseMultiset(str, x -> x);
        assertEquals(original, reconstructed);
    }
}
