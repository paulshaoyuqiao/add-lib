package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BooleanVectorLogicDDManagerTest extends DDManagerTest {

    private static final int N = 2;

    private BooleanVectorLogicDDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new BooleanVectorLogicDDManager(N);
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testCustomOperation() {

        /* Get BooleanVector values */
        BooleanVector b00 = new BooleanVector(false, false);
        BooleanVector b01 = new BooleanVector(false, true);
        BooleanVector b10 = new BooleanVector(true, false);
        BooleanVector b11 = new BooleanVector(true, true);

        /* Get constant DDs */
        XDD<BooleanVector> const00 = ddManager.constant(b00);
        XDD<BooleanVector> const01 = ddManager.constant(b01);
        XDD<BooleanVector> const10 = ddManager.constant(b10);
        XDD<BooleanVector> const11 = ddManager.constant(b11);

        /* Get some simple DDs */
        XDD<BooleanVector> const10_if_var0_else_const00 = ddManager.ithVar(0, const10, const00);
        XDD<BooleanVector> const01_if_var1_else_const00 = ddManager.ithVar(1, const01, const00);

        /* Assert identity */
        XDD<BooleanVector> alsoIdentity = const10_if_var0_else_const00.or(const01_if_var1_else_const00);
        XDD<BooleanVector> identity = const10_if_var0_else_const00.apply(BooleanVector::or, const01_if_var1_else_const00);
        assertEquals(identity, alsoIdentity);
        assertEquals(const00, identity.e().e());
        assertEquals(const01, identity.e().t());
        assertEquals(const10, identity.t().e());
        assertEquals(const11, identity.t().t());
        assertEquals(b00, identity.e().e().v());
        assertEquals(b01, identity.e().t().v());
        assertEquals(b10, identity.t().e().v());
        assertEquals(b11, identity.t().t().v());

        /* Release memory */
        const10_if_var0_else_const00.recursiveDeref();
        const01_if_var1_else_const00.recursiveDeref();
        alsoIdentity.recursiveDeref();
        identity.recursiveDeref();
        const00.recursiveDeref();
        const01.recursiveDeref();
        const10.recursiveDeref();
        const11.recursiveDeref();
    }

    @Test
    public void testComposition() {

        /* Get constant DDs */
        XDD<BooleanVector> const00 = ddManager.constant(new BooleanVector(false, false));
        XDD<BooleanVector> const01 = ddManager.constant(new BooleanVector(false, true));
        XDD<BooleanVector> const10 = ddManager.constant(new BooleanVector(true, false));

        /* Get identity DD */
        XDD<BooleanVector> idVar0 = ddManager.ithVar(0, const10, const00);
        XDD<BooleanVector> idVar1 = ddManager.ithVar(1, const01, const00);
        XDD<BooleanVector> id = idVar0.or(idVar1);
        const00.recursiveDeref();
        const01.recursiveDeref();
        const10.recursiveDeref();

        /* Assert compositions with id */
        XDD<BooleanVector> idVar1_comp_id = id.monadicApply(bn -> idVar1.eval(bn.data()).v());
        XDD<BooleanVector> idVar0_comp_id = id.monadicApply(bn -> idVar0.eval(bn.data()).v());
        XDD<BooleanVector> id_comp_idVar1 = idVar1.monadicApply(bn -> id.eval(bn.data()).v());
        XDD<BooleanVector> id_comp_idVar0 = idVar0.monadicApply(bn -> id.eval(bn.data()).v());
        assertEquals(idVar1, idVar1_comp_id);
        assertEquals(idVar0, idVar0_comp_id);
        assertEquals(idVar1, id_comp_idVar1);
        assertEquals(idVar0, id_comp_idVar0);

        /* Release memory */
        idVar0.recursiveDeref();
        idVar1.recursiveDeref();
        id.recursiveDeref();
        idVar1_comp_id.recursiveDeref();
        idVar0_comp_id.recursiveDeref();
        id_comp_idVar1.recursiveDeref();
        id_comp_idVar0.recursiveDeref();
    }

    @Test
    public void testParseElement() {

        /* Repeat test a few times with varying values */
        for (int i = 0; i < 4; i++) {
            boolean b0 = i % 2 == 0;
            boolean b1 = (i / 2) % 2 == 0;

            /* Assert parseElement is iverse of toString for values b0, b1 */
            XDD<BooleanVector> xddBooleanVector = ddManager.constant(new BooleanVector(b0, b1));
            String str = xddBooleanVector.toString();
            XDD<BooleanVector> xddBooleanVectorReproduced = ddManager.constant(ddManager.parseElement(str));;
            assertEquals(xddBooleanVector, xddBooleanVectorReproduced);

            /* Release memory */
            xddBooleanVector.recursiveDeref();
            xddBooleanVectorReproduced.recursiveDeref();
        }
    }
}
