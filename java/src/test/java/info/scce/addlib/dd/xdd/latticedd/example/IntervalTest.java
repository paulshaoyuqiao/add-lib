package info.scce.addlib.dd.xdd.latticedd.example;

import org.junit.Test;

import static info.scce.addlib.dd.xdd.latticedd.example.Interval.parseInterval;
import static org.junit.Assert.*;

public class IntervalTest {

    @Test
    public void testEmpty() {
        assertEquals(parseInterval("(0, 0)"), Interval.empty());
        assertEquals(parseInterval("(15, 1)"), Interval.empty());
        assertEquals(parseInterval("[5, -1]"), Interval.empty());
        assertEquals(parseInterval("[51, 1)"), Interval.empty());
        assertEquals(parseInterval("(5, -1]"), Interval.empty());
        assertEquals(parseInterval("(5, -1]"), parseInterval("[51, 1)"));
        assertEquals(parseInterval("(15, 1)"), parseInterval("[51, 1)"));
    }

    @Test
    public void testComplete() {

        /* We consider the (-Infinity, Infinity) the complete interval */
        assertEquals(parseInterval("(-Infinity, Infinity)"), Interval.complete());

        /* Therefore, [-Infinity, Infinity] is invalid */
        try {
            parseInterval("[-Infinity, Infinity]");
            fail("Expected exception was not thrown");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage().toLowerCase().contains("infinity"));
        }
    }

    @Test
    public void testContains() {

        /* Positive examples */
        assertTrue(parseInterval("[3, 5]").contains(Interval.empty()));
        assertTrue(parseInterval("[3, Infinity)").contains(parseInterval("[3, 5]")));
        assertTrue(parseInterval("[3, Infinity)").contains(parseInterval("[3, Infinity)")));
        assertTrue(parseInterval("[3, Infinity)").contains(parseInterval("(3, Infinity)")));
        assertTrue(parseInterval("(-Infinity, Infinity)").contains(parseInterval("(5, Infinity)")));
        assertTrue(parseInterval("[3, 5]").contains(parseInterval("[3, -3]")));
        assertTrue(parseInterval("[1, 2]").contains(parseInterval("[1, 2)")));
        assertTrue(parseInterval("[1, 2]").contains(parseInterval("(1, 2]")));
        assertTrue(parseInterval("[1, 2]").contains(parseInterval("(1, 2)")));

        /* Negative examples */
        assertFalse(parseInterval("[3, Infinity)").contains(parseInterval("[1, 2]")));
        assertFalse(parseInterval("[1, 2]").contains(parseInterval("[1, 3]")));
        assertFalse(parseInterval("[1, 2]").contains(parseInterval("[4, 6]")));
        assertFalse(parseInterval("[1, 2]").contains(parseInterval("[0, 0.5]")));
        assertFalse(parseInterval("[1, 2)").contains(parseInterval("[1, 2]")));
        assertFalse(parseInterval("(1, 2]").contains(parseInterval("[1, 2]")));
        assertFalse(parseInterval("(1, 2)").contains(parseInterval("[1, 2]")));
        assertFalse(parseInterval("(1, 2]").contains(parseInterval("[1, 2]")));
        assertFalse(parseInterval("(1, 2]").contains(parseInterval("[1, 2)")));
    }

    @Test
    public void testUnion() {
        assertEquals(parseInterval("[2, 3]").union(parseInterval("(3, 5]")), parseInterval("[2, 5]"));
        assertEquals(parseInterval("[2, 5]").union(parseInterval("[3, 6]")), parseInterval("[2, 6]"));
        assertEquals(parseInterval("[2, 5)").union(parseInterval("(5, 6]")), parseInterval("[2, 6]"));
        assertEquals(parseInterval("[2, 2]").union(parseInterval("(5, 6]")), parseInterval("[2, 6]"));
        assertEquals(parseInterval("[2, 3]").union(parseInterval("[3, 6]")), parseInterval("[2, 6]"));
        assertEquals(parseInterval("(2, 3]").union(parseInterval("[3, 6]")), parseInterval("(2, 6]"));
        assertEquals(parseInterval("[2, 3]").union(parseInterval("[3, 6)")), parseInterval("[2, 6)"));
        assertEquals(parseInterval("(2, 3]").union(parseInterval("[3, 6)")), parseInterval("(2, 6)"));
        assertEquals(parseInterval("(-Infinity, 3]").union(parseInterval("[3, 6)")), parseInterval("(-Infinity, 6)"));
        assertEquals(parseInterval("(2, 3]").union(parseInterval("[3, Infinity)")), parseInterval("(2, Infinity)"));
    }

    @Test
    public void testUnionCommutative() {
        for (int i = 0; i < 64; i++) {
            Interval a = randomInterval();
            Interval b = randomInterval();
            assertEquals(a.union(b), b.union(a));
        }
    }

    @Test
    public void testEmptyNeutralToUnion() {
        for (int i = 0; i < 64; i++) {
            Interval a = randomInterval();
            assertEquals(a, a.union(Interval.empty()));
            assertEquals(a, Interval.empty().union(a));
        }
    }

    @Test
    public void testUnionIdempotent() {
        for (int i = 0; i < 64; i++) {
            Interval a = randomInterval();
            assertEquals(a, a.union(a));
        }
    }

    @Test
    public void testIntersection() {
        assertEquals(parseInterval("[2, 3]").intersect(parseInterval("(3, 5]")), Interval.empty());
        assertEquals(parseInterval("[2, 5]").intersect(parseInterval("[3, 6]")), parseInterval("[3, 5]"));
        assertEquals(parseInterval("[2, 5)").intersect(parseInterval("(5, 6]")), Interval.empty());
        assertEquals(parseInterval("[2, 2]").intersect(parseInterval("(5, 6]")), Interval.empty());
        assertEquals(parseInterval("[2, 3]").intersect(parseInterval("[3, 6]")), parseInterval("[3, 3]"));
        assertEquals(parseInterval("(2, 3]").intersect(parseInterval("[3, 6]")), parseInterval("[3, 3]"));
        assertEquals(parseInterval("[2, 3]").intersect(parseInterval("[3, 6)")), parseInterval("[3, 3]"));
        assertEquals(parseInterval("(2, 3]").intersect(parseInterval("[3, 6)")), parseInterval("[3, 3]"));
        assertEquals(parseInterval("[1, 2)").intersect(parseInterval("[1, 2)")), parseInterval("[1, 2)"));
        assertEquals(parseInterval("(-10, 3)").intersect(parseInterval("(0, 6)")), parseInterval("(0, 3)"));
        assertEquals(parseInterval("(-Infinity, 3)").intersect(parseInterval("(0, Infinity)")), parseInterval("(0, 3)"));
    }

    @Test
    public void testIntersectionCommutative() {
        for (int i = 0; i < 64; i++) {
            Interval a = randomInterval();
            Interval b = randomInterval();
            assertEquals(a.intersect(b), b.intersect(a));
        }
    }

    @Test
    public void testCompleteNeutralToIntersection() {
        for (int i = 0; i < 64; i++) {
            Interval a = randomInterval();
            assertEquals(a, a.intersect(Interval.complete()));
            assertEquals(a, Interval.complete().intersect(a));
        }
    }

    @Test
    public void testIntersectionIdempotent() {
        for (int i = 0; i < 64; i++) {
            Interval a = randomInterval();
            assertEquals(a, a.intersect(a));
        }
    }

    private Interval randomInterval() {
        boolean lbIncluded = Math.random() < 0.6;
        boolean ubIncluded = Math.random() < 0.3;
        double lb = Math.random() * 12 - 34;
        double ub = Math.random() * 567 - 89;
        return new Interval(lb, lbIncluded, ub, ubIncluded);
    }
}