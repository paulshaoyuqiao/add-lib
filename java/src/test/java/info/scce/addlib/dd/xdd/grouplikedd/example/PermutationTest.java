package info.scce.addlib.dd.xdd.grouplikedd.example;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PermutationTest {

    @Test
    public void testComposition() {
        Permutation f = new Permutation(2, 4, 3, 0, 1);
        Permutation g = new Permutation(3, 1, 2, 0, 4);
        Permutation expected = new Permutation(0, 4, 3, 2, 1);
        Permutation actual = f.compose(g);
        assertEquals(expected, actual);
    }

    @Test
    public void testIdentity() {
        int[] expectedData = new int[]{0, 1, 2, 3, 4};
        Permutation expected = new Permutation(expectedData);
        Permutation actual = Permutation.identity(5);
        assertEquals("incorrect identity", expected, actual);
        assertTrue(Arrays.equals(expectedData, actual.data()));
    }

    @Test
    public void testIdentityAndInverse() {
        Permutation id = Permutation.identity(5);
        Permutation f = new Permutation(2, 4, 3, 0, 1);
        assertEquals("f = id*f does not hold", f, id.compose(f));
        assertEquals("f = f*id does not hold", f, f.compose(id));
        assertEquals("--f = f does not hold", f.inverse().inverse(), f);
        assertEquals("-id = id does not hold", id.inverse(), id);
    }

    @Test
    public void testParsePermutation() {
        Permutation perm = new Permutation(4, 3, 0, 2, 1);
        String str = perm.toString();
        Permutation permutationReproduced = Permutation.parsePermutation(str);
        assertEquals(perm, permutationReproduced);
    }

    @Test
    public void testParseBooleanVectorEmpty() {
        Permutation perm = new Permutation();
        String str = perm.toString();
        Permutation reproduced = Permutation.parsePermutation(str);
        assertEquals(perm, reproduced);
    }
}
