package info.scce.addlib.dd.xdd.latticedd.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ComplementableSetTest {

    @Test
    public void testComplementableSet() {

        /* Some test sets */
        ComplementableSet<String> people = new ComplementableSet<>(
                new String[]{"Hans", "Peter", "Meier", "Alex"});
        ComplementableSet<String> morePeople = new ComplementableSet<>(
                new String[]{"Hans", "Bernd", "Alex", "Jan", "Hermann"});

        /* Assert union */
        ComplementableSet<String> actualUnion = people.union(morePeople);
        ComplementableSet<String> expectedUnion = new ComplementableSet<>(
                new String[]{"Hans", "Bernd", "Jan", "Peter", "Meier", "Alex", "Hermann"});
        assertEquals(actualUnion, expectedUnion);

        /* Assert intersect */
        ComplementableSet<String> actualIntersection = people.intersect(morePeople);
        ComplementableSet<String> expectedIntersection = new ComplementableSet<>(new String[]{"Hans", "Alex"});
        assertEquals(actualIntersection, expectedIntersection);

        /* Assert complement using De Morgan's law */
        ComplementableSet<String> actualUnionDeMorgan = people.complement().intersect(morePeople.complement())
                .complement();
        assertEquals(actualUnionDeMorgan, expectedUnion);
        ComplementableSet<String> actualIntersectionDeMorgan = people.complement().union(morePeople.complement())
                .complement();
        assertEquals(actualIntersectionDeMorgan, expectedIntersection);
    }

    @Test
    public void testParseComplementableSetEmpty() {
        testParseComplementableSet(ComplementableSet.<String>emptySet());
    }

    @Test
    public void testParseComplementableSetComplete() {
        testParseComplementableSet(ComplementableSet.<String>completeSet());
    }

    @Test
    public void testParseComplementableSet() {
        String[] names = new String[]{"Hans", "Ulf", "Jan"};
        ComplementableSet<String> namesSet = new ComplementableSet<>(names);
        testParseComplementableSet(namesSet);
        testParseComplementableSet(namesSet.complement());
    }

    private void testParseComplementableSet(ComplementableSet<String> complSet) {
        String str = complSet.toString();
        ComplementableSet<String> reproduced = ComplementableSet.<String>parseComplementableSet(str, x -> x);
        assertEquals(complSet, reproduced);
    }
}
