package info.scce.addlib.dd.xdd.ringlikedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ColorDDManagerTest extends DDManagerTest {

    private static final Color someRed = new Color(230, 100, 0);
    private static final Color someGreen = new Color(30, 170, 125);
    private static final Color someLightGreen = new Color(150, 240, 60);
    private static final Color someGray = new Color(222, 222, 222);

    private ColorDDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new ColorDDManager();
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testZeroElement() {

        /* Assert zero element */
        assertEquals(Color.BLACK, ddManager.zeroElement());

        /* Assert zero element DD */
        XDD<Color> zero = ddManager.zero();
        assertEquals(Color.BLACK, zero.v());
        zero.recursiveDeref();
    }

    @Test
    public void testOneElement() {

        /* Assert one element */
        assertEquals(Color.WHITE, ddManager.oneElement());

        /* Assert one element DD */
        XDD<Color> one = ddManager.one();
        assertEquals(Color.WHITE, one.v());
        one.recursiveDeref();
    }


    @Test
    public void testMult() {

        /* Expected products */
        Color expected_ab = new Color(27, 66, 0);
        Color expected_ac = new Color(135, 94, 0);
        Color expected_bc = new Color(17, 160, 29);

        /* Actual products */
        XDD<Color> a = ddManager.constant(someRed);
        XDD<Color> b = ddManager.constant(someGreen);
        XDD<Color> c = ddManager.constant(someLightGreen);
        XDD<Color> actual_ab = a.mult(b);
        XDD<Color> actual_ac = a.mult(c);
        XDD<Color> actual_bc = b.mult(c);
        a.recursiveDeref();
        b.recursiveDeref();
        c.recursiveDeref();

        /* Assert products */
        assertEquals(expected_ab, actual_ab.v());
        assertEquals(expected_ac, actual_ac.v());
        assertEquals(expected_bc, actual_bc.v());
        actual_ab.recursiveDeref();
        actual_ac.recursiveDeref();
        actual_bc.recursiveDeref();
    }

    @Test
    public void testAdd() {

        /* Expected products */
        Color expected_ab = new Color(255, 255, 125);
        Color expected_ac = new Color(255, 255, 60);
        Color expected_bc = new Color(180, 255, 185);

        /* Actual products */
        XDD<Color> a = ddManager.constant(someRed);
        XDD<Color> b = ddManager.constant(someGreen);
        XDD<Color> c = ddManager.constant(someLightGreen);
        XDD<Color> actual_ab = a.add(b);
        XDD<Color> actual_ac = a.add(c);
        XDD<Color> actual_bc = b.add(c);
        a.recursiveDeref();
        b.recursiveDeref();
        c.recursiveDeref();

        /* Assert products */
        assertEquals(expected_ab, actual_ab.v());
        assertEquals(expected_ac, actual_ac.v());
        assertEquals(expected_bc, actual_bc.v());
        actual_ab.recursiveDeref();
        actual_ac.recursiveDeref();
        actual_bc.recursiveDeref();
    }

    @Test
    public void testAddCommutativity() {

        /* Get some constant DDs */
        XDD<Color> a = ddManager.constant(someRed);
        XDD<Color> b = ddManager.constant(someGreen);
        XDD<Color> c = ddManager.constant(someLightGreen);

        /* Assert commutativity */
        XDD<Color> ab = a.add(b);
        XDD<Color> ba = b.add(a);
        assertEquals(ab, ba);
        XDD<Color> bc = b.add(c);
        XDD<Color> cb = c.add(b);
        assertEquals(bc, cb);

        /* Dereference DDs */
        a.recursiveDeref();
        b.recursiveDeref();
        c.recursiveDeref();
        ab.recursiveDeref();
        ba.recursiveDeref();
        bc.recursiveDeref();
        cb.recursiveDeref();
    }

    @Test
    public void testMultCommutativity() {

        /* Get some constant DDs */
        XDD<Color> a = ddManager.constant(someRed);
        XDD<Color> b = ddManager.constant(someGreen);
        XDD<Color> c = ddManager.constant(someLightGreen);

        /* Assert commutativity */
        XDD<Color> ab = a.mult(b);
        XDD<Color> ba = b.mult(a);
        assertEquals(ab, ba);
        XDD<Color> bc = b.mult(c);
        XDD<Color> cb = c.mult(b);
        assertEquals(bc, cb);

        /* Dereference DDs */
        a.recursiveDeref();
        b.recursiveDeref();
        c.recursiveDeref();
        ab.recursiveDeref();
        ba.recursiveDeref();
        bc.recursiveDeref();
        cb.recursiveDeref();
    }

    @Test
    public void testMultAssociativity() {

        /* Get some constant DDs */
        XDD<Color> a = ddManager.constant(someRed);
        XDD<Color> b = ddManager.constant(someGreen);
        XDD<Color> c = ddManager.constant(someLightGreen);

        /* Assert associativity */
        XDD<Color> ab = a.mult(b);
        XDD<Color> ab_c = ab.mult(c);
        XDD<Color> bc = b.mult(c);
        XDD<Color> a_bc = a.mult(bc);
        assertEquals(ab_c, a_bc);


        /* Dereference DDs */
        a.recursiveDeref();
        b.recursiveDeref();
        c.recursiveDeref();
        ab.recursiveDeref();
        ab_c.recursiveDeref();
        bc.recursiveDeref();
        a_bc.recursiveDeref();
    }

    @Test
    public void testPlusAssociativity() {

        /* Get some constant DDs */
        XDD<Color> a = ddManager.constant(someRed);
        XDD<Color> b = ddManager.constant(someGreen);
        XDD<Color> c = ddManager.constant(someLightGreen);

        /* Assert associativity */
        XDD<Color> ab = a.add(b);
        XDD<Color> ab_c = ab.add(c);
        XDD<Color> bc = b.add(c);
        XDD<Color> a_bc = a.add(bc);
        assertEquals(ab_c, a_bc);


        /* Dereference DDs */
        a.recursiveDeref();
        b.recursiveDeref();
        c.recursiveDeref();
        ab.recursiveDeref();
        ab_c.recursiveDeref();
        bc.recursiveDeref();
        a_bc.recursiveDeref();
    }

    @Test
    public void testGrayscaleOperator() {

        /* Test operator on some gray */
        Color expectedGrayscale = someGray;
        Color actualGrayscale = ColorDDManager.grayscale(someGray);
        assertEquals(expectedGrayscale, actualGrayscale);

        /* Test operator on some colours and expect it to be some form of gray */
        assertGray(ColorDDManager.grayscale(someRed));
        assertGray(ColorDDManager.grayscale(someGreen));
        assertGray(ColorDDManager.grayscale(someLightGreen));
        assertGray(ColorDDManager.grayscale(someGray));
    }

    private void assertGray(Color color) {
        int r = color.getRed();
        int g = color.getGreen();
        int b = color.getBlue();
        assertTrue(r == g && g == b);
        assertTrue(0 < r && r < 255);
    }

    @Test
    public void testAvg() {
        Color expectedYellow = new Color(130, 135, 62);
        Color actualYellow = ColorDDManager.avg(someRed, someGreen);
        assertEquals(expectedYellow, actualYellow);
        Color expectedLightYellow = new Color(190, 170, 30);
        Color actualLightYellow = ColorDDManager.avg(someRed, someLightGreen);
        assertEquals(expectedLightYellow, actualLightYellow);
    }

    @Test
    public void testAvgAsOperator() {

        /* Get constant DDs */
        XDD<Color> a = ddManager.constant(someRed);
        XDD<Color> b = ddManager.constant(someGreen);

        /* Assert avg operator */
        Color expectedYellow = new Color(130, 135, 62);
        XDD<Color> c = a.apply(ColorDDManager::avg, b);
        Color actualYellow = c.v();
        assertEquals(expectedYellow, actualYellow);

        /* Dereference DDs */
        a.recursiveDeref();
        b.recursiveDeref();
        c.recursiveDeref();
    }
}