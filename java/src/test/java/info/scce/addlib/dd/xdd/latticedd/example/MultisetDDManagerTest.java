package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class MultisetDDManagerTest extends DDManagerTest {

    private MultisetDDManager<Integer> ddManager = null;

    @Before
    public void setUp() {
        ddManager = new MultisetDDManager<Integer>() {
            @Override
            protected Integer parseMultisetElement(String str) {
                return Integer.parseInt(str);
            }
        };
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testParseElement() {

        /* Get some constants */
        XDD<Multiset<Integer>> a = ddManager.constant(new Multiset<>(1, 1, 2, 5));
        XDD<Multiset<Integer>> b = ddManager.constant(new Multiset<>(77, 3, 3, 9));

        /* Reconstruct constants and assert equality */
        XDD<Multiset<Integer>> reconstructed_a = ddManager.constant(ddManager.parseElement(a.toString()));
        XDD<Multiset<Integer>> reconstructed_b = ddManager.constant(ddManager.parseElement(b.toString()));
        assertEquals(a, reconstructed_a);
        assertEquals(b, reconstructed_b);

        /* Release memory */
        a.recursiveDeref();
        b.recursiveDeref();
        reconstructed_a.recursiveDeref();
        reconstructed_b.recursiveDeref();
    }
}