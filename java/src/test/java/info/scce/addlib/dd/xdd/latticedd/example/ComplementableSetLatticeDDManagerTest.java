package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ComplementableSetLatticeDDManagerTest extends DDManagerTest {

    private ComplementableSetLatticeDDManager<String> ddManager = null;

    @Before
    public void setUp() {
        ddManager = new ComplementableSetLatticeDDManager<String>() {
            @Override
            protected String parseComplementableSetElement(String str) {
                return str;
            }
        };
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testParseElement() {
        XDD<ComplementableSet<String>> people = ddManager.constant(new ComplementableSet<>(
                new String[]{"Hans", "Peter", "Meier", "Alex"}));
        XDD<ComplementableSet<String>> morePeople = ddManager.constant(new ComplementableSet<>(
                new String[]{"Hans", "Bernd", "Alex", "Jan", "Hermann"}));
        XDD<ComplementableSet<String>> peopleParsed = ddManager.constant(ddManager.parseElement(people.toString()));
        XDD<ComplementableSet<String>> morePeopleParsed = ddManager.constant(ddManager.parseElement(morePeople.toString()));
        assertEquals(people, peopleParsed);
        assertEquals(morePeople, morePeopleParsed);
        people.recursiveDeref();
        peopleParsed.recursiveDeref();
        morePeople.recursiveDeref();
        morePeopleParsed.recursiveDeref();
    }
}