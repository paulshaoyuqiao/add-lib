package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProbabilisticFuzzyLogicDDManagerTest extends DDManagerTest {

    private static final double EPS = 0.00001;

    private ProbabilisticFuzzyLogicDDManager ddManager = null;
    private XDD<Double> a = null;
    private XDD<Double> b = null;

    @Before
    public void setUp() {
        ddManager = new ProbabilisticFuzzyLogicDDManager();
        a = ddManager.constant(0.2);
        b = ddManager.constant(0.3);
    }

    @After
    public void tearDown() {
        a.recursiveDeref();
        b.recursiveDeref();
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testAnd() {
        XDD<Double> a_and_b = a.and(b);
        assertEquals(0.06, a_and_b.v().doubleValue(), EPS);
        a_and_b.recursiveDeref();
    }

    @Test
    public void testOr() {
        XDD<Double> a_or_b = a.or(b);
        assertEquals(0.44, a_or_b.v().doubleValue(), EPS);
        a_or_b.recursiveDeref();
    }

    @Test
    public void testNot() {
        XDD<Double> not_a = a.not();
        assertEquals(0.8, not_a.v().doubleValue(), EPS);
        not_a.recursiveDeref();
    }

    @Test
    public void testBot() {
        XDD<Double> zero = ddManager.bot();
        assertEquals(0, zero.v().doubleValue(), EPS);
        zero.recursiveDeref();
    }

    @Test
    public void testTop() {
        XDD<Double> one = ddManager.top();
        assertEquals(1, one.v().doubleValue(), EPS);
        one.recursiveDeref();
    }

    @Test
    public void testRangeCorrection() {
        XDD<Double> two = ddManager.constant(2.0);
        XDD<Double> three = ddManager.constant(3.0);
        XDD<Double> two_and_three = two.and(three);
        assertEquals(1.0, two_and_three.v().doubleValue(), EPS);
        two.recursiveDeref();
        three.recursiveDeref();
        two_and_three.recursiveDeref();
    }
}
