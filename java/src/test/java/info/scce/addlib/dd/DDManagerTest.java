package info.scce.addlib.dd;

import static org.junit.Assert.assertEquals;

public class DDManagerTest {

    protected void assertRefCountZeroAndQuit(DDManager ddManager) {

        /* Check that no unwanted references remain */
        int expectedRefCount = 0;
        int actualRefCount = ddManager.checkZeroRef();
        assertEquals(expectedRefCount, actualRefCount);

        /* Release memory */
        ddManager.quit();
    }
}
