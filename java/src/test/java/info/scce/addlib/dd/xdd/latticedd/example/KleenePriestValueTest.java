package info.scce.addlib.dd.xdd.latticedd.example;

import org.junit.Test;

import static info.scce.addlib.dd.xdd.latticedd.example.KleenePriestValue.*;
import static org.junit.Assert.assertEquals;

public class KleenePriestValueTest {

    @Test
    public void testKleenePriestConjunction() {
        assertEquals(FALSE, FALSE.and(FALSE));
        assertEquals(FALSE, FALSE.and(TRUE));
        assertEquals(FALSE, TRUE.and(FALSE));
        assertEquals(TRUE, TRUE.and(TRUE));
        assertEquals(UNKNOWN, TRUE.and(UNKNOWN));
        assertEquals(UNKNOWN, UNKNOWN.and(UNKNOWN));
        assertEquals(FALSE, UNKNOWN.and(FALSE));
    }

    @Test
    public void testKleenePriestDisjunction() {
        assertEquals(FALSE, FALSE.or(FALSE));
        assertEquals(TRUE, FALSE.or(TRUE));
        assertEquals(TRUE, TRUE.or(FALSE));
        assertEquals(TRUE, TRUE.or(TRUE));
        assertEquals(UNKNOWN, FALSE.or(UNKNOWN));
        assertEquals(UNKNOWN, UNKNOWN.or(UNKNOWN));
        assertEquals(TRUE, UNKNOWN.or(TRUE));
    }

    @Test
    public void testKleenePriestNgeation() {
        assertEquals(TRUE, FALSE.not());
        assertEquals(FALSE, TRUE.not());
        assertEquals(UNKNOWN, UNKNOWN.not());
    }

    @Test
    public void testParseKleenePriestValue() {
        assertEquals(TRUE, KleenePriestValue.parseKleenePriestValue("true"));
        assertEquals(FALSE, KleenePriestValue.parseKleenePriestValue("false"));
        assertEquals(UNKNOWN, KleenePriestValue.parseKleenePriestValue("unknown"));
    }
}
