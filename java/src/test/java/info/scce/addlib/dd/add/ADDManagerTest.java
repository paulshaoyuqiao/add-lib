package info.scce.addlib.dd.add;

import com.google.common.collect.Lists;
import info.scce.addlib.dd.DDManager;
import info.scce.addlib.dd.DDManagerException;
import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.DDReorderingType;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.ringlikedd.example.ArithmeticDDManager;
import info.scce.addlib.traverser.PreorderTraverser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.function.BinaryOperator;

import static org.junit.Assert.*;

public class ADDManagerTest extends DDManagerTest {

    private ADDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new ADDManager();
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testNamedVar() {
        String validName = "abcABC09_";
        ADD f = ddManager.namedVar(validName);
        f.recursiveDeref();
    }

    @Test
    public void testNamedVarInvalid() {
        String invalidName = "abc <= 0.3";
        try {
            ddManager.namedVar(invalidName);
            fail();
        } catch (DDManagerException e) {
            assertTrue(e.getMessage().contains(invalidName));
        }
    }

    @Test
    public void testInterManagerOperations() {

        /* Get another ADDManager */
        ADDManager other_ddManager = new ADDManager();

        /* Get operands */
        ADD var0 = ddManager.ithVar(0);
        ADD other_var0 = other_ddManager.ithVar(0);

        /* Try forbidden operation between different ADDManagers */
        try {
            var0.times(other_var0);
            fail("Expected exception was not thrown");
        } catch (DDManagerException e) {
            assertTrue(e.getMessage().contains(DDManager.class.getSimpleName()));
            assertTrue(e.getMessage().contains(ADD.class.getSimpleName()));
            assertTrue(e.getMessage().contains("must share the same"));
        }
        var0.recursiveDeref();
        other_var0.recursiveDeref();

        /* Quit local ADDManager separately */
        assertRefCountZeroAndQuit(other_ddManager);
    }

    @Test
    public void testApply() {

        /* Get some variables */
        ADD var0 = ddManager.ithVar(0);
        ADD var1 = ddManager.ithVar(1);

        /* Get some constants */
        ADD const1 = ddManager.readOne();
        ADD const10 = ddManager.constant(10);

        /* Build var0 + var1 */
        ADD var0_plus_var1 = var0.plus(var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Build const1 + const10 */
        ADD const1_plus_const10 = const1.plus(const10);
        const1.recursiveDeref();
        const10.recursiveDeref();

        /* Build (var0 + var1 + Build const1 + const10) % 7 */
        BinaryOperator<Double> sumMod7 = (left, right) -> (left + right) % 7;
        ADD var0_plus_var1_plus_const1_plus_const10_mod7 = var0_plus_var1.apply(sumMod7, const1_plus_const10);
        var0_plus_var1.recursiveDeref();
        const1_plus_const10.recursiveDeref();

        /* Get terminal nodes */
        ADD t00 = var0_plus_var1_plus_const1_plus_const10_mod7.e().e();
        ADD t01 = var0_plus_var1_plus_const1_plus_const10_mod7.e().t();
        ADD t10 = var0_plus_var1_plus_const1_plus_const10_mod7.t().e();
        ADD t11 = var0_plus_var1_plus_const1_plus_const10_mod7.t().t();

        /* Assert terminal nodes */
        assertEquals((11.0 + 0.0) % 7, t00.v(), 0.0);
        assertEquals((11.0 + 1.0) % 7, t01.v(), 0.0);
        assertEquals((11.0 + 1.0) % 7, t10.v(), 0.0);
        assertEquals((11.0 + 2.0) % 7, t11.v(), 0.0);
        var0_plus_var1_plus_const1_plus_const10_mod7.recursiveDeref();
    }

    @Test
    public void testMonadicApply() {

        /* Get some constants */
        ADD const1 = ddManager.readOne();
        ADD const10 = ddManager.constant(10);

        /* Build const1 + const10 + 5 */
        ADD const1_plus_const10 = const1.plus(const10);
        const1.recursiveDeref();
        const10.recursiveDeref();
        ADD const1_plus_const10_plus_5 = const1_plus_const10.monadicApply(x -> x + 5);
        const1_plus_const10.recursiveDeref();

        /* Assert value */
        double expectedValue = 16.0;
        double actualValue = const1_plus_const10_plus_5.v();
        assertEquals(expectedValue, actualValue, 0.0);
        const1_plus_const10_plus_5.recursiveDeref();
    }

    @Test
    public void testEvalADD() {

        /* Get some variables and constants */
        ADD var0 = ddManager.ithVar(0);
        ADD var1 = ddManager.ithVar(1);
        ADD zero = ddManager.readZero();
        ADD one = ddManager.readOne();
        ADD two = ddManager.constant(2);

        /* Build var0 + var1 */
        ADD var0_plus_var1 = var0.plus(var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Get terminal nodes */
        ADD var0_plus_var1_ass00 = var0_plus_var1.eval(false, false);
        ADD var0_plus_var1_ass01 = var0_plus_var1.eval(false, true);
        ADD var0_plus_var1_ass10 = var0_plus_var1.eval(true, false);
        ADD var0_plus_var1_ass11 = var0_plus_var1.eval(true, true);

        /* Assert terminal nodes */
        assertEquals(zero, var0_plus_var1_ass00);
        assertEquals(one, var0_plus_var1_ass01);
        assertEquals(one, var0_plus_var1_ass10);
        assertEquals(two, var0_plus_var1_ass11);
        zero.recursiveDeref();
        one.recursiveDeref();
        two.recursiveDeref();
        var0_plus_var1.recursiveDeref();
    }

    @Test
    public void testMonadicApplyWithManyDifferentCallbacks() {

        /* Get some variables */
        ADD var0 = ddManager.ithVar(0);
        ADD var1 = ddManager.ithVar(1);

        /* Build sum var0 + var1 */
        ADD var0_plus_var1 = var0.plus(var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Add values 2, 3, ..., 12 one after the other */
        ADD var0_plus_var1_plus_2 = var0_plus_var1.monadicApply(t -> t + 2);
        var0_plus_var1.recursiveDeref();
        ADD var0_plus_var1_plus_5 = var0_plus_var1_plus_2.monadicApply(t -> t + 3);
        var0_plus_var1_plus_2.recursiveDeref();
        ADD var0_plus_var1_plus_9 = var0_plus_var1_plus_5.monadicApply(t -> t + 4);
        var0_plus_var1_plus_5.recursiveDeref();
        ADD var0_plus_var1_plus_14 = var0_plus_var1_plus_9.monadicApply(t -> t + 5);
        var0_plus_var1_plus_9.recursiveDeref();
        ADD var0_plus_var1_plus_20 = var0_plus_var1_plus_14.monadicApply(t -> t + 6);
        var0_plus_var1_plus_14.recursiveDeref();
        ADD var0_plus_var1_plus_27 = var0_plus_var1_plus_20.monadicApply(t -> t + 7);
        var0_plus_var1_plus_20.recursiveDeref();
        ADD var0_plus_var1_plus_35 = var0_plus_var1_plus_27.monadicApply(t -> t + 8);
        var0_plus_var1_plus_27.recursiveDeref();
        ADD var0_plus_var1_plus_44 = var0_plus_var1_plus_35.monadicApply(t -> t + 9);
        var0_plus_var1_plus_35.recursiveDeref();
        ADD var0_plus_var1_plus_54 = var0_plus_var1_plus_44.monadicApply(t -> t + 10);
        var0_plus_var1_plus_44.recursiveDeref();
        ADD var0_plus_var1_plus_65 = var0_plus_var1_plus_54.monadicApply(t -> t + 11);
        var0_plus_var1_plus_54.recursiveDeref();
        ADD var0_plus_var1_plus_77 = var0_plus_var1_plus_65.monadicApply(t -> t + 12);
        var0_plus_var1_plus_65.recursiveDeref();

        /* Assert terminal nodes */
        assertEquals(77.0, var0_plus_var1_plus_77.eval(false, false).v(), 0.0);
        assertEquals(78.0, var0_plus_var1_plus_77.eval(false, true).v(), 0.0);
        assertEquals(78.0, var0_plus_var1_plus_77.eval(true, false).v(), 0.0);
        assertEquals(79.0, var0_plus_var1_plus_77.eval(true, true).v(), 0.0);
        var0_plus_var1_plus_77.recursiveDeref();
    }

    @Test
    public void testApplyWithManyDifferentCallbacks() {

        /* Get some variables */
        ADD var0 = ddManager.ithVar(0);
        ADD var1 = ddManager.ithVar(1);

        /* Multiplication */
        ADD expected_mult = var0.times(var1);
        ADD actual_mult = var0.apply((t, u) -> t * u, var1);
        assertEquals(expected_mult, actual_mult);
        actual_mult.recursiveDeref();
        expected_mult.recursiveDeref();

        /* Addition */
        ADD expected_plus = var0.plus(var1);
        ADD actual_plus = var0.apply((t, u) -> t + u, var1);
        assertEquals(expected_plus, actual_plus);
        actual_plus.recursiveDeref();
        expected_plus.recursiveDeref();

        /* Substraction */
        ADD expected_minus = var0.minus(var1);
        ADD actual_minus = var0.apply((t, u) -> t - u, var1);
        assertEquals(expected_minus, actual_minus);
        actual_minus.recursiveDeref();
        expected_minus.recursiveDeref();

        /* Minimum */
        ADD expected_minimum = var0.minimum(var1);
        ADD actual_minimum = var0.apply(Double::min, var1);
        assertEquals(expected_minimum, actual_minimum);
        actual_minimum.recursiveDeref();
        expected_minimum.recursiveDeref();

        /* Maximum */
        ADD expected_maximum = var0.maximum(var1);
        ADD actual_maximum = var0.apply(Double::max, var1);
        assertEquals(expected_maximum, actual_maximum);
        actual_maximum.recursiveDeref();
        expected_maximum.recursiveDeref();

        /* Disjunction */
        ADD expected_or = var0.or(var1);
        ADD actual_or = var0.apply((t, u) -> t.equals(0.0) && u.equals(0.0) ? 0.0 : 1.0, var1);
        assertEquals(expected_or, actual_or);
        actual_or.recursiveDeref();
        expected_or.recursiveDeref();

        /* Negated conjunction */
        ADD expected_nand = var0.nand(var1);
        ADD actual_nand = var0.apply((t, u) -> 1 - (t * u), var1);
        assertEquals(expected_nand, actual_nand);
        actual_nand.recursiveDeref();
        expected_nand.recursiveDeref();

        /* Negated disjunction */
        ADD expected_nor = var0.nor(var1);
        ADD actual_nor = var0.apply((t, u) -> t.equals(0.0) && u.equals(0.0) ? 1.0 : 0.0, var1);
        assertEquals(expected_nor, actual_nor);
        actual_nor.recursiveDeref();
        expected_nor.recursiveDeref();

        /* Exclusive disjunction */
        ADD expected_xor = var0.xor(var1);
        ADD actual_xor = var0.apply((t, u) -> t + u == 1.0 ? 1.0 : 0.0, var1);
        assertEquals(expected_xor, actual_xor);
        actual_xor.recursiveDeref();
        expected_xor.recursiveDeref();

        /* Exclusive negated disjunction */
        ADD expected_xnor = var0.xnor(var1);
        ADD actual_xnor = var0.apply((t, u) -> u.equals(t) ? 1.0 : 0.0, var1);
        assertEquals(expected_xnor, actual_xnor);
        actual_xnor.recursiveDeref();
        expected_xnor.recursiveDeref();

        /* Greater than indication */
        ADD expected_oneZeroMaximum = var0.oneZeroMaximum(var1);
        ADD actual_oneZeroMaximum = var0.apply((t, u) -> t > u ? 1.0 : 0.0, var1);
        assertEquals(expected_oneZeroMaximum, actual_oneZeroMaximum);
        actual_oneZeroMaximum.recursiveDeref();
        expected_oneZeroMaximum.recursiveDeref();

        /* And something else */
        ADD actual_modulo_2_plus_right = var0.apply((t, u) -> (t % 2) + u, var1);
        assertEquals(0, 0.0, actual_modulo_2_plus_right.eval(false, false).v());
        assertEquals(0, 1.0, actual_modulo_2_plus_right.eval(false, true).v());
        assertEquals(0, 0.0, actual_modulo_2_plus_right.eval(true, false).v());
        assertEquals(0, 1.0, actual_modulo_2_plus_right.eval(true, true).v());
        actual_modulo_2_plus_right.recursiveDeref();
        var0.recursiveDeref();
        var1.recursiveDeref();
    }

    @Test
    public void testTimesSimilarToDocumentation() {

        /* Get a constant */
        ADD f = ddManager.constant(5.0);

        /* Multiply constant with some variables */
        for (int i = 3; i >= 0; i--) {

            /* Get the variable */
            ADD var = ddManager.ithVar(i);

            /* Multiply the variable to the product */
            ADD tmp = var.times(f);
            f.recursiveDeref();
            var.recursiveDeref();
            f = tmp;
        }

        /* Assert all paths */
        for (int i = 0; i < 16; i++) {
            boolean[] assignment = {((i >> 3) & 1) == 1, ((i >> 2) & 1) == 1, ((i >> 1) & 1) == 1, (i & 1) == 1};
            double expectedValue = i == 15 ? 5.0 : 0.0;
            ADD terminal = f.eval(assignment);
            double actualValue = terminal.v();
            assertEquals(expectedValue, actualValue, 0.0);
        }

        /* Release memory */
        f.recursiveDeref();
    }

    @Test
    public void testTimesAsInDocumentation() {

        /* Initialize DDManager with default values */
        ADDManager ddManager = new ADDManager();

        /* Get a constant */
        ADD f = ddManager.constant(5);

        /* Multiply constant with some variables */
        for (int i = 3; i >= 0; i--) {

            /* Get the variable */
            ADD var = ddManager.ithVar(i);

            /* Multiply the variable to the product */
            ADD tmp = var.times(f);
            f.recursiveDeref();
            var.recursiveDeref();
            f = tmp;
        }

        /* ... */

        /* Release memory */
        f.recursiveDeref();
        ddManager.quit();
    }

    @Test
    public void testApplySimilarToDocumentation() {

        /* Get some variables */
        ADD var0 = ddManager.ithVar(0);
        ADD var1 = ddManager.ithVar(1);

        /* Build sum8: var0 + var1 + 8 */
        ADD sum8 = var0.apply((a, b) -> a + b + 8, var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Assert values */
        ADD terminal8 = sum8.e().e();
        assertEquals(8.0, terminal8.v(), 0.0);
        ADD terminal10 = sum8.t().t();
        assertEquals(10.0, terminal10.v(), 0.0);

        /* Release memory */
        sum8.recursiveDeref();
    }

    @Test
    public void testApplyAsInDocumentation() {

        /* Initialize DDManager with default values */
        ADDManager ddManager = new ADDManager();

        /* Get some variables */
        ADD var0 = ddManager.ithVar(0);
        ADD var1 = ddManager.ithVar(1);

        /* Build sum8: var0 + var1 + 8 */
        ADD sum8 = var0.apply((a, b) -> a + b + 8, var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* ... */

        /* Release memory */
        sum8.recursiveDeref();
        ddManager.quit();
    }

    @Test
    public void testReorderNone() {

        /* Get some variables */
        ADD var0 = ddManager.ithVar(0);
        ADD var1 = ddManager.ithVar(1);

        /* Build exclusive disjunction */
        ADD xor = var0.xor(var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Get fingerprint of the order prior to reduceHeap */
        List<ADD> preorderBeforeReduceHeap = Lists.newArrayList(new PreorderTraverser<ADD>(xor));

        /* Assert that DDReorderingType.NONE does not change the order */
        ddManager.reduceHeap(DDReorderingType.NONE, 0);
        List<ADD> preorderAfterReduceHeap = Lists.newArrayList(new PreorderTraverser<ADD>(xor));
        assertEquals(preorderBeforeReduceHeap, preorderAfterReduceHeap);
        xor.recursiveDeref();
    }

    @Test
    public void testReorderSame() {

        /* Get Some variables */
        ADD var0 = ddManager.ithVar(0);
        ADD var1 = ddManager.ithVar(1);
        ADD var2 = ddManager.ithVar(2);
        ADD var3 = ddManager.ithVar(3);

        /* Build disjunction: var0 | var1 */
        ADD var0_or_var1 = var0.or(var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Build conjunction: var2 & var3 */
        ADD var2_and_var3 = var2.times(var3);
        var2.recursiveDeref();
        var3.recursiveDeref();

        /* Build an ADD with a variable order dependant structure: (var0 | var1) & (var2 & var3) */
        ADD subject = var0_or_var1.times(var2_and_var3);
        var0_or_var1.recursiveDeref();
        var2_and_var3.recursiveDeref();

        /* Get fingerprint of the order prior to reduceHeap */
        List<ADD> preorderBeforeReduceHeap = Lists.newArrayList(new PreorderTraverser<ADD>(subject));

        /* Assert that DDReorderingType.ANNEALING changes the order */
        ddManager.reduceHeap(DDReorderingType.ANNEALING, 0);
        List<ADD> preorderAfterReduceHeap = Lists.newArrayList(new PreorderTraverser<ADD>(subject));
        assertNotEquals(preorderBeforeReduceHeap, preorderAfterReduceHeap);
        subject.recursiveDeref();
    }

    @Test
    public void testReorderRandom() {

        /* Get some variables */
        ADD var0 = ddManager.ithVar(0);
        ADD var1 = ddManager.ithVar(1);

        /* Build exclusive disjunction */
        ADD xor = var0.xor(var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Just check that nothing breaks */
        int A_FEW_TIMES = 16;
        for (int i = 0; i < A_FEW_TIMES; i++)
            ddManager.reduceHeap(DDReorderingType.RANDOM, 0);
        xor.recursiveDeref();
    }

    @Test
    public void testReorderWithLargerExample() {

        /* Sum up the variables from 0 to n */
        int n = 128;
        ADD sum = ddManager.constant(0);
        for (int i = 1; i < n; i++) {
            ADD var = ddManager.ithVar(i);
            ADD tmp = sum.plus(var);
            var.recursiveDeref();
            sum.recursiveDeref();
            sum = tmp;
        }

        /* Just check that nothing breaks */
        int A_FEW_TIMES = 16;
        for (int i = 0; i < A_FEW_TIMES; i++)
            ddManager.reduceHeap(DDReorderingType.RANDOM, 0);
        sum.recursiveDeref();
    }

    @Test
    public void testToXDD() {

        /* Get Some variables */
        ADD var0 = ddManager.ithVar(0);
        ADD var1 = ddManager.ithVar(1);
        ADD var2 = ddManager.ithVar(2);
        ADD var3 = ddManager.ithVar(3);

        /* Build disjunction: var0 | var1 */
        ADD var0_or_var1 = var0.or(var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Build conjunction: var2 & var3 */
        ADD var2_times_var3 = var2.times(var3);
        var2.recursiveDeref();
        var3.recursiveDeref();

        /* Build an ADD: (var0 | var1) + var2 * var3 */
        ADD add = var0_or_var1.plus(var2_times_var3);
        var0_or_var1.recursiveDeref();
        var2_times_var3.recursiveDeref();

        /* Convert to XDD */
        ArithmeticDDManager ddManagerTarget = new ArithmeticDDManager();
        XDD<Double> xdd = add.toXDD(ddManagerTarget);

        /* Assert equality */
        for (int i = 0; i < 16; i++) {
            boolean a = ((i >> 0) & 1) == 1;
            boolean b = ((i >> 1) & 1) == 1;
            boolean c = ((i >> 2) & 1) == 1;
            boolean d = ((i >> 3) & 1) == 1;
            assertEquals(add.eval(a, b, c, d).v(), xdd.eval(a, b, c, d).v().doubleValue(), 0.0);
        }

        /* Assert variable names were preserved */
        assertEquals(add.readName(), xdd.readName());
        assertEquals(add.t().readName(), xdd.t().readName());
        assertEquals(add.e().t().readName(), xdd.e().t().readName());

        /* Release memory */
        add.recursiveDeref();
        xdd.recursiveDeref();
        assertRefCountZeroAndQuit(ddManagerTarget);
    }

    public void testSecureDeref() {
        ADD A = ddManager.ithVar(0);
        A.recursiveDeref();
        try {
            A.recursiveDeref();
            fail("ref count should have been zero");
        }catch (DDManagerException e){
            assertEquals(e.getMessage(),"Cannot dereference unreferenced DD");
        }
    }

}
