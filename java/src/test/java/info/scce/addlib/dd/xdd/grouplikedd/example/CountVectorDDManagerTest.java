package info.scce.addlib.dd.xdd.grouplikedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CountVectorDDManagerTest extends DDManagerTest {

    private static final int N = 4;

    private CountVectorDDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new CountVectorDDManager(N);
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testNeutralElement() {

        /* Get constants */
        XDD<CountVector> neutral = ddManager.neutral();
        XDD<CountVector> someCount = ddManager.constant(new CountVector(0, 1, 2, 3));

        /* Join with neutral element */
        XDD<CountVector> neutral_join_someCount = neutral.join(someCount);
        XDD<CountVector> someCount_join_neutral = someCount.join(neutral);
        XDD<CountVector> neutral_join_neutral = neutral.join(neutral);

        /* Assert results */
        assertEquals(someCount, neutral_join_someCount);
        assertEquals(someCount, someCount_join_neutral);
        assertEquals(neutral, neutral_join_neutral);

        /* Release memory */
        neutral.recursiveDeref();
        someCount.recursiveDeref();
        neutral_join_someCount.recursiveDeref();
        someCount_join_neutral.recursiveDeref();
        neutral_join_neutral.recursiveDeref();
    }

    @Test
    public void testJoin() {

        /* Get constants */
        XDD<CountVector> a = ddManager.constant(new CountVector(987, 654, 321, 0));
        XDD<CountVector> b = ddManager.constant(new CountVector(12, 34, 56, 78));
        XDD<CountVector> sum = ddManager.constant(new CountVector(987 + 12, 654 + 34, 321 + 56, 0 + 78));

        /* Assert join */
        XDD<CountVector> a_join_b = a.join(b);
        assertEquals(sum, a_join_b);

        /* Release memory */
        a.recursiveDeref();
        b.recursiveDeref();
        sum.recursiveDeref();
        a_join_b.recursiveDeref();
    }

    @Test
    public void testParseElement() {

        /* Repeat test a few times with varying values */
        for (int x = 123; x < 4567; x += 89) {

            /* Assert parseElement is iverse of toString for the value of x */
            XDD<CountVector> xddCount = ddManager.constant(new CountVector(x % 2, x % 3, x % 5, x % 7));
            String str = xddCount.toString();
            XDD<CountVector> xddCountReproduced = ddManager.constant(ddManager.parseElement(str));
            assertEquals(xddCount, xddCountReproduced);

            /* Release memory */
            xddCount.recursiveDeref();
            xddCountReproduced.recursiveDeref();
        }
    }
}
