package info.scce.addlib.dd.xdd.latticedd.example;

import org.junit.Test;

import static java.lang.Math.random;
import static org.junit.Assert.assertEquals;

public class BooleanVectorTest {

    @Test
    public void testAnd() {
        BooleanVector a = new BooleanVector(true, false, true, false);
        BooleanVector b = new BooleanVector(true, true, false, false);
        BooleanVector expected_a_and_b = new BooleanVector(true, false, false, false);
        BooleanVector actual_a_and_b = a.and(b);
        assertEquals(expected_a_and_b, actual_a_and_b);
    }

    @Test
    public void testOr() {
        BooleanVector a = new BooleanVector(true, false, true, false);
        BooleanVector b = new BooleanVector(true, true, false, false);
        BooleanVector expected_a_or_b = new BooleanVector(true, true, true, false);
        BooleanVector actual_a_or_b = a.or(b);
        assertEquals(expected_a_or_b, actual_a_or_b);
    }

    @Test
    public void testNot() {
        BooleanVector a = new BooleanVector(true, false, true, true);
        BooleanVector expected_not_a = new BooleanVector(false, true, false, false);
        BooleanVector actual_not_a = a.not();
        assertEquals(expected_not_a, actual_not_a);
    }

    @Test
    public void testParseBooleanVectorRandom() {

        /* Repeat test a few times with random vlaues */
        for (int i = 0; i < 64; i++) {

            /* Assert that a parsing is reverse of toString for a random vector */
            BooleanVector randVec = new BooleanVector(random() < 0.3, random() < 0.8, random() < 0.7, random() < 0.5);
            String str = randVec.toString();
            BooleanVector reproduced = BooleanVector.parseBooleanVector(str);
            assertEquals(randVec, reproduced);
        }
    }

    @Test
    public void testParseBooleanVectorEmpty() {
        BooleanVector vec = new BooleanVector();
        String str = vec.toString();
        BooleanVector reproduced = BooleanVector.parseBooleanVector(str);
        assertEquals(vec, reproduced);
    }
}
