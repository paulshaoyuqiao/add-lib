package info.scce.addlib.dd.xdd.latticedd.example;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class PartitionTest {

    private Partition<String> people = new Partition<>(new String[]{"Tiziana", "Bernhard", "Alnis", "Frederik"});
    private Partition<String> profs = new Partition<>(new String[]{"Tiziana", "Bernhard"});
    private Partition<String> limerick = new Partition<>(new String[]{"Tiziana", "Frederik"});

    @Test
    public void testMeet() {
        HashSet<HashSet<String>> expected_profs_meet_limerick = new HashSet<>();
        expected_profs_meet_limerick.add(new HashSet<>(Collections.singletonList("Frederik")));
        expected_profs_meet_limerick.add(new HashSet<>(Collections.singletonList("Tiziana")));
        expected_profs_meet_limerick.add(new HashSet<>(Collections.singletonList("Bernhard")));
        assertEquals(expected_profs_meet_limerick, profs.meet(limerick).blocks());
        assertEquals(expected_profs_meet_limerick, limerick.meet(profs).blocks());
    }

    @Test
    public void testJoin() {
        HashSet<HashSet<String>> expected_profs_join_limerick = new HashSet<>();
        expected_profs_join_limerick.add(new HashSet<>(Arrays.asList("Frederik", "Tiziana", "Bernhard")));
        assertEquals(expected_profs_join_limerick, profs.join(limerick).blocks());
        assertEquals(expected_profs_join_limerick, limerick.join(profs).blocks());
    }

    @Test
    public void testJoin2() {
        Partition<String> expected_profs_join_people = people;
        assertEquals(expected_profs_join_people, profs.join(people));
        assertEquals(expected_profs_join_people, people.join(profs));
    }

    @Test
    public void testBlocks() {
        Set<Set<String>> expected_profs_meet_people_blocks = new HashSet<>();
        expected_profs_meet_people_blocks.add(new HashSet<>(Arrays.asList("Frederik", "Alnis")));
        expected_profs_meet_people_blocks.add(new HashSet<>(Arrays.asList("Tiziana", "Bernhard")));
        assertEquals(expected_profs_meet_people_blocks, profs.meet(people).blocks());
        assertEquals(expected_profs_meet_people_blocks, people.meet(profs).blocks());
    }

    @Test
    public void testParsePartitionEmpty() {
        testParsePartition(Partition.emptyPartition());
    }

    @Test
    public void testParsePartitionSingleBlock() {
        testParsePartition(profs);
        testParsePartition(people);
        testParsePartition(limerick);
    }

    @Test
    public void testParsePartitionMultipleBlocks() {
        testParsePartition(people.meet(limerick).meet(profs));
        testParsePartition(people.meet(limerick));
        testParsePartition(limerick.meet(profs));
    }

    public void testParsePartition(Partition<String> original) {
        String str = original.toString();
        Partition<String> reconstructed = Partition.parsePartition(str, x -> x);
        assertEquals(original, reconstructed);
    }
}
