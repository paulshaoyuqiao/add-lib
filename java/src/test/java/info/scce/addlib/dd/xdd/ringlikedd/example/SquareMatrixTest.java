package info.scce.addlib.dd.xdd.ringlikedd.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SquareMatrixTest {

    @Test
    public void testSquareMatrix() {

        /* Get some test matrices */
        SquareMatrix m0 = new SquareMatrix(new double[][]{{1, 2}, {4, 8}});
        SquareMatrix m1 = new SquareMatrix(new double[][]{{2, 0}, {8, 16}});

        /* Assert additive operations */
        SquareMatrix expectedSum = new SquareMatrix(new double[][]{{3, 2}, {12, 24}});
        SquareMatrix expectedAddInverse0 = new SquareMatrix(new double[][]{{-1, -2}, {-4, -8}});
        SquareMatrix expectedAddInverse1 = new SquareMatrix(new double[][]{{-2, -0}, {-8, -16}});
        assertEquals(expectedSum, m0.add(m1));
        assertEquals(expectedAddInverse0, m0.addInverse());
        assertEquals(expectedAddInverse1, m1.addInverse());
    }

    @Test
    public void testParseSquareMatrix() {
        SquareMatrix m = new SquareMatrix(new double[][]{{0.0, -2.0}, {4.0, 16.0}});
        String str = m.toString();
        SquareMatrix reproduced = SquareMatrix.parseSquareMatrix(str);
        assertEquals(m, reproduced);
    }
}
