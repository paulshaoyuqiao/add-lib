package info.scce.addlib.dd.xdd.grouplikedd.example;

import org.junit.Test;

import static java.lang.Math.random;
import static org.junit.Assert.assertEquals;

public class CountVectorTest {

    @Test
    public void testJoin() {

        /* Get some */
        CountVector zero = CountVector.zero(3);
        CountVector a = new CountVector(1, 3, 7);
        CountVector b = new CountVector(9, 3, 2);
        CountVector c = new CountVector(4, 1, 5);

        /* Assert some sums */
        assertEquals(a, a.add(zero));
        assertEquals(new CountVector(10, 6, 9), a.add(b));
        assertEquals(new CountVector(5, 4, 12), a.add(c));
        assertEquals(new CountVector(13, 4, 7), b.add(c));
    }

    @Test
    public void testParseCountVector() {
        CountVector a = new CountVector(1, 2, 6);
        CountVector b = new CountVector(7, 3, 1);
        CountVector aParsed = CountVector.parseCountVector("[1, 2, 6]");
        CountVector bParsed = CountVector.parseCountVector("[7, 3, 1]");
        assertEquals(a, aParsed);
        assertEquals(b, bParsed);
    }

    @Test
    public void testParseCountVectorEmpty() {
        CountVector vec = new CountVector();
        String str = vec.toString();
        CountVector reproduced = CountVector.parseCountVector(str);
        assertEquals(vec, reproduced);
    }

    @Test
    public void testParseCountVectorRandom() {

        /* Repeat test a few times with random vlaues */
        for (int i = 0; i < 64; i++) {

            /* Assert that a parsing is reverse of toString for a random vector */
            CountVector randVec = new CountVector((int) (16 * random()), (int) (1234 * random()), (int) (789456 * random()));
            String str = randVec.toString();
            CountVector reproduced = CountVector.parseCountVector(str);
            assertEquals(randVec, reproduced);
        }
    }
}