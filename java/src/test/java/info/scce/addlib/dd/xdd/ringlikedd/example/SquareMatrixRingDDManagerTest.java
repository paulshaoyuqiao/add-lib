package info.scce.addlib.dd.xdd.ringlikedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SquareMatrixRingDDManagerTest extends DDManagerTest {

    private SquareMatrixRingDDManager ddManager = null;

    private final static int N = 2;

    @Before
    public void setUp() {
        ddManager = new SquareMatrixRingDDManager(N);
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testParseElement() {

        /* Get some constants */
        XDD<SquareMatrix> a = ddManager.constant(new SquareMatrix(new double[][]{{2.0, 3.2}, {4.0, 1.5}}));
        XDD<SquareMatrix> b = ddManager.constant(new SquareMatrix(new double[][]{{5.3, 3.7}, {4.1, 4.2}}));

        /* Reconstruct and assert equality */
        XDD<SquareMatrix> reconstructed_a = ddManager.constant(ddManager.parseElement(a.toString()));
        XDD<SquareMatrix> reconstructed_b = ddManager.constant(ddManager.parseElement(b.toString()));
        assertEquals(a, reconstructed_a);
        assertEquals(b, reconstructed_b);

        /* Release memory */
        a.recursiveDeref();
        b.recursiveDeref();
        reconstructed_a.recursiveDeref();
        reconstructed_b.recursiveDeref();
    }
}