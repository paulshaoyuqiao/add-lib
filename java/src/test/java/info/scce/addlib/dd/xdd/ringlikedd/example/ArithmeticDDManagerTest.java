package info.scce.addlib.dd.xdd.ringlikedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.grouplikedd.example.StringMonoidDDManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ArithmeticDDManagerTest extends DDManagerTest {

    public final static double EPS = 0.00001;

    private ArithmeticDDManager ddManager;

    private XDD<Double> five;
    private XDD<Double> four;
    private XDD<Double> sevenPointFive;
    private XDD<Double> minusTwelve;
    private XDD<Double> threePointTwo;

    @Before
    public void setUp() {

        /* Set up DDManager */
        ddManager = new ArithmeticDDManager();

        /* Set up come constants */
        five = ddManager.constant(5.0);
        four = ddManager.constant(4.0);
        sevenPointFive = ddManager.constant(7.5);
        minusTwelve = ddManager.constant(-12.0);
        threePointTwo = ddManager.constant(3.2);
    }

    @After
    public void tearDown() {

        /* Release memory of constants */
        five.recursiveDeref();
        four.recursiveDeref();
        sevenPointFive.recursiveDeref();
        minusTwelve.recursiveDeref();
        threePointTwo.recursiveDeref();

        /* Tear down DDManager */
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testAdd() {

        /* Assert addition */
        XDD<Double> five_plus_threePointTwo = five.add(threePointTwo);
        XDD<Double> threePointTwo_plus_five = threePointTwo.add(five);
        XDD<Double> threePointTwo_plus_five_plus_minusTwelve = threePointTwo_plus_five.add(minusTwelve);
        XDD<Double> sevenPointFive_plus_sevenPointFive = sevenPointFive.add(sevenPointFive);
        assertEquals(8.2, five_plus_threePointTwo.v(), EPS);
        assertEquals(five_plus_threePointTwo.v(), threePointTwo_plus_five.v(), EPS);
        assertEquals(-3.8, threePointTwo_plus_five_plus_minusTwelve.v(), EPS);
        assertEquals(15.0, sevenPointFive_plus_sevenPointFive.v(), EPS);

        /* Release memory */
        five_plus_threePointTwo.recursiveDeref();
        threePointTwo_plus_five.recursiveDeref();
        threePointTwo_plus_five_plus_minusTwelve.recursiveDeref();
        sevenPointFive_plus_sevenPointFive.recursiveDeref();
    }

    @Test
    public void testAddInverse() {

        /* Assert additive inversion */
        XDD<Double> minus_five = five.addInverse();
        XDD<Double> minus_threePointTwo = threePointTwo.addInverse();
        XDD<Double> minus_minus_threePointTwo = minus_threePointTwo.addInverse();
        XDD<Double> minus_sevenPointFive = sevenPointFive.addInverse();
        assertEquals(-5.0, minus_five.v(), EPS);
        assertEquals(-3.2, minus_threePointTwo.v(), EPS);
        assertEquals(threePointTwo.v(), minus_minus_threePointTwo.v(), EPS);
        assertEquals(-7.5, minus_sevenPointFive.v(), EPS);

        /* Release memory */
        minus_five.recursiveDeref();
        minus_threePointTwo.recursiveDeref();
        minus_minus_threePointTwo.recursiveDeref();
        minus_sevenPointFive.recursiveDeref();
    }

    @Test
    public void testMult() {

        /* Assert multiplication */
        XDD<Double> five_times_threePointTwo = five.mult(threePointTwo);
        XDD<Double> threePointTwo_times_five = threePointTwo.mult(five);
        XDD<Double> threePointTwo_times_five_times_minusTwelve = threePointTwo_times_five.mult(minusTwelve);
        XDD<Double> minusTwelve_times_Twelve = minusTwelve.mult(minusTwelve);
        assertEquals(16.0, five_times_threePointTwo.v(), EPS);
        assertEquals(five_times_threePointTwo.v(), threePointTwo_times_five.v(), EPS);
        assertEquals(-192, threePointTwo_times_five_times_minusTwelve.v(), EPS);
        assertEquals(144.0, minusTwelve_times_Twelve.v(), EPS);

        /* Release memory */
        five_times_threePointTwo.recursiveDeref();
        threePointTwo_times_five.recursiveDeref();
        threePointTwo_times_five_times_minusTwelve.recursiveDeref();
        minusTwelve_times_Twelve.recursiveDeref();
    }

    @Test
    public void multInverse() {

        /* Assert multiplicative inversion */
        XDD<Double> inv_five = five.multInverse();
        XDD<Double> inv_threePointTwo = threePointTwo.multInverse();
        XDD<Double> inv_inv_threePointTwo = inv_threePointTwo.multInverse();
        XDD<Double> inv_sevenPointFive = sevenPointFive.multInverse();
        assertEquals(0.2, inv_five.v(), EPS);
        assertEquals(0.3125, inv_threePointTwo.v(), EPS);
        assertEquals(threePointTwo.v(), inv_inv_threePointTwo.v(), EPS);
        assertEquals(0.1333333333, inv_sevenPointFive.v(), EPS);

        /* Release memory */
        inv_five.recursiveDeref();
        inv_threePointTwo.recursiveDeref();
        inv_inv_threePointTwo.recursiveDeref();
        inv_sevenPointFive.recursiveDeref();
    }

    @Test
    public void inf() {

        /* Assert infimum */
        XDD<Double> five_inf_threePointTwo = five.inf(threePointTwo);
        XDD<Double> threePointTwo_inf_five = threePointTwo.inf(five);
        XDD<Double> threePointTwo_inf_five_minusTwelve = threePointTwo_inf_five.inf(minusTwelve);
        XDD<Double> sevenPointFive_inf_sevenPointFive = sevenPointFive.inf(sevenPointFive);
        assertEquals(3.2, five_inf_threePointTwo.v(), EPS);
        assertEquals(five_inf_threePointTwo.v(), threePointTwo_inf_five.v(), EPS);
        assertEquals(-12.0, threePointTwo_inf_five_minusTwelve.v(), EPS);
        assertEquals(7.5, sevenPointFive_inf_sevenPointFive.v(), EPS);

        /* Release memory */
        five_inf_threePointTwo.recursiveDeref();
        threePointTwo_inf_five.recursiveDeref();
        threePointTwo_inf_five_minusTwelve.recursiveDeref();
        sevenPointFive_inf_sevenPointFive.recursiveDeref();
    }

    @Test
    public void sup() {

        /* Assert supremum */
        XDD<Double> five_sup_threePointTwo = five.sup(threePointTwo);
        XDD<Double> threePointTwo_sup_five = threePointTwo.sup(five);
        XDD<Double> threePointTwo_sup_five_sup_minusTwelve = threePointTwo_sup_five.sup(minusTwelve);
        XDD<Double> sevenPointFive_sup_sevenPointFive = sevenPointFive.sup(sevenPointFive);
        assertEquals(5.0, five_sup_threePointTwo.v(), EPS);
        assertEquals(five_sup_threePointTwo.v(), threePointTwo_sup_five.v(), EPS);
        assertEquals(5.0, threePointTwo_sup_five_sup_minusTwelve.v(), EPS);
        assertEquals(7.5, sevenPointFive_sup_sevenPointFive.v(), EPS);

        /* Release memory */
        five_sup_threePointTwo.recursiveDeref();
        threePointTwo_sup_five.recursiveDeref();
        threePointTwo_sup_five_sup_minusTwelve.recursiveDeref();
        sevenPointFive_sup_sevenPointFive.recursiveDeref();
    }

    @Test
    public void testParseElement() {

        /* Get some constants */
        XDD<Double> a = ddManager.constant(5.0);
        XDD<Double> b = ddManager.constant(7.3);

        /* Reconstruct and assert equality */
        XDD<Double> reconstructed_a = ddManager.constant(ddManager.parseElement(a.toString()));
        XDD<Double> reconstructed_b = ddManager.constant(ddManager.parseElement(b.toString()));
        assertEquals(a, reconstructed_a);
        assertEquals(b, reconstructed_b);

        /* Release memory */
        a.recursiveDeref();
        b.recursiveDeref();
        reconstructed_a.recursiveDeref();
        reconstructed_b.recursiveDeref();
    }

    @Test
    public void testMonadicTransform() {

        /* Build test DD */
        XDD<Double> var0 = ddManager.namedVar("hey");
        XDD<Double> var1 = ddManager.namedVar("ho");
        XDD<Double> sum = var0.add(var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Transform test DD */
        StringMonoidDDManager ddManager2 = new StringMonoidDDManager();
        XDD<String> sumStr = sum.monadicTransform(ddManager2, x -> x > 0 ? "> zero" : "<= zero");

        /* Assert terminals */
        assertEquals("> zero", sumStr.t().v());
        assertEquals("> zero", sumStr.e().t().v());
        assertEquals("<= zero", sumStr.e().e().v());

        /* Assert variable names and indices */
        assertEquals(0, sumStr.readIndex());
        assertEquals("hey", sumStr.readName());
        assertEquals(1, sumStr.e().readIndex());
        assertEquals("ho", sumStr.e().readName());
        sum.recursiveDeref();
        sumStr.recursiveDeref();

        /* Release memory */
        assertRefCountZeroAndQuit(ddManager2);
    }
}
