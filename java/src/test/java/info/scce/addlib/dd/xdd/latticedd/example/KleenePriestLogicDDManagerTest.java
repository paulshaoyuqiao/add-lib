package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.codegenerator.DotGenerator;
import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static info.scce.addlib.dd.xdd.latticedd.example.KleenePriestValue.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class KleenePriestLogicDDManagerTest extends DDManagerTest {

    private KleenePriestLogicDDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new KleenePriestLogicDDManager();
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testSimilarToDocumentation() {

        /* A weak necessary criterion  */
        XDD<KleenePriestValue> recent = ddManager.namedVar("recentlyReceived", TRUE, UNKNOWN);

        /* A strong necessary criterion */
        XDD<KleenePriestValue> addrBook = ddManager.namedVar("senderInAddressBook", TRUE, FALSE);

        /* The conjunction of all necessary criteria */
        XDD<KleenePriestValue> urgentMail = recent.and(addrBook);
        recent.recursiveDeref();
        addrBook.recursiveDeref();

        /* Assert terminal values */
        assertEquals(TRUE, urgentMail.t().t().v());
        assertEquals(FALSE, urgentMail.t().e().v());
        assertEquals(UNKNOWN, urgentMail.e().t().v());
        assertEquals(FALSE, urgentMail.e().e().v());

        /* Print for documentation */
        new DotGenerator<XDD<KleenePriestValue>>().generateToStdOut(urgentMail, "urgentMail");

        /* Release memory */
        urgentMail.recursiveDeref();
    }

    @Test
    public void testAsInDocumentation() {

        /* Initialize DDManager with default values */
        KleenePriestLogicDDManager ddManager = new KleenePriestLogicDDManager();

        /* A weak necessary criterion  */
        XDD<KleenePriestValue> recent = ddManager.namedVar("recentlyReceived", TRUE, UNKNOWN);

        /* A strong necessary criterion */
        XDD<KleenePriestValue> addrBook = ddManager.namedVar("senderInAddressBook", TRUE, FALSE);

        /* The conjunction of all necessary criteria */
        XDD<KleenePriestValue> urgentMail = recent.and(addrBook);
        recent.recursiveDeref();
        addrBook.recursiveDeref();

        /* ... */

        /* Release memory */
        urgentMail.recursiveDeref();
        ddManager.quit();
    }

    @Test
    public void testParseElement() {

        /* Get sine constants */
        XDD<KleenePriestValue> yes = ddManager.constant(TRUE);
        XDD<KleenePriestValue> no = ddManager.constant(FALSE);
        XDD<KleenePriestValue> maybe = ddManager.constant(UNKNOWN);

        /* Reconstruct constants and assert equality */
        XDD<KleenePriestValue> reconstructed_yes = ddManager.constant(ddManager.parseElement(yes.toString()));
        XDD<KleenePriestValue> reconstructed_no = ddManager.constant(ddManager.parseElement(no.toString()));
        XDD<KleenePriestValue> reconstructed_maybe = ddManager.constant(ddManager.parseElement(maybe.toString()));
        assertEquals(yes, reconstructed_yes);
        assertEquals(no, reconstructed_no);
        assertEquals(maybe, reconstructed_maybe);

        /* Test malformed input */
        try {
            ddManager.parseElement("haus");
            fail("parseElement did not fail when it should");
        } catch (RuntimeException e) {
        }

        /* Release memory */
        yes.recursiveDeref();
        no.recursiveDeref();
        maybe.recursiveDeref();
        reconstructed_yes.recursiveDeref();
        reconstructed_no.recursiveDeref();
        reconstructed_maybe.recursiveDeref();
    }
}
