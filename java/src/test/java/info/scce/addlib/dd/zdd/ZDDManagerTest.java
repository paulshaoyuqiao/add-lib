package info.scce.addlib.dd.zdd;

import info.scce.addlib.dd.DDManagerTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ZDDManagerTest extends DDManagerTest {

    private ZDDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new ZDDManager();
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testSimple() {

        /* Get constants */
        ZDD zero = ddManager.readZero();
        ZDD one = ddManager.readZddOne(0);

        /* Test change */
        ZDD c = one.change(0);
        ZDD c_t = c.t();
        assertEquals(one, c_t);
        ZDD c_e = c.e();
        assertEquals(zero, c_e);

        /* Test change */
        ZDD d = one.change(1);
        ZDD d_t = d.t();
        assertEquals(one, d_t);
        ZDD d_e = d.e();
        assertEquals(zero, d_e);

        /* Test union */
        ZDD e = c.union(d);
        ZDD e_t = e.t();
        assertEquals(one, e_t);
        ZDD e_e = e.e();
        ZDD e_e_t = e_e.t();
        assertEquals(one, e_e_t);
        ZDD e_e_e = e_e.e();
        assertEquals(zero, e_e_e);

        /* Test union */
        ZDD f = one.union(e);
        ZDD f_t = f.t();
        assertEquals(one, f_t);
        ZDD f_e = f.e();
        ZDD f_e_t = f_e.t();
        assertEquals(one, f_e_t);
        ZDD f_e_e = f_e.e();
        assertEquals(one, f_e_e);

        /* Test diff */
        ZDD g = f.diff(c);
        ZDD g_t = g.t();
        assertEquals(one, g_t);
        ZDD g_e = g.e();
        assertEquals(one, g_e);

        /* Release memory */
        zero.recursiveDeref();
        one.recursiveDeref();
        c.recursiveDeref();
        d.recursiveDeref();
        e.recursiveDeref();
        f.recursiveDeref();
        g.recursiveDeref();
    }
}
