package info.scce.addlib.dd.xdd;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.grouplikedd.example.StringMonoidDDManager;
import info.scce.addlib.dd.xdd.ringlikedd.example.ArithmeticDDManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class XDDManagerTest extends DDManagerTest {

    private XDDManager<String> ddManager = null;

    @Before
    public void setUp() {
        ddManager = new XDDManager<>();
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testConstant() {

        /* Test constant creation */
        XDD<String> c = ddManager.constant("Some string");
        String expected = "Some string";
        String actual = c.v();
        assertEquals(expected, actual);
        c.recursiveDeref();
    }

    @Test
    public void testNewVarFromElements() {

        /* Test variable creation from elements */
        XDD<String> f = ddManager.newVar("yes", "no");
        String expected_f_then = "yes";
        String expected_f_else = "no";
        String actual_f_then = f.t().v();
        String actual_f_else = f.e().v();
        assertEquals(expected_f_then, actual_f_then);
        assertEquals(expected_f_else, actual_f_else);
        f.recursiveDeref();
    }

    @Test
    public void testNewVarFromConstants() {

        /* Test variable creation from constants */
        XDD<String> c1 = ddManager.constant("yes");
        XDD<String> c0 = ddManager.constant("no");
        XDD<String> f = ddManager.newVar(c1, c0);
        String expected_f_then = "yes";
        String expected_f_else = "no";
        String actual_f_then = f.t().v();
        String actual_f_else = f.e().v();
        assertEquals(expected_f_then, actual_f_then);
        assertEquals(expected_f_else, actual_f_else);
        f.recursiveDeref();
        c0.recursiveDeref();
        c1.recursiveDeref();
    }

    @Test
    public void testIthVarFromElements() {

        /* Test variable creation from elements */
        XDD<String> f = ddManager.ithVar(12, "yes", "no");
        String expected_f_then = "yes";
        String expected_f_else = "no";
        String actual_f_then = f.t().v();
        String actual_f_else = f.e().v();
        assertEquals(expected_f_then, actual_f_then);
        assertEquals(expected_f_else, actual_f_else);
        f.recursiveDeref();
    }

    @Test
    public void testIthVarFromConstants() {

        /* Test variable creation from constants */
        XDD<String> c1 = ddManager.constant("yes");
        XDD<String> c0 = ddManager.constant("no");
        XDD<String> f = ddManager.ithVar(3, c1, c0);
        String expected_f_then = "yes";
        String expected_f_else = "no";
        String actual_f_then = f.t().v();
        String actual_f_else = f.e().v();
        assertEquals(expected_f_then, actual_f_then);
        assertEquals(expected_f_else, actual_f_else);
        f.recursiveDeref();
        c0.recursiveDeref();
        c1.recursiveDeref();
    }

    @Test
    public void testNamedVarFromElements() {

        /* Test variable creation from elements */
        XDD<String> f = ddManager.namedVar("foo", "yes", "no");
        String expected_f_then = "yes";
        String expected_f_else = "no";
        String actual_f_then = f.t().v();
        String actual_f_else = f.e().v();
        assertEquals(expected_f_then, actual_f_then);
        assertEquals(expected_f_else, actual_f_else);
        f.recursiveDeref();
    }

    @Test
    public void testNamedVarFromConstants() {

        /* Test variable creation from constants */
        XDD<String> c1 = ddManager.constant("yes");
        XDD<String> c0 = ddManager.constant("no");
        XDD<String> f = ddManager.namedVar("bar", c1, c0);
        String expected_f_then = "yes";
        String expected_f_else = "no";
        String actual_f_then = f.t().v();
        String actual_f_else = f.e().v();
        assertEquals(expected_f_then, actual_f_then);
        assertEquals(expected_f_else, actual_f_else);
        f.recursiveDeref();
        c0.recursiveDeref();
        c1.recursiveDeref();
    }

    @Test
    public void testSecureDeref() {
        XDD<String> c1 = ddManager.constant("yes");
        XDD<String> c0 = ddManager.constant("no");
        XDD<String> f = ddManager.namedVar("bar", c1, c0);
        f.recursiveDeref();
        c0.recursiveDeref();
        c1.recursiveDeref();
        try {
            c0.recursiveDeref();
            fail("Reference count should be 0");
        } catch (RuntimeException e) {
            assertEquals("Cannot dereference unreferenced DD", e.getMessage());

        }
    }
}
