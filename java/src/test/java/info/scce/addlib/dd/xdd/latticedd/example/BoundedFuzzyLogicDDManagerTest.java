package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BoundedFuzzyLogicDDManagerTest extends DDManagerTest {

    public final static double EPS = 0.00001;

    private BoundedFuzzyLogicDDManager ddManager = null;
    private XDD<Double> a = null;
    private XDD<Double> b = null;
    private XDD<Double> c;

    @Before
    public void setUp() {
        ddManager = new BoundedFuzzyLogicDDManager();
        a = ddManager.constant(0.8);
        b = ddManager.constant(0.4);
        c = ddManager.constant(0.2);
    }

    @After
    public void tearDown() {
        a.recursiveDeref();
        b.recursiveDeref();
        c.recursiveDeref();
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testNot() {
        XDD<Double> not_a = a.not();
        assertEquals(0.2, not_a.v().doubleValue(), EPS);
        not_a.recursiveDeref();
    }

    @Test
    public void testAnd() {
        XDD<Double> a_and_b = a.and(b);
        assertEquals(0.2, a_and_b.v().doubleValue(), EPS);
        a_and_b.recursiveDeref();
    }

    @Test
    public void testOr() {
        XDD<Double> b_or_c = b.or(c);
        assertEquals(0.6, b_or_c.v().doubleValue(), EPS);
        b_or_c.recursiveDeref();
    }

    @Test
    public void testBot() {
        XDD<Double> zero = ddManager.bot();
        assertEquals(0, zero.v().doubleValue(), EPS);
        zero.recursiveDeref();
    }

    @Test
    public void testTop() {
        XDD<Double> one = ddManager.top();
        assertEquals(1, one.v().doubleValue(), EPS);
        one.recursiveDeref();
    }

    @Test
    public void testRangeCorrection() {
        XDD<Double> two = ddManager.constant(2.0);
        XDD<Double> three = ddManager.constant(3.0);
        XDD<Double> two_or_three = two.or(three);
        assertEquals(1.0, two_or_three.v().doubleValue(), EPS);
        two.recursiveDeref();
        three.recursiveDeref();
        two_or_three.recursiveDeref();
    }
}