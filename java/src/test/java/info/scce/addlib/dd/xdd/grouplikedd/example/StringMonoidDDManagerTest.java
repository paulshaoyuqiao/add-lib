package info.scce.addlib.dd.xdd.grouplikedd.example;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringMonoidDDManagerTest extends DDManagerTest {

    private StringMonoidDDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new StringMonoidDDManager();
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testHelloWorldConstant() {

        /* Assert neutral element */
        XDD<String> hello = ddManager.constant("Hello");
        XDD<String> e = ddManager.neutral();
        XDD<String> hello_join_e = hello.join(e);
        XDD<String> e_join_hello = e.join(hello);
        assertEquals(hello, hello_join_e);
        assertEquals(hello, e_join_hello);
        assertEquals(hello.v(), hello_join_e.v());
        assertEquals(hello.v(), e_join_hello.v());
        e.recursiveDeref();
        hello_join_e.recursiveDeref();
        e_join_hello.recursiveDeref();

        /* Assert concatenation */
        XDD<String> expected_hello_join_world = ddManager.constant("Hello World!");
        XDD<String> world = ddManager.constant(" World!");
        XDD<String> actual_hello_join_world = hello.join(world);
        assertEquals(expected_hello_join_world, actual_hello_join_world);
        assertEquals(expected_hello_join_world.v(), actual_hello_join_world.v());
        assertEquals("Hello World!", actual_hello_join_world.v());
        hello.recursiveDeref();
        expected_hello_join_world.recursiveDeref();
        world.recursiveDeref();
        actual_hello_join_world.recursiveDeref();
    }

    @Test
    public void testHelloWorldTree() {

        /* Build the decision diagram */
        XDD<String> hello = ddManager.namedVar("sayHello", "Hello ", "***** ");
        XDD<String> world = ddManager.namedVar("sayWorld", "World!", "*****!");
        XDD<String> hello_join_world = hello.join(world);
        hello.recursiveDeref();
        world.recursiveDeref();

        /* Assert decision diagram */
        XDD<String> expected_hello_join_world_then_then = ddManager.constant("Hello World!");
        XDD<String> expected_hello_join_world_then_else = ddManager.constant("Hello *****!");
        XDD<String> expected_hello_join_world_else_then = ddManager.constant("***** World!");
        XDD<String> expected_hello_join_world_else_else = ddManager.constant("***** *****!");
        XDD<String> actual_hello_join_world_then_then = hello_join_world.t().t();
        XDD<String> actual_hello_join_world_then_else = hello_join_world.t().e();
        XDD<String> actual_hello_join_world_else_then = hello_join_world.e().t();
        XDD<String> actual_hello_join_world_else_else = hello_join_world.e().e();
        assertEquals(expected_hello_join_world_then_then, actual_hello_join_world_then_then);
        assertEquals(expected_hello_join_world_then_then.v(), actual_hello_join_world_then_then.v());
        assertEquals(expected_hello_join_world_then_else, actual_hello_join_world_then_else);
        assertEquals(expected_hello_join_world_then_else.v(), actual_hello_join_world_then_else.v());
        assertEquals(expected_hello_join_world_else_then, actual_hello_join_world_else_then);
        assertEquals(expected_hello_join_world_else_then.v(), actual_hello_join_world_else_then.v());
        assertEquals(expected_hello_join_world_else_else, actual_hello_join_world_else_else);
        assertEquals(expected_hello_join_world_else_else.v(), actual_hello_join_world_else_else.v());
        expected_hello_join_world_then_then.recursiveDeref();
        expected_hello_join_world_then_else.recursiveDeref();
        expected_hello_join_world_else_then.recursiveDeref();
        expected_hello_join_world_else_else.recursiveDeref();
        hello_join_world.recursiveDeref();
    }

    @Test
    public void testParseElement() {

        /* Assert parseElement is inverse of toString */
        XDD<String> helloWorld = ddManager.constant("Hello World!");
        String str = helloWorld.toString();
        XDD<String> helloWorldReproduced = ddManager.constant(ddManager.parseElement(str));
        assertEquals(helloWorld, helloWorldReproduced);

        /* Release memory */
        helloWorld.recursiveDeref();
        helloWorldReproduced.recursiveDeref();
    }
}
