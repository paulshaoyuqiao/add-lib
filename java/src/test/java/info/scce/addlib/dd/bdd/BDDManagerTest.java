package info.scce.addlib.dd.bdd;

import info.scce.addlib.dd.DDManager;
import info.scce.addlib.dd.DDManagerException;
import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.latticedd.example.BooleanLogicDDManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BDDManagerTest extends DDManagerTest {

    private BDDManager ddManager = null;

    @Before
    public void setUp() {
        ddManager = new BDDManager();
    }

    @After
    public void tearDown() {
        assertRefCountZeroAndQuit(ddManager);
        ddManager = null;
    }

    @Test
    public void testInterManagerOperations() {

        /* Get another BDDManager */
        BDDManager other_ddManager = new BDDManager();

        /* Get operands */
        BDD var0 = ddManager.ithVar(0);
        BDD other_var0 = other_ddManager.ithVar(0);

        /* Try forbidden operation between different BDDManagers */
        try {
            var0.or(other_var0);
            fail("Expected exception was not thrown");
        } catch (DDManagerException e) {
            assertTrue(e.getMessage().contains(DDManager.class.getSimpleName()));
            assertTrue(e.getMessage().contains(BDD.class.getSimpleName()));
            assertTrue(e.getMessage().contains("must share the same"));
        }
        var0.recursiveDeref();
        other_var0.recursiveDeref();

        /* Quit local ADDManager separately */
        assertRefCountZeroAndQuit(other_ddManager);
    }

    @Test
    public void testEval() {

        /* Get variables */
        BDD var0 = ddManager.ithVar(0);
        BDD var1 = ddManager.ithVar(1);

        /* Get some constants */
        BDD one = ddManager.readOne();
        BDD zero = ddManager.readLogicZero();

        /* Build disjunction: var0 | var1 */
        BDD var0_or_var1 = var0.or(var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Get terminal nodes */
        BDD t00 = var0_or_var1.e().e();
        BDD t01 = var0_or_var1.e().t();
        BDD t1 = var0_or_var1.t();

        /* Assert terminal nodes */
        assertEquals(zero, t00);
        assertEquals(one, t01);
        assertEquals(one, t1);
        var0_or_var1.recursiveDeref();
        one.recursiveDeref();
        zero.recursiveDeref();
    }

    @Test
    public void testOrSimilarToDocumentation() {

        /* Get the variables */
        BDD var0 = ddManager.ithVar(0);
        BDD var1 = ddManager.ithVar(1);

        /* Build the disjunction */
        BDD disjunction = var0.or(var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Evaluate disjunction for assignment var0 := 1, var1 := 0 */
        BDD terminal = disjunction.eval(true, false);

        /* See if the terminal is what we expect it to be */
        BDD one = ddManager.readOne();
        assertEquals(one, terminal);

        /* Release memory */
        disjunction.recursiveDeref();
        one.recursiveDeref();
    }

    @Test
    public void testOrAsInDocumentation() {

        /* Initialize DDManager with default values */
        BDDManager ddManager = new BDDManager();

        /* Get the variables */
        BDD var0 = ddManager.ithVar(0);
        BDD var1 = ddManager.ithVar(1);

        /* Build the disjunction */
        BDD disjunction = var0.or(var1);
        var0.recursiveDeref();
        var1.recursiveDeref();

        /* Evaluate disjunction for assignment var0 := 1, var1 := 0 */
        BDD terminal = disjunction.eval(true, false);

        /* See if the terminal is what we expect it to be */
        BDD one = ddManager.readOne();
        System.out.println("[[ terminal.equals(one) ]] = " + terminal.equals(one));

        /* Release memory */
        disjunction.recursiveDeref();
        one.recursiveDeref();
        ddManager.quit();
    }

    @Test
    public void testToXDD() {

        /* Get constants and variables */
        BDD one = ddManager.readOne();
        BDD var0 = ddManager.namedVar("a");
        BDD var1 = ddManager.namedVar("b");

        /* Build the implication */
        BDD not_var0 = var0.not();
        var0.recursiveDeref();
        BDD var0_impl_var1 = not_var0.or(var1);
        not_var0.recursiveDeref();
        var1.recursiveDeref();

        /* Convert to XDD */
        BDD expected = var0_impl_var1;
        XDD<Boolean> actual = var0_impl_var1.toXDD(new BooleanLogicDDManager());


        /* Assert equality */
        assertEquals(expected.eval(true, true).equals(one), actual.eval(true, true).v());
        assertEquals(expected.eval(true, false).equals(one), actual.eval(true, false).v());
        assertEquals(expected.eval(false, true).equals(one), actual.eval(false, true).v());
        assertEquals(expected.eval(false, false).equals(one), actual.eval(false, false).v());

        /* Release memory */
        one.recursiveDeref();
        expected.recursiveDeref();
        actual.recursiveDeref();
    }

    @Test
    public void testSecureDeref() {
        assertEquals(0,ddManager.checkZeroRef());
        BDD A = ddManager.ithVar(0);
        A.recursiveDeref();
        try {
            A.recursiveDeref();
            fail("ref count should have been zero");
        }catch (DDManagerException e){
            assertEquals(e.getMessage(),"Cannot dereference unreferenced DD");
        }
    }
}