package info.scce.addlib.serializer;

import info.scce.addlib.dd.DD;
import info.scce.addlib.dd.DDManager;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintWriter;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class DDSerializerTest {

    private DDSerializer ddSerializer;

    @Before
    public void setUp() throws Exception {
        ddSerializer = new DDSerializer() {

            @Override
            protected void serialize(PrintWriter pw, DD f) {
            }

            @Override
            protected DD deserialize(DDManager ddManager, Scanner sc) {
                return null;
            }
        };
    }

    @Test
    public void testStringEscaping() {
        char[] criticalChars = new char[]{'a', 'b', 'c', '\\', ';', '\n', '\r'};
        for (int i = 0; i < 256; i++) {
            String expected = randomString(criticalChars, 128);

            /* Escape n times */
            int n = (int) (1 + Math.random() * 3);
            String actual = expected;
            for (int j = 0; j < n; j++)
                actual = ddSerializer.escapeString(actual);

            /* Unescape */
            for (int j = 0; j < n; j++)
                actual = ddSerializer.unescapeString(actual);

            assertEquals(expected, actual);
        }
    }

    private String randomString(char[] chars, int length) {
        char[] str = new char[length];
        for (int i = 0; i < str.length; i++)
            str[i] = chars[(int) (Math.random() * chars.length)];
        return new String(str);
    }
}