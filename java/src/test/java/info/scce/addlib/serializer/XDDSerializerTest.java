package info.scce.addlib.serializer;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.grouplikedd.example.StringMonoidDDManager;
import info.scce.addlib.dd.xdd.latticedd.example.KleenePriestLogicDDManager;
import info.scce.addlib.dd.xdd.latticedd.example.KleenePriestValue;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;

import static info.scce.addlib.dd.xdd.latticedd.example.KleenePriestValue.*;
import static org.junit.Assert.assertEquals;

public class XDDSerializerTest extends DDManagerTest {

    @Rule
    public final TemporaryFolder tmpFolder = new TemporaryFolder();

    private XDDSerializer<KleenePriestValue> kleenePriestSerializer = null;
    private KleenePriestLogicDDManager kleenePriestDDManager = null;
    private XDD<KleenePriestValue> kleenePriestExample = null;

    private XDDSerializer<String> stringMonoidSerializer = null;
    private StringMonoidDDManager stringMonoidDDManager = null;
    private XDD<String> stringMonoidExample = null;

    @Before
    public void setUp() {

        /* Set up Kleene Priest logic example */
        kleenePriestSerializer = new XDDSerializer<>();
        kleenePriestDDManager = new KleenePriestLogicDDManager();
        XDD<KleenePriestValue> recent = kleenePriestDDManager.namedVar("recentlyReceived", TRUE, UNKNOWN);
        XDD<KleenePriestValue> addrBook = kleenePriestDDManager.namedVar("senderInAddressBook", TRUE, FALSE);
        kleenePriestExample = recent.and(addrBook);
        recent.recursiveDeref();
        addrBook.recursiveDeref();

        /* Set up string monoid example */
        stringMonoidSerializer = new XDDSerializer<>();
        stringMonoidDDManager = new StringMonoidDDManager();
        XDD<String> a = stringMonoidDDManager.ithVar(0, "\\;\n", "\\c");
        XDD<String> b = stringMonoidDDManager.ithVar(0, "\\;\n", "c\\c;\\\\;\\\\\\c\\;c;;c");
        stringMonoidExample = stringMonoidDDManager.ithVar(1, a, b);
        a.recursiveDeref();
        b.recursiveDeref();
    }

    @After
    public void tearDown() {

        /* Tear down Kleene Priest logic example */
        kleenePriestExample.recursiveDeref();
        assertRefCountZeroAndQuit(kleenePriestDDManager);
        kleenePriestDDManager = null;

        /* Tear down string monoid example */
        stringMonoidExample.recursiveDeref();
        assertRefCountZeroAndQuit(stringMonoidDDManager);
        stringMonoidDDManager = null;
    }

    @Test
    public void testSerializationToFile() throws IOException {

        /* Serialize DDs */
        File targetFile = tmpFolder.newFile("some.dd");
        kleenePriestSerializer.serialize(targetFile, kleenePriestExample);

        /* Reconstruct DDs */
        XDD<KleenePriestValue> reconstruction = kleenePriestSerializer.deserialize(kleenePriestDDManager, targetFile);

        /* Assert equality */
        assertEquals(kleenePriestExample, reconstruction);
        reconstruction.recursiveDeref();
    }

    @Test
    public void testSerializationToStream() throws IOException {

        /* Serialize DDs */
        PipedOutputStream out = new PipedOutputStream();
        PipedInputStream in = new PipedInputStream();
        in.connect(out);
        kleenePriestSerializer.serialize(out, kleenePriestExample);

        /* Reconstruct DDs */
        XDD<KleenePriestValue> reconstruction = kleenePriestSerializer.deserialize(kleenePriestDDManager, in);

        /* Assert equality */
        assertEquals(kleenePriestExample, reconstruction);
        reconstruction.recursiveDeref();
    }

    @Test
    public void testSerializationToString() {

        /* Serialize DDs */
        String str = stringMonoidSerializer.serialize(stringMonoidExample);

        /* Reconstruct DDs */
        XDD<String> reconstruction = stringMonoidSerializer.deserialize(stringMonoidDDManager, str);

        /* Assert equality */
        assertEquals(stringMonoidExample, reconstruction);
        reconstruction.recursiveDeref();
    }
}
