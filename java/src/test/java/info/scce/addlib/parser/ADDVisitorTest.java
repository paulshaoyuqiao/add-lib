package info.scce.addlib.parser;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.add.ADDManager;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ADDVisitorTest extends DDManagerTest {

    @Test
    public void testADDSimple() {
        ADDManager ddManager = new ADDManager();

        ADD zero = ddManager.constant(0);
        ADD parsedZero = ddManager.parse("0.0");
        assertEquals(zero, parsedZero);
        zero.recursiveDeref();
        parsedZero.recursiveDeref();

        ADD five = ddManager.constant(5.0);
        ADD parsedFive = ddManager.parse("5.0");
        assertEquals(five, parsedFive);
        parsedFive.recursiveDeref();

        ADD sevenPointSeven = ddManager.constant(7.7);
        ADD parsedSevenPointSeven = ddManager.parse("7.7");
        assertEquals(sevenPointSeven, parsedSevenPointSeven);
        parsedSevenPointSeven.recursiveDeref();

        ADD fivePlusSevenPointSeven = five.plus(sevenPointSeven);
        ADD parsedFivePlusparsedSevenPointSeven = ddManager.parse("5.0 + 7.7");
        assertEquals(fivePlusSevenPointSeven, parsedFivePlusparsedSevenPointSeven);
        parsedFivePlusparsedSevenPointSeven.recursiveDeref();
        fivePlusSevenPointSeven.recursiveDeref();

        ADD fiveMinusSevenPointSeven = five.minus(sevenPointSeven);
        ADD parsedFiveMinusSevenPointSeven = ddManager.parse("5.0 - 7.7");
        assertEquals(fiveMinusSevenPointSeven, parsedFiveMinusSevenPointSeven);
        fiveMinusSevenPointSeven.recursiveDeref();
        parsedFiveMinusSevenPointSeven.recursiveDeref();
        five.recursiveDeref();
        sevenPointSeven.recursiveDeref();

        ADD parsedFivePlusFiveTimesFive = ddManager.parse("5.0 + 5.0 * 5.0");
        assertEquals(30.0, parsedFivePlusFiveTimesFive.v(), 0);
        parsedFivePlusFiveTimesFive.recursiveDeref();

        ADD parsedFivePlusFiveTimesFivePriority = ddManager.parse("(5.0 + 5.0) * 5.0");
        assertEquals(50.0, parsedFivePlusFiveTimesFivePriority.v(), 0);
        parsedFivePlusFiveTimesFivePriority.recursiveDeref();

        ADD parsedFiveMinusTwoMinusOne = ddManager.parse("5.0 - 2.0 - 1.0");
        assertEquals(2.0, parsedFiveMinusTwoMinusOne.v(), 0);
        parsedFiveMinusTwoMinusOne.recursiveDeref();

        ADD parsedFiveMinusTwoMinusOnePriority = ddManager.parse("5.0 - (2.0 - 1.0)");
        assertEquals(4.0, parsedFiveMinusTwoMinusOnePriority.v(), 0);
        parsedFiveMinusTwoMinusOnePriority.recursiveDeref();

        /* Release memory */
        assertEquals(0, ddManager.checkZeroRef());
        ddManager.quit();
    }
}
