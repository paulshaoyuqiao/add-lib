package info.scce.addlib.parser;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.add.ADDManager;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.latticedd.example.BooleanVector;
import info.scce.addlib.dd.xdd.latticedd.example.BooleanVectorLogicDDManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class XDDVisitorTest {

    @Test
    public void testXDDSimple() {
        BooleanVectorLogicDDManager ddManager = new BooleanVectorLogicDDManager(2);
        
        XDD<BooleanVector> one = ddManager.one();
        XDD<BooleanVector> parsedOne = ddManager.parse("\"[true, true]\"");
        assertEquals(one, parsedOne);
        parsedOne.recursiveDeref();

        XDD<BooleanVector> zero = ddManager.zero();
        XDD<BooleanVector> parsedZero = ddManager.parse("'[false, false]'");
        assertEquals(zero, parsedZero);
        parsedZero.recursiveDeref();

        XDD<BooleanVector> oneOrZero = one.or(zero);
        XDD<BooleanVector> parsedOneOrZero = ddManager.parse("'[true, true]' or '[false, false]'");
        assertEquals(oneOrZero, parsedOneOrZero);
        oneOrZero.recursiveDeref();
        parsedOneOrZero.recursiveDeref();

        XDD<BooleanVector> oneAndZero = one.and(zero);
        XDD<BooleanVector> parsedOneAndZero = ddManager.parse("'[true, true]' and '[false, false]'");
        assertEquals(oneAndZero, parsedOneAndZero);
        oneAndZero.recursiveDeref();
        parsedOneAndZero.recursiveDeref();

        XDD<BooleanVector> notOne = ddManager.parse("not '[true, true]'");
        XDD<BooleanVector> ff = ddManager.constant(new BooleanVector(false, false));
        assertEquals(ff, notOne);
        notOne.recursiveDeref();
        ff.recursiveDeref();

        XDD<BooleanVector> notZero = ddManager.parse("not '[false, false]'");
        XDD<BooleanVector> tt = ddManager.constant(new BooleanVector(true, true));
        assertEquals(tt, notZero);
        notZero.recursiveDeref();
        tt.recursiveDeref();

        one.recursiveDeref();
        zero.recursiveDeref();

        /* Release memory */
        assertEquals(0, ddManager.checkZeroRef());
        ddManager.quit();
    }
}
