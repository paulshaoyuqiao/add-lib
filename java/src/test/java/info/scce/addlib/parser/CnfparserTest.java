package info.scce.addlib.parser;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.bdd.BDD;
import info.scce.addlib.dd.bdd.BDDManager;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.latticedd.example.BooleanLogicDDManager;
import info.scce.addlib.viewer.DotViewer;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CnfparserTest extends DDManagerTest {

    @Rule
    public final TemporaryFolder tmpFolder = new TemporaryFolder();

    private BDDManager ddManager;
    private File workdir;
    private File redirectedOutput;
    private File redirectedError;

    @Before
    public void setUp() {
        ddManager = new BDDManager();
        try {
            workdir = tmpFolder.newFolder("workdir_" + getClass().getSimpleName());
            redirectedOutput = tmpFolder.newFile("redirectedOutput");
            redirectedError = tmpFolder.newFile("redirectedError");
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @After
    public void tearDown() {
        /* Release memory */
        assertRefCountZeroAndQuit(ddManager);
    }

    @Test
    public void testParseCnf(){
        File cnfFile = new File(workdir,"cnfFile");
        try {
            PrintWriter printWriter = new PrintWriter(cnfFile);
            printWriter.write(" -1 2 3 0\n" +
                                "1 4 -5 0\n" +
                                "-2 -1 4 0");
            printWriter.close();
            CnfParser cnfParser = new CnfParser();
            BDD bddParsed = cnfParser.parseCnf(ddManager,cnfFile);
            BDD var0 = ddManager.ithVar(1);
            BDD var1 = ddManager.ithVar(2);
            BDD var2 = ddManager.ithVar(3);
            BDD var3 = ddManager.ithVar(4);
            BDD var4 = ddManager.ithVar(5);
            BDD notvar0 = var0.not();
            BDD notvar1 = var1.not();
            BDD notvar4 = var4.not();
            BDD int1 = notvar0.or(var1);
            BDD int2 = int1.or(var2);
            BDD int3 = var0.or(var3);
            BDD int4 = int3.or(notvar4);
            BDD int5 = notvar1.or(notvar0);
            BDD int6 = int5.or(var3);
            BDD int7 = int2.and(int4);
            BDD bdd = int6.and(int7);

            /*
            DotViewer<XDD<Boolean>> dotViewer =new DotViewer<>();
            XDD<Boolean> xdd1= bdd.toXDD(new BooleanLogicDDManager());
            XDD<Boolean> xdd2= bddParsed.toXDD(new BooleanLogicDDManager());

            dotViewer.view(xdd1,"eins");
            dotViewer.view(xdd2,"parsed");
            dotViewer.waitUntilAllClosed();
            */

            assertEquals(bdd,bddParsed);

            for(int i =0;i<32;i++){
                boolean a = String.format("%05d",Integer.parseInt(Integer.toBinaryString(i))).charAt(0) == '1';
                boolean b = String.format("%05d",Integer.parseInt(Integer.toBinaryString(i))).charAt(1) == '1';
                boolean c = String.format("%05d",Integer.parseInt(Integer.toBinaryString(i))).charAt(2) == '1';
                boolean d = String.format("%05d",Integer.parseInt(Integer.toBinaryString(i))).charAt(3) == '1';
                boolean e = String.format("%05d",Integer.parseInt(Integer.toBinaryString(i))).charAt(4) == '1';
                assertEquals(bdd.eval(a,b,c,d,e),bddParsed.eval(a,b,c,d,e));
            }
            var0.recursiveDeref();
            var1.recursiveDeref();
            var2.recursiveDeref();
            var3.recursiveDeref();
            var4.recursiveDeref();
            notvar0.recursiveDeref();
            notvar1.recursiveDeref();
            notvar4.recursiveDeref();
            int1.recursiveDeref();
            int2.recursiveDeref();
            int3.recursiveDeref();
            int4.recursiveDeref();
            int5.recursiveDeref();
            int6.recursiveDeref();
            int7.recursiveDeref();
            bdd.recursiveDeref();
            bddParsed.recursiveDeref();
        }catch (FileNotFoundException e){
            fail(e.getMessage());
        }
    }
}
