package info.scce.addlib.parser;

import info.scce.addlib.dd.bdd.BDD;
import info.scce.addlib.dd.bdd.BDDManager;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BDDVisitorTest {

    @Test
    public void testSimpleBDD() {
        BDDManager ddManager = new BDDManager();

        BDD trueBDD = ddManager.parse("1");
        BDD one = ddManager.readOne();
        assertEquals(one, trueBDD);
        trueBDD.recursiveDeref();

        BDD falseBDD = ddManager.parse("0");
        BDD zero = ddManager.readLogicZero();
        assertEquals(zero, falseBDD);
        falseBDD.recursiveDeref();

        BDD zeroAndZero = ddManager.parse("0 & 0");
        assertEquals(zero, zeroAndZero);
        BDD zeroAndOne = ddManager.parse("0 & 1");
        assertEquals(zero, zeroAndOne);
        BDD oneAndZero = ddManager.parse("1 & 0");
        assertEquals(zero, oneAndZero);
        BDD oneAndOne = ddManager.parse("1 & 1");
        assertEquals(one, oneAndOne);
        zeroAndZero.recursiveDeref();
        zeroAndOne.recursiveDeref();
        oneAndZero.recursiveDeref();
        oneAndOne.recursiveDeref();

        BDD zeroOrZero = ddManager.parse("0 | 0");
        assertEquals(zero, zeroOrZero);
        BDD zeroOrOne = ddManager.parse("0 | 1");
        assertEquals(one, zeroOrOne);
        BDD oneOrZero = ddManager.parse("1 | 0");
        assertEquals(one, oneOrZero);
        BDD oneOrOne = ddManager.parse("1 | 1");
        assertEquals(one, oneOrOne);
        zeroOrZero.recursiveDeref();
        zeroOrOne.recursiveDeref();
        oneOrZero.recursiveDeref();
        oneOrOne.recursiveDeref();

        BDD notOne = ddManager.parse("! 1");
        assertEquals(zero, notOne);
        BDD notZero = ddManager.parse("! 0 and not false");
        assertEquals(one, notZero);
        notOne.recursiveDeref();
        notZero.recursiveDeref();

        zero.recursiveDeref();
        one.recursiveDeref();

        /* Release memory */
        assertEquals(0, ddManager.checkZeroRef());
        ddManager.quit();
    }

    @Test
    public void testVariables() {
        BDDManager ddManager = new BDDManager();
        BDD one = ddManager.readOne();
        BDD zero = ddManager.readLogicZero();

        BDD parsedVar0 = ddManager.parse("a");
        BDD var0 = ddManager.ithVar(0);
        assertEquals(var0, parsedVar0);
        var0.recursiveDeref();

        BDD parsedVar1 = ddManager.parse("b");
        BDD var1 = ddManager.ithVar(1);
        assertEquals(var1, parsedVar1);
        var1.recursiveDeref();

        BDD parsedVar0OrparsedVar1 = parsedVar0.or(parsedVar1);
        BDD or00 = parsedVar0OrparsedVar1.eval(false, false);
        BDD or01 = parsedVar0OrparsedVar1.eval(false, true);
        BDD or10 = parsedVar0OrparsedVar1.eval(true, false);
        BDD or11 = parsedVar0OrparsedVar1.eval(true, true);

        assertEquals(zero, or00);
        assertEquals(one, or01);
        assertEquals(one, or10);
        assertEquals(one, or11);
        parsedVar0OrparsedVar1.recursiveDeref();

        BDD parsedVar0AndparsedVar1 = parsedVar0.and(parsedVar1);
        BDD and00 = parsedVar0AndparsedVar1.eval(false, false);
        BDD and01 = parsedVar0AndparsedVar1.eval(false, true);
        BDD and10 = parsedVar0AndparsedVar1.eval(true, false);
        BDD and11 = parsedVar0AndparsedVar1.eval(true, true);

        assertEquals(zero, and00);
        assertEquals(zero, and01);
        assertEquals(zero, and10);
        assertEquals(one, and11);
        parsedVar0AndparsedVar1.recursiveDeref();
        parsedVar0.recursiveDeref();
        parsedVar1.recursiveDeref();
        one.recursiveDeref();
        zero.recursiveDeref();

        /* Release memory */
        assertEquals(0, ddManager.checkZeroRef());
        ddManager.quit();
    }
}
