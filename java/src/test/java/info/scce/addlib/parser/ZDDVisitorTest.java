package info.scce.addlib.parser;

import info.scce.addlib.dd.zdd.ZDD;
import info.scce.addlib.dd.zdd.ZDDManager;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ZDDVisitorTest {

    @Test
    public void testZddParserAB() {
        ZDDManager zddManager = new ZDDManager();
        ZDD A = zddManager.parse("false");
        ZDD B = zddManager.parse("true");

        ZDD C = zddManager.parse("CHANGE true 0");
        ZDD C_T = C.t();
        assertEquals(B, C_T);
        ZDD C_E = C.e();
        assertEquals(A, C_E);

        ZDD D = zddManager.parse("CHANGE true 1");
        ZDD D_T = D.t();
        assertEquals(B, D_T);
        ZDD D_E = D.e();
        assertEquals(A, D_E);

        ZDD E = zddManager.parse("(CHANGE true 0)UNION(CHANGE true 1)");
        ZDD E_T = E.t();
        assertEquals(B, E_T);
        ZDD E_E = E.e();
        ZDD E_ET = E_E.t();
        assertEquals(B, E_ET);
        ZDD E_EE = E_E.e();
        assertEquals(A, E_EE);

        ZDD F = zddManager.parse("true UNION ((CHANGE true 0) UNION (CHANGE true 1))");
        ZDD F_T = F.t();
        assertEquals(B, F_T);
        ZDD F_E = F.e();
        ZDD F_ET = F_E.t();
        assertEquals(B, F_ET);
        ZDD F_EE = F_E.e();
        assertEquals(B, F_EE);

        ZDD G = zddManager.parse("(true UNION ((CHANGE true 0) UNION (CHANGE true 1))) DIFF (CHANGE true 0)");
        ZDD G_T = G.t();
        assertEquals(B, G_T);
        ZDD G_E = G.e();
        assertEquals(B, G_E);

        A.recursiveDeref();
        B.recursiveDeref();
        C.recursiveDeref();
        D.recursiveDeref();
        E.recursiveDeref();
        F.recursiveDeref();
        G.recursiveDeref();

        assertEquals(0, zddManager.checkZeroRef());
        zddManager.quit();
    }

    @Test
    public void testZDDParserZDDOne() {
        ZDDManager zddManager = new ZDDManager();
        ZDD A = zddManager.parse("false");
        ZDD B = zddManager.parse("true");
        ZDD D = zddManager.parse("CHANGE (zTrue 1) 1");
        ZDD D_T = D.t();
        assertEquals(B, D_T);
        ZDD D_E = D.e();
        assertEquals(A, D_E);
    }
}
