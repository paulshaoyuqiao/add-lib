package info.scce.addlib.cudd;

import org.junit.Test;

public class NativeLibraryLoaderTest {

    @Test
    public void testLoadAddLib() {
        NativeLibraryLoader nativeLibLoader = new NativeLibraryLoader("addlib");
        nativeLibLoader.ensureNativeLibraryLoaded();
    }
}
