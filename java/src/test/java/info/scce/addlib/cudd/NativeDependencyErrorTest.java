package info.scce.addlib.cudd;


import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class NativeDependencyErrorTest {

    @Test
    public void testOSArchMentioned() {
        NativeDependencyError e = new NativeDependencyError("Some random message");
        String osName = System.getProperty("os.name");
        assertTrue(e.toString().contains(osName));
    }

    @Test
    public void testOSNameMentioned() {
        NativeDependencyError e = new NativeDependencyError("Yet another random message");
        String osArch = System.getProperty("os.arch");
        assertTrue(e.toString().contains(osArch));
    }
}
