package info.scce.addlib.cudd;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static info.scce.addlib.cudd.Cudd.*;
import static info.scce.addlib.cudd.DD_AOP.Cudd_addPlus;
import static info.scce.addlib.cudd.DD_AOP.Cudd_addTimes;
import static info.scce.addlib.utils.Conversions.*;
import static org.junit.Assert.*;

public class CuddTest {

    private long ddManager = NULL;

    @Before
    public void setUp() {

        /* Initialize with default values */
        ddManager = Cudd_Init(0, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);
    }

    @After
    public void tearDown() {

        /* Check that no unwanted references remain */
        int expectedRefCount = 0;
        int actualRefCount = Cudd_CheckZeroRef(ddManager);
        assertEquals(expectedRefCount, actualRefCount);

        /* Release memory */
        Cudd_Quit(ddManager);
        ddManager = NULL;
    }

    @Test
    public void testIsComplement() {

        /* Assert new variable not complemented */
        long var0 = Cudd_bddIthVar(ddManager, 0);
        assertFalse(asBoolean(Cudd_IsComplement(var0)));

        /* Assert not to complement decision diagram */
        long not_var0 = Cudd_Not(var0);
        assertTrue(asBoolean(Cudd_IsComplement(not_var0)));
    }

    @Test
    public void testNot() {
        long ptr = 0;
        long actualCompl = Cudd_Not(ptr);
        long expectedCompl = 1;
        assertEquals(expectedCompl, actualCompl);
    }

    @Test
    public void testBddOr() {
        /* Get some variables */
        long var0 = Cudd_bddIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_bddIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Build disjunction: var0 | var1 */
        long var0_or_var1 = Cudd_bddOr(ddManager, var0, var1);
        Cudd_Ref(var0_or_var1);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);

        /* Get some constants */
        long zero = Cudd_ReadLogicZero(ddManager);
        Cudd_Ref(zero);
        long one = Cudd_ReadOne(ddManager);
        Cudd_Ref(one);

        /* Get disjunction terminal nodes */
        long var0_or_var1_ass00 = Cudd_Eval(ddManager, var0_or_var1, asInts(false, false));
        long var0_or_var1_ass01 = Cudd_Eval(ddManager, var0_or_var1, asInts(false, true));
        long var0_or_var1_ass10 = Cudd_Eval(ddManager, var0_or_var1, asInts(true, false));
        long var0_or_var1_ass11 = Cudd_Eval(ddManager, var0_or_var1, asInts(true, true));

        /* Assert disjunction terminal nodes */
        assertEquals(zero, var0_or_var1_ass00);
        assertEquals(one, var0_or_var1_ass01);
        assertEquals(one, var0_or_var1_ass10);
        assertEquals(one, var0_or_var1_ass11);
        Cudd_RecursiveDeref(ddManager, zero);
        Cudd_RecursiveDeref(ddManager, one);
        Cudd_RecursiveDeref(ddManager, var0_or_var1);
    }

    @Test
    public void testAddMonadicApplyThroughCallback() {

        /* Get some variables */
        long var0 = Cudd_addIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_addIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Build sum: var0 + var1 */
        long var0_plus_var1 = Cudd_addApply(ddManager, Cudd_addPlus, var0, var1);
        Cudd_Ref(var0_plus_var1);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);

        /* Increment by 5 with callback to Java */
        DD_MAOP_Fn maopInc5 = new DD_MAOP_Fn() {

            @Override
            public long apply(long ddManager, long f) {
                if (asBoolean(Cudd_IsConstant(f)))
                    return Cudd_addConst(ddManager, Cudd_V(f) + 5);
                return NULL;
            }
        };
        long var0_plus_var1_inc5 = Cudd_addMonadicApply(ddManager, maopInc5, var0_plus_var1);
        Cudd_Ref(var0_plus_var1_inc5);
        Cudd_RecursiveDeref(ddManager, var0_plus_var1);

        /* Get sum + 5 terminals nodes */
        long var0_plus_var1_inc5_ass00 = Cudd_Eval(ddManager, var0_plus_var1_inc5, asInts(false, false));
        long var0_plus_var1_inc5_ass01 = Cudd_Eval(ddManager, var0_plus_var1_inc5, asInts(false, true));
        long var0_plus_var1_inc5_ass10 = Cudd_Eval(ddManager, var0_plus_var1_inc5, asInts(true, false));
        long var0_plus_var1_inc5_ass11 = Cudd_Eval(ddManager, var0_plus_var1_inc5, asInts(true, true));

        /* Assert sum terminal nodes */
        assertEquals(5.0, Cudd_V(var0_plus_var1_inc5_ass00), 0);
        assertEquals(6.0, Cudd_V(var0_plus_var1_inc5_ass01), 0);
        assertEquals(6.0, Cudd_V(var0_plus_var1_inc5_ass10), 0);
        assertEquals(7.0, Cudd_V(var0_plus_var1_inc5_ass11), 0);
        Cudd_RecursiveDeref(ddManager, var0_plus_var1_inc5);
    }

    @Test
    public void testAddMonadicApplyThroughCallback2() {

        /* Get some variables */
        long var0 = Cudd_addIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_addIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Get some constants */
        long const5 = Cudd_addConst(ddManager, 5);
        Cudd_Ref(const5);

        /* Build sum: var0 + var1 */
        long var0_plus_var1 = Cudd_addApply(ddManager, Cudd_addPlus, var0, var1);
        Cudd_Ref(var0_plus_var1);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);
        long var0_plus_var1_plus_const5 = Cudd_addApply(ddManager, Cudd_addPlus, var0_plus_var1, const5);
        Cudd_Ref(var0_plus_var1_plus_const5);
        Cudd_RecursiveDeref(ddManager, var0_plus_var1);
        Cudd_RecursiveDeref(ddManager, const5);

        /* Build inverse of the sum */
        final Set<Long> actualInvokationsOnTerminals = new HashSet<>();
        DD_MAOP_Fn maopInv = new DD_MAOP_Fn() {

            @Override
            public long apply(long ddManager, long f) {
                if (asBoolean(Cudd_IsConstant(f))) {
                    actualInvokationsOnTerminals.add(f);
                    double inverse = 1.0 / Cudd_V(f);
                    return Cudd_addConst(ddManager, inverse);
                }
                return NULL;
            }
        };
        long inverse_of_var0_plus_var1_plus_const5 = Cudd_addMonadicApply(ddManager, maopInv, var0_plus_var1_plus_const5);
        Cudd_Ref(inverse_of_var0_plus_var1_plus_const5);

        /* Assert invocations on sum terminal nodes */
        Set<Long> expectedInvokationsOnTerminals = new HashSet<>();
        expectedInvokationsOnTerminals.add(Cudd_Eval(ddManager, var0_plus_var1_plus_const5, asInts(false, false)));
        expectedInvokationsOnTerminals.add(Cudd_Eval(ddManager, var0_plus_var1_plus_const5, asInts(false, true)));
        expectedInvokationsOnTerminals.add(Cudd_Eval(ddManager, var0_plus_var1_plus_const5, asInts(true, true)));
        assertEquals(expectedInvokationsOnTerminals, actualInvokationsOnTerminals);
        Cudd_RecursiveDeref(ddManager, var0_plus_var1_plus_const5);

        /* Get inverse of sum terminals nodes */
        long inverse_of_var0_plus_var1_plus_const5_ass00 = Cudd_Eval(ddManager, inverse_of_var0_plus_var1_plus_const5, asInts(false, false));
        long inverse_of_var0_plus_var1_plus_const5_ass01 = Cudd_Eval(ddManager, inverse_of_var0_plus_var1_plus_const5, asInts(false, true));
        long inverse_of_var0_plus_var1_plus_const5_ass10 = Cudd_Eval(ddManager, inverse_of_var0_plus_var1_plus_const5, asInts(true, false));
        long inverse_of_var0_plus_var1_plus_const5_ass11 = Cudd_Eval(ddManager, inverse_of_var0_plus_var1_plus_const5, asInts(true, true));

        /* Assert inverse of sum terminal nodes */
        assertEquals(1.0 / 5.0, Cudd_V(inverse_of_var0_plus_var1_plus_const5_ass00), 0.0);
        assertEquals(1.0 / 6.0, Cudd_V(inverse_of_var0_plus_var1_plus_const5_ass01), 0.0);
        assertEquals(1.0 / 6.0, Cudd_V(inverse_of_var0_plus_var1_plus_const5_ass10), 0.0);
        assertEquals(1.0 / 7.0, Cudd_V(inverse_of_var0_plus_var1_plus_const5_ass11), 0.0);
        Cudd_RecursiveDeref(ddManager, inverse_of_var0_plus_var1_plus_const5);
    }

    @Test
    public void testAddApplyThroughCallback() {

        /* Get some variables */
        long var0 = Cudd_addIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_addIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Build sum + 5: var0 + var1 + 5 */
        final Set<Long> actualInvokationFingerprints = new HashSet<>();
        DD_AOP_Fn aopPlusPlus5 = new DD_AOP_Fn() {

            @Override
            public long apply(long ddManager, long f, long g) {
                if (asBoolean(Cudd_IsConstant(f)) && asBoolean(Cudd_IsConstant(g))) {
                    long invokationFingerprint = f + g;
                    actualInvokationFingerprints.add(invokationFingerprint);
                    double sumPlus5 = Cudd_V(f) + Cudd_V(g) + 5;
                    return Cudd_addConst(ddManager, sumPlus5);
                }
                return NULL;
            }
        };
        long var0_plus_var1_plus_5 = Cudd_addApply(ddManager, aopPlusPlus5, var0, var1);
        Cudd_Ref(var0_plus_var1_plus_5);

        /* Assert invocations on var0 respectively var1 terminals */
        Set<Long> expectedInvokationFingerprints = new HashSet<>();
        expectedInvokationFingerprints.add(Cudd_E(var0) + Cudd_E(var1));
        expectedInvokationFingerprints.add(Cudd_E(var0) + Cudd_T(var1));
        expectedInvokationFingerprints.add(Cudd_T(var0) + Cudd_E(var1));
        expectedInvokationFingerprints.add(Cudd_T(var0) + Cudd_T(var1));
        assertEquals(expectedInvokationFingerprints, actualInvokationFingerprints);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);

        /* Get sum + 5 terminals nodes */
        long var0_plus_var1_plus_5_ass00 = Cudd_Eval(ddManager, var0_plus_var1_plus_5, asInts(false, false));
        long var0_plus_var1_plus_5_ass01 = Cudd_Eval(ddManager, var0_plus_var1_plus_5, asInts(false, true));
        long var0_plus_var1_plus_5_ass10 = Cudd_Eval(ddManager, var0_plus_var1_plus_5, asInts(true, false));
        long var0_plus_var1_plus_5_ass11 = Cudd_Eval(ddManager, var0_plus_var1_plus_5, asInts(true, true));

        /* Assert inverse of sum terminal nodes */
        assertEquals(5.0, Cudd_V(var0_plus_var1_plus_5_ass00), 0.0);
        assertEquals(6.0, Cudd_V(var0_plus_var1_plus_5_ass01), 0.0);
        assertEquals(6.0, Cudd_V(var0_plus_var1_plus_5_ass10), 0.0);
        assertEquals(7.0, Cudd_V(var0_plus_var1_plus_5_ass11), 0.0);
        Cudd_RecursiveDeref(ddManager, var0_plus_var1_plus_5);
    }

    @Test
    public void testAddApply() {

        /* Get some variables */
        long var0 = Cudd_addIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_addIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Get some constants */
        long const5 = Cudd_addConst(ddManager, 5.67);
        Cudd_Ref(const5);
        long const1 = Cudd_ReadOne(ddManager);
        Cudd_Ref(const1);
        long const0 = Cudd_ReadZero(ddManager);
        Cudd_Ref(const0);

        /* Build var1 * const5 */
        long var1_times_const5 = Cudd_addApply(ddManager, Cudd_addTimes, var1, const5);
        Cudd_Ref(var1_times_const5);
        Cudd_RecursiveDeref(ddManager, var1);
        Cudd_RecursiveDeref(ddManager, const5);

        /* Build var1 * const5 + var0 */
        long var1_times_const5_plus_var0 = Cudd_addApply(ddManager, Cudd_addPlus, var1_times_const5, var0);
        Cudd_Ref(var1_times_const5_plus_var0);
        Cudd_RecursiveDeref(ddManager, var1_times_const5);
        Cudd_RecursiveDeref(ddManager, var0);

        /* Build var1 * const5 + var0 + const1 */
        long var1_times_const5_plus_var0_plus_const1 = Cudd_addApply(ddManager, Cudd_addPlus, var1_times_const5_plus_var0, const1);
        Cudd_Ref(var1_times_const5_plus_var0_plus_const1);
        Cudd_RecursiveDeref(ddManager, var1_times_const5_plus_var0);
        Cudd_RecursiveDeref(ddManager, const1);

        /* Get terminals nodes */
        long var1_times_const5_plus_var0_plus_const1_ass00 = Cudd_Eval(ddManager, var1_times_const5_plus_var0_plus_const1, asInts(false, false));
        long var1_times_const5_plus_var0_plus_const1_ass01 = Cudd_Eval(ddManager, var1_times_const5_plus_var0_plus_const1, asInts(false, true));
        long var1_times_const5_plus_var0_plus_const1_ass10 = Cudd_Eval(ddManager, var1_times_const5_plus_var0_plus_const1, asInts(true, false));
        long var1_times_const5_plus_var0_plus_const1_ass11 = Cudd_Eval(ddManager, var1_times_const5_plus_var0_plus_const1, asInts(true, true));

        /* Assert terminal nodes */
        assertEquals(1.0, Cudd_V(var1_times_const5_plus_var0_plus_const1_ass00), 0.0);
        assertEquals(6.67, Cudd_V(var1_times_const5_plus_var0_plus_const1_ass01), 0.0);
        assertEquals(2.0, Cudd_V(var1_times_const5_plus_var0_plus_const1_ass10), 0.0);
        assertEquals(7.67, Cudd_V(var1_times_const5_plus_var0_plus_const1_ass11), 0.0);

        /* Build ... * const0 */
        long whatever_times_const0 = Cudd_addApply(ddManager, Cudd_addTimes, const0, var1_times_const5_plus_var0_plus_const1);
        Cudd_Ref(whatever_times_const0);
        Cudd_RecursiveDeref(ddManager, var1_times_const5_plus_var0_plus_const1);

        /* Assert zero */
        assertEquals(whatever_times_const0, const0);
        Cudd_RecursiveDeref(ddManager, const0);
        Cudd_RecursiveDeref(ddManager, whatever_times_const0);
    }

    @Test
    public void testBddCompose() {

        /* Get some variables */
        long var0 = Cudd_bddIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_bddIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Get some constants */
        long one = Cudd_ReadOne(ddManager);
        Cudd_Ref(one);

        /* Build disjunction: var0 | var1 */
        long var0_or_var1 = Cudd_bddOr(ddManager, var0, var1);
        Cudd_Ref(var0_or_var1);

        /* Build composition with disjunction */
        long composition_one_or_var1 = Cudd_bddCompose(ddManager, var0_or_var1, one, 0);
        Cudd_Ref(composition_one_or_var1);
        Cudd_RecursiveDeref(ddManager, var0_or_var1);

        /* Assert composition with disjunction */
        assertEquals(one, composition_one_or_var1);
        Cudd_RecursiveDeref(ddManager, composition_one_or_var1);

        /* Build conjunction: var0 & var1 */
        long var0_and_var1 = Cudd_bddAnd(ddManager, var0, var1);
        Cudd_Ref(var0_and_var1);

        /* Build composition with conjunction */
        long composition_var0_and_one = Cudd_bddCompose(ddManager, var0_and_var1, one, 1);
        Cudd_Ref(composition_var0_and_one);
        Cudd_RecursiveDeref(ddManager, var0_and_var1);
        Cudd_RecursiveDeref(ddManager, one);

        /* Assert composition with conjunction */
        assertEquals(var0, composition_var0_and_one);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);
        Cudd_RecursiveDeref(ddManager, composition_var0_and_one);
    }

    @Test
    public void testBddVectorCompose() {

        /* Get some variables */
        long var0 = Cudd_bddIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_bddIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Build var0 == var1 */
        long var0_xnor_var1 = Cudd_bddXnor(ddManager, var0, var1);
        Cudd_Ref(var0_xnor_var1);

        /* Build conjunction: var0 & var1 */
        long var0_and_var1 = Cudd_bddAnd(ddManager, var0, var1);
        Cudd_Ref(var0_and_var1);

        /* Build disjunction: var0 | var1 */
        long var0_or_var1 = Cudd_bddOr(ddManager, var0, var1);
        Cudd_Ref(var0_or_var1);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);

        /* Assert vector composition */
        long[] vector = new long[]{var0_and_var1, var0_or_var1};
        long actualComposition = Cudd_bddVectorCompose(ddManager, var0_xnor_var1, vector);
        Cudd_Ref(actualComposition);
        long expectedComposition = var0_xnor_var1;
        assertEquals(expectedComposition, actualComposition);
        Cudd_RecursiveDeref(ddManager, actualComposition);
        Cudd_RecursiveDeref(ddManager, var0_and_var1);
        Cudd_RecursiveDeref(ddManager, var0_or_var1);
        Cudd_RecursiveDeref(ddManager, var0_xnor_var1);
    }

    @Test
    public void testBddVectorCompose2() {

        /* Get some variables */
        long var0 = Cudd_bddIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_bddIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Build conjunction: var0 & var1 */
        long var0_and_var1 = Cudd_bddAnd(ddManager, var0, var1);
        Cudd_Ref(var0_and_var1);

        /* Build disjunction: var0 | var1 */
        long var0_or_var1 = Cudd_bddOr(ddManager, var0, var1);
        Cudd_Ref(var0_or_var1);

        /* Assert composition */
        long expectedComposition = var0_and_var1;
        long[] vector = new long[]{var0_and_var1, var0_or_var1};
        long actualComposition = Cudd_bddVectorCompose(ddManager, var0, vector);
        Cudd_Ref(actualComposition);
        assertEquals(expectedComposition, actualComposition);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);
        Cudd_RecursiveDeref(ddManager, var0_and_var1);
        Cudd_RecursiveDeref(ddManager, var0_or_var1);
        Cudd_RecursiveDeref(ddManager, actualComposition);
    }

    @Test
    public void testEval() {

        /* Get some variables */
        long var0 = Cudd_bddIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_bddIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Get some constants */
        long zero = Cudd_ReadLogicZero(ddManager);
        Cudd_Ref(zero);
        long one = Cudd_ReadOne(ddManager);
        Cudd_Ref(one);

        /* Build conjunction: var0 & var1 */
        long var0_and_var1 = Cudd_bddAnd(ddManager, var0, var1);
        Cudd_Ref(var0_and_var1);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);

        /* Assert evaluation */
        assertEquals(zero, Cudd_Eval(ddManager, var0_and_var1, asInts(false, false)));
        assertEquals(zero, Cudd_Eval(ddManager, var0_and_var1, asInts(false, true)));
        assertEquals(zero, Cudd_Eval(ddManager, var0_and_var1, asInts(true, false)));
        assertEquals(one, Cudd_Eval(ddManager, var0_and_var1, asInts(true, true)));
        Cudd_RecursiveDeref(ddManager, zero);
        Cudd_RecursiveDeref(ddManager, one);
        Cudd_RecursiveDeref(ddManager, var0_and_var1);
    }

    @Test
    public void testAddIthVar() {

        /* Get some variables */
        long var0 = Cudd_bddIthVar(ddManager, 0);
        Cudd_Ref(var0);

        /* Get some constants */
        long zero = Cudd_ReadLogicZero(ddManager);
        long one = Cudd_ReadOne(ddManager);
        Cudd_Ref(zero);
        Cudd_Ref(one);

        /* Assert variable */
        assertEquals(one, Cudd_T(var0));
        assertEquals(zero, Cudd_E(var0));
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, one);
        Cudd_RecursiveDeref(ddManager, zero);
    }

    @Test
    public void testBddOrSimilarToDocumentation() {

        /* Get the variables */
        long var0 = Cudd_bddIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_bddIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Build the disjunction */
        long disjunction = Cudd_bddOr(ddManager, var0, var1);
        Cudd_Ref(disjunction);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);

        /* Evaluate disjunction for assignment var0 := 1, var1 := 0 */
        int[] assignment = {1, 0};
        long terminal = Cudd_Eval(ddManager, disjunction, assignment);

        /* See if the terminal is what we expect it to be */
        long one = Cudd_ReadOne(ddManager);
        Cudd_Ref(one);
        assertEquals(terminal, one);

        /* Release memory */
        Cudd_RecursiveDeref(ddManager, one);
        Cudd_RecursiveDeref(ddManager, disjunction);
    }

    @Test
    public void testBddOrAsInDocumentation() {

        /* Initialize DDManager with default values */
        long ddManager = Cudd_Init(0, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);

        /* Get the variables */
        long var0 = Cudd_bddIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_bddIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Build the disjunction */
        long disjunction = Cudd_bddOr(ddManager, var0, var1);
        Cudd_Ref(disjunction);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);

        /* Evaluate disjunction for assignment var0 := 1, var1 := 0 */
        int[] assignment = {1, 0};
        long terminal = Cudd_Eval(ddManager, disjunction, assignment);

        /* See if the terminal is what we expect it to be */
        long one = Cudd_ReadOne(ddManager);
        Cudd_Ref(one);
        System.out.println("[[ terminal == one ]] = " + (terminal == one));
        assertEquals(terminal, one);

        /* Release memory */
        Cudd_RecursiveDeref(ddManager, one);
        Cudd_RecursiveDeref(ddManager, disjunction);
        Cudd_Quit(ddManager);
    }

    @Test
    public void testAddTimesSimilarToDocumentation() {

        /* Get a constant */
        long f = Cudd_addConst(ddManager, 5);
        Cudd_Ref(f);

        /* Multiply constant with some variables */
        for (int i = 3; i >= 0; i--) {

            /* Get the variable */
            long var = Cudd_addIthVar(ddManager, i);
            Cudd_Ref(var);

            /* Multiply the variable to the product */
            long tmp = Cudd_addApply(ddManager, Cudd_addTimes, var, f);
            Cudd_Ref(tmp);
            Cudd_RecursiveDeref(ddManager, f);
            Cudd_RecursiveDeref(ddManager, var);
            f = tmp;
        }

        /* Release memory */
        Cudd_RecursiveDeref(ddManager, f);
    }

    @Test
    public void testAddTimesAsInDocumentation() {

        /* Initialize DDManager with default values */
        long ddManager = Cudd_Init(0, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);

        /* Get a constant */
        long f = Cudd_addConst(ddManager, 5);
        Cudd_Ref(f);

        /* Multiply constant with some variables */
        for (int i = 3; i >= 0; i--) {

            /* Get the variable */
            long var = Cudd_addIthVar(ddManager, i);
            Cudd_Ref(var);

            /* Multiply the variable to the product */
            long tmp = Cudd_addApply(ddManager, Cudd_addTimes, var, f);
            Cudd_Ref(tmp);
            Cudd_RecursiveDeref(ddManager, f);
            Cudd_RecursiveDeref(ddManager, var);
            f = tmp;
        }

        /* Release memory */
        Cudd_RecursiveDeref(ddManager, f);
        Cudd_Quit(ddManager);
    }

    @Test
    public void testAddApplySimilarToDocumentation() {

        /* Get some variables */
        long var0 = Cudd_addIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_addIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Build sum8: var0 + var1 + 8 */
        long sum8 = Cudd_addApply(ddManager, new DD_AOP_Fn() {

            @Override
            public long apply(long ddManager, long f, long g) {
                if (asBoolean(Cudd_IsConstant(f)) && asBoolean(Cudd_IsConstant(g))) {
                    double value = Cudd_V(f) + Cudd_V(g) + 8;
                    return Cudd_addConst(ddManager, value);
                }
                return NULL;
            }
        }, var0, var1);
        Cudd_Ref(sum8);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);

        /* Assert values */
        long terminal8 = Cudd_E(Cudd_E(sum8));
        assertEquals(8.0, Cudd_V(terminal8), 0.0);
        long terminal10 = Cudd_T(Cudd_T(sum8));
        assertEquals(10.0, Cudd_V(terminal10), 0.0);

        /* Release memory */
        Cudd_RecursiveDeref(ddManager, sum8);
    }

    @Test
    public void testAddApplyAsInDocumentation() {

        /* Initialize DDManager with default values */
        long ddManager = Cudd_Init(0, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);

        /* Get some variables */
        long var0 = Cudd_addIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_addIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Build sum8: var0 + var1 + 8 */
        long sum8 = Cudd_addApply(ddManager, new DD_AOP_Fn() {

            @Override
            public long apply(long ddManager, long f, long g) {
                if (Cudd_IsConstant(f) > 0 && Cudd_IsConstant(g) > 0) {
                    double value = Cudd_V(f) + Cudd_V(g) + 8;
                    return Cudd_addConst(ddManager, value);
                }
                return NULL;
            }
        }, var0, var1);
        Cudd_Ref(sum8);
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);

        /* ... */

        /* Release memory */
        Cudd_RecursiveDeref(ddManager, sum8);
        Cudd_Quit(ddManager);
    }

    @Test
    public void testNodeReadIndex() {

        /* Get some node */
        long var0 = Cudd_addIthVar(ddManager, 0);
        Cudd_Ref(var0);

        /* Assert node index */
        int expected = 0;
        int actual = Cudd_NodeReadIndex(var0);
        assertEquals(expected, actual);
        Cudd_RecursiveDeref(ddManager, var0);
    }

    @Test
    public void testApplyRtException() {

        /* Get some variables */
        long var0 = Cudd_addIthVar(ddManager, 0);
        Cudd_Ref(var0);
        long var1 = Cudd_addIthVar(ddManager, 1);
        Cudd_Ref(var1);

        /* Catch runtime exception without crashing the VM */
        try {
            Cudd_addApply(ddManager, new DD_AOP_Fn() {

                @Override
                public long apply(long ddManager, long f, long g) {
                    throw new RuntimeException("some runtime exception");
                }
            }, var0, var1);
            fail("Expected RuntimeException was not thrown");
        } catch (RuntimeException e) {
            assertEquals("some runtime exception", e.getMessage());
        }
        Cudd_RecursiveDeref(ddManager, var0);
        Cudd_RecursiveDeref(ddManager, var1);
    }

    @Test
    public void testMonadicApplyRtException() {

        /* Get some variables */
        long var0 = Cudd_addIthVar(ddManager, 0);
        Cudd_Ref(var0);

        /* Catch runtime exception without crashing the VM */
        try {
            Cudd_addMonadicApply(ddManager, new DD_MAOP_Fn() {

                @Override
                public long apply(long ddManager, long f) {
                    throw new RuntimeException("some runtime exception");
                }
            }, var0);
            fail("Expected RuntimeException was not thrown");
        } catch (RuntimeException e) {
            assertEquals("some runtime exception", e.getMessage());
        }
        Cudd_RecursiveDeref(ddManager, var0);
    }

    @Test
    public void testZddSimple() {
        /* Variables are named as in Figure 8: "Supression of irrelevant variables"
         * and the same operations are executed and tested
         * https://en.wikipedia.org/wiki/Zero-suppressed_decision_diagram */

        long zddManager = Cudd_Init(0, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);
        long A = Cudd_ReadZero(zddManager);
        Cudd_Ref(A);
        long B = Cudd_ReadZddOne(zddManager, 0);
        Cudd_Ref(B);

        long C = Cudd_zddChange(zddManager, B, 0);
        Cudd_Ref(C);
        long C_T = Cudd_T(C);
        Cudd_Ref(C_T);
        assertEquals(B, C_T);
        Cudd_RecursiveDerefZdd(zddManager, C_T);
        long C_E = Cudd_E(C);
        Cudd_Ref(C_E);
        assertEquals(A, C_E);
        Cudd_RecursiveDerefZdd(zddManager, C_E);

        long D = Cudd_zddChange(zddManager, B, 1);
        Cudd_Ref(D);
        long D_T = Cudd_T(D);
        Cudd_Ref(D_T);
        assertEquals(B, D_T);
        Cudd_RecursiveDerefZdd(zddManager, D_T);
        long D_E = Cudd_E(D);
        Cudd_Ref(D_E);
        assertEquals(A, D_E);
        Cudd_RecursiveDerefZdd(zddManager, D_E);

        /* x_1 is root instead of x_2(as in wikipedia figure)
         * because nodes are ordered in increasing order by default */
        long E = Cudd_zddUnion(zddManager, C, D);
        Cudd_Ref(E);
        long E_T = Cudd_T(E);
        Cudd_Ref(E_T);
        assertEquals(B, E_T);
        Cudd_RecursiveDerefZdd(zddManager, E_T);
        long E_E = Cudd_E(E);
        Cudd_Ref(E_E);
        long E_ET = Cudd_T(E_E);
        Cudd_Ref(E_ET);
        assertEquals(B, E_ET);
        Cudd_RecursiveDerefZdd(zddManager, E_ET);
        long E_EE = Cudd_E(E_E);
        Cudd_Ref(E_EE);
        assertEquals(A, E_EE);
        Cudd_RecursiveDerefZdd(zddManager, E_EE);
        Cudd_RecursiveDerefZdd(zddManager, E_E);

        long F = Cudd_zddUnion(zddManager, B, E);
        Cudd_Ref(F);
        long F_T = Cudd_T(F);
        Cudd_Ref(F_T);
        assertEquals(B, F_T);
        Cudd_RecursiveDerefZdd(zddManager, F_T);
        long F_E = Cudd_E(F);
        Cudd_Ref(F_E);
        long F_ET = Cudd_T(F_E);
        Cudd_Ref(F_ET);
        assertEquals(B, F_ET);
        Cudd_RecursiveDerefZdd(zddManager, F_ET);
        long F_EE = Cudd_E(F_E);
        Cudd_Ref(F_EE);
        assertEquals(B, F_EE);
        Cudd_RecursiveDerefZdd(zddManager, F_EE);
        Cudd_RecursiveDerefZdd(zddManager, F_E);

        long G = Cudd_zddDiff(zddManager, F, C);
        Cudd_Ref(G);
        long G_T = Cudd_T(G);
        Cudd_Ref(G_T);
        assertEquals(B, G_T);
        Cudd_RecursiveDerefZdd(zddManager, G_T);
        long G_E = Cudd_T(G);
        Cudd_Ref(G_E);
        assertEquals(B, G_E);
        Cudd_RecursiveDerefZdd(zddManager, G_E);

        Cudd_RecursiveDerefZdd(zddManager, A);
        Cudd_RecursiveDerefZdd(zddManager, B);
        Cudd_RecursiveDerefZdd(zddManager, C);
        Cudd_RecursiveDerefZdd(zddManager, D);
        Cudd_RecursiveDerefZdd(zddManager, E);
        Cudd_RecursiveDerefZdd(zddManager, F);
        Cudd_RecursiveDerefZdd(zddManager, G);

        assertEquals(0, Cudd_CheckZeroRef(zddManager));
        Cudd_Quit(zddManager);
    }
}


