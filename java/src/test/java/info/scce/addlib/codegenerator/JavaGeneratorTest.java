package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.grouplikedd.example.StringMonoidDDManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class JavaGeneratorTest extends CodeGeneratorTest {

    @Test
    public void testGeneratedJava() throws IOException, InterruptedException {

        /* Generate code */
        String className = "SumOfPredicatesABC";
        File genFile = new File(workdir, className + ".java");
        JavaGenerator<ADD> generator = new JavaGenerator<ADD>().withClassName(className).withStaticMethodsEnabled();
        generator.generateToFileSystem(genFile, sumOfPredicatesABC);

        /* Copy test program */
        File testProgramFile = new File(workdir, "TestProgram.java");
        File predicatesFile = new File(workdir, "Predicates.java");
        URL testProgramResource = getClass().getClassLoader().getResource("TestProgram.java");
        if (testProgramResource != null)
            FileUtils.copyURLToFile(testProgramResource, testProgramFile);
        URL predicatesResource = getClass().getClassLoader().getResource("Predicates.java");
        if (predicatesResource != null)
            FileUtils.copyURLToFile(predicatesResource, predicatesFile);

        /* Compile test program */
        File testProgramClasses = new File(workdir, "classes");
        testProgramClasses.mkdir();
        assertSuccessfulExecution("javac", genFile.getAbsolutePath(), testProgramFile.getAbsolutePath(),
                predicatesFile.getAbsolutePath(), "-d", testProgramClasses.getAbsolutePath());

        /* Execute test program */
        assertSuccessfulExecution("java", "-cp", testProgramClasses.getAbsolutePath(), "TestProgram");
    }

    @Test
    public void testExactCodeGenerationExampleForDocumentation() {

        /* Label the decision diagram's root to identify its function in the generated code. This name is used to
         * invoke the function later. */
        LabelledRegularDD<XDD<String>> root = new LabelledRegularDD<>(helloWorld, "helloWorld");

        /* Use the Java code generator */
        JavaGenerator<XDD<String>> g = new JavaGenerator<XDD<String>>().withClassName("HelloWorld");
        g.generateToStdOut(root);
    }

    @Test
    public void testResultOfCodeGenerationExampleForDocumentation() throws IOException {

        /* Code from above generating the Java implementation to a string */
        LabelledRegularDD<XDD<String>> root = new LabelledRegularDD<>(helloWorld, "helloWorld");
        JavaGenerator<XDD<String>> g = new JavaGenerator<XDD<String>>().withClassName("HelloWorld");
        String actualJavaCode = g.generateToString(root);

        /* Get expected Java code */
        ClassLoader cl = getClass().getClassLoader();
        InputStream expectedJavaCodeIStream = cl.getResourceAsStream("HelloWorld.java");
        String expectedJavaCode = IOUtils.toString(expectedJavaCodeIStream, Charset.defaultCharset());

        /* Assert equality except for varying method names */
        assertEqualsIgnorePlatformSpecificLineSeparator(withoutConcreteIds(expectedJavaCode), withoutConcreteIds(actualJavaCode));
    }

}
