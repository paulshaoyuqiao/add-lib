package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.grouplikedd.example.StringMonoidDDManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class CGeneratorTest extends CodeGeneratorTest {

    @Test
    public void testGeneratedC() throws IOException, InterruptedException {

        /* Generate code */
        String className = "SumOfPredicatesABC";
        File genFile = new File(workdir, className + ".h");
        CGenerator<ADD> generator = new CGenerator<>();
        generator.generateToFileSystem(genFile, sumOfPredicatesABC);

        /* Copy test program */
        File testProgramFile = new File(workdir, "TestProgram.c");
        File predicatesFile = new File(workdir, "Predicates.h");
        URL testProgramResource = getClass().getClassLoader().getResource("TestProgram.c");
        if (testProgramResource != null)
            FileUtils.copyURLToFile(testProgramResource, testProgramFile);
        URL predicatesResource = getClass().getClassLoader().getResource("Predicates.h");
        if (predicatesResource != null)
            FileUtils.copyURLToFile(predicatesResource, predicatesFile);

        /* Compile test program */
        File testProgramClass = new File(workdir, "test.out");
        assertSuccessfulExecution("gcc", testProgramFile.getAbsolutePath(), "-o", testProgramClass.getAbsolutePath());

        /* Execute test program */
        assertSuccessfulExecution(testProgramClass.getAbsolutePath());
    }

    @Test
    public void testResultOfCodeGenerationExampleForDocumentation() throws IOException {

        /* Generate the C implementation to a string */
        LabelledRegularDD<XDD<String>> root = new LabelledRegularDD<>(helloWorld, "helloWorld");
        CGenerator<XDD<String>> g = new CGenerator<XDD<String>>();
        String actualCCode = g.generateToString(root);

        /* Get expected C code */
        ClassLoader cl = getClass().getClassLoader();
        InputStream expectedCCodeIStream = cl.getResourceAsStream("HelloWorld.c");
        String expectedCCode = IOUtils.toString(expectedCCodeIStream, Charset.defaultCharset());

        /* Assert equality except for varying method names */
        assertEqualsIgnorePlatformSpecificLineSeparator(withoutConcreteIds(expectedCCode), withoutConcreteIds(actualCCode));

    }

}
