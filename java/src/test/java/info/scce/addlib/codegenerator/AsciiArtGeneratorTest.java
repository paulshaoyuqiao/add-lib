package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.add.ADD;
import org.junit.Test;

import java.io.IOException;

public class AsciiArtGeneratorTest extends CodeGeneratorTest {

    @Test
    public void testExamples() {
        AsciiArtGenerator<ADD> generator = new AsciiArtGenerator<>();
        generator.generateToStdOut(sumOfPredicatesABC);
        generator.generateToStdOut(xorAB);
    }
}
