package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.grouplikedd.example.StringMonoidDDManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class CSharpGeneratorTest extends CodeGeneratorTest {

    private static final String EVAL_ID_REGEX = "eval[0-9]*";

    @Test
    public void testGeneratedCSharp() throws IOException, InterruptedException {

        /* Generate code */
        String className = "SumOfPredicatesABC";
        File genFile = new File(workdir, className + ".cs");
        CSharpGenerator<ADD> generator = new CSharpGenerator<ADD>().withClassName(className).withNamespace("TestCSharp").withStaticMethodsEnabled();
        generator.generateToFileSystem(genFile, sumOfPredicatesABC);

        /* Copy test program */
        File testProgramFile = new File(workdir, "TestProgram.cs");
        File predicatesFile = new File(workdir, "Predicates.cs");
        URL testProgramResource = getClass().getClassLoader().getResource("TestProgram.cs");
        if (testProgramResource != null)
            FileUtils.copyURLToFile(testProgramResource, testProgramFile);
        URL predicatesResource = getClass().getClassLoader().getResource("Predicates.cs");
        if (predicatesResource != null)
            FileUtils.copyURLToFile(predicatesResource, predicatesFile);

        /* Compile test program */
        File testProgramClass = new File(workdir, "TestProgram.exe");
        assertSuccessfulExecution("mcs", testProgramFile.getAbsolutePath(), predicatesFile.getAbsolutePath(), genFile.getAbsolutePath());

        /* Execute test program */
        assertSuccessfulExecution("mono", testProgramClass.getAbsolutePath());
    }

    @Test
    public void testResultOfCodeGenerationExampleForDocumentation() throws IOException {

        /* Generate the CSharp implementation to a string */
        LabelledRegularDD<XDD<String>> root = new LabelledRegularDD<>(helloWorld, "helloWorld");
        CSharpGenerator<XDD<String>> g = new CSharpGenerator<XDD<String>>().withClassName("HelloWorld").withNamespace("TestCSharp");
        String actualCSharpCode = g.generateToString(root);

        /* Get expected CSharp code */
        ClassLoader cl = getClass().getClassLoader();
        InputStream expectedCSharpCodeIStream = cl.getResourceAsStream("HelloWorld.cs");
        String expectedCSharpCode = IOUtils.toString(expectedCSharpCodeIStream, Charset.defaultCharset());

        /* Assert equality except for varying method names */
        assertEqualsIgnorePlatformSpecificLineSeparator(withoutConcreteIds(expectedCSharpCode), withoutConcreteIds(actualCSharpCode));
    }

}
