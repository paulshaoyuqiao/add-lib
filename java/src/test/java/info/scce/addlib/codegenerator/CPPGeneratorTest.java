package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.xdd.XDD;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class CPPGeneratorTest extends CodeGeneratorTest {

    @Test
    public void testGeneratedCPlusPlus() throws IOException, InterruptedException {

        /* Generate code */
        String className = "SumOfPredicatesABC";
        File genFile = new File(workdir, className + ".h");
        CPPGenerator<ADD> generator = new CPPGenerator<ADD>().withClassName(className).withStaticMethodsEnabled();
        generator.generateToFileSystem(genFile, sumOfPredicatesABC);

        /* Copy test program */
        File testProgramFile = new File(workdir, "TestProgram.cpp");
        File predicatesFile = new File(workdir, "PredicatesPlusPlus.h");
        URL testProgramResource = getClass().getClassLoader().getResource("TestProgram.cpp");
        if (testProgramResource != null)
            FileUtils.copyURLToFile(testProgramResource, testProgramFile);
        URL predicatesResource = getClass().getClassLoader().getResource("PredicatesPlusPlus.h");
        if (predicatesResource != null)
            FileUtils.copyURLToFile(predicatesResource, predicatesFile);

        /* Compile test program */
        File testProgramClass = new File(workdir, "test.out");
        assertSuccessfulExecution("g++", testProgramFile.getAbsolutePath(), "-o", testProgramClass.getAbsolutePath());

        /* Execute test program */
        assertSuccessfulExecution(testProgramClass.getAbsolutePath());
    }

    @Test
    public void testResultOfCodeGenerationExampleForDocumentation() throws IOException {

        /* Generate the CPlusPlus implementation to a string */
        LabelledRegularDD<XDD<String>> root = new LabelledRegularDD<>(helloWorld, "helloWorld");
        CPPGenerator<XDD<String>> g = new CPPGenerator<XDD<String>>().withClassName("HelloWorld");
        String actualCPlusPlusCode = g.generateToString(root);

        /* Get expected CPlusPlus code */
        ClassLoader cl = getClass().getClassLoader();
        InputStream expectedCPlusPlusCodeIStream = cl.getResourceAsStream("HelloWorld.cpp");
        String expectedCPlusPlusCode = IOUtils.toString(expectedCPlusPlusCodeIStream, Charset.defaultCharset());

        /* Assert equality except for varying method names */
        assertEqualsIgnorePlatformSpecificLineSeparator(withoutConcreteIds(expectedCPlusPlusCode), withoutConcreteIds(actualCPlusPlusCode));
    }

}
