package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.add.ADDManager;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.grouplikedd.example.StringMonoidDDManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CodeGeneratorTest extends DDManagerTest {

    private static final String NL_REGEX = "\\r\\n?|\\n";
    private static final String EVAL_ID_REGEX = "eval[0-9]*";

    @Rule
    public final TemporaryFolder tmpFolder = new TemporaryFolder();

    protected ADDManager ddManager = null;
    protected File workdir;
    protected File redirectedOutput;
    protected File redirectedError;
    protected LabelledRegularDD<ADD> sumOfPredicatesABC;
    protected LabelledRegularDD<ADD> xorAB;
    protected XDD<String> helloWorld;
    private StringMonoidDDManager stringMonoidDDManager;

    @Before
    public void setUp() {
        /* Create managers */
        ddManager = new ADDManager();
        stringMonoidDDManager = new StringMonoidDDManager();

        /* Create test DDs*/
        sumOfPredicatesABC = sumOfPredicatesABC();
        helloWorld = helloWorld();
        xorAB = xorAB();
        try {
            workdir = tmpFolder.newFolder("workdir_" + getClass().getSimpleName());
            redirectedOutput = tmpFolder.newFile("redirectedOutput");
            redirectedError = tmpFolder.newFile("redirectedError");
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @After
    public void tearDown() {

        /* Release test ADDs */
        sumOfPredicatesABC.dd().recursiveDeref();
        sumOfPredicatesABC = null;
        helloWorld.recursiveDeref();
        helloWorld = null;
        xorAB.dd().recursiveDeref();
        xorAB = null;

        /* Release memory */
        assertRefCountZeroAndQuit(ddManager);
        assertRefCountZeroAndQuit(stringMonoidDDManager);
    }

    private LabelledRegularDD<ADD> sumOfPredicatesABC() {

        /* Get variables */
        ADD predA = ddManager.namedVar("predA");
        ADD predB = ddManager.namedVar("predB");
        ADD predC = ddManager.namedVar("predC");

        /* a + b */
        ADD sumAB = predA.plus(predB);
        predA.recursiveDeref();
        predB.recursiveDeref();

        /* a + b + c */
        ADD sumABC = sumAB.plus(predC);
        sumAB.recursiveDeref();
        predC.recursiveDeref();

        /* Give it a name */
        return new LabelledRegularDD<>(sumABC, "sumOfPredicatesABC");
    }

    private XDD<String> helloWorld() {

        /* Create variables */
        XDD<String> hello = stringMonoidDDManager.namedVar("sayHello", "Hello ", "***** ");
        XDD<String> world = stringMonoidDDManager.namedVar("sayWorld", "World!", "*****!");

        /* Create HelloWorld-XDD */
        helloWorld = hello.join(world);
        hello.recursiveDeref();
        world.recursiveDeref();

        return helloWorld;
    }

    private LabelledRegularDD<ADD> xorAB() {

        /* Get variables */
        ADD x0 = ddManager.namedVar("SomeLongVariableName");
        ADD x1 = ddManager.namedVar("SomeName");

        /* Build exclusive disjunction */
        ADD xor = x0.xor(x1);
        x0.recursiveDeref();
        x1.recursiveDeref();

        /* Give it a name */
        return new LabelledRegularDD<>(xor, "sumOfPredicatesABC");
    }

    /* Additional assertions */

    protected void assertSuccessfulExecution(String... command) throws IOException, InterruptedException {
        Process p = new ProcessBuilder()
                .command(command)
                .redirectOutput(redirectedOutput).redirectError(redirectedError)
                .start();
        int expectedExitCode = 0;
        int actualExitCode = p.waitFor();
        assertEquals(expectedExitCode, actualExitCode);
    }

    protected void assertEqualsIgnorePlatformSpecificLineSeparator(String expected, String actual) {
        String expectedWithSystemLineSeparator= expected.replaceAll(NL_REGEX, System.lineSeparator());
        String actualWithSystemLineSeparator = actual.replaceAll(NL_REGEX, System.lineSeparator());
        assertEquals(expectedWithSystemLineSeparator, actualWithSystemLineSeparator);
    }

    protected String withoutConcreteIds(String code) {
        return code.replaceAll(EVAL_ID_REGEX, "eval********");
    }
}
