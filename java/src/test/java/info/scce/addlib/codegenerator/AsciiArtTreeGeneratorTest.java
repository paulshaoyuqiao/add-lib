package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.add.ADD;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.fail;

public class AsciiArtTreeGeneratorTest extends CodeGeneratorTest {

    @Test
    public void testExamples() {
        AsciiArtTreeGenerator<ADD> generator = new AsciiArtTreeGenerator<>();
        generator.generateToStdOut(sumOfPredicatesABC);
        generator.generateToStdOut(xorAB);
    }
}
