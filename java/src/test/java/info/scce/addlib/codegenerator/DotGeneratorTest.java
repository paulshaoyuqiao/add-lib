package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.add.ADD;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class DotGeneratorTest extends CodeGeneratorTest {

    @Test
    public void testGeneratedDotSyntax() throws IOException, InterruptedException {

        /* Generate code */
        String graphName = "SumOfPredicatesABC";
        File dotFile = new File(workdir, graphName + ".dot");
        DotGenerator<ADD> generator = new DotGenerator<ADD>().withGraphName(graphName);
        generator.generateToStdOut(sumOfPredicatesABC);
        generator.generateToFileSystem(dotFile, sumOfPredicatesABC);

        /* Compile test program */
        File pngFile = new File(workdir, graphName + ".png");
        assertSuccessfulExecution("dot", dotFile.getAbsolutePath(), "-Tsvg");
        assertSuccessfulExecution("dot", dotFile.getAbsolutePath(), "-Tpng", "-o", pngFile.getAbsolutePath());
    }
}
