package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.grouplikedd.example.StringMonoidDDManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PythonGeneratorTest extends CodeGeneratorTest {

    @Test
    public void testGeneratedPython() throws IOException, InterruptedException {

        /* Generate code */
        String className = "SumOfPredicatesABC";
        File genFile = new File(workdir, className + ".py");
        PythonGenerator<ADD> generator = new PythonGenerator<ADD>().withClassName(className);
        generator.generateToFileSystem(genFile, sumOfPredicatesABC);

        /* Copy test program */
        File testProgramFile = new File(workdir, "TestProgram.py");
        File predicatesFile = new File(workdir, "Predicates.py");
        URL testProgramResource = getClass().getClassLoader().getResource("TestProgram.py");
        if (testProgramResource != null)
            FileUtils.copyURLToFile(testProgramResource, testProgramFile);
        URL predicatesResource = getClass().getClassLoader().getResource("Predicates.py");
        if (predicatesResource != null)
            FileUtils.copyURLToFile(predicatesResource, predicatesFile);

        /* Execute test program */
        assertSuccessfulExecution("python", testProgramFile.getAbsolutePath());
    }

    @Test
    public void testResultOfCodeGenerationExampleForDocumentation() throws IOException {

        /* Generate the Python implementation to a string */
        LabelledRegularDD<XDD<String>> root = new LabelledRegularDD<>(helloWorld, "helloWorld");
        PythonGenerator<XDD<String>> g = new PythonGenerator<XDD<String>>().withClassName("HelloWorld");
        String actualPythonCode = g.generateToString(root);
        
        /* Get expected Python code */
        ClassLoader cl = getClass().getClassLoader();
        InputStream expectedPythonCodeIStream = cl.getResourceAsStream("HelloWorld.py");
        String expectedPythonCode = IOUtils.toString(expectedPythonCodeIStream, Charset.defaultCharset());

        /* Assert equality except for varying method names */
        assertEqualsIgnorePlatformSpecificLineSeparator(withoutConcreteIds(expectedPythonCode), withoutConcreteIds(actualPythonCode));
    }

}
