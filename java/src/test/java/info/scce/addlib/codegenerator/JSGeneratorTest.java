package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.xdd.XDD;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class JSGeneratorTest extends CodeGeneratorTest {

    @Test
    public void testGeneratedJavaScript() throws IOException, InterruptedException {

        /* Generate code */
        String className = "SumOfPredicatesABC";
        File genFile = new File(workdir, className + ".js");
        JSGenerator<ADD> generator = new JSGenerator<ADD>().withClassName(className).withStaticMethodsEnabled();
        generator.generateToFileSystem(genFile, sumOfPredicatesABC);

        /* Copy test program */
        File testProgramFile = new File(workdir, "TestProgram.js");
        File predicatesFile = new File(workdir, "Predicates.js");
        URL testProgramResource = getClass().getClassLoader().getResource("TestProgram.js");
        if (testProgramResource != null)
            FileUtils.copyURLToFile(testProgramResource, testProgramFile);
        URL predicatesResource = getClass().getClassLoader().getResource("Predicates.js");
        if (predicatesResource != null)
            FileUtils.copyURLToFile(predicatesResource, predicatesFile);

        /* Execute test program */
        assertSuccessfulExecution("node", testProgramFile.getAbsolutePath());
    }

    @Test
    public void testResultOfCodeGenerationExampleForDocumentation() throws IOException {

        /* Generate the JavaScript implementation to a string */
        LabelledRegularDD<XDD<String>> root = new LabelledRegularDD<>(helloWorld, "helloWorld");
        JSGenerator<XDD<String>> g = new JSGenerator<XDD<String>>().withClassName("HelloWorld");
        String actualJavaScriptCode = g.generateToString(root);

        /* Get expected JavaScript code */
        ClassLoader cl = getClass().getClassLoader();
        InputStream expectedJavaScriptCodeIStream = cl.getResourceAsStream("HelloWorld.js");
        String expectedJavaScriptCode = IOUtils.toString(expectedJavaScriptCodeIStream, Charset.defaultCharset());

        /* Assert equality except for varying method names */
        assertEqualsIgnorePlatformSpecificLineSeparator(withoutConcreteIds(expectedJavaScriptCode), withoutConcreteIds(actualJavaScriptCode));
    }

}
