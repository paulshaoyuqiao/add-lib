const Predicates = require("./Predicates.js");
const SumOfPredicatesABC = require("./SumOfPredicatesABC.js");
let test = new Predicates(predA, predB, predC);
var result = SumOfPredicatesABC.sumOfPredicatesABC(test);
if (result === "2.0")
	process.exit(0);
else
	process.exit(1);

function predA() {
	return true;
}

function predB() {
	return false;
}

function predC() {
	return true;
}
