module.exports = class Predicates {
	constructor(predA, predB, predC) {
		this._predA = predA();
		this._predB = predB();
		this._predC = predC();
	}
	predA(){return this._predA}
	predB(){return this._predB}
	predC(){return this._predC}
}
