package info.scce.addlib.serializer;

import info.scce.addlib.dd.DD;
import info.scce.addlib.dd.DDManager;

import java.io.*;
import java.util.Scanner;

public abstract class DDSerializer<M extends DDManager<D>, D extends DD<M, D>> {

    /* Serialisation */

    public String serialize(D f) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        serialize(pw, f);
        return sw.toString();
    }

    public void serialize(File file, D f) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(file);
        serialize(pw, f);
        pw.close();
    }

    public void serialize(OutputStream out, D f) {
        PrintWriter pw = new PrintWriter(out, true);
        serialize(pw, f);
    }

    protected abstract void serialize(PrintWriter pw, D f);

    /* Deserialisation */

    public D deserialize(M ddManager, String str) {
        StringReader sr = new StringReader(str);
        Scanner sc = new Scanner(sr);
        return deserialize(ddManager, sc);
    }

    public D deserialize(M ddManager, File file) throws FileNotFoundException {
        Scanner sc = new Scanner(file);
        D f = deserialize(ddManager, sc);
        sc.close();
        return f;
    }

    public D deserialize(M ddManager, InputStream in) {
        Scanner sc = new Scanner(in);
        return deserialize(ddManager, sc);
    }

    protected abstract D deserialize(M ddManager, Scanner sc);

    /* String escaping */

    protected String escapeString(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            /* Replace NL, CR, colon, and backslash with escaped two-char sequence */
            if (c == '\n') sb.append("\\n");
            else if (c == '\r') sb.append("\\r");
            else if (c == ';') sb.append("\\c");
            else if (c == '\\') sb.append("\\b");
            else sb.append(c);
        }
        return sb.toString();
    }

    protected String unescapeString(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == '\\') {

                /* Escaped sequence detected: next char determines NL, CR, colon, or backslash */
                char c2 = str.charAt(++i);
                if (c2 == 'n') sb.append('\n');
                if (c2 == 'r') sb.append('\r');
                if (c2 == 'c') sb.append(';');
                if (c2 == 'b') sb.append('\\');

            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
