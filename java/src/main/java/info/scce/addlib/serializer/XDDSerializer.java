package info.scce.addlib.serializer;

import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.XDDManager;
import info.scce.addlib.traverser.PostorderTraverser;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static info.scce.addlib.dd.DDManager.RESERVED_VAR_NAMES_REGEX;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;

public class XDDSerializer<E> extends DDSerializer<XDDManager<E>, XDD<E>> {

    @Override
    protected void serialize(PrintWriter pw, XDD<E> f) {
        for (XDD<E> g : new PostorderTraverser<XDD<E>>(f)) {
            if (g.isConstant()) {
                pw.print(g.ptr() + ";");
                pw.print("constant" + ";");
                pw.print(escapeString(g.v().toString()) + ";");
                pw.println();
            } else {
                pw.print(g.ptr() + ";");
                pw.print("non-constant" + ";");
                pw.print(escapeString(g.readName()) + ";");
                pw.print(g.t().ptr() + ";");
                pw.print(g.e().ptr() + ";");
                pw.println();
            }
        }
        pw.println(f.ptr() + ";" + "root" + ";");
        pw.flush();
    }

    @Override
    protected XDD<E> deserialize(XDDManager<E> ddManager, Scanner sc) {

        /* Build intermediate results */
        Map<Long, XDD<E>> cache = new HashMap<>();
        XDD<E> result = null;

        while (result == null && sc.hasNextLine()) {

            /* Parse common part of the line */
            String line = sc.nextLine();
            String[] parts = line.split(";");
            long id = parseLong(parts[0]);
            String type = parts[1];

            /* Treat cases separately */
            if (type.equals("root")) {

                /* This is the root. We're done. */
                result = cache.get(id);
                result.ref();

            } else if (type.equals("constant")) {

                /* Constant decision diagram defined by its value */
                String strValue = unescapeString(parts[2]);
                E value = ddManager.parseElement(strValue);
                XDD<E> f = ddManager.constant(value);
                cache.put(id, f);

            } else if (type.equals("non-constant")) {

                /* Non-constant decision diagram defined by its variable name and children */
                String varName = unescapeString(parts[2]);
                long idThen = parseLong(parts[3]);
                long idElse = parseLong(parts[4]);

                /* Find children in cache (They have appeared earlier) */
                XDD<E> t = cache.get(idThen);
                XDD<E> e = cache.get(idElse);

                if (varName.matches(RESERVED_VAR_NAMES_REGEX)) {
                    String strIdx = varName.replaceAll("[^0-9]", "");
                    int i = parseInt(strIdx);
                    XDD<E> f = ddManager.ithVar(i, t, e);
                    cache.put(id, f);
                } else {
                    XDD<E> f = ddManager.namedVar(varName, t, e);
                    cache.put(id, f);
                }
            }
        }

        /* Release intermediate results */
        for (XDD<E> f : cache.values())
            f.recursiveDeref();

        return result;
    }
}
