package info.scce.addlib.dd.xdd.grouplikedd;

public abstract class MonoidDDManager<T> extends MagmaDDManager<T> {

    public MonoidDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public MonoidDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public MonoidDDManager() {
        super();
    }

    @Override
    protected abstract T neutralElement();
}
