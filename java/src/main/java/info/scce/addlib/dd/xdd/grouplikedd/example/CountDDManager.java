package info.scce.addlib.dd.xdd.grouplikedd.example;

import info.scce.addlib.dd.xdd.grouplikedd.MonoidDDManager;

public class CountDDManager extends MonoidDDManager<Integer> {

    public CountDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public CountDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public CountDDManager() {
        super();
    }

    @Override
    protected Integer neutralElement() {
        return 0;
    }

    @Override
    protected Integer join(Integer left, Integer right) {
        return left + right;
    }

    @Override
    public Integer parseElement(String str) {
        return Integer.parseInt(str);
    }
}
