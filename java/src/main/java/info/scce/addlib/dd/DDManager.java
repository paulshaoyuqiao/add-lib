package info.scce.addlib.dd;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.HashMap;
import java.util.Map;

import static info.scce.addlib.cudd.Cudd.*;
import static info.scce.addlib.utils.Conversions.asBoolean;

public abstract class DDManager<D extends DD<?, ?>> {

    public static final String RESERVED_VAR_NAMES_REGEX = "x[0-9]+";
    public static final String VAR_NAMES_REGEX = "[a-zA-Z][a-zA-Z0-9_]*";

    protected final long ptr;
    private final BiMap<String, Integer> knownVarNames;
    private Map<Long, Integer> refCounts;

    public DDManager(long ptr) {
        this.ptr = ptr;
        this.knownVarNames = HashBiMap.create();
        this.refCounts = new HashMap<>();
    }

    public DDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        this(Cudd_Init(numVars, numVarsZ, numSlots, cacheSize, maxMemory));
    }

    public DDManager(int numVars, int numVarsZ, long maxMemory) {
        this(numVars, numVarsZ, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, maxMemory);
    }

    public DDManager() {
        this(0, 0, 0);
    }

    public long ptr() {
        return ptr;
    }

    public void quit() {
        Cudd_Quit(ptr);
    }

    public boolean reduceHeap(DDReorderingType heuristic, int minsize) {
        return asBoolean(Cudd_ReduceHeap(ptr, heuristic.cuddReorderingType(), minsize));
    }

    public int checkZeroRef() {
        return Cudd_CheckZeroRef(ptr);
    }

    public void incRefCount(long ptr) {
        if (refCounts.containsKey(ptr))
            refCounts.put(ptr, refCounts.get(ptr) + 1);
        else
            refCounts.put(ptr, 1);
    }

    public void decRefCount(long ptr) {
        if (refCounts.containsKey(ptr)) {
            if (refCounts.get(ptr) > 1)
                refCounts.put(ptr, refCounts.get(ptr) - 1);
            else
                refCounts.remove(ptr);
        } else {
            throw new DDManagerException("Cannot dereference unreferenced DD");
        }
    }

    public int readPerm(int i) {
        return Cudd_ReadPerm(ptr, i);
    }

    /* Variable name mapping */

    public int varIdx(String varName) {
        if (varName.matches(RESERVED_VAR_NAMES_REGEX)) {
            String idxStr = varName.substring(1);
            return Integer.parseInt(idxStr);
        } else if (varName.matches(VAR_NAMES_REGEX)) {
            if (!knownVarNames.containsKey(varName))
                knownVarNames.put(varName, knownVarNames.size());
            return knownVarNames.get(varName);
        } else {
            throw new DDManagerException("Variable names must be of the form " + VAR_NAMES_REGEX + " but '" + varName +
                    "' is not");
        }
    }

    public String varName(int varIdx) {
        if (!knownVarNames.containsValue(varIdx))
            knownVarNames.put("x" + varIdx, varIdx);
        return knownVarNames.inverse().get(varIdx);
    }

    /* Required DDManager methods */

    public abstract D namedVar(String name);

    public abstract D ithVar(int i);

    public abstract D parse(String str);
}
