package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.xdd.latticedd.CompleteLatticeDDManager;

public class MultisetDDManager<E> extends CompleteLatticeDDManager<Multiset<E>> {

    public MultisetDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public MultisetDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public MultisetDDManager() {
        super();
    }

    @Override
    protected Multiset<E> meet(Multiset<E> left, Multiset<E> right) {
        return left.intersect(right);
    }

    @Override
    protected Multiset<E> join(Multiset<E> left, Multiset<E> right) {
        return left.union(right);
    }

    @Override
    protected Multiset<E> intersect(Multiset<E> left, Multiset<E> right) {
        return meet(left, right);
    }

    @Override
    protected Multiset<E> union(Multiset<E> left, Multiset<E> right) {
        return join(left, right);
    }

    @Override
    public Multiset<E> parseElement(String str) {
        return Multiset.parseMultiset(str, this::parseMultisetElement);
    }

    protected E parseMultisetElement(String str) {
        throw undefinedInAlgebraicStructureException("parseMultisetElement");
    }
}
