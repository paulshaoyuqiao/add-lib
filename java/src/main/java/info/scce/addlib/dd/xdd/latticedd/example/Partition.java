package info.scce.addlib.dd.xdd.latticedd.example;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Partition<T> {

    private final HashSet<HashSet<T>> blocks;

    public Partition() {
        blocks = new HashSet<>();
    }

    public Partition(T... block0) {
        blocks = new HashSet<>();
        blocks.add(new HashSet<>(Arrays.asList(block0)));
    }

    private Partition(HashSet<HashSet<T>> blocks) {
        this.blocks = blocks;
    }

    public static <T> Partition<T> emptyPartition() {
        return new Partition<>();
    }

    public int size() {
        return blocks.size();
    }

    public HashSet<HashSet<T>> blocks() {
        return blocks;
    }

    public Partition<T> meet(Partition<T> other) {
        HashSet<HashSet<T>> meetBlocks = new HashSet<>();
        LinkedList<HashSet<T>> q0 = new LinkedList<>(blocks);
        LinkedList<HashSet<T>> q1 = new LinkedList<>(other.blocks);

        /* For every block in q0 we find the blocks in q1 to intersect them */
        while (!q0.isEmpty()) {
            HashSet<T> b0 = new HashSet<>(q0.poll());
            ListIterator<HashSet<T>> it = q1.listIterator();
            while (it.hasNext()) {
                HashSet<T> b1 = new HashSet<>(it.next());
                if (b0.equals(b1)) {
                    it.remove();
                } else if (intersectionNotEmpty(b0, b1)) {
                    HashSet<T> intersection = intersect(b0, b1);
                    b0.removeAll(intersection);
                    b1.removeAll(intersection);
                    it.remove();
                    if (!b1.isEmpty())
                        it.add(b1);
                    it.add(intersection);
                }
            }
            if (!b0.isEmpty())
                meetBlocks.add(b0);
        }

        /* Remaining blocks in q1 share no elements with processed blocks */
        meetBlocks.addAll(q1);

        return new Partition<>(meetBlocks);
    }

    public static <T> Partition<T> parsePartition(String str, Function<String, T> parseElement) {

        /* Special case: empty partition */
        if (str.equals("[]"))
            return new Partition<>();

        /* Strip outer brackets */
        String strCommaSeparatedBlocks = str.substring(2, str.length() - 2);

        /* Strip inner brackets */
        String[] strBlocks = strCommaSeparatedBlocks.split("\\], \\[");

        HashSet<HashSet<T>> blocks = new HashSet<>();
        for (int i = 0; i < strBlocks.length; i++) {
            String strCommaSeparatedElems = strBlocks[i];
            String[] strElems = strCommaSeparatedElems.split(", ");
            HashSet<T> b = new HashSet<>();
            for (int j = 0; j < strElems.length; j++) {
                T elem = parseElement.apply(strElems[j]);
                b.add(elem);
            }
            blocks.add(b);
        }
        return new Partition<>(blocks);
    }

    public Partition<T> join(Partition<T> other) {
        HashSet<HashSet<T>> joinBlocks = new HashSet<>();
        LinkedList<HashSet<T>> q0 = new LinkedList<>(blocks);
        LinkedList<HashSet<T>> q1 = new LinkedList<>(other.blocks);

        /* For every block in q0 we find the blocks in q1 to union them */
        while (!q0.isEmpty()) {
            HashSet<T> b0 = new HashSet<>(q0.poll());
            ListIterator<HashSet<T>> it = q1.listIterator();
            while (it.hasNext()) {
                HashSet<T> b1 = it.next();
                if (b0.equals(b1)) {
                    it.remove();
                } else if (intersectionNotEmpty(b0, b1)) {
                    b0.addAll(b1);
                    it.remove();
                }
            }
            joinBlocks.add(b0);
        }

        /* Remaining blocks in q1 share no elements with processed blocks */
        joinBlocks.addAll(q1);

        return new Partition<>(joinBlocks);
    }

    private boolean intersectionNotEmpty(HashSet<T> a, HashSet<T> b) {
        for (T x : a) {
            if (b.contains(x))
                return true;
        }
        return false;
    }

    private HashSet<T> intersect(HashSet<T> a, HashSet<T> b) {
        HashSet<T> s = new HashSet<>(a);
        s.retainAll(b);
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Partition<?> partition = (Partition<?>) o;
        return Objects.equals(blocks, partition.blocks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(blocks);
    }

    @Override
    public String toString() {
        return "[" + blocks.stream().map(Object::toString).collect(Collectors.joining(", ")) + "]";
    }
}
