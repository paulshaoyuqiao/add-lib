package info.scce.addlib.dd;

public class DDManagerException extends RuntimeException {

    private static final long serialVersionUID = 7607716244282000402L;

    public DDManagerException(String message) {
        super(message);
    }
}
