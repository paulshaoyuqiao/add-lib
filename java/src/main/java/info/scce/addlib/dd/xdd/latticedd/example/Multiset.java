package info.scce.addlib.dd.xdd.latticedd.example;

import java.util.HashMap;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class Multiset<E> {

    private HashMap<E, Integer> multiplicity;

    public Multiset(E... elements) {
        multiplicity = new HashMap<>();
        for (E e : elements) {
            if (multiplicity.containsKey(e))
                multiplicity.put(e, multiplicity.get(e) + 1);
            else
                multiplicity.put(e, 1);
        }
    }

    private Multiset(HashMap<E, Integer> multiplicity) {
        this.multiplicity = multiplicity;
    }

    public static <E> Multiset<E> emptySet() {
        return new Multiset<E>(new HashMap<E, Integer>());
    }

    public static <E> Multiset<E> parseMultiset(String str, Function<String, E> parseElement) {

        /* Strip brackets */
        String commaSeparatedMults = str.substring(1, str.length() - 1);

        String[] strMults = commaSeparatedMults.split(", ");
        HashMap<E, Integer> multiplicity = new HashMap<E, Integer>();
        for (int i = 0; i < strMults.length; i++) {
            if (!strMults[i].isEmpty()) {
                String[] strMultPair = strMults[i].split(": ");
                E parsed = parseElement.apply(strMultPair[0]);
                int n = Integer.parseInt(strMultPair[1]);
                multiplicity.put(parsed, n);
            }
        }
        return new Multiset<>(multiplicity);
    }

    public HashMap<E, Integer> multiplicity() {
        return multiplicity;
    }

    public Multiset<E> intersect(Multiset<E> other) {
        HashMap<E, Integer> intersection = new HashMap<>();
        for (E e : multiplicity.keySet()) {
            if (other.multiplicity.containsKey(e))
                intersection.put(e, min(multiplicity.get(e), other.multiplicity.get(e)));
        }
        return new Multiset<E>(intersection);
    }

    public Multiset<E> union(Multiset<E> other) {
        HashMap<E, Integer> union = new HashMap<>();
        for (E e : multiplicity.keySet()) {
            if (other.multiplicity.containsKey(e))
                union.put(e, max(multiplicity.get(e), other.multiplicity.get(e)));
            else
                union.put(e, multiplicity.get(e));
        }
        for (E e : other.multiplicity.keySet()) {
            if (!multiplicity.containsKey(e))
                union.put(e, other.multiplicity.get(e));
        }
        return new Multiset<E>(union);
    }

    public boolean includes(Multiset<E> other) {
        for (E e : multiplicity.keySet()) {
            if (other.multiplicity.containsKey(e) && other.multiplicity.get(e) > multiplicity.get(e))
                return false;
        }
        for (E e : other.multiplicity.keySet()) {
            if (!multiplicity.containsKey(e))
                return false;
        }
        return true;
    }

    public Multiset<E> plus(Multiset<E> other) {
        HashMap<E, Integer> sum = new HashMap<>();
        for (E e : multiplicity.keySet()) {
            if (other.multiplicity.containsKey(e))
                sum.put(e, multiplicity.get(e) + other.multiplicity.get(e));
            else
                sum.put(e, multiplicity.get(e));
        }
        for (E e : other.multiplicity.keySet()) {
            if (!multiplicity.containsKey(e))
                sum.put(e, other.multiplicity.get(e));
        }
        return new Multiset<E>(sum);
    }

    public int size() {
        int size = 0;
        for (int n : multiplicity.values())
            size += n;
        return size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Multiset<?> multiset = (Multiset<?>) o;
        return Objects.equals(multiplicity, multiset.multiplicity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(multiplicity);
    }

    @Override
    public String toString() {
        String strCommaSeparated = multiplicity.entrySet().stream()
                .map(e -> e.getKey() + ": " + e.getValue()).collect(Collectors.joining(", "));
        return "[" + strCommaSeparated + "]";
    }
}
