package info.scce.addlib.dd.xdd.grouplikedd;

import info.scce.addlib.dd.xdd.XDDManager;

public abstract class MagmaDDManager<T> extends XDDManager<T> {

    public MagmaDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public MagmaDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public MagmaDDManager() {
        super();
    }

    @Override
    protected abstract T join(T left, T right);
}
