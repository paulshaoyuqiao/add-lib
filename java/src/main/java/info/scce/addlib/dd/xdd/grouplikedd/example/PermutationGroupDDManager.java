package info.scce.addlib.dd.xdd.grouplikedd.example;

import info.scce.addlib.dd.xdd.grouplikedd.GroupDDManager;

public class PermutationGroupDDManager extends GroupDDManager<Permutation> {

    private final int n;

    public PermutationGroupDDManager(int n, int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
        this.n = n;
    }

    public PermutationGroupDDManager(int n, int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
        this.n = n;
    }

    public PermutationGroupDDManager(int n) {
        super();
        this.n = n;
    }

    @Override
    protected Permutation inverse(Permutation x) {
        return x.inverse();
    }

    @Override
    protected Permutation neutralElement() {
        return Permutation.identity(n);
    }

    @Override
    protected Permutation join(Permutation left, Permutation right) {
        return left.compose(right);
    }

    @Override
    public Permutation parseElement(String str) {
        return Permutation.parsePermutation(str);
    }
}
