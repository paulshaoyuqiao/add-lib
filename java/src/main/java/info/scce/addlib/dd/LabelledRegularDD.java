package info.scce.addlib.dd;

public class LabelledRegularDD<D extends RegularDD<?, D>> {

    public static final String LABEL_REGEX = "[a-zA-Z][a-zA-Z0-9_]*";

    private D dd;
    private String label;

    public LabelledRegularDD(D regularDD, String label) {
        if (!label.matches(LABEL_REGEX))
            throw new DDManagerException("Labels must be of the form " + LABEL_REGEX + " but '" + label + "' is not");
        this.dd = regularDD;
        this.label = label;
    }

    public D dd() {
        return dd;
    }

    public String label() {
        return label;
    }

    /* Other methods */

    @Override
    public boolean equals(Object otherObj) {
        if (otherObj instanceof LabelledRegularDD<?>) {
            LabelledRegularDD<?> other = (LabelledRegularDD<?>) otherObj;
            return dd.equals(other.dd) && label.equals(other.label);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return dd.hashCode() + label.hashCode();
    }

    @Override
    public String toString() {
        return label;
    }
}
