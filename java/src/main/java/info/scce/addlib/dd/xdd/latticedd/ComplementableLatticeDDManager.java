package info.scce.addlib.dd.xdd.latticedd;

public abstract class ComplementableLatticeDDManager<T> extends BoundedLatticeDDManager<T> {

    public ComplementableLatticeDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public ComplementableLatticeDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public ComplementableLatticeDDManager() {
        super();
    }

    @Override
    protected abstract T compl(T x);
}
