package info.scce.addlib.dd.zdd;

import info.scce.addlib.dd.DD;

import static info.scce.addlib.cudd.Cudd.*;
import static info.scce.addlib.utils.Conversions.asInts;

public class ZDD extends DD<ZDDManager, ZDD> {

    public ZDD(long ptr, ZDDManager manager) {
        super(ptr, manager);
    }

    /* Required DD methods */

    @Override
    protected ZDD thisCasted() {
        return this;
    }

	@Override
	public void recursiveDeref() {
        /* Throws an exception if ptr is unreferenced */
        this.ddManager.decRefCount(ptr);

        Cudd_RecursiveDerefZdd(ddManager.ptr(), ptr);
	}
	
    @Override
    public ZDD t() {
        assertNonConstant();
        long resultPtr = Cudd_T(ptr);
        return new ZDD(resultPtr, ddManager);
    }

    @Override
    public ZDD e() {
        assertNonConstant();
        long resultPtr = Cudd_E(ptr);
        return new ZDD(resultPtr, ddManager);
    }

    @Override
    public ZDD eval(boolean... input) {
        long resultPtr = Cudd_Eval(ddManager.ptr(), ptr, asInts(input));
        return new ZDD(resultPtr, ddManager);
    }

    /* Other methods */

    public ZDD union(ZDD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_zddUnion(ddManager.ptr(), ptr, g.ptr);
        return new ZDD(resultPtr, ddManager).withRef();
    }

    public ZDD intersect(ZDD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_zddIntersect(ddManager.ptr(), ptr, g.ptr);
        return new ZDD(resultPtr, ddManager).withRef();
    }

    public ZDD diff(ZDD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_zddDiff(ddManager.ptr(), ptr, g.ptr);
        return new ZDD(resultPtr, ddManager).withRef();
    }

    public ZDD change(int var) {
        long resultPtr = Cudd_zddChange(ddManager.ptr(), ptr, var);
        return new ZDD(resultPtr, ddManager).withRef();
    }

    public ZDD subset0(int var) {
        long resultPtr = Cudd_zddSubset0(ddManager.ptr(), ptr, var);
        return new ZDD(resultPtr, ddManager).withRef();
    }

    public ZDD subset1(int var) {
        long resultPtr = Cudd_zddSubset1(ddManager.ptr(), ptr, var);
        return new ZDD(resultPtr, ddManager).withRef();
    }

    @Override
    public String toString() {
        if (isConstant()) {
            ZDD one = ddManager.readOne();
            String lbl = this.equals(one) ? "1" : "0";
            one.recursiveDeref();
            return lbl;
        }
        return super.toString();
    }
}
