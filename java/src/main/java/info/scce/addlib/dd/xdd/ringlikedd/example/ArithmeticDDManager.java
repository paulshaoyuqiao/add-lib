package info.scce.addlib.dd.xdd.ringlikedd.example;

import info.scce.addlib.dd.xdd.ringlikedd.FieldDDManager;

import static java.lang.Double.parseDouble;
import static java.lang.Math.max;
import static java.lang.Math.min;


public class ArithmeticDDManager extends FieldDDManager<Double> {

    public ArithmeticDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public ArithmeticDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public ArithmeticDDManager() {
        super();
    }

    @Override
    protected Double zeroElement() {
        return 0.0;
    }

    @Override
    protected Double oneElement() {
        return 1.0;
    }

    @Override
    protected Double add(Double a, Double b) {
        return a + b;
    }

    @Override
    protected Double addInverse(Double a) {
        return -a;
    }

    @Override
    protected Double mult(Double a, Double b) {
        return a * b;
    }

    @Override
    protected Double multInverse(Double a) {
        return 1.0 / a;
    }

    @Override
    protected Double inf(Double a, Double b) {
        return min(a, b);
    }

    @Override
    protected Double sup(Double a, Double b) {
        return max(a, b);
    }

    @Override
    public Double parseElement(String input) {
        return parseDouble(input);
    }
}
