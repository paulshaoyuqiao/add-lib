package info.scce.addlib.dd.xdd;

import info.scce.addlib.cudd.DD_AOP_Fn;
import info.scce.addlib.cudd.DD_MAOP_Fn;
import info.scce.addlib.dd.RegularDD;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;

import static info.scce.addlib.cudd.Cudd.*;
import static info.scce.addlib.utils.Conversions.NULL;
import static info.scce.addlib.utils.Conversions.asBoolean;

public class XDD<E> extends RegularDD<XDDManager<E>, XDD<E>> {

    public XDD(long ptr, XDDManager<E> ddManager) {
        super(ptr, ddManager);
    }

    /* Value mapping */

    public E v() {
        assertConstant();
        double valueId = Cudd_V(ptr);
        return ddManager.v(valueId);
    }

    /* Operations */

    public XDD<E> apply(final BinaryOperator<E> op, XDD<E> g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_addApply(ddManager.ptr(), new DD_AOP_Fn() {

            public long apply(long ddManager, long f, long g) {
                if (asBoolean(Cudd_IsConstant(f)) && asBoolean(Cudd_IsConstant(g))) {
                    double leftElementIdentifier = Cudd_V(f);
                    double rightElementIdentifier = Cudd_V(g);
                    E left = XDD.this.ddManager.v(leftElementIdentifier);
                    E right = XDD.this.ddManager.v(rightElementIdentifier);
                    E result = op.apply(left, right);
                    double resultElementIdentifier = XDD.this.ddManager.valueId(result);
                    return Cudd_addConst(ddManager, resultElementIdentifier);
                }
                return NULL;
            }
        }, ptr, g.ptr);
        return new XDD<>(resultPtr, ddManager).withRef();
    }

    public XDD<E> apply2(final BinaryOperator<XDD<E>> op, XDD<E> g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_addApply(ddManager.ptr(), new DD_AOP_Fn() {

            public long apply(long ddManager, long fPtr, long gPtr) {
                XDD<E> f = new XDD<>(fPtr, XDD.this.ddManager);
                XDD<E> g = new XDD<>(gPtr, XDD.this.ddManager);
                XDD<E> result = op.apply(f, g);
                return result == null ? NULL : result.ptr;
            }
        }, ptr, g.ptr);
        return new XDD<>(resultPtr, ddManager).withRef();
    }

    public XDD<E> monadicApply(final UnaryOperator<E> op) {
        long resultPtr = Cudd_addMonadicApply(ddManager.ptr(), new DD_MAOP_Fn() {

            public long apply(long ddManager, long f) {
                if (asBoolean(Cudd_IsConstant(f))) {
                    double xElementIdentifier = Cudd_V(f);
                    E x = XDD.this.ddManager.v(xElementIdentifier);
                    E result = op.apply(x);
                    double resultElementIdentifier = XDD.this.ddManager.valueId(result);
                    return Cudd_addConst(ddManager, resultElementIdentifier);
                }
                return NULL;
            }
        }, ptr);
        return new XDD<>(resultPtr, ddManager).withRef();
    }

    public XDD<E> monadicApply2(final UnaryOperator<XDD<E>> op) {
        long resultPtr = Cudd_addMonadicApply(ddManager.ptr(), new DD_MAOP_Fn() {

            public long apply(long ddManager, long fPtr) {
                XDD<E> f = new XDD<>(fPtr, XDD.this.ddManager);
                XDD<E> result = op.apply(f);
                return result == null ? NULL : result.ptr;
            }
        }, ptr);
        return new XDD<>(resultPtr, ddManager).withRef();
    }

    public <E2, E3> XDD<E3> transform(XDDManager<E3> ddManagerTarget, BiFunction<E, E2, E3> op, XDD<E2> g) {
        throw new RuntimeException("Not implemented");
    }

    public <E2, E3> XDD<E3> transform2(XDDManager<E3> ddManagerTarget, BiFunction<XDD<E>, XDD<E2>, XDD<E3>> op,
                                       XDD<E2> g) {

        throw new RuntimeException("Not implemented");
    }

    public <E2> XDD<E2> monadicTransform(XDDManager<E2> ddManagerTarget, Function<E, E2> op) {
        HashMap<XDD<E>, XDD<E2>> cache = new HashMap<>();
        XDD<E2> result = this.monadicTransformRecursive(ddManagerTarget, op, cache);

        /* Dereference intermediate results */
        cache.remove(this);
        for (XDD<E2> g : cache.values())
            g.recursiveDeref();

        return result;
    }

    private <E2> XDD<E2> monadicTransformRecursive(XDDManager<E2> ddManagerTarget, Function<E, E2> op,
                                                   Map<XDD<E>, XDD<E2>> cache) {

        if (!cache.containsKey(this)) {
            XDD<E2> result;
            if (isConstant()) {
                E2 v2 = op.apply(v());
                result = ddManagerTarget.constant(v2);
            } else {
                String name = readName();
                int idx = ddManagerTarget.varIdx(name);
                XDD<E2> t = t().monadicTransformRecursive(ddManagerTarget, op, cache);
                XDD<E2> e = e().monadicTransformRecursive(ddManagerTarget, op, cache);
                result = ddManagerTarget.ithVar(idx, t, e);
            }
            cache.put(this, result);
        }
        return cache.get(this);
    }

    public <E2> XDD<E2> monadicTransorm2(XDDManager<E2> ddManagerTarget, Function<XDD<E>, XDD<E2>> op) {
        throw new RuntimeException("Not implemented");
    }

    /* Named operations defined in XDDManager */

    public XDD<E> inverse() {
        return monadicApply(ddManager::inverse);
    }

    public XDD<E> compl() {
        return monadicApply(ddManager::compl);
    }

    public XDD<E> not() {
        return monadicApply(ddManager::not);
    }

    public XDD<E> multInverse() {
        return monadicApply(ddManager::multInverse);
    }

    public XDD<E> addInverse() {
        return monadicApply(ddManager::addInverse);
    }

    public XDD<E> meet(XDD<E> g) {
        return apply(ddManager::meet, g);
    }

    public XDD<E> inf(XDD<E> g) {
        return apply(ddManager::inf, g);
    }

    public XDD<E> intersect(XDD<E> g) {
        return apply(ddManager::intersect, g);
    }

    public XDD<E> and(XDD<E> g) {
        return apply(ddManager::and, g);
    }

    public XDD<E> mult(XDD<E> g) {
        return apply(ddManager::mult, g);
    }

    public XDD<E> join(XDD<E> g) {
        return apply(ddManager::join, g);
    }

    public XDD<E> sup(XDD<E> g) {
        return apply(ddManager::sup, g);
    }

    public XDD<E> union(XDD<E> g) {
        return apply(ddManager::union, g);
    }

    public XDD<E> or(XDD<E> g) {
        return apply(ddManager::or, g);
    }

    public XDD<E> add(XDD<E> g) {
        return apply(ddManager::add, g);
    }

    /* Required DD methods */

    @Override
    protected XDD<E> thisCasted() {
        return this;
    }

    @Override
    public XDD<E> t() {
        assertNonConstant();
        long thenPtr = Cudd_T(ptr);
        return new XDD<>(thenPtr, ddManager);
    }

    @Override
    public XDD<E> e() {
        assertNonConstant();
        long elsePtr = Cudd_E(ptr);
        return new XDD<>(elsePtr, ddManager);
    }

    @Override
    public XDD<E> eval(boolean... input) {
        int[] inputAsInt = new int[input.length];
        for (int i = 0; i < input.length; i++) {
            if (input[i])
                inputAsInt[i] = 1;
        }
        long evalPtr = Cudd_Eval(ddManager.ptr(), ptr, inputAsInt);
        return new XDD<>(evalPtr, ddManager);
    }

    /* Other methods */

    @Override
    public String toString() {
        if (isConstant())
            return v().toString();
        return super.toString();
    }
}
