package info.scce.addlib.dd.xdd.latticedd;

public abstract class BooleanLatticeDDManager<T> extends ComplementableLatticeDDManager<T> {

    public BooleanLatticeDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public BooleanLatticeDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public BooleanLatticeDDManager() {
        super();
    }

    @Override
    protected T zeroElement() {
        return botElement();
    }

    @Override
    protected T oneElement() {
        return topElement();
    }

    @Override
    protected T not(T x) {
        return compl(x);
    }

    @Override
    protected T and(T left, T right) {
        return meet(left, right);
    }

    @Override
    protected T or(T left, T right) {
        return join(left, right);
    }
}
