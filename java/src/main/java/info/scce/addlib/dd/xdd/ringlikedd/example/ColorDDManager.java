package info.scce.addlib.dd.xdd.ringlikedd.example;

import info.scce.addlib.dd.xdd.ringlikedd.SemiringDDManager;

import java.awt.*;

import static java.lang.Math.min;
import static java.lang.Math.round;

public class ColorDDManager extends SemiringDDManager<Color> {

    public ColorDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public ColorDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public ColorDDManager() {
        super();
    }

    @Override
    protected Color zeroElement() {
        return new Color(0, 0, 0);
    }

    @Override
    protected Color oneElement() {
        return new Color(255, 255, 255);
    }

    @Override
    protected Color mult(Color color0, Color color1) {
        int r = (color0.getRed() * color1.getRed()) / 255;
        int g = (color0.getGreen() * color1.getGreen()) / 255;
        int b = (color0.getBlue() * color1.getBlue()) / 255;
        return new Color(r, g, b);
    }

    @Override
    protected Color add(Color color0, Color color1) {
        int r = min(color0.getRed() + color1.getRed(), 255);
        int g = min(color0.getGreen() + color1.getGreen(), 255);
        int b = min(color0.getBlue() + color1.getBlue(), 255);
        return new Color(r, g, b);
    }

    public static Color avg(Color color0, Color color1) {
        int r = (color0.getRed() + color1.getRed()) / 2;
        int g = (color0.getGreen() + color1.getGreen()) / 2;
        int b = (color0.getBlue() + color1.getBlue()) / 2;
        return new Color(r, g, b);
    }

    public static Color grayscale(Color color) {
        int gray = (int) round(0.2126 * color.getRed() + 0.7152 * color.getGreen() + 0.0722 * color.getBlue());
        return new Color(gray, gray, gray);
    }
}

