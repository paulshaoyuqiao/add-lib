package info.scce.addlib.dd.xdd.latticedd;

import info.scce.addlib.dd.xdd.XDDManager;

public abstract class CompleteLatticeDDManager<T> extends XDDManager<T> {

    public CompleteLatticeDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public CompleteLatticeDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public CompleteLatticeDDManager() {
        super();
    }

    @Override
    protected abstract T meet(T left, T right);

    @Override
    protected abstract T join(T left, T right);
}
