package info.scce.addlib.dd.xdd.ringlikedd;

public abstract class FieldDDManager<T> extends RingDDManager<T> {

    public FieldDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public FieldDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public FieldDDManager() {
        super();
    }

    @Override
    protected abstract T multInverse(T x);
}
