package info.scce.addlib.dd.xdd.ringlikedd.example;

import info.scce.addlib.dd.xdd.ringlikedd.RingDDManager;

public class SquareMatrixRingDDManager extends RingDDManager<SquareMatrix> {

    private final int n;

    public SquareMatrixRingDDManager(int n, int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
        this.n = n;
    }

    public SquareMatrixRingDDManager(int n, int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
        this.n = n;
    }

    public SquareMatrixRingDDManager(int n) {
        super();
        this.n = n;
    }

    @Override
    protected SquareMatrix addInverse(SquareMatrix x) {
        return x.addInverse();
    }

    @Override
    protected SquareMatrix zeroElement() {
        return SquareMatrix.zero(n);
    }

    @Override
    protected SquareMatrix oneElement() {
        return SquareMatrix.identity(n);
    }

    @Override
    protected SquareMatrix mult(SquareMatrix left, SquareMatrix right) {
        return left.mult(right);
    }

    @Override
    protected SquareMatrix add(SquareMatrix left, SquareMatrix right) {
        return left.add(right);
    }

    @Override
    public SquareMatrix parseElement(String str) {
        return SquareMatrix.parseSquareMatrix(str);
    }
}
