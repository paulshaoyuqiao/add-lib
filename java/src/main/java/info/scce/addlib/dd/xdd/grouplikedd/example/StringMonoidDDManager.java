package info.scce.addlib.dd.xdd.grouplikedd.example;

import info.scce.addlib.dd.xdd.grouplikedd.MonoidDDManager;

public class StringMonoidDDManager extends MonoidDDManager<String> {

    public StringMonoidDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public StringMonoidDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public StringMonoidDDManager() {
        super();
    }

    @Override
    protected String neutralElement() {
        return "";
    }

    @Override
    protected String join(String left, String right) {
        return left + right;
    }

    @Override
    public String parseElement(String str) {
        return str;
    }
}
