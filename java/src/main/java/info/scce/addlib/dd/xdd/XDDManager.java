package info.scce.addlib.dd.xdd;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import info.scce.addlib.dd.DDManager;
import info.scce.addlib.dd.DDManagerException;
import info.scce.addlib.parser.XDDLanguageLexer;
import info.scce.addlib.parser.XDDLanguageParser;
import info.scce.addlib.parser.XDDVisitor;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import static info.scce.addlib.cudd.Cudd.*;

public class XDDManager<T> extends DDManager<XDD<T>> {

    private BiMap<T, Double> valueMap;
    private double nextValueId;

    public XDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
        initElementsMap();
    }

    public XDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
        initElementsMap();
    }

    public XDDManager() {
        super();
        initElementsMap();
    }

    private void initElementsMap() {
        valueMap = HashBiMap.create();
        nextValueId = 0;
    }

    /* Construct constant XDDs */

    public XDD<T> constant(T value) {
        long resultPtr = Cudd_addConst(ptr(), valueId(value));
        return new XDD<>(resultPtr, this).withRef();
    }

    public XDD<T> neutral() {
        return constant(neutralElement());
    }

    public XDD<T> bot() {
        return constant(botElement());
    }

    public XDD<T> top() {
        return constant(topElement());
    }

    public XDD<T> zero() {
        return constant(zeroElement());
    }

    public XDD<T> one() {
        return constant(oneElement());
    }

    /* Construct single variable XDDs */

    public XDD<T> namedVar(String name) {
        return ithVar(varIdx(name));
    }

    public XDD<T> namedVar(String name, XDD<T> t, XDD<T> e) {
        return ithVar(varIdx(name), t, e);
    }

    public XDD<T> namedVar(String name, T t, T e) {
        return ithVar(varIdx(name), t, e);
    }

    public XDD<T> ithVar(int i) {
        return ithVar(i, oneElement(), zeroElement());
    }

    public XDD<T> ithVar(int i, XDD<T> t, XDD<T> e) {
        return varFromUnreferencedAddVarPtr(Cudd_addIthVar(ptr(), i), t, e);
    }

    public XDD<T> ithVar(int i, T t, T e) {
        return varFromUnreferencedAddVarPtr(Cudd_addIthVar(ptr(), i), t, e);
    }

    public XDD<T> newVar() {
        return newVar(oneElement(), zeroElement());
    }

    public XDD<T> newVar(XDD<T> t, XDD<T> e) {
        return varFromUnreferencedAddVarPtr(Cudd_addNewVar(ptr()), t, e);
    }

    public XDD<T> newVar(T t, T e) {
        return varFromUnreferencedAddVarPtr(Cudd_addNewVar(ptr()), t, e);
    }

    public XDD<T> newVarAtLevel(int level) {
        return newVarAtLevel(level, oneElement(), zeroElement());
    }

    public XDD<T> newVarAtLevel(int level, XDD<T> t, XDD<T> e) {
        return varFromUnreferencedAddVarPtr(Cudd_addNewVarAtLevel(ptr(), level), t, e);
    }

    public XDD<T> newVarAtLevel(int level, T t, T e) {
        return varFromUnreferencedAddVarPtr(Cudd_addNewVarAtLevel(ptr(), level), t, e);
    }

    private XDD<T> varFromUnreferencedAddVarPtr(long unreferencedAddVarPtr, T v1, T v0) {
        Cudd_Ref(unreferencedAddVarPtr);
        long addVarPtr = unreferencedAddVarPtr;
        XDD<T> t = constant(v1);
        XDD<T> e = constant(v0);
        long resultPtr = Cudd_addIte(ptr(), addVarPtr, t.ptr(), e.ptr());
        XDD<T> result = new XDD<>(resultPtr, this).withRef();
        Cudd_RecursiveDeref(ptr(), addVarPtr);
        t.recursiveDeref();
        e.recursiveDeref();
        return result;
    }

    private XDD<T> varFromUnreferencedAddVarPtr(long unreferencedAddVarPtr, XDD<T> t, XDD<T> e) {
        Cudd_Ref(unreferencedAddVarPtr);
        long addVarPtr = unreferencedAddVarPtr;
        long resultPtr = Cudd_addIte(ptr(), addVarPtr, t.ptr(), e.ptr());
        XDD<T> result = new XDD<>(resultPtr, this).withRef();
        Cudd_RecursiveDeref(ptr(), addVarPtr);
        return result;
    }

    /* Value mapping */

    public T v(double valueId) {
        return valueMap.inverse().get(valueId);
    }

    public double valueId(T element) {
        if (!valueMap.containsKey(element))
            valueMap.put(element, nextValueId++);
        return valueMap.get(element);
    }

    /* Algebraic structure definition */

    protected T neutralElement() {
        throw undefinedInAlgebraicStructureException("neutralElement");
    }

    protected T botElement() {
        throw undefinedInAlgebraicStructureException("botElement");
    }

    protected T topElement() {
        throw undefinedInAlgebraicStructureException("topElement");
    }

    protected T zeroElement() {
        throw undefinedInAlgebraicStructureException("zeroElement");
    }

    protected T oneElement() {
        throw undefinedInAlgebraicStructureException("oneElement");
    }

    protected T inverse(T x) {
        throw undefinedInAlgebraicStructureException("inverse");
    }

    protected T compl(T x) {
        throw undefinedInAlgebraicStructureException("compl");
    }

    protected T not(T x) {
        throw undefinedInAlgebraicStructureException("not");
    }

    protected T multInverse(T x) {
        throw undefinedInAlgebraicStructureException("multInverse");
    }

    protected T addInverse(T x) {
        throw undefinedInAlgebraicStructureException("addInverse");
    }

    protected T meet(T left, T right) {
        throw undefinedInAlgebraicStructureException("meet");
    }

    protected T inf(T left, T right) {
        throw undefinedInAlgebraicStructureException("inf");
    }

    protected T intersect(T left, T right) {
        throw undefinedInAlgebraicStructureException("intersect");
    }

    protected T and(T left, T right) {
        throw undefinedInAlgebraicStructureException("and");
    }

    protected T mult(T left, T right) {
        throw undefinedInAlgebraicStructureException("mult");
    }

    protected T join(T left, T right) {
        throw undefinedInAlgebraicStructureException("join");
    }

    protected T sup(T left, T right) {
        throw undefinedInAlgebraicStructureException("sup");
    }

    protected T union(T left, T right) {
        throw undefinedInAlgebraicStructureException("union");
    }

    protected T or(T left, T right) {
        throw undefinedInAlgebraicStructureException("or");
    }

    protected T add(T left, T right) {
        throw undefinedInAlgebraicStructureException("add");
    }

    public T parseElement(String str) {
        throw undefinedInAlgebraicStructureException("parseElement");
    }

    protected DDManagerException undefinedInAlgebraicStructureException(String what) {
        return new DDManagerException(getClass().getSimpleName() + " does not define " + what);
    }

    public XDD<T> parse(String str) {
        ANTLRInputStream inputStream = new ANTLRInputStream(str);
        XDDLanguageLexer lexer = new XDDLanguageLexer(inputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        XDDLanguageParser parser = new XDDLanguageParser(tokens);
        ParseTree tree = parser.start();
        XDDVisitor<T> ast = new XDDVisitor<>(this);
        return ast.visit(tree);
    }
}
