package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.xdd.latticedd.BooleanLatticeDDManager;

import static java.lang.Double.parseDouble;

public class BoundedFuzzyLogicDDManager extends BooleanLatticeDDManager<Double> {

    public BoundedFuzzyLogicDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public BoundedFuzzyLogicDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public BoundedFuzzyLogicDDManager() {
        super();
    }

    @Override
    protected Double meet(Double left, Double right) {
        return ensureRange(left + right - 1);
    }

    @Override
    protected Double join(Double left, Double right) {
        return ensureRange(left + right);
    }

    @Override
    protected Double botElement() {
        return 0.0;
    }

    @Override
    protected Double topElement() {
        return 1.0;
    }

    @Override
    protected Double compl(Double x) {
        return ensureRange(1.0 - x);
    }

    @Override
    public Double parseElement(String str) {
        return ensureRange(parseDouble(str));
    }

    private double ensureRange(double x) {
        if (x > 1.0) return 1.0;
        else if (x < 0.0) return 0.0;
        else return x;
    }
}
