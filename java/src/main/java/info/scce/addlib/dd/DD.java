package info.scce.addlib.dd;

import static info.scce.addlib.cudd.Cudd.*;
import static info.scce.addlib.utils.Conversions.asBoolean;

public abstract class DD<M extends DDManager<D>, D extends DD<?, D>> {

    protected final long ptr;
    protected final M ddManager;

    public DD(long ptr, M ddManager) {
        this.ptr = ptr;
        this.ddManager = ddManager;
    }

    /* Common DD methods */

    public long ptr() {
        return ptr;
    }

    public M ddManager() {
        return ddManager;
    }

    public D withRef() {
        ref();
        return thisCasted();
    }

    /* CUDD wrapper */

    public void ref() {
        this.ddManager.incRefCount(ptr);
        Cudd_Ref(ptr);
    }

    public void recursiveDeref() {

        /* Throws an exception if ptr is unreferenced */
        this.ddManager.decRefCount(ptr);

        Cudd_RecursiveDeref(ddManager.ptr(), ptr);
    }

    public int readIndex() {
        assertNonConstant();
        return Cudd_NodeReadIndex(ptr);
    }

    public String readName() {
        return ddManager().varName(readIndex());
    }

    public boolean isConstant() {
        return asBoolean(Cudd_IsConstant(ptr));
    }

    public int dagSize() {
        return Cudd_DagSize(ptr);
    }

    /* Required DD methods */

    protected abstract D thisCasted();

    public abstract D t();

    public abstract D e();

    public abstract D eval(boolean... input);

    /* Assertions */

    protected void assertEqualDDManager(DD<?, ?> dd) {
        if (!ddManager.equals(dd.ddManager())) {
            throw new DDManagerException(getClass().getSimpleName() + " operands must share the same " +
                    DDManager.class.getSimpleName());
        }
    }

    protected void assertEqualDDManager(DD<?, ?>... dds) {
        for (DD<?, ?> dd : dds)
            assertEqualDDManager(dd);
    }

    protected void assertConstant() {
        if (!isConstant())
            throw new DDException("Expected constant " + getClass().getSimpleName());
    }

    protected void assertNonConstant() {
        if (isConstant())
            throw new DDException("Expected non-constant " + getClass().getSimpleName());
    }

    /* Other methods */

    @Override
    public boolean equals(Object otherObj) {
        if (otherObj instanceof DD<?, ?>) {
            DD<?, ?> other = (DD<?, ?>) otherObj;
            return ptr == other.ptr;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(ptr);
    }

    @Override
    public String toString() {
        if (isConstant())
            return "?";
        return readName();
    }
}
