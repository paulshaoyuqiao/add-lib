package info.scce.addlib.dd;

public abstract class RegularDD<M extends DDManager<D>, D extends RegularDD<?, D>> extends DD<M, D> {

    public RegularDD(long ptr, M ddManager) {
        super(ptr, ddManager);
    }
}
