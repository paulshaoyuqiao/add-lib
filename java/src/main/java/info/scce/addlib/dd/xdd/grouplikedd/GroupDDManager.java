package info.scce.addlib.dd.xdd.grouplikedd;

public abstract class GroupDDManager<T> extends MonoidDDManager<T> {

    public GroupDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public GroupDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public GroupDDManager() {
        super();
    }

    @Override
    protected abstract T inverse(T x);
}
