package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.xdd.latticedd.BooleanLatticeDDManager;

import static java.lang.Double.parseDouble;
import static java.lang.Math.max;
import static java.lang.Math.min;

public class MinMaxFuzzyLogicDDManager extends BooleanLatticeDDManager<Double> {

    public MinMaxFuzzyLogicDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public MinMaxFuzzyLogicDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public MinMaxFuzzyLogicDDManager() {
        super();
    }

    @Override
    protected Double meet(Double left, Double right) {
        return min(left, right);
    }

    @Override
    protected Double join(Double left, Double right) {
        return max(left, right);
    }

    @Override
    protected Double botElement() {
        return 0.0;
    }

    @Override
    protected Double topElement() {
        return 1.0;
    }

    @Override
    protected Double compl(Double x) {
        return 1.0 - x;
    }

    @Override
    public Double parseElement(String str) {
        return parseDouble(str);
    }
}
