package info.scce.addlib.dd.xdd.latticedd.example;

public class FuzzyValue {

    protected static final Double ONE = 1.0;
    protected static final Double ZERO = 0.0;

    /*
     * FuzzyValue checks inputs for valid fuzzy values and gives back the neutral elements.
     * Some fuzzy logics are implemented in this package, (regarding t-, s-norm and complement)
     * like Min-Max, Probabilistic and Bounded (Łukasiewicz).
     * */

    private FuzzyValue() {
    }

    /**
     * Checks for the given arguments whether they are valid fuzzy values, else throws an exception.
     *
     * @param vals
     */
    public static void isFuzzyValue(Double... vals) {
        for (Double val : vals) {
            if (val < 0 || val > 1) {
                throw new IllegalArgumentException("Values for fuzzy logic have to be in the set [0,1]! Given value: " + val);
            }
        }
    }

    /**
     * Parses a fuzzy value from a given String. If the argument val isn't parsable or isn't a fuzzy value, then an exception is thrown.
     *
     * @param val
     * @return
     */
    public static Double parseFuzzyValue(String val) {
        try {
            Double valD = Double.parseDouble(val);
            FuzzyValue.isFuzzyValue(valD);
            return valD;
        } catch (NullPointerException | NumberFormatException e) {
            throw new NumberFormatException("String must be a parsable to a Double! Given value: " + val);
        }
    }
}