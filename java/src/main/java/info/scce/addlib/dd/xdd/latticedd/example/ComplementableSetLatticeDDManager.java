package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.xdd.latticedd.ComplementableLatticeDDManager;

public class ComplementableSetLatticeDDManager<E> extends ComplementableLatticeDDManager<ComplementableSet<E>> {

    public ComplementableSetLatticeDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public ComplementableSetLatticeDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public ComplementableSetLatticeDDManager() {
        super();
    }

    @Override
    protected ComplementableSet<E> meet(ComplementableSet<E> left, ComplementableSet<E> right) {
        return left.intersect(right);
    }

    @Override
    protected ComplementableSet<E> join(ComplementableSet<E> left, ComplementableSet<E> right) {
        return left.union(right);
    }

    @Override
    protected ComplementableSet<E> botElement() {
        return ComplementableSet.emptySet();
    }

    @Override
    protected ComplementableSet<E> topElement() {
        return ComplementableSet.completeSet();
    }

    @Override
    protected ComplementableSet<E> compl(ComplementableSet<E> x) {
        return x.complement();
    }

    @Override
    protected ComplementableSet<E> intersect(ComplementableSet<E> left, ComplementableSet<E> right) {
        return meet(left, right);
    }

    @Override
    protected ComplementableSet<E> union(ComplementableSet<E> left, ComplementableSet<E> right) {
        return join(left, right);
    }

    @Override
    public ComplementableSet<E> parseElement(String str) {
        return ComplementableSet.parseComplementableSet(str, this::parseComplementableSetElement);
    }

    protected E parseComplementableSetElement(String str) {
        throw undefinedInAlgebraicStructureException("parseComplementableSetElement");
    }
}
