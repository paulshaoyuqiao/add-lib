package info.scce.addlib.dd.xdd.latticedd;

public abstract class BoundedLatticeDDManager<T> extends CompleteLatticeDDManager<T> {

    public BoundedLatticeDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public BoundedLatticeDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public BoundedLatticeDDManager() {
        super();
    }

    @Override
    protected abstract T botElement();

    @Override
    protected abstract T topElement();
}
