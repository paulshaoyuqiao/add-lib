package info.scce.addlib.dd.bdd;

import info.scce.addlib.dd.DDManager;
import info.scce.addlib.parser.BDDLanguageLexer;
import info.scce.addlib.parser.BDDLanguageParser;
import info.scce.addlib.parser.BDDVisitor;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import static info.scce.addlib.cudd.Cudd.*;

public class BDDManager extends DDManager<BDD> {

    public BDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public BDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public BDDManager() {
        super();
    }

    /* Construct primitive BDDs */

    public BDD readOne() {
        long ddNodePtr = Cudd_ReadOne(ptr);
        return new BDD(ddNodePtr, this).withRef();
    }

    public BDD readLogicZero() {
        long ddNodePtr = Cudd_ReadLogicZero(ptr);
        return new BDD(ddNodePtr, this).withRef();
    }

    public BDD namedVar(String name) {
        return ithVar(varIdx(name));
    }

    public BDD ithVar(int var) {
        long ddNodePtr = Cudd_bddIthVar(ptr, var);
        return new BDD(ddNodePtr, this).withRef();
    }

    public BDD newVar() {
        long ddNodePtr = Cudd_bddNewVar(ptr);
        return new BDD(ddNodePtr, this).withRef();
    }

    public BDD newVarAtLevel(int level) {
        long ddNodePtr = Cudd_bddNewVarAtLevel(ptr, level);
        return new BDD(ddNodePtr, this).withRef();
    }

    @Override
    public BDD parse(String str) {
        ANTLRInputStream inputStream = new ANTLRInputStream(str);
        BDDLanguageLexer lexer = new BDDLanguageLexer(inputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        BDDLanguageParser parser = new BDDLanguageParser(tokens);
        BDDVisitor ast = new BDDVisitor(this);
        return ast.visit(parser.expr());
    }
}
