package info.scce.addlib.dd.xdd.ringlikedd;

public abstract class RingDDManager<T> extends SemiringDDManager<T> {

    public RingDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public RingDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public RingDDManager() {
        super();
    }

    @Override
    protected abstract T addInverse(T x);
}
