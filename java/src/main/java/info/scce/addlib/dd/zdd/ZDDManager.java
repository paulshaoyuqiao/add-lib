package info.scce.addlib.dd.zdd;


import info.scce.addlib.dd.DDManager;
import info.scce.addlib.parser.ZDDLanguageLexer;
import info.scce.addlib.parser.ZDDLanguageParser;
import info.scce.addlib.parser.ZDDVisitor;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import static info.scce.addlib.cudd.Cudd.*;

public class ZDDManager extends DDManager<ZDD> {

    public ZDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public ZDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public ZDDManager() {
        super();
    }

    /* Construct primitive ZDDs */

    public ZDD readOne() {
        long ddNodePtr = Cudd_ReadOne(ptr);
        return new ZDD(ddNodePtr, this).withRef();
    }

    public ZDD readZddOne(int i) {
        long ddNodePtr = Cudd_ReadZddOne(ptr, i);
        return new ZDD(ddNodePtr, this).withRef();
    }

    public ZDD readZero() {
        long ddNodePtr = Cudd_ReadZero(ptr);
        return new ZDD(ddNodePtr, this).withRef();
    }

    public ZDD namedVar(String name) {
        return ithVar(varIdx(name));
    }

    public ZDD ithVar(int i) {
        long ddNodePtr = Cudd_zddIthVar(ptr, i);
        return new ZDD(ddNodePtr, this).withRef();
    }

    @Override
    public ZDD parse(String zddAsString) {
        ANTLRInputStream inputStream = new ANTLRInputStream(zddAsString);
        ZDDLanguageLexer lexer = new ZDDLanguageLexer(inputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ZDDLanguageParser parser = new ZDDLanguageParser(tokens);
        ZDDVisitor ast = new ZDDVisitor(this);
        return ast.visit(parser.start());
    }
}
