package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.xdd.latticedd.BooleanLatticeDDManager;

import static java.lang.Boolean.parseBoolean;


public class BooleanLogicDDManager extends BooleanLatticeDDManager<Boolean> {

    public BooleanLogicDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public BooleanLogicDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public BooleanLogicDDManager() {
        super();
    }

    @Override
    protected Boolean meet(Boolean left, Boolean right) {
        return left && right;
    }

    @Override
    protected Boolean join(Boolean left, Boolean right) {
        return left || right;
    }

    @Override
    protected Boolean botElement() {
        return false;
    }

    @Override
    protected Boolean topElement() {
        return true;
    }

    @Override
    protected Boolean compl(Boolean x) {
        return !x;
    }

    @Override
    public Boolean parseElement(String str) {
        return parseBoolean(str);
    }
}
