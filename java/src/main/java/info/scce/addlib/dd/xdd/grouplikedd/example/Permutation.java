package info.scce.addlib.dd.xdd.grouplikedd.example;

import java.util.Arrays;

public class Permutation {

    private final int[] p;

    public Permutation(int... p) {
        this.p = p;
    }

    public static Permutation identity(int n) {
        int[] p = new int[n];
        for (int i = 0; i < n; i++)
            p[i] = i;
        return new Permutation(p);
    }

    public static Permutation parsePermutation(String str) {
        int[] p;
        if (str.isEmpty()) {
            p = new int[0];
        } else {
            String[] strMaps = str.split(", ");
            p = new int[strMaps.length];
            for (int i = 0; i < strMaps.length; i++) {
                String strTo = strMaps[i].split(" -> ")[1];
                p[i] = Integer.parseInt(strTo);
            }
        }
        return new Permutation(p);
    }

    public int size() {
        return p.length;
    }

    public int[] data() {
        return p;
    }

    public Permutation compose(Permutation g) {
        int[] pComposed = new int[p.length];
        for (int i = 0; i < p.length; i++)
            pComposed[i] = p[g.p[i]];
        return new Permutation(pComposed);
    }

    public Permutation inverse() {
        int[] pInverse = new int[p.length];
        for (int i = 0; i < p.length; i++)
            pInverse[p[i]] = i;
        return new Permutation(pInverse);
    }

    @Override
    public boolean equals(Object otherObj) {
        if (otherObj instanceof Permutation) {
            Permutation other = (Permutation) otherObj;
            return Arrays.equals(p, other.p);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(p);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        if (i < p.length)
            sb.append(i + " -> " + p[i++]);
        while (i < p.length)
            sb.append(", " + i + " -> " + p[i++]);
        return sb.toString();
    }
}
