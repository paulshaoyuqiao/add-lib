package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.xdd.latticedd.CompleteLatticeDDManager;

public class PartitionLatticeDDManager<T> extends CompleteLatticeDDManager<Partition<T>> {

    public PartitionLatticeDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public PartitionLatticeDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public PartitionLatticeDDManager() {
        super();
    }

    @Override
    protected Partition<T> meet(Partition<T> left, Partition<T> right) {
        return left.meet(right);
    }

    @Override
    protected Partition<T> join(Partition<T> left, Partition<T> right) {
        return left.join(right);
    }

    @Override
    public Partition<T> parseElement(String input) {
        return Partition.parsePartition(input,this::parsePartitionElement);
    }

    protected T parsePartitionElement(String input){
        throw undefinedInAlgebraicStructureException("parsePartitionElement");
    }
}
