package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.xdd.latticedd.BooleanLatticeDDManager;

public class BooleanVectorLogicDDManager extends BooleanLatticeDDManager<BooleanVector> {

    private final int n;

    public BooleanVectorLogicDDManager(int n, int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
        this.n = n;
    }

    public BooleanVectorLogicDDManager(int n, int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
        this.n = n;
    }

    public BooleanVectorLogicDDManager(int n) {
        super();
        this.n = n;
    }

    @Override
    protected BooleanVector compl(BooleanVector x) {
        return x.not();
    }

    @Override
    protected BooleanVector botElement() {
        return BooleanVector.zero(n);
    }

    @Override
    protected BooleanVector topElement() {
        return BooleanVector.one(n);
    }

    @Override
    protected BooleanVector meet(BooleanVector left, BooleanVector right) {
        return left.and(right);
    }

    @Override
    protected BooleanVector join(BooleanVector left, BooleanVector right) {
        return left.or(right);
    }

    @Override
    public BooleanVector parseElement(String str) {
        return BooleanVector.parseBooleanVector(str);
    }
}
