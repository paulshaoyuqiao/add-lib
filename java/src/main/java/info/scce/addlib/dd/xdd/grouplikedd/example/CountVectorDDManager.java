package info.scce.addlib.dd.xdd.grouplikedd.example;

import info.scce.addlib.dd.xdd.grouplikedd.MonoidDDManager;

public class CountVectorDDManager extends MonoidDDManager<CountVector> {

    private final int n;

    public CountVectorDDManager(int n, int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
        this.n = n;
    }

    public CountVectorDDManager(int n, int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
        this.n = n;
    }

    public CountVectorDDManager(int n) {
        super();
        this.n = n;
    }

    @Override
    protected CountVector neutralElement() {
        return CountVector.zero(n);
    }

    @Override
    protected CountVector join(CountVector left, CountVector right) {
        return left.add(right);
    }

    @Override
    public CountVector parseElement(String str) {
        return CountVector.parseCountVector(str);
    }
}
