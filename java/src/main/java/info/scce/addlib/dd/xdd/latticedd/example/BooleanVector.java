package info.scce.addlib.dd.xdd.latticedd.example;

import java.util.Arrays;

public class BooleanVector {

    private final boolean[] b;

    public BooleanVector(boolean... b) {
        this.b = b;
    }

    public static BooleanVector zero(int n) {
        boolean[] b = new boolean[n];
        for (int i = 0; i < b.length; i++)
            b[i] = false;
        return new BooleanVector(b);
    }

    public static BooleanVector one(int n) {
        boolean[] b = new boolean[n];
        for (int i = 0; i < b.length; i++)
            b[i] = true;
        return new BooleanVector(b);
    }

    public static BooleanVector parseBooleanVector(String str) {

        /* Strip brackets */
        String strCommaSeparatedBools = str.substring(1, str.length() - 1);

        boolean[] b;
        if (strCommaSeparatedBools.isEmpty()) {
            b = new boolean[0];
        } else {
            String[] strBools = strCommaSeparatedBools.split(", ");
            b = new boolean[strBools.length];
            for (int i = 0; i < b.length; i++)
                b[i] = Boolean.parseBoolean(strBools[i]);
        }
        return new BooleanVector(b);
    }

    public int n() {
        return b.length;
    }

    public boolean[] data() {
        return b;
    }

    public BooleanVector not() {
        boolean[] negation = new boolean[b.length];
        for (int i = 0; i < b.length; i++)
            negation[i] = !b[i];
        return new BooleanVector(negation);
    }

    public BooleanVector and(BooleanVector other) {
        boolean[] conjunction = new boolean[b.length];
        for (int i = 0; i < b.length; i++)
            conjunction[i] = b[i] && other.b[i];
        return new BooleanVector(conjunction);
    }

    public BooleanVector or(BooleanVector other) {
        boolean[] disjunction = new boolean[b.length];
        for (int i = 0; i < b.length; i++)
            disjunction[i] = b[i] || other.b[i];
        return new BooleanVector(disjunction);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BooleanVector bnLogic = (BooleanVector) o;
        return Arrays.equals(b, bnLogic.b);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(b);
    }

    @Override
    public String toString() {
        return Arrays.toString(b);
    }
}
