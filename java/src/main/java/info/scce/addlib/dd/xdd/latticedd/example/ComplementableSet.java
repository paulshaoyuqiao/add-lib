package info.scce.addlib.dd.xdd.latticedd.example;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

public class ComplementableSet<E> {

    private final boolean compl;
    private final Set<E> explRepr;

    public ComplementableSet(E[] elements) {
        compl = false;
        explRepr = new HashSet<>();
        explRepr.addAll(Arrays.asList(elements));
    }

    private ComplementableSet(Set<E> explRepr, boolean compl) {
        this.compl = compl;
        this.explRepr = explRepr;
    }

    public static <T> ComplementableSet<T> emptySet() {
        return new ComplementableSet<>(new HashSet<>(), false);
    }

    public static <T> ComplementableSet<T> completeSet() {
        return new ComplementableSet<>(new HashSet<>(), true);
    }

    public static <T> ComplementableSet<T> parseComplementableSet(String str, Function<String, T> parseElement) {
        boolean compl = str.endsWith("^C");

        /* Strip brackets and complement */
        int trimEnd = compl ? 3 : 1;
        String strCommaSeparatedExplReprs = str.substring(1, str.length() - trimEnd);

        String[] strExplReprs = strCommaSeparatedExplReprs.split(", ");
        Set<T> explRepr = new HashSet<>();
        for (int i = 0; i < strExplReprs.length; i++) {
            if (!strExplReprs[i].isEmpty()) {
                T parsed = parseElement.apply(strExplReprs[i]);
                explRepr.add(parsed);
            }
        }
        return new ComplementableSet<T>(explRepr, compl);
    }

    public boolean contains(E x) {
        boolean explReprContains = explRepr.contains(x);
        return compl != explReprContains;
    }

    public Set<E> explRepr() {
        return explRepr;
    }

    public boolean compl() {
        return compl;
    }

    public ComplementableSet<E> intersect(ComplementableSet<E> other) {
        if (compl) {
            if (other.compl)
                return new ComplementableSet<>(explUnion(explRepr, other.explRepr), true);
            else
                return new ComplementableSet<>(explDiff(other.explRepr, explRepr), false);
        } else {
            if (other.compl)
                return new ComplementableSet<>(explDiff(explRepr, other.explRepr), false);
            else
                return new ComplementableSet<>(explIntersect(explRepr, other.explRepr), false);
        }
    }

    public ComplementableSet<E> union(ComplementableSet<E> other) {
        if (compl) {
            if (other.compl)
                return new ComplementableSet<>(explIntersect(explRepr, other.explRepr), true);
            else
                return new ComplementableSet<>(explDiff(explRepr, other.explRepr), true);
        } else {
            if (other.compl)
                return new ComplementableSet<>(explDiff(other.explRepr, explRepr), true);
            else
                return new ComplementableSet<>(explUnion(explRepr, other.explRepr), false);
        }
    }

    private HashSet<E> explIntersect(Set<E> a, Set<E> b) {
        HashSet<E> s = new HashSet<>(a);
        s.retainAll(b);
        return s;
    }

    private HashSet<E> explUnion(Set<E> a, Set<E> b) {
        HashSet<E> s = new HashSet<>();
        s.addAll(a);
        s.addAll(b);
        return s;
    }

    private HashSet<E> explDiff(Set<E> a, Set<E> b) {
        HashSet<E> s = new HashSet<>(a);
        s.removeAll(b);
        return s;
    }

    public ComplementableSet<E> complement() {
        return new ComplementableSet<>(explRepr, !compl);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComplementableSet<?> that = (ComplementableSet<?>) o;
        return compl == that.compl && Objects.equals(explRepr, that.explRepr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(compl, explRepr);
    }

    @Override
    public String toString() {
        String strExplRepr = explRepr.toString();
        String strExplReprStripped = strExplRepr.substring(1, strExplRepr.length() - 1);
        return "[" + strExplReprStripped + "]" + (compl ? "^C" : "");
    }
}
