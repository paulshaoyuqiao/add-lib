package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.xdd.latticedd.BoundedLatticeDDManager;

public class IntervalDDManager extends BoundedLatticeDDManager<Interval> {

    public IntervalDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public IntervalDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public IntervalDDManager() {
        super();
    }

    @Override
    protected Interval botElement() {
        return Interval.empty();
    }

    @Override
    protected Interval topElement() {
        return Interval.complete();
    }

    @Override
    protected Interval meet(Interval left, Interval right) {
        return left.intersect(right);
    }

    @Override
    protected Interval join(Interval left, Interval right) {
        return left.union(right);
    }

    @Override
    protected Interval intersect(Interval left, Interval right) {
        return meet(left, right);
    }

    @Override
    protected Interval union(Interval left, Interval right) {
        return join(left, right);
    }

    @Override
    public Interval parseElement(String str) {
        return Interval.parseInterval(str);
    }
}
