package info.scce.addlib.dd.xdd.grouplikedd.example;

import java.util.Arrays;

public class CountVector {

    private int[] c;

    public CountVector(int... c) {
        this.c = c;
    }

    public static CountVector zero(int n) {
        int[] c = new int[n];
        for (int i = 0; i < c.length; i++)
            c[i] = 0;
        return new CountVector(c);
    }

    public static CountVector parseCountVector(String str) {

        /* Strip brackets */
        String strCommaSeparatedInts = str.substring(1, str.length() - 1);

        int[] c;
        if (strCommaSeparatedInts.isEmpty()) {
            c = new int[0];
        } else {
            String[] strInts = strCommaSeparatedInts.split(", ");
            int n = strInts.length;
            c = new int[n];
            for (int i = 0; i < c.length; i++)
                c[i] = Integer.parseInt(strInts[i]);
        }
        return new CountVector(c);
    }

    public int n() {
        return c.length;
    }

    public int[] data() {
        return c;
    }

    public CountVector add(CountVector other) {
        int[] sum = new int[n()];
        for (int i = 0; i < sum.length; i++)
            sum[i] = c[i] + other.c[i];
        return new CountVector(sum);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountVector count = (CountVector) o;
        return Arrays.equals(c, count.c);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(c);
    }

    @Override
    public String toString() {
        return Arrays.toString(c);
    }
}
