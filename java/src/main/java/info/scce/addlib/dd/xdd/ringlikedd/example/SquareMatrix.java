package info.scce.addlib.dd.xdd.ringlikedd.example;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.util.Objects;

public class SquareMatrix {

    private final RealMatrix m;

    public SquareMatrix(double[][] data) {
        this(MatrixUtils.createRealMatrix(data));
    }

    private SquareMatrix(RealMatrix m) {
        this.m = m;
    }

    public static SquareMatrix zero(int n) {
        return new SquareMatrix(MatrixUtils.createRealMatrix(n, n));
    }

    public static SquareMatrix identity(int n) {
        return new SquareMatrix(MatrixUtils.createRealIdentityMatrix(n));
    }

    public static SquareMatrix parseSquareMatrix(String str) {

        /* Strip outer brackets */
        String strCommaSeparatedRows = str.substring(2, str.length() - 2);

        /* Strip inner brackets */
        String[] strRows = strCommaSeparatedRows.split("\\},\\{");

        double[][] data = new double[strRows.length][];
        for (int i = 0; i < strRows.length; i++) {
            String strCommaSeparatedValues = strRows[i];
            String[] strValues = strCommaSeparatedValues.split(",");
            double[] row = new double[strValues.length];
            for (int j = 0; j < strValues.length; j++) {
                double value = Double.parseDouble(strValues[j]);
                row[j] = value;
            }
            data[i] = row;
        }
        return new SquareMatrix(data);
    }

    public int size() {
        return m.getColumnDimension();
    }

    public double[][] data() {
        return m.getData();
    }

    public SquareMatrix mult(SquareMatrix other) {
        return new SquareMatrix(m.multiply(other.m));
    }

    public SquareMatrix multInverse() {
        return new SquareMatrix(new LUDecomposition(m).getSolver().getInverse());
    }

    public SquareMatrix add(SquareMatrix other) {
        return new SquareMatrix(m.add(other.m));
    }

    public SquareMatrix addInverse() {
        return new SquareMatrix(m.scalarMultiply(-1));
    }

    @Override
    public boolean equals(Object o) { //needs to accept epsilon differences
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SquareMatrix that = (SquareMatrix) o;
        return m.equals(that.m);
    }

    @Override
    public int hashCode() {
        return Objects.hash(m);
    }

    @Override
    public String toString() {
        return m.toString().replace("Array2DRowRealMatrix", "");
    }
}
