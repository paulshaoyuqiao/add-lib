package info.scce.addlib.dd;

import info.scce.addlib.cudd.Cudd_ReorderingType;

import static info.scce.addlib.cudd.Cudd_ReorderingType.*;

public enum DDReorderingType {

    SAME(CUDD_REORDER_SAME),
    NONE(CUDD_REORDER_NONE),
    RANDOM(CUDD_REORDER_RANDOM),
    RANDOM_PIVOT(CUDD_REORDER_RANDOM_PIVOT),
    SIFT(CUDD_REORDER_SIFT),
    SIFT_CONVERGE(CUDD_REORDER_SIFT_CONVERGE),
    SYMM_SIFT(CUDD_REORDER_SYMM_SIFT),
    SYMM_SIFT_CONV(CUDD_REORDER_SYMM_SIFT_CONV),
    WINDOW2(CUDD_REORDER_WINDOW2),
    WINDOW3(CUDD_REORDER_WINDOW3),
    WINDOW4(CUDD_REORDER_WINDOW4),
    WINDOW2_CONV(CUDD_REORDER_WINDOW2_CONV),
    WINDOW3_CONV(CUDD_REORDER_WINDOW3_CONV),
    WINDOW4_CONV(CUDD_REORDER_WINDOW4_CONV),
    GROUP_SIFT(CUDD_REORDER_GROUP_SIFT),
    GROUP_SIFT_CONV(CUDD_REORDER_GROUP_SIFT_CONV),
    ANNEALING(CUDD_REORDER_ANNEALING),
    GENETIC(CUDD_REORDER_GENETIC),
    LINEAR(CUDD_REORDER_LINEAR),
    LINEAR_CONVERGE(CUDD_REORDER_LINEAR_CONVERGE),
    LAZY_SIFT(CUDD_REORDER_LAZY_SIFT),
    EXACT(CUDD_REORDER_EXACT);

    private final Cudd_ReorderingType cuddReorderingType;

    DDReorderingType(Cudd_ReorderingType cuddReorderingType) {
        this.cuddReorderingType = cuddReorderingType;
    }

    public Cudd_ReorderingType cuddReorderingType() {
        return cuddReorderingType;
    }
}
