package info.scce.addlib.dd.add;

import info.scce.addlib.dd.DDManager;
import info.scce.addlib.parser.ADDLanguageLexer;
import info.scce.addlib.parser.ADDLanguageParser;
import info.scce.addlib.parser.ADDVisitor;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import static info.scce.addlib.cudd.Cudd.*;


public class ADDManager extends DDManager<ADD> {

    public ADDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public ADDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public ADDManager() {
        super();
    }

    public double readEpsilon() {
        return Cudd_ReadEpsilon(ptr);
    }

    public void setEpsilon(double epsilon) {
        Cudd_SetEpsilon(ptr, epsilon);
    }

    public void setBackground(long background) {
        Cudd_SetBackground(ptr, background);
    }

    /* Construct primitive ADDs */

    public ADD readOne() {
        long ddNodePtr = Cudd_ReadOne(ptr);
        return new ADD(ddNodePtr, this).withRef();
    }

    public ADD readZero() {
        long ddNodePtr = Cudd_ReadZero(ptr);
        return new ADD(ddNodePtr, this).withRef();
    }

    public ADD readPlusInfinity() {
        long ddNodePtr = Cudd_ReadPlusInfinity(ptr);
        return new ADD(ddNodePtr, this).withRef();
    }

    public ADD readMinusInfinity() {
        long ddNodePtr = Cudd_ReadMinusInfinity(ptr);
        return new ADD(ddNodePtr, this).withRef();
    }

    public ADD constant(double value) {
        long ddNodePtr = Cudd_addConst(ptr, value);
        return new ADD(ddNodePtr, this).withRef();
    }

    public ADD namedVar(String name) {
        return ithVar(varIdx(name));
    }

    public ADD ithVar(int i) {
        long ddNodePtr = Cudd_addIthVar(ptr, i);
        return new ADD(ddNodePtr, this).withRef();
    }

    public ADD newVar() {
        long ddNodePtr = Cudd_addNewVar(ptr);
        return new ADD(ddNodePtr, this).withRef();
    }

    public ADD newVarAtLevel(int level) {
        long ddNodePtr = Cudd_addNewVarAtLevel(ptr, level);
        return new ADD(ddNodePtr, this).withRef();
    }

    public ADD parse(String str) {
        ANTLRInputStream inputStream = new ANTLRInputStream(str);
        ADDLanguageLexer lexer = new ADDLanguageLexer(inputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ADDLanguageParser parser = new ADDLanguageParser(tokens);
        ADDVisitor ast = new ADDVisitor(this);
        return ast.visit(parser.expr());
    }
}
