package info.scce.addlib.dd;

public class DDException extends RuntimeException {

    private static final long serialVersionUID = -1472041452920732795L;

    public DDException(String message) {
        super(message);
    }
}
