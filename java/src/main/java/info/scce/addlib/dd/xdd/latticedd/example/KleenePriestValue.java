package info.scce.addlib.dd.xdd.latticedd.example;

public enum KleenePriestValue {

    TRUE, UNKNOWN, FALSE;

    public static KleenePriestValue parseKleenePriestValue(String str) {
        if (str.equalsIgnoreCase("true")) return TRUE;
        else if (str.equalsIgnoreCase("false")) return FALSE;
        else if (str.equalsIgnoreCase("unknown")) return UNKNOWN;
        throw new RuntimeException("Could not parse '" + str + "'");
    }

    public KleenePriestValue and(KleenePriestValue other) {
        if (this == FALSE || other == FALSE) return FALSE;
        else if (this == TRUE && other == TRUE) return TRUE;
        return UNKNOWN;
    }

    public KleenePriestValue or(KleenePriestValue other) {
        if (this == TRUE || other == TRUE) return TRUE;
        else if (this == FALSE && other == FALSE) return FALSE;
        return UNKNOWN;
    }

    public KleenePriestValue not() {
        if (this == TRUE) return FALSE;
        else if (this == FALSE) return TRUE;
        return UNKNOWN;
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
