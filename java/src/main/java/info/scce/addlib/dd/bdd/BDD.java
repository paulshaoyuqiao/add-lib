package info.scce.addlib.dd.bdd;

import info.scce.addlib.dd.DD;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.latticedd.example.BooleanLogicDDManager;

import java.util.HashMap;
import java.util.Map;

import static info.scce.addlib.cudd.Cudd.*;
import static info.scce.addlib.utils.Conversions.*;

public class BDD extends DD<BDDManager, BDD> {

    public BDD(long ptr, BDDManager ddManager) {
        super(ptr, ddManager);
    }

    /* CUDD wrapper */

    public BDD not() {
        long resultPtr = Cudd_Not(ptr);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD ite(BDD t, BDD e) {
        assertEqualDDManager(t, e);
        long resultPtr = Cudd_bddIte(ddManager.ptr(), ptr, t.ptr, e.ptr);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD iteLimit(BDD t, BDD e, int limit) {
        assertEqualDDManager(t, e);
        long resultPtr = Cudd_bddIteLimit(ddManager.ptr(), ptr, t.ptr, e.ptr, limit);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD iteConstant(BDD t, BDD e) {
        assertEqualDDManager(t, e);
        long resultPtr = Cudd_bddIteConstant(ddManager.ptr(), ptr, t.ptr, e.ptr);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD intersect(BDD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_bddIntersect(ddManager.ptr(), ptr, g.ptr);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD and(BDD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_bddAnd(ddManager.ptr(), ptr, g.ptr);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD andLimit(BDD g, int limit) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_bddAndLimit(ddManager.ptr(), ptr, g.ptr, limit);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD or(BDD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_bddOr(ddManager.ptr(), ptr, g.ptr);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD orLimit(BDD g, int limit) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_bddOrLimit(ddManager.ptr(), ptr, g.ptr, limit);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD nand(BDD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_bddNand(ddManager.ptr(), ptr, g.ptr);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD nor(BDD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_bddNor(ddManager.ptr(), ptr, g.ptr);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD xor(BDD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_bddXor(ddManager.ptr(), ptr, g.ptr);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD xnor(BDD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_bddXnor(ddManager.ptr(), ptr, g.ptr);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD xnorLimit(BDD g, int limit) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_bddXnorLimit(ddManager.ptr(), ptr, g.ptr, limit);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public boolean leq(BDD g) {
        assertEqualDDManager(g);
        int result = Cudd_bddLeq(ddManager.ptr(), ptr, g.ptr);
        return asBoolean(result);
    }

    public BDD compose(BDD g, int v) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_bddCompose(ddManager.ptr(), ptr, g.ptr, v);
        return new BDD(resultPtr, ddManager).withRef();
    }

    public BDD vectorCompose(BDD... vector) {
        assertEqualDDManager(vector);
        long resultPtr = Cudd_bddVectorCompose(ddManager.ptr(), ptr, ptrs(vector));
        return new BDD(resultPtr, ddManager).withRef();
    }

    /* Required DD methods */

    @Override
    protected BDD thisCasted() {
        return this;
    }

    @Override
    public BDD t() {
        assertNonConstant();
        long resultPtr = Cudd_T(ptr);
        return new BDD(resultPtr, ddManager);
    }

    @Override
    public BDD e() {
        assertNonConstant();
        long resultPtr = Cudd_E(ptr);
        return new BDD(resultPtr, ddManager);
    }

    @Override
    public BDD eval(boolean... input) {
        long resultPtr = Cudd_Eval(ddManager.ptr(), ptr, asInts(input));
        return new BDD(resultPtr, ddManager);
    }

    /* Conversion to XDD */

    public XDD<Boolean> toXDD(BooleanLogicDDManager ddManagerTarget) {
        HashMap<BDD, XDD<Boolean>> cache = new HashMap<>();
        XDD<Boolean> result = this.toXDDRecursive(ddManagerTarget, cache);

        /* Dereference intermediate results */
        cache.remove(this);
        for (XDD<Boolean> g : cache.values())
            g.recursiveDeref();

        return result;
    }

    private XDD<Boolean> toXDDRecursive(BooleanLogicDDManager ddManagerTarget, Map<BDD, XDD<Boolean>> cache) {
        if (!cache.containsKey(this)) {
            XDD<Boolean> result;
            if (isConstant()) {
                BDD bddOne = ddManager.readOne();
                boolean value = this.equals(bddOne);
                result = ddManagerTarget.constant(value);
                bddOne.recursiveDeref();
            } else {
                String name = readName();
                int idx = ddManagerTarget.varIdx(name);
                XDD<Boolean> t = t().toXDDRecursive(ddManagerTarget, cache);
                XDD<Boolean> e = e().toXDDRecursive(ddManagerTarget, cache);
                if (asBoolean(Cudd_IsComplement(this.ptr))) {
                    XDD<Boolean> tmp = t;
                    t = e;
                    e = tmp;
                }
                result = ddManagerTarget.ithVar(idx, t, e);
            }
            cache.put(this, result);
        }
        return cache.get(this);
    }

    /* Other methods */

    @Override
    public String toString() {
        if (isConstant()) {
            BDD one = ddManager.readOne();
            String lbl = this.equals(one) ? "1" : "0";
            one.recursiveDeref();
            return lbl;
        }
        return super.toString();
    }
}
