package info.scce.addlib.dd.xdd.ringlikedd;

import info.scce.addlib.dd.xdd.XDDManager;

public abstract class SemiringDDManager<T> extends XDDManager<T> {

    public SemiringDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public SemiringDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public SemiringDDManager() {
        super();
    }

    @Override
    protected abstract T zeroElement();

    @Override
    protected abstract T oneElement();

    @Override
    protected abstract T mult(T left, T right);

    @Override
    protected abstract T add(T left, T right);
}
