package info.scce.addlib.dd.xdd.latticedd.example;

import info.scce.addlib.dd.xdd.latticedd.BooleanLatticeDDManager;


public class KleenePriestLogicDDManager extends BooleanLatticeDDManager<KleenePriestValue> {

    public KleenePriestLogicDDManager(int numVars, int numVarsZ, int numSlots, int cacheSize, long maxMemory) {
        super(numVars, numVarsZ, numSlots, cacheSize, maxMemory);
    }

    public KleenePriestLogicDDManager(int numVars, int numVarsZ, long maxMemory) {
        super(numVars, numVarsZ, maxMemory);
    }

    public KleenePriestLogicDDManager() {
        super();
    }

    @Override
    protected KleenePriestValue meet(KleenePriestValue left, KleenePriestValue right) {
        return left.and(right);
    }

    @Override
    protected KleenePriestValue join(KleenePriestValue left, KleenePriestValue right) {
        return left.or(right);
    }

    @Override
    protected KleenePriestValue botElement() {
        return KleenePriestValue.FALSE;
    }

    @Override
    protected KleenePriestValue topElement() {
        return KleenePriestValue.TRUE;
    }

    @Override
    protected KleenePriestValue compl(KleenePriestValue x) {
        return x.not();
    }

    @Override
    public KleenePriestValue parseElement(String str) {
        return KleenePriestValue.parseKleenePriestValue(str);
    }
}
