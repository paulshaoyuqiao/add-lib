package info.scce.addlib.dd.add;

import info.scce.addlib.cudd.DD_AOP;
import info.scce.addlib.cudd.DD_AOP_Fn;
import info.scce.addlib.cudd.DD_MAOP;
import info.scce.addlib.cudd.DD_MAOP_Fn;
import info.scce.addlib.dd.RegularDD;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.ringlikedd.example.ArithmeticDDManager;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

import static info.scce.addlib.cudd.Cudd.*;
import static info.scce.addlib.cudd.DD_AOP.*;
import static info.scce.addlib.cudd.DD_MAOP.Cudd_addLog;
import static info.scce.addlib.utils.Conversions.*;

public class ADD extends RegularDD<ADDManager, ADD> {

    public ADD(long ptr, ADDManager ddManager) {
        super(ptr, ddManager);
    }

    /* CUDD wrapper */

    public double v() {
        assertConstant();
        return Cudd_V(ptr);
    }

    public ADD ite(ADD t, ADD e) {
        assertEqualDDManager(t, e);
        long resultPtr = Cudd_addIte(ddManager.ptr(), ptr, t.ptr, e.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    public ADD iteConstant(ADD t, ADD e) {
        assertEqualDDManager(t, e);
        long resultPtr = Cudd_addIteConstant(ddManager.ptr(), ptr, t.ptr, e.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    public ADD evalConst(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_addEvalConst(ddManager.ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    public ADD cmpl() {
        long resultPtr = Cudd_addCmpl(ddManager.ptr(), ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    public boolean leq(ADD g) {
        assertEqualDDManager(g);
        return Cudd_addLeq(ddManager.ptr(), ptr, g.ptr) > 0;
    }

    public ADD compose(ADD g, int v) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_addCompose(ddManager.ptr(), ptr, g.ptr, v);
        return new ADD(resultPtr, ddManager).withRef();
    }

    public ADD vectorCompose(ADD... vector) {
        assertEqualDDManager(vector);
        long resultPtr = Cudd_addVectorCompose(ddManager.ptr(), ptr, ptrs(vector));
        return new ADD(resultPtr, ddManager).withRef();
    }

    public ADD plus(ADD g) {
        return apply(Cudd_addPlus, g);
    }

    public ADD times(ADD g) {
        return apply(Cudd_addTimes, g);
    }

    public ADD threshold(ADD g) {
        return apply(Cudd_addThreshold, g);
    }

    public ADD setNZ(ADD g) {
        return apply(Cudd_addSetNZ, g);
    }

    public ADD divide(ADD g) {
        return apply(Cudd_addDivide, g);
    }

    public ADD minus(ADD g) {
        return apply(Cudd_addMinus, g);
    }

    public ADD minimum(ADD g) {
        return apply(Cudd_addMinimum, g);
    }

    public ADD maximum(ADD g) {
        return apply(Cudd_addMaximum, g);
    }

    public ADD oneZeroMaximum(ADD g) {
        return apply(Cudd_addOneZeroMaximum, g);
    }

    public ADD diff(ADD g) {
        return apply(Cudd_addDiff, g);
    }

    public ADD agreement(ADD g) {
        return apply(Cudd_addAgreement, g);
    }

    public ADD or(ADD g) {
        return apply(Cudd_addOr, g);
    }

    public ADD nand(ADD g) {
        return apply(Cudd_addNand, g);
    }

    public ADD nor(ADD g) {
        return apply(Cudd_addNor, g);
    }

    public ADD xor(ADD g) {
        return apply(Cudd_addXor, g);
    }

    public ADD xnor(ADD g) {
        return apply(Cudd_addXnor, g);
    }

    public ADD log() {
        return monadicApply(Cudd_addLog);
    }

    private ADD apply(DD_AOP op, ADD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_addApply(ddManager.ptr(), op, ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    private ADD monadicApply(DD_MAOP op) {
        long resultPtr = Cudd_addMonadicApply(ddManager.ptr(), op, ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /* Non-trivial operations */

    public ADD apply(final BinaryOperator<Double> op, ADD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_addApply(ddManager.ptr(), new DD_AOP_Fn() {

            public long apply(long ddManager, long f, long g) {
                if (asBoolean(Cudd_IsConstant(f)) && asBoolean(Cudd_IsConstant(g))) {
                    double left = Cudd_V(f);
                    double right = Cudd_V(g);
                    try {
                        double result = op.apply(left, right);
                        return Cudd_addConst(ddManager, result);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return NULL;
                    }
                }
                return NULL;
            }
        }, ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    public ADD apply2(final BinaryOperator<ADD> op, final ADD g) {
        assertEqualDDManager(g);
        long resultPtr = Cudd_addApply(ddManager.ptr(), new DD_AOP_Fn() {

            public long apply(long ddManager, long fPtr, long gPtr) {
                ADD f = new ADD(fPtr, ADD.this.ddManager);
                ADD g = new ADD(gPtr, ADD.this.ddManager);
                try {
                    ADD result = op.apply(f, g);
                    return result == null ? NULL : result.ptr;
                } catch (Exception e) {
                    e.printStackTrace();
                    return NULL;
                }
            }
        }, ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    public ADD monadicApply(final UnaryOperator<Double> op) {
        long resultPtr = Cudd_addMonadicApply(ddManager.ptr(), new DD_MAOP_Fn() {

            public long apply(long ddManager, long f) {
                if (asBoolean(Cudd_IsConstant(f))) {
                    double x = Cudd_V(f);
                    try {
                        double result = op.apply(x);
                        return Cudd_addConst(ddManager, result);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return NULL;
                    }
                }
                return NULL;
            }
        }, ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    public ADD monadicApply2(final UnaryOperator<ADD> op) {
        long resultPtr = Cudd_addMonadicApply(ddManager.ptr(), new DD_MAOP_Fn() {

            public long apply(long ddManager, long fPtr) {
                ADD f = new ADD(fPtr, ADD.this.ddManager);
                try {
                    ADD result = op.apply(f);
                    return result == null ? NULL : result.ptr;
                } catch (Exception e) {
                    e.printStackTrace();
                    return NULL;
                }
            }
        }, ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /* Conversion to XDD */

    public XDD<Double> toXDD(ArithmeticDDManager ddManagerTarget) {
        HashMap<ADD, XDD<Double>> cache = new HashMap<>();
        XDD<Double> result = this.toXDDRecursive(ddManagerTarget, cache);

        /* Dereference intermediate results */
        cache.remove(this);
        for (XDD<Double> g : cache.values())
            g.recursiveDeref();

        return result;
    }

    private XDD<Double> toXDDRecursive(ArithmeticDDManager ddManagerTarget, Map<ADD, XDD<Double>> cache) {
        if (!cache.containsKey(this)) {
            XDD<Double> result;
            if (isConstant()) {
                result = ddManagerTarget.constant(v());
            } else {
                String name = readName();
                int idx = ddManagerTarget.varIdx(name);
                XDD<Double> t = t().toXDDRecursive(ddManagerTarget, cache);
                XDD<Double> e = e().toXDDRecursive(ddManagerTarget, cache);
                result = ddManagerTarget.ithVar(idx, t, e);
            }
            cache.put(this, result);
        }
        return cache.get(this);
    }

    /* Required DD methods */

    @Override
    protected ADD thisCasted() {
        return this;
    }

    @Override
    public ADD t() {
        assertNonConstant();
        long resultPtr = Cudd_T(ptr);
        return new ADD(resultPtr, ddManager);
    }

    @Override
    public ADD e() {
        assertNonConstant();
        long resultPtr = Cudd_E(ptr);
        return new ADD(resultPtr, ddManager);
    }

    @Override
    public ADD eval(boolean... input) {
        long resultPtr = Cudd_Eval(ddManager.ptr(), ptr, asInts(input));
        return new ADD(resultPtr, ddManager);
    }

    /* Other methods */

    @Override
    public String toString() {
        if (isConstant())
            return Double.toString(v());
        return super.toString();
    }
}



























