package info.scce.addlib.cudd;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.StandardCopyOption;

import static java.io.File.createTempFile;
import static java.nio.file.Files.copy;

public class NativeLibraryLoader {

    private final String nativeLibName;
    private boolean loaded;

    public NativeLibraryLoader(String nativeLibName) {
        this.nativeLibName = nativeLibName;
        this.loaded = false;
    }

    private static OSFamily nativeLibOSFamily() {
        String osName = System.getProperty("os.name").toLowerCase();

        /* Recognize OS family by keywords */
        if (osName.contains("linux"))
            return OSFamily.LINUX;
        else if (osName.contains("mac"))
            return OSFamily.MAC;
        else if (osName.contains("windows"))
            return OSFamily.WINDOWS;

        throw new NativeDependencyError("Unknown or unsupported OS family");
    }

    private static OSArch nativeLibOSArch() {
        String osArch = System.getProperty("os.arch").toLowerCase();

        /* Recognize OS architecture by keywords */
        if (osArch.contains("x86_64") || osArch.contains("amd64"))
            return OSArch.X64;

        throw new NativeDependencyError("Unknown or unsupported OS architecture");
    }

    public void ensureNativeLibraryLoaded() {
        if (!loaded) {
            loadNativeLibrary();
            loaded = true;
        }
    }

    private void loadNativeLibrary() {

        /* Find native library in the resources */
        String nativeLibResourceName = nativeLibResourceName();
        InputStream nativeLibIStream = getClass().getResourceAsStream(nativeLibResourceName);
        if (nativeLibIStream == null)
            throw new NativeDependencyError("Missing resource '" + nativeLibResourceName + "'");

        /* Copy native library to temporary file */
        File nativeLibTmpFile = nativeLibTmpFile();
        try {
            copy(nativeLibIStream, nativeLibTmpFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new NativeDependencyError("Failed to copy native library from resource '"
                    + nativeLibResourceName + "' to temporary file '" + nativeLibTmpFile.toPath() + "'", e);
        }

        /* Load native library from temporary file */
        System.load(nativeLibTmpFile.getAbsolutePath());
    }

    private String nativeLibResourceName() {

        /* Derive native library's resource filename from OS family */
        String filename;
        OSFamily osFamily = nativeLibOSFamily();
        if (osFamily == OSFamily.LINUX)
            filename = "lib" + nativeLibName + ".so";
        else if (osFamily == OSFamily.MAC)
            filename = "lib" + nativeLibName + ".dylib";
        else if (osFamily == OSFamily.WINDOWS)
            filename = nativeLibName + ".dll";
        else throw new NativeDependencyError("Could not determine native library resource name");

        OSArch osArch = nativeLibOSArch();
        String osPkg = osFamily.toString().toLowerCase();
        String archPkg = osArch.toString().toLowerCase();
        return "nativelib/" + osPkg + "/" + archPkg + "/" + filename;
    }

    private File nativeLibTmpFile() {

        /* Determine native library temporary file prefix  */
        String prefix;
        OSFamily osFamily = nativeLibOSFamily();
        if (osFamily == OSFamily.LINUX || osFamily == OSFamily.MAC)
            prefix = "lib" + nativeLibName;
        else if (osFamily == OSFamily.WINDOWS)
            prefix = nativeLibName;
        else throw new NativeDependencyError("Could not determine native library temporary filename prefix");

        /* Determine native library temporary file suffix  */
        String suffix;
        if (osFamily == OSFamily.LINUX)
            suffix = ".so";
        else if (osFamily == OSFamily.MAC)
            suffix = ".dylib";
        else if (osFamily == OSFamily.WINDOWS)
            suffix = ".dll";
        else throw new NativeDependencyError("Could not determine native library temporary filename suffix");

        /* This will fail on Windows because the DLL is still load when the attempt of deletion is made. Unfortunately,
         * there isn't an easy fix. */
        try {
            File tempFile = createTempFile(prefix, suffix);
            tempFile.deleteOnExit();
            return tempFile;
        } catch (IOException e) {
            throw new NativeDependencyError("Failed to create temporary file");
        }
    }

    private enum OSFamily {
        LINUX,
        MAC,
        WINDOWS;
    }

    private enum OSArch {
        X64;
    }
}
