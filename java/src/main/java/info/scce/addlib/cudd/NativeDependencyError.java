package info.scce.addlib.cudd;

public class NativeDependencyError extends Error {

    public NativeDependencyError(String msg) {
        super(withSystemProperties(msg));
    }

    public NativeDependencyError(String msg, Throwable cause) {
        super(withSystemProperties(msg), cause);
    }

    private static String withSystemProperties(String msg) {
        return msg + " (" + System.getProperty("os.name") + ", " + System.getProperty("os.arch") + ")";
    }
}
