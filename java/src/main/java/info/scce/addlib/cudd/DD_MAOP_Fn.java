package info.scce.addlib.cudd;

public abstract class DD_MAOP_Fn extends DD_Fn {

    public long applyAndPostponeRtException(long ddManager, long f) {
        try {
            return apply(ddManager, f);
        } catch (RuntimeException e) {
            postponeRtException(e);
            return f;
        }
    }

    public abstract long apply(long ddManager, long f);
}
