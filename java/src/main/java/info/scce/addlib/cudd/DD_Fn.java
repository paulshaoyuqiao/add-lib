package info.scce.addlib.cudd;

public abstract class DD_Fn {

    private static long nextId = 0;

    private long id_ = -1;
    private RuntimeException postponedRtException = null;

    /* Unique identifier for this Java callback to allow for caching in the native library */
    public long id() {
        if (id_ < 0)
            id_ = nextId++;
        return id_;
    }

    protected void postponeRtException(RuntimeException e) {
        postponedRtException = e;
    }

    public void rethrowPostponedRtExceptionIfAny() {
        if (postponedRtException != null)
            throw postponedRtException;
    }
}
