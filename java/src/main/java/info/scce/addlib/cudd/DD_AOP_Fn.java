package info.scce.addlib.cudd;

public abstract class DD_AOP_Fn extends DD_Fn {

    public long applyAndPostponeRtException(long ddManager, long f, long g) {
        try {
            return apply(ddManager, f, g);
        } catch (RuntimeException e) {
            postponeRtException(e);
            return f;
        }
    }

    public abstract long apply(long ddManager, long f, long g);
}
