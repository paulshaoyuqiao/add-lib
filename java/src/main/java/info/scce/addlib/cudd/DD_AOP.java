package info.scce.addlib.cudd;

public enum DD_AOP {

    Cudd_addPlus,
    Cudd_addTimes,
    Cudd_addThreshold,
    Cudd_addSetNZ,
    Cudd_addDivide,
    Cudd_addMinus,
    Cudd_addMinimum,
    Cudd_addMaximum,
    Cudd_addOneZeroMaximum,
    Cudd_addDiff,
    Cudd_addAgreement,
    Cudd_addOr,
    Cudd_addNand,
    Cudd_addNor,
    Cudd_addXor,
    Cudd_addXnor
}
