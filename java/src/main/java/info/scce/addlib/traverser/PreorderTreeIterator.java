package info.scce.addlib.traverser;

import info.scce.addlib.dd.DD;

import java.util.Collections;
import java.util.List;

public class PreorderTreeIterator<D extends DD<?, D>> extends RecursiveTreeIterator<D> {

    public PreorderTreeIterator(D root) {
        this(Collections.singletonList(root));
    }

    public PreorderTreeIterator(List<D> root) {
        super(root);
    }

    @Override
    protected D processDD(D f, int i) {
        switch (i) {
            case 0:
                return f;
            case 1:
                if (!f.isConstant())
                    pushDD(f.e());
                return null;
            case 2:
                if (!f.isConstant())
                    pushDD(f.t());
                return null;
            default:
                popDD();
                return null;
        }
    }
}
