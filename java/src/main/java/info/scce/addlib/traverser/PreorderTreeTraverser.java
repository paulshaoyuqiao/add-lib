package info.scce.addlib.traverser;

import info.scce.addlib.dd.DD;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PreorderTreeTraverser<D extends DD<?, D>> implements Iterable<D> {

    private final List<D> roots;

    public PreorderTreeTraverser(D root) {
        this(Collections.singletonList(root));
    }

    public PreorderTreeTraverser(List<D> roots) {
        this.roots = roots;
    }

    @Override
    public Iterator<D> iterator() {
        return new PreorderTreeIterator<>(roots);
    }
}
