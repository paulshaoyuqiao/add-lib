package info.scce.addlib.traverser;

import info.scce.addlib.dd.DD;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class RecursiveIterator<D extends DD<?, D>> extends RecursiveTreeIterator<D> {

    private final Set<D> completed;

    public RecursiveIterator(D root) {
        this(Collections.singletonList(root));
    }

    public RecursiveIterator(List<D> roots) {
        super(roots);
        completed = new HashSet<>();
    }

    @Override
    protected D processDD(D f, int i) {

        /* Ensure to process each node only if not completed */
        if (completed.contains(f)) {
            popDD();
            return null;
        }

        return processUnseenDD(f, i);
    }

    @Override
    protected D popDD() {

        /* Remember completed nodes */
        D f = super.popDD();
        completed.add(f);
        return f;
    }

    protected abstract D processUnseenDD(D f, int i);
}
