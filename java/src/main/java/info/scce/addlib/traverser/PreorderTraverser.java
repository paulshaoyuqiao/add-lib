package info.scce.addlib.traverser;

import info.scce.addlib.dd.DD;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PreorderTraverser<D extends DD<?, D>> implements Iterable<D> {

    private final List<D> roots;

    public PreorderTraverser(D root) {
        this(Collections.singletonList(root));
    }

    public PreorderTraverser(List<D> roots) {
        this.roots = roots;
    }

    @Override
    public Iterator<D> iterator() {
        return new PreorderIterator<>(roots);
    }
}
