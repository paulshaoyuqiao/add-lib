package info.scce.addlib.traverser;

import info.scce.addlib.dd.DD;

import java.util.Collections;
import java.util.List;

public class InorderTreeIterator<D extends DD<?, D>> extends RecursiveTreeIterator<D> {

    public InorderTreeIterator(D root) {
        this(Collections.singletonList(root));
    }

    public InorderTreeIterator(List<D> root) {
        super(root);
    }

    @Override
    protected D processDD(D f, int i) {
        switch (i) {
            case 0:
                if (!f.isConstant())
                    pushDD(f.e());
                return null;
            case 1:
                return f;
            case 2:
                if (!f.isConstant())
                    pushDD(f.t());
                return null;
            default:
                popDD();
                return null;
        }
    }

}
