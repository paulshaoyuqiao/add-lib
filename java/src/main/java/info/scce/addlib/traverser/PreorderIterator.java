package info.scce.addlib.traverser;

import info.scce.addlib.dd.DD;

import java.util.Collections;
import java.util.List;

public class PreorderIterator<D extends DD<?, D>> extends RecursiveIterator<D> {

    public PreorderIterator(D root) {
        this(Collections.singletonList(root));
    }

    public PreorderIterator(List<D> root) {
        super(root);
    }

    @Override
    protected D processUnseenDD(D f, int i) {
        switch (i) {
            case 0:
                return f;
            case 1:
                if (!f.isConstant())
                    pushDD(f.e());
                return null;
            case 2:
                if (!f.isConstant())
                    pushDD(f.t());
                return null;
            default:
                popDD();
                return null;
        }
    }
}
