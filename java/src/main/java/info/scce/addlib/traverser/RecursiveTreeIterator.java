package info.scce.addlib.traverser;

import info.scce.addlib.dd.DD;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public abstract class RecursiveTreeIterator<D extends DD<?, D>> implements Iterator<D> {

    private final Iterator<D> roots;
    private final Stack<Call> callStack;
    private D buffer = null;
    public RecursiveTreeIterator(D root) {
        this(Collections.singletonList(root));
    }

    public RecursiveTreeIterator(List<D> roots) {
        this.roots = roots.iterator();
        this.callStack = new Stack<>();
    }

    protected void pushDD(D dd) {
        Call call = new Call(dd);
        callStack.push(call);
    }

    protected D popDD() {
        return callStack.pop().dd();
    }

    @Override
    public boolean hasNext() {
        return fillBuffer() != null;
    }

    @Override
    public D next() {
        D bufferCpy = fillBuffer();
        buffer = null;
        return bufferCpy;
    }

    private D fillBuffer() {
        while (buffer == null && (!callStack.isEmpty() || roots.hasNext())) {

            /* Move along the roots when one was completed */
            if (callStack.isEmpty() && roots.hasNext())
                pushDD(roots.next());

            /* Process calls until buffer filled */
            while (buffer == null && !callStack.isEmpty()) {
                Call top = callStack.peek();
                buffer = processDD(top.dd(), top.i());
                top.inc();
            }
        }
        return buffer;
    }

    protected abstract D processDD(D f, int i);

    protected class Call {

        private final D dd;
        private int i;

        public Call(D dd) {
            this.dd = dd;
            this.i = 0;
        }

        public D dd() {
            return dd;
        }

        public int i() {
            return i;
        }

        public void inc() {
            i++;
        }
    }
}
