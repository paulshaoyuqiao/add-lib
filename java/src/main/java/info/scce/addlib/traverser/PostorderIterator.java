package info.scce.addlib.traverser;

import info.scce.addlib.dd.DD;

import java.util.Collections;
import java.util.List;

public class PostorderIterator<D extends DD<?, D>> extends RecursiveIterator<D> {

    public PostorderIterator(D root) {
        this(Collections.singletonList(root));
    }

    public PostorderIterator(List<D> root) {
        super(root);
    }

    @Override
    protected D processUnseenDD(D f, int i) {
        switch (i) {
            case 0:
                if (!f.isConstant())
                    pushDD(f.e());
                return null;
            case 1:
                if (!f.isConstant())
                    pushDD(f.t());
                return null;
            case 2:
                return f;
            default:
                popDD();
                return null;
        }
    }

}
