package info.scce.addlib.traverser;

import info.scce.addlib.dd.DD;

import java.util.Collections;
import java.util.List;

public class InorderIterator<D extends DD<?, D>> extends RecursiveIterator<D> {

    public InorderIterator(D root) {
        this(Collections.singletonList(root));
    }

    public InorderIterator(List<D> root) {
        super(root);
    }

    @Override
    protected D processUnseenDD(D f, int i) {
        switch (i) {
            case 0:
                if (!f.isConstant())
                    pushDD(f.e());
                return null;
            case 1:
                return f;
            case 2:
                if (!f.isConstant())
                    pushDD(f.t());
                return null;
            default:
                popDD();
                return null;
        }
    }

}
