package info.scce.addlib.traverser;

import info.scce.addlib.dd.DD;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PostorderTraverser<D extends DD<?, D>> implements Iterable<D> {

    private final List<D> roots;

    public PostorderTraverser(D root) {
        this(Collections.singletonList(root));
    }

    public PostorderTraverser(List<D> roots) {
        this.roots = roots;
    }

    @Override
    public Iterator<D> iterator() {
        return new PostorderIterator<>(roots);
    }
}