package info.scce.addlib.parser;

import info.scce.addlib.dd.bdd.BDD;
import info.scce.addlib.dd.bdd.BDDManager;

public class BDDVisitor extends BDDLanguageBaseVisitor<BDD> {

    private BDDManager ddManager;

    public BDDVisitor(BDDManager ddManager) {
        this.ddManager = ddManager;
    }

    @Override
    public BDD visitNotExpr(BDDLanguageParser.NotExprContext ctx) {

        /* Visit children and apply operation */
        BDD child = visit(ctx.ex);
        BDD result = child.not();

        /* Release memory */
        child.recursiveDeref();
        return result;
    }

    @Override
    public BDD visitAtomExpr(BDDLanguageParser.AtomExprContext ctx) {
        return visit(ctx.ap);
    }

    @Override
    public BDD visitOrExpr(BDDLanguageParser.OrExprContext ctx) {

        /* Visit children and apply operation */
        BDD left = visit(ctx.left);
        BDD right = visit(ctx.right);
        BDD result = left.or(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public BDD visitParenExpr(BDDLanguageParser.ParenExprContext ctx) {
        return visit(ctx.ex);
    }

    @Override
    public BDD visitAndExpr(BDDLanguageParser.AndExprContext ctx) {

        /* Visit children and apply operation */
        BDD left = visit(ctx.left);
        BDD right = visit(ctx.right);
        BDD result = left.and(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public BDD visitTrueExpr(BDDLanguageParser.TrueExprContext ctx) {
        return ddManager.readOne();
    }

    @Override
    public BDD visitFalseExpr(BDDLanguageParser.FalseExprContext ctx) {
        return ddManager.readLogicZero();
    }

    @Override
    public BDD visitVarExpr(BDDLanguageParser.VarExprContext ctx) {
        String varName = ctx.var.getText();
        return ddManager.namedVar(varName);
    }
}
