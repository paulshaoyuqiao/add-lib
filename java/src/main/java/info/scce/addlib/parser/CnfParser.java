package info.scce.addlib.parser;

import info.scce.addlib.dd.bdd.BDD;
import info.scce.addlib.dd.bdd.BDDManager;

import java.io.*;
import java.util.Scanner;

public class CnfParser {

    public BDD parseCnf(BDDManager manager, String str) {
        StringReader sr = new StringReader(str);
        Scanner sc = new Scanner(sr);
        return parseCnf(manager, sc);
    }
    
    public BDD parseCnf(BDDManager manager, File file) throws FileNotFoundException {
        Scanner sc = new Scanner(file);
        return parseCnf(manager, sc);
    }

    public BDD parseCnf(BDDManager manager, InputStream in) {
        Scanner sc = new Scanner(in);
        return parseCnf(manager, sc);
    }

    private BDD parseCnf(BDDManager manager, Scanner sc) {
        BDD newret;
        BDD ret = manager.readOne();
        while (sc.hasNextLine()) {
            String current = sc.nextLine();
            if (!current.startsWith("c") && !current.startsWith("p")) {
                if (current.startsWith("%")) {
                    break;
                }
                BDD clause = manager.readLogicZero();
                BDD newclause;
                String[] vars = current.split(" ");
                for (int i = 0; i < vars.length; i++) {
                    if (vars[i].equals("0")) {
                        break;
                    }
                    if (vars[i].equals("")) continue;
                    BDD lit;
                    if (vars[i].startsWith("-")) {
                        lit = manager.ithVar(Integer.parseInt(vars[i].substring(1)));
                        BDD newlit = lit.not();
                        lit.recursiveDeref();
                        lit = newlit;
                    } else {
                        lit = manager.ithVar(Integer.parseInt(vars[i]));
                    }

                    newclause = clause.or(lit);
                    clause.recursiveDeref();
                    clause = newclause;
                    lit.recursiveDeref();
                }

                newret = ret.and(clause);
                ret.recursiveDeref();
                ret = newret;
                clause.recursiveDeref();
            }
        }
        return ret;
    }
}
