package info.scce.addlib.parser;

import info.scce.addlib.dd.zdd.ZDD;
import info.scce.addlib.dd.zdd.ZDDManager;

public class ZDDVisitor extends ZDDLanguageBaseVisitor<ZDD> {

    private ZDDManager ddManager;

    @Override
    public ZDD visitStart(ZDDLanguageParser.StartContext ctx) {
        return visit(ctx.ex);
    }

    public ZDDVisitor(ZDDManager ddManager) {
        this.ddManager = ddManager;
    }

    @Override
    public ZDD visitVarExpr(ZDDLanguageParser.VarExprContext ctx) {
        String varName = ctx.var.getText();
        return ddManager.namedVar(varName);
    }

    @Override
    public ZDD visitZddOneExpr(ZDDLanguageParser.ZddOneExprContext ctx) {
        int i = Integer.parseInt(ctx.i.getText());
        return ddManager.readZddOne(i);
    }

    @Override
    public ZDD visitNotExpr(ZDDLanguageParser.NotExprContext ctx) {

        /* Visit children and apply operation */
        ZDD left = visit(ctx.left);
        ZDD right = visit(ctx.right);
        ZDD result = left.diff(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public ZDD visitAtomExpr(ZDDLanguageParser.AtomExprContext ctx) {
        return visit(ctx.ap);
    }

    @Override
    public ZDD visitChangeExpr(ZDDLanguageParser.ChangeExprContext ctx) {

        /* Visit children and apply operation */
        ZDD child = visit(ctx.ex);
        int var = Integer.parseInt(ctx.v.getText());
        ZDD result = child.change(var);

        /* Release memory */
        child.recursiveDeref();
        return result;
    }

    @Override
    public ZDD visitOrExpr(ZDDLanguageParser.OrExprContext ctx) {

        /* Visit children and apply operation */
        ZDD left = visit(ctx.left);
        ZDD right = visit(ctx.right);
        ZDD result = left.union(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public ZDD visitParenExpr(ZDDLanguageParser.ParenExprContext ctx) {
        return visit(ctx.ex);
    }

    @Override
    public ZDD visitAndExpr(ZDDLanguageParser.AndExprContext ctx) {

        /* Visit children and apply operation */
        ZDD left = visit(ctx.left);
        ZDD right = visit(ctx.right);
        ZDD result = left.intersect(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public ZDD visitTrueExpr(ZDDLanguageParser.TrueExprContext ctx) {
        return ddManager.readOne();
    }

    @Override
    public ZDD visitFalseExpr(ZDDLanguageParser.FalseExprContext ctx) {
        return ddManager.readZero();
    }

}
