package info.scce.addlib.parser;

import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.add.ADDManager;

public class ADDVisitor extends ADDLanguageBaseVisitor<ADD> {

    private ADDManager ddManager;

    public ADDVisitor(ADDManager ddManager) {
        this.ddManager = ddManager;
    }

    @Override
    public ADD visitVarExpr(ADDLanguageParser.VarExprContext ctx) {
        String varName = ctx.var.getText();
        return ddManager.namedVar(varName);
    }

    @Override
    public ADD visitRealExpr(ADDLanguageParser.RealExprContext ctx) {
        double value = Double.parseDouble(ctx.real.getText());
        return ddManager.constant(value);
    }

    @Override
    public ADD visitPlusMinusExpr(ADDLanguageParser.PlusMinusExprContext ctx) {

        /* Visit children */
        ADD left = visit(ctx.left);
        ADD right = visit(ctx.right);

        /* Apply operation */
        ADD result;
        char op = ctx.op.getText().charAt(0);
        if (op == '+')
            result = left.plus(right);
        else if (op == '-')
            result = left.minus(right);
        else
            throw new RuntimeException("unexpected operator '" + op + "'");

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public ADD visitMulDivExpr(ADDLanguageParser.MulDivExprContext ctx) {

        /* Visit children */
        ADD left = visit(ctx.left);
        ADD right = visit(ctx.right);
        ADD result;

        /* Apply operation */
        char op = ctx.op.getText().charAt(0);
        if (op == '*')
            result = left.times(right);
        else if (op == '/')
            result = left.divide(right);
        else
            throw new RuntimeException("unexpected operator '" + op + "'");

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public ADD visitParenExpr(ADDLanguageParser.ParenExprContext ctx) {
        return visit(ctx.ex);
    }
}
