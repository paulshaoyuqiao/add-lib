package info.scce.addlib.parser;

import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.XDDManager;

public class XDDVisitor<E> extends XDDLanguageBaseVisitor<XDD<E>> {

    private XDDManager<E> ddManager;

    public XDDVisitor(XDDManager<E> ddManager) {
        this.ddManager = ddManager;
    }

    @Override
    public XDD<E> visitStart(XDDLanguageParser.StartContext ctx) {
        return visit(ctx.ex);
    }

    @Override
    public XDD<E> visitInverseExpr(XDDLanguageParser.InverseExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> child = visit(ctx.ex);
        XDD<E> result = child.inverse();

        /* Release memory */
        child.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitComplExpr(XDDLanguageParser.ComplExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> child = visit(ctx.ex);
        XDD<E> result = child.compl();

        /* Release memory */
        child.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitNotExpr(XDDLanguageParser.NotExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> child = visit(ctx.ex);
        XDD<E> result = child.not();

        /* Release memory */
        child.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitMeetExpr(XDDLanguageParser.MeetExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> left = visit(ctx.left);
        XDD<E> right = visit(ctx.right);
        XDD<E> result = left.meet(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitInfExpr(XDDLanguageParser.InfExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> left = visit(ctx.left);
        XDD<E> right = visit(ctx.right);
        XDD<E> result = left.inf(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitIntersectExpr(XDDLanguageParser.IntersectExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> left = visit(ctx.left);
        XDD<E> right = visit(ctx.right);
        XDD<E> result = left.intersect(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitAndExpr(XDDLanguageParser.AndExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> left = visit(ctx.left);
        XDD<E> right = visit(ctx.right);
        XDD<E> result = left.and(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitMultExpr(XDDLanguageParser.MultExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> left = visit(ctx.left);
        XDD<E> right = visit(ctx.right);
        XDD<E> result = left.mult(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitJoinExpr(XDDLanguageParser.JoinExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> left = visit(ctx.left);
        XDD<E> right = visit(ctx.right);
        XDD<E> result = left.join(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitSupExpr(XDDLanguageParser.SupExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> left = visit(ctx.left);
        XDD<E> right = visit(ctx.right);
        XDD<E> result = left.sup(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitUnionExpr(XDDLanguageParser.UnionExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> left = visit(ctx.left);
        XDD<E> right = visit(ctx.right);
        XDD<E> result = left.union(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitOrExpr(XDDLanguageParser.OrExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> left = visit(ctx.left);
        XDD<E> right = visit(ctx.right);
        XDD<E> result = left.or(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitAddExpr(XDDLanguageParser.AddExprContext ctx) {

        /* Visit children and apply operation */
        XDD<E> left = visit(ctx.left);
        XDD<E> right = visit(ctx.right);
        XDD<E> result = left.add(right);

        /* Release memory */
        left.recursiveDeref();
        right.recursiveDeref();
        return result;
    }

    @Override
    public XDD<E> visitTerminalExpr(XDDLanguageParser.TerminalExprContext ctx) {
        String str = unescapeString(ctx.getText());
        E terminal = ddManager.parseElement(str);
        return ddManager.constant(terminal);
    }

    private String unescapeString(String str) {
        if (str.length() > 2) {
            char charFst = str.charAt(0);
            char charLst = str.charAt(str.length() - 1);
            String strBody = str.substring(1, str.length() - 1);
            if (charFst == charLst) {
                if (charFst == '"')
                    return strBody.replace("\\\"", "\"");
                else if (charFst == '\'')
                    return strBody.replace("\\'", "'");
            }
        }
        return null;
    }
}
