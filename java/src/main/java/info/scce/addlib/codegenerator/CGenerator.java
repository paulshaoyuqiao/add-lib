/* This file was generated with the ADD-Lib Gen Tools
 * http://add-lib.scce.info/ */

package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.RegularDD;
import info.scce.addlib.traverser.PreorderTraverser;

import java.io.PrintStream;
import java.util.List;

public class CGenerator<D extends RegularDD<?, D>> extends CodeGenerator<D> {

    private String paramParamType = "Predicates";

    public CGenerator<D> withParamType(String par) {
        this.paramParamType = par;
        return this;
    }

    @Override
    public void generate(PrintStream out, List<LabelledRegularDD<D>> roots) {
        out.println("/* This file was generated with the ADD-Lib");
        out.println(" * http://add-lib.scce.info/ */");
        out.println("");
        out.println("");
        out.println("Predicates predicates;");
        out.println("char* returnstr;");
        out.println("");
        for (LabelledRegularDD<D> x0 : roots) {
            returnType(out);
            out.print(" ");
            out.print(x0.label());
            out.print("(");
            paramList(out);
            out.println(");");
        }
        out.println("");
        out.println("");
        for (LabelledRegularDD<D> x1 : roots) {
            out.println("");
            returnType(out);
            out.print(" ");
            out.print(x1.label());
            out.print("(");
            paramList(out);
            out.println(") {");
            out.println("    predicates = pPredicates;");
            out.print("    goto eval");
            out.print(x1.dd().ptr());
            out.println(";");
            out.println("    end:");
            out.println("    return returnstr;");
            out.println("");
            for (D x2 : new PreorderTraverser<D>(unlabelledRoots(roots))) {
                if (!x2.isConstant()) {
                    out.println("");
                    out.print("    eval");
                    out.print(x2.ptr());
                    out.println(":");
                    out.println("    ");
                    out.print("    if (");
                    varCondition(out, x1, x2);
                    out.println("){");
                    out.print("        goto eval");
                    out.print(x2.t().ptr());
                    out.println(";");
                    out.println("    }else{");
                    out.print("        goto eval");
                    out.print(x2.e().ptr());
                    out.println(";");
                    out.println("    }");
                }
            }
            for (D x3 : new PreorderTraverser<D>(unlabelledRoots(roots))) {
                if (x3.isConstant()) {
                    out.println("");
                    out.print("    eval");
                    out.print(x3.ptr());
                    out.println(":");
                    out.println("");
                    out.print("    returnstr = ");
                    resultInstatiation(out, x1, x3);
                    out.println(";");
                    out.println("    goto end;");
                    out.println("");
                }
            }
            out.println("}");
        }
        out.println("");
    }

    protected void paramList(PrintStream out) {
        if (this.paramParamType != null)
            out.print(this.paramParamType);
        out.print(" pPredicates");
    }

    protected void returnType(PrintStream out) {
        out.print("char*");
    }

    protected void resultInstatiation(PrintStream out, LabelledRegularDD<D> x1, D x3) {
        out.print("\"");
        out.print(x3.toString());
        out.print("\"");
    }

    protected void varCondition(PrintStream out, LabelledRegularDD<D> x1, D x2) {
        out.print("predicates.");
        out.print(x2.readName());
        out.print("()");
    }
}