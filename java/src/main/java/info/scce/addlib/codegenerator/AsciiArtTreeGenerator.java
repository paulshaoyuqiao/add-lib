package info.scce.addlib.codegenerator;

import com.google.common.base.Strings;
import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.RegularDD;
import info.scce.addlib.traverser.InorderTraverser;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;

public class AsciiArtTreeGenerator<D extends RegularDD<?, D>> extends CodeGenerator<D> {

    private String space;
    private String econnect;
    private String tconnect;
    private List<List<String>> table;



    @Override
    public void generate(PrintStream out, List<LabelledRegularDD<D>> roots) {

        D root = roots.get(0).dd();
        table = new ArrayList<>();

        int stringLength = findMaxLabelLength(root);

        //spaceholders proportional to boxLength
        space = Strings.repeat(" ", stringLength);
        tconnect = Strings.repeat("-", stringLength);
        econnect = Strings.repeat(" ", stringLength / 2) + ":" +
                Strings.repeat(" ", (stringLength + 1) / 2 - 1);
        fillTable(0,0,root,stringLength);
        printTable(out);
    }

    //return value is the amount of space between parent and true child
    public int fillTable(int row, int col, D root, int stringLength){
        int spaceRight=1;
        while (table.size() <= row) {
            table.add(new ArrayList<>());
        }
        while(table.get(row).size()<=col){
            table.get(row).add(space);
        }
        if(!root.isConstant()){
            table.get(row).set(col,center(root.readName(),stringLength));
            while (table.size() <= row+1) {
                table.add(new ArrayList<>());
            }
            while(table.get(row+1).size()<=col){
                table.get(row+1).add(space);
            }
            table.get(row+1).set(col,econnect);
            spaceRight = fillTable(row+2,col,root.e(),stringLength);
            for(int i=0;i<spaceRight;i++)
                table.get(row).add(tconnect);
            spaceRight += fillTable(row,col+spaceRight+1,root.t(),stringLength)+1;
        }else{
            table.get(row).set(col,center(root.toString(),stringLength));
        }
        return spaceRight;
    }

    public void printTable(PrintStream out){
        for(List<String> row:table){
            for(String s:row){
                out.print(s);
            }
            out.println();
        }
    }

    private int findMaxLabelLength(D f) {
        int length = 0;
        for (D g : new InorderTraverser<D>(f)) {
            if (g.isConstant()) length = max(length, g.toString().length());
            else length = max(length, g.readName().length());
        }
        return length;
    }

    public static String center(String text, int len){
        if (len <= text.length())
            return text.substring(0, len);
        int before = (len - text.length())/2;
        if (before == 0)
            return String.format("%-" + len + "s", text);
        int rest = len - before;
        return String.format("%" + before + "s%-" + rest + "s", "", text);
    }
}
