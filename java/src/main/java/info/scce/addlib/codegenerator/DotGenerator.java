/* This file was generated with the ADD-Lib Gen Tools
 * http://add-lib.scce.info/ */

package info.scce.addlib.codegenerator;

import java.io.PrintStream;
import java.util.List;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.RegularDD;
import info.scce.addlib.traverser.PreorderTraverser;

public class DotGenerator<D extends RegularDD<?, D>> extends CodeGenerator<D> {

    private String paramGraphName = "ADDLibDecisionService";

    public DotGenerator<D> withGraphName(String gra) {
        this.paramGraphName = gra;
        return this;
    }

    @Override
    public void generate(PrintStream out, List<LabelledRegularDD<D>> roots) {
        out.println("/* This file was generated with the ADD-Lib");
        out.println(" * http://add-lib.scce.info/ */");
        out.println("");
        out.print("digraph \"");
        if (this.paramGraphName != null)
            out.print(this.paramGraphName);
        out.println("\" {");
        out.println("");
        out.println("    bgcolor = transparent");
        out.print("    node [");
        nodeStyle(out);
        out.println("]");
        out.print("    edge [");
        edgeStyle(out);
        out.println("]");
        for (LabelledRegularDD<D> x0 : roots) {
            out.println("");
            out.print("    \"f");
            out.print(x0.dd().ptr());
            out.print("\" [");
            ddFunctionStyle(out, x0);
            out.print(", label = \"");
            out.print(x0.label());
            out.println("\"]");
            out.print("    \"f");
            out.print(x0.dd().ptr());
            out.print("\" -> \"n");
            out.print(x0.dd().ptr());
            out.print("\" [");
            ddFunctionThenStyle(out, x0);
            out.println("]");
        }
        for (D x1 : new PreorderTraverser<D>(unlabelledRoots(roots))) {
            if (!x1.isConstant()) {
                out.println("");
                out.print("    \"n");
                out.print(x1.ptr());
                out.print("\" [");
                internalDdStyle(out, x1);
                out.print(", label = \"");
                internalDdLabel(out, x1);
                out.println("\"]");
                out.print("    \"n");
                out.print(x1.ptr());
                out.print("\" -> \"n");
                out.print(x1.e().ptr());
                out.print("\" [");
                internalDdElseStyle(out, x1);
                out.println("]");
                out.print("    \"n");
                out.print(x1.ptr());
                out.print("\" -> \"n");
                out.print(x1.t().ptr());
                out.print("\" [");
                internalDdThenStyle(out, x1);
                out.println("]");
            }
        }
        for (D x2 : new PreorderTraverser<D>(unlabelledRoots(roots))) {
            if (x2.isConstant()) {
                out.println("");
                out.print("    \"n");
                out.print(x2.ptr());
                out.print("\" [");
                constantDdStyle(out, x2);
                out.print(", label = \"");
                constantDdLabel(out, x2);
                out.println("\"]");
            }
        }
        out.println("}");
    }

    protected void internalDdThenStyle(PrintStream out, D x1) {
        out.print("style = solid");
    }

    protected void internalDdElseStyle(PrintStream out, D x1) {
        out.print("style = dashed");
    }

    protected void ddFunctionStyle(PrintStream out, LabelledRegularDD<D> x0) {
        out.print("fillcolor = lightgray, shape = rectangle");
    }

    protected void constantDdLabel(PrintStream out, D x2) {
        out.print(x2.toString());
    }

    protected void ddFunctionThenStyle(PrintStream out, LabelledRegularDD<D> x0) {
        out.print("style = solid");
    }

    protected void internalDdStyle(PrintStream out, D x1) {
        out.print("fillcolor = white, shape = ellipse");
    }

    protected void edgeStyle(PrintStream out) {
        out.print("arrowhead = none");
    }

    protected void nodeStyle(PrintStream out) {
        out.print("style = filled, fontsize = 14, fontname = Arial");
    }

    protected void internalDdLabel(PrintStream out, D x1) {
        out.print(x1.readName());
    }

    protected void constantDdStyle(PrintStream out, D x2) {
        out.print("fillcolor = white, shape = rect");
    }
}
