/* This file was generated with the ADD-Lib Gen Tools
 * http://add-lib.scce.info/ */

package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.RegularDD;
import info.scce.addlib.traverser.PreorderTraverser;

import java.io.PrintStream;
import java.util.List;

public class CPPGenerator<D extends RegularDD<?, D>> extends CodeGenerator<D> {

    private String paramParamType = "Predicates";
    private String paramClassName = "ADDLibDecisionService";
    private boolean enableableParamStaticMethods;

    public CPPGenerator<D> withParamType(String par) {
        this.paramParamType = par;
        return this;
    }

    public CPPGenerator<D> withClassName(String cla) {
        this.paramClassName = cla;
        return this;
    }

    public CPPGenerator<D> withStaticMethodsEnabled(boolean sta) {
        this.enableableParamStaticMethods = sta;
        return this;
    }

    public CPPGenerator<D> withStaticMethodsEnabled() {
        return withStaticMethodsEnabled(true);
    }

    public CPPGenerator<D> withStaticMethodsDisabled() {
        return withStaticMethodsEnabled(false);
    }

    @Override
    public void generate(PrintStream out, List<LabelledRegularDD<D>> roots) {
        out.println("/* This file was generated with the ADD-Lib Gen Tools");
        out.println(" * http://add-lib.scce.info/ */");
        out.println("");
        out.print("class ");
        if (this.paramClassName != null)
            out.print(this.paramClassName);
        out.println(" {");
        out.println("public:");
        for (LabelledRegularDD<D> x0 : roots) {
            out.println("");
            if (this.enableableParamStaticMethods) {
                out.print("static ");
            }
            returnType(out, x0);
            out.print(" ");
            out.print(x0.label());
            out.print("(");
            paramList(out, x0);
            out.println("){");
            out.println("    char const* result;");
            out.println("    Predicates predicates = pPredicates;");
            out.print("    goto eval");
            out.print(x0.dd().ptr());
            out.println(";");
            out.println("    end:");
            out.println("    return result;");
            out.println("");
            out.print("    ");
            for (D x1 : new PreorderTraverser<D>(unlabelledRoots(roots))) {
                if (!x1.isConstant()) {
                    out.println("");
                    out.print("    eval");
                    out.print(x1.ptr());
                    out.println(":");
                    out.print("    if (");
                    varCondition(out, x0, x1);
                    out.println(")");
                    out.print("        goto eval");
                    out.print(x1.t().ptr());
                    out.println(";");
                    out.println("    else");
                    out.print("        goto eval");
                    out.print(x1.e().ptr());
                    out.println(";");
                    out.println("");
                    out.print("    ");
                }
            }
            out.print("    ");
            for (D x2 : new PreorderTraverser<D>(unlabelledRoots(roots))) {
                if (x2.isConstant()) {
                    out.println("");
                    out.print("    eval");
                    out.print(x2.ptr());
                    out.println(":");
                    out.print("    result = ");
                    resultInstatiation(out, x0, x2);
                    out.println(";");
                    out.println("    goto end;");
                    out.println("");
                    out.print("    ");
                }
            }
            out.println("}");
        }
        out.println("");
        out.println("};");
    }

    protected void paramList(PrintStream out, LabelledRegularDD<D> x0) {
        if (this.paramParamType != null)
            out.print(this.paramParamType);
        out.print(" pPredicates");
    }

    protected void resultInstatiation(PrintStream out, LabelledRegularDD<D> x0, D x2) {
        out.print("\"");
        out.print(x2.toString());
        out.print("\"");
    }

    protected void varCondition(PrintStream out, LabelledRegularDD<D> x0, D x1) {
        out.print("predicates.");
        out.print(x1.readName());
        out.print("()");
    }

    protected void returnType(PrintStream out, LabelledRegularDD<D> x0) {
        out.print("char const*");
    }
}