package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.RegularDD;

import java.io.*;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

public abstract class CodeGenerator<D extends RegularDD<?, D>> {

    /* Generate to string */

    public String generateToString(D root, String label) {
        return generateToString(new LabelledRegularDD<>(root, label));
    }

    public String generateToString(LabelledRegularDD<D> root) {
        return generateToString(Collections.singletonList(root));
    }

    public String generateToString(List<LabelledRegularDD<D>> roots) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String encoding = "utf-8";
        generate(baos, roots);
        return baos.toString();
    }

    /* Generate to file system */

    public void generateToFileSystem(File target, D root, String label) throws FileNotFoundException {
        generateToFileSystem(target, new LabelledRegularDD<>(root, label));
    }

    public void generateToFileSystem(File target, String cs, D root, String label)
            throws FileNotFoundException, UnsupportedEncodingException {

        generateToFileSystem(target, cs, new LabelledRegularDD<>(root, label));
    }

    public void generateToFileSystem(File target, LabelledRegularDD<D> root) throws FileNotFoundException {
        generateToFileSystem(target, Collections.singletonList(root));
    }

    public void generateToFileSystem(File target, String cs, LabelledRegularDD<D> root)
            throws FileNotFoundException, UnsupportedEncodingException {

        generateToFileSystem(target, cs, Collections.singletonList(root));
    }

    public void generateToFileSystem(File target, List<LabelledRegularDD<D>> roots) throws FileNotFoundException {
        PrintStream ps = new PrintStream(target);
        generate(ps, roots);
        ps.close();
    }

    public void generateToFileSystem(File target, String cs, List<LabelledRegularDD<D>> roots)
            throws FileNotFoundException, UnsupportedEncodingException {

        PrintStream ps = new PrintStream(target, cs);
        generate(ps, roots);
        ps.close();
    }

    /* Generate to standard output stream */

    public void generateToStdOut(D root, String label) {
        generateToStdOut(new LabelledRegularDD<>(root, label));
    }

    public void generateToStdOut(String cs, D root, String label) throws UnsupportedEncodingException {
        generateToStdOut(cs, new LabelledRegularDD<>(root, label));
    }

    public void generateToStdOut(LabelledRegularDD<D> root) {
        generateToStdOut(Collections.singletonList(root));
    }

    public void generateToStdOut(String cs, LabelledRegularDD<D> root) throws UnsupportedEncodingException {
        generateToStdOut(cs, Collections.singletonList(root));
    }

    public void generateToStdOut(List<LabelledRegularDD<D>> roots) {
        generate(System.out, roots);
        System.out.flush();
    }

    public void generateToStdOut(String cs, List<LabelledRegularDD<D>> roots) throws UnsupportedEncodingException {
        generate(System.out, cs, roots);
        System.out.flush();
    }

    /* Generate to output stream */

    public void generate(OutputStream out, D root, String label) {
        generate(out, new LabelledRegularDD<>(root, label));
    }

    public void generate(OutputStream out, String cs, D root, String label) throws UnsupportedEncodingException {
        generate(out, cs, new LabelledRegularDD<>(root, label));
    }

    public void generate(OutputStream out, LabelledRegularDD<D> root) {
        generate(out, Collections.singletonList(root));
    }

    public void generate(OutputStream out, String cs, LabelledRegularDD<D> root) throws UnsupportedEncodingException {
        generate(out, cs, Collections.singletonList(root));
    }

    public void generate(OutputStream out, List<LabelledRegularDD<D>> roots) {
        PrintStream ps = new PrintStream(out, true);
        generate(ps, roots);
        ps.flush();
    }

    public void generate(OutputStream out, String cs, List<LabelledRegularDD<D>> roots)
            throws UnsupportedEncodingException {

        PrintStream ps = new PrintStream(out, true, cs);
        generate(ps, roots);
        ps.flush();
    }

    /* Required generate method */

    public abstract void generate(PrintStream out, List<LabelledRegularDD<D>> roots);

    /* Helper methods for subclasses */

    protected List<D> unlabelledRoots(List<LabelledRegularDD<D>> roots) {
        return roots.stream().map(LabelledRegularDD::dd).collect(toList());
    }
}
