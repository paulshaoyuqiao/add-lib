/* This file was generated with the ADD-Lib Gen Tools
 * http://add-lib.scce.info/ */

package info.scce.addlib.codegenerator;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.RegularDD;
import info.scce.addlib.traverser.PreorderTraverser;

import java.io.PrintStream;
import java.util.List;

public class JSGenerator<D extends RegularDD<?, D>> extends CodeGenerator<D> {

    private String paramClassName = "ADDLibDecisionService";
    private boolean enableableParamStaticMethods;

    public JSGenerator<D> withClassName(String cla) {
        this.paramClassName = cla;
        return this;
    }

    public JSGenerator<D> withStaticMethodsEnabled(boolean sta) {
        this.enableableParamStaticMethods = sta;
        return this;
    }

    public JSGenerator<D> withStaticMethodsEnabled() {
        return withStaticMethodsEnabled(true);
    }

    public JSGenerator<D> withStaticMethodsDisabled() {
        return withStaticMethodsEnabled(false);
    }

    @Override
    public void generate(PrintStream out, List<LabelledRegularDD<D>> roots) {
        out.println("/* This file was generated with the ADD-Lib Gen Tools");
        out.println(" * http://add-lib.scce.info/ */");
        out.println("");
        out.print("module.exports = class ");
        if (this.paramClassName != null)
            out.print(this.paramClassName);
        out.println(" {");
        for (LabelledRegularDD<D> x0 : roots) {
            out.println("");
            out.print("    ");
            if (this.enableableParamStaticMethods) {
                out.print("static ");
            }
            out.print(x0.label());
            out.print("(");
            paramList(out);
            out.println(") {");
            out.print("        return ");
            if (this.enableableParamStaticMethods) {
                out.print("this.");
            }
            out.print("eval");
            out.print(x0.dd().ptr());
            out.println("(predicates);");
            out.println("    }");
        }
        for (D x1 : new PreorderTraverser<D>(unlabelledRoots(roots))) {
            if (!x1.isConstant()) {
                out.println("");
                out.print("    ");
                if (this.enableableParamStaticMethods) {
                    out.print("static ");
                }
                out.print("eval");
                out.print(x1.ptr());
                out.print("(");
                paramList(out);
                out.println(") {");
                out.print("        if (");
                varCondition(out, x1);
                out.println(")");
                out.print("            return ");
                if (this.enableableParamStaticMethods) {
                    out.print("this.");
                }
                out.print("eval");
                out.print(x1.t().ptr());
                out.println("(predicates);");
                out.println("        else");
                out.print("            return ");
                if (this.enableableParamStaticMethods) {
                    out.print("this.");
                }
                out.print("eval");
                out.print(x1.e().ptr());
                out.println("(predicates);");
                out.println("    }");
            }
        }
        for (D x2 : new PreorderTraverser<D>(unlabelledRoots(roots))) {
            if (x2.isConstant()) {
                out.println("");
                out.print("    ");
                if (this.enableableParamStaticMethods) {
                    out.print("static ");
                }
                out.print("eval");
                out.print(x2.ptr());
                out.print("(");
                paramList(out);
                out.println(") {");
                out.print("        return ");
                resultInstatiation(out, x2);
                out.println(";");
                out.println("    }");
            }
        }
        out.println("}");
        out.println("");
    }

    protected void paramList(PrintStream out) {
        out.print("predicates");
    }

    protected void resultInstatiation(PrintStream out, D x2) {
        out.print("\"");
        out.print(x2.toString());
        out.print("\"");
    }

    protected void varCondition(PrintStream out, D x1) {
        out.print("predicates.");
        out.print(x1.readName());
        out.print("()");
    }
}