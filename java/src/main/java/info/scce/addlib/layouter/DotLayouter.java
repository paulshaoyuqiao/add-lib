package info.scce.addlib.layouter;

import info.scce.addlib.codegenerator.DotGenerator;
import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.RegularDD;
import info.scce.addlib.traverser.PreorderTraverser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Double.parseDouble;
import static java.lang.Long.parseLong;

public class DotLayouter<D extends RegularDD<?, D>> extends Layouter<D> {

    private static final String DOUBLE_REGEX = "[+-]?[0-9]*\\.[0-9]+([eE][+-]?[0-9]+)?";
    private static final String NODE_REGEX = "node" + " n(?<ptr>[0-9]+)" + " (?<x>" + DOUBLE_REGEX + ")" + " (?<y>"
            + DOUBLE_REGEX + ")" + " (?<w>" + DOUBLE_REGEX + ")" + " (?<h>" + DOUBLE_REGEX + ")" + ".*";
    private final DotGenerator<D> dotGenerator;
    private Pattern dotOutLinePattern = null;
    private Layouter<D> fallbackLayouter = null;

    public DotLayouter() {
        dotGenerator = new DotGenerator<>();
    }

    public DotLayouter<D> withFallbackLayouter(Layouter<D> fallbackLayouter) {
        this.fallbackLayouter = fallbackLayouter;
        return this;
    }

    public static boolean isAvailable() {
        try {
            Process process = new ProcessBuilder("dot", "-V").start();
            process.waitFor();
            int exitValue = process.exitValue();
            boolean executionSuccessful = exitValue == 0;
            if (executionSuccessful)
                return true;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected Map<D, BoundingBox> computeLayout(List<D> roots) {

        /* Try to compute layout with dot */
        if (isAvailable()) {
            try {

                /* Setup sub process */
                ProcessBuilder pb = new ProcessBuilder("dot", "-Tplain")
                        .redirectError(ProcessBuilder.Redirect.INHERIT);
                Process p = pb.start();

                /* Generate and feed dot */
                dotGenerator.generate(p.getOutputStream(), withRandomLabels(roots));
                p.getOutputStream().close();

                /* Read and parse output */
                InputStream in = p.getInputStream();
                Map<D, BoundingBox> layout = parseDotOut(in, ddByPtr(roots));
                return layout;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /* Fall back to other layouter if dot is not available or errors occurred */
        if (fallbackLayouter == null)
            throw new LayouterException("Could not layout with dot and fallback layouter was not set");
        return fallbackLayouter.computeLayout(roots);
    }

    private List<LabelledRegularDD<D>> withRandomLabels(List<D> roots) {
        List<LabelledRegularDD<D>> labelled = new ArrayList<>();
        for (D r : roots)
            labelled.add(new LabelledRegularDD<>(r, "f"));
        return labelled;
    }

    private Map<Long, D> ddByPtr(List<D> roots) {
        Map<Long, D> ddByPtr = new HashMap<>();
        for (D f : new PreorderTraverser<>(roots))
            ddByPtr.put(f.ptr(), f);
        return ddByPtr;
    }

    private Map<D, BoundingBox> parseDotOut(InputStream in, Map<Long, D> ddByPtr) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        Map<D, BoundingBox> layout = new HashMap<>();
        String line;
        while ((line = reader.readLine()) != null) {
            Matcher m = dotOutLinePattern().matcher(line);
            if (m.matches()) {
                long ptr = parseLong(m.group("ptr"));
                double x = parseDouble(m.group("x"));
                double y = parseDouble(m.group("y"));
                double w = parseDouble(m.group("w"));
                double h = parseDouble(m.group("h"));
                layout.put(ddByPtr.get(ptr), new BoundingBox(x, y, w, h));
            }
        }
        return layout;
    }

    private Pattern dotOutLinePattern() {
        if (dotOutLinePattern == null)
            dotOutLinePattern = Pattern.compile(NODE_REGEX);
        return dotOutLinePattern;
    }
}
