package info.scce.addlib.layouter;

public class LayouterException extends RuntimeException {

    private static final long serialVersionUID = 7642297043805654866L;

    public LayouterException(String msg) {
        super(msg);
    }
}
