package info.scce.addlib.layouter;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class BoundingBox {

    private final double x, y, w, h;

    public BoundingBox(double x, double y, double w, double h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }

    public double x2() {
        return x() + w();
    }

    public double y2() {
        return y() + h();
    }

    public double w() {
        return w;
    }

    public double h() {
        return h;
    }

    public double area() {
        return w * h;
    }

    public double overlap(BoundingBox other) {
        double overlapW = max(0, min(x2(), other.x2()) - max(x, other.x));
        double overlapH = max(0, min(y2(), other.y2()) - max(y, other.y));
        return overlapW * overlapH;
    }

    public boolean overlaps(BoundingBox other) {
        return overlap(other) > 0;
    }

    public boolean contains(BoundingBox other) {
        return x <= other.x && y <= other.y && other.x2() <= x2() && other.y2() <= y2();
    }

    @Override
    public boolean equals(Object otherObj) {
        if (otherObj instanceof BoundingBox) {
            BoundingBox other = (BoundingBox) otherObj;
            return x == other.x && y == other.y && w == other.w && h == other.h;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Double.hashCode(x) + Double.hashCode(y) + Double.hashCode(w) + Double.hashCode(h);
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + w + ", " + h + ")";
    }
}
