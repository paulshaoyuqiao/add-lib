package info.scce.addlib.layouter;

import info.scce.addlib.dd.RegularDD;

import java.util.*;

import static java.lang.Math.max;

public class SimpleLayouter<D extends RegularDD<?, D>> extends Layouter<D> {

    private double anchorX = 0.0;
    private double anchorY = 0.0;
    private double nodeMarginX = 1.0;
    private double nodeMarginY = 1.0;
    private double nodeWidth = 0.5;
    private double nodeHeight = 0.5;
    private double nodeWidthPerCharacter = 0.0;

    public SimpleLayouter<D> withAnchorPosition(double anchorX, double anchorY) {
        this.anchorX = anchorX;
        this.anchorY = anchorY;
        return this;
    }

    public SimpleLayouter<D> withNodeMargin(double nodeMarginX, double nodeMarginY) {
        this.nodeMarginX = nodeMarginX;
        this.nodeMarginY = nodeMarginY;
        return this;
    }

    public SimpleLayouter<D> withNodeDimension(double nodeWidth, double nodeHeight, double widthPerCharacter) {
        this.nodeWidth = nodeWidth;
        this.nodeHeight = nodeHeight;
        nodeWidthPerCharacter = widthPerCharacter;
        return this;
    }

    @Override
    protected Map<D, BoundingBox> computeLayout(List<D> roots) {
        List<List<D>> sortedLayers = sortedLayers(roots);
        return computeLayoutFromSortedList(sortedLayers);
    }

    private List<List<D>> sortedLayers(List<D> roots) {
        List<List<D>> sortedLayers = new ArrayList<>();
        List<D> constants = new ArrayList<>();
        Set<D> seen = new HashSet<>();
        for (D dd : roots)
            sortedLayersRecur(dd, sortedLayers, constants, seen);
        sortedLayers.add(constants);
        return sortedLayers;
    }

    private void sortedLayersRecur(D dd, List<List<D>> sortedLayers, List<D> constants, Set<D> seen) {
        if (!seen.contains(dd)) {
            seen.add(dd);
            if (dd.isConstant()) {
                constants.add(dd);
            } else {
                int i = dd.readIndex();
                while (sortedLayers.size() <= i)
                    sortedLayers.add(new ArrayList<>());
                sortedLayers.get(i).add(dd);
                sortedLayersRecur(dd.t(), sortedLayers, constants, seen);
                sortedLayersRecur(dd.e(), sortedLayers, constants, seen);
            }
        }
    }

    private Map<D, BoundingBox> computeLayoutFromSortedList(List<List<D>> sortedLayers) {
        HashMap<D, BoundingBox> layout = new HashMap<>();
        double maxLayerWidth = maxLayerWidth(sortedLayers);
        double y = anchorY;
        for (List<D> layer : sortedLayers) {
            double x = layerX(layer, maxLayerWidth);
            for (D node : layer) {
                double nodeWidth = nodeWidth(node);
                layout.put(node, new BoundingBox(x, y, nodeWidth, nodeHeight));
                x += nodeWidth + nodeMarginX;
            }
            if (!layer.isEmpty())
                y += nodeHeight + nodeMarginY;
        }
        return layout;
    }

    private double maxLayerWidth(List<List<D>> sortedLayers) {
        double maxLayerWidth = 0.0;
        for (List<D> layer : sortedLayers)
            maxLayerWidth = max(maxLayerWidth, layerWidth(layer));
        return maxLayerWidth;
    }

    private double layerWidth(List<D> layer) {
        double width = 0.0;
        Iterator<D> it = layer.iterator();
        if (it.hasNext())
            width += nodeWidth(it.next());
        while (it.hasNext())
            width += nodeMarginX + nodeWidth(it.next());
        return width;
    }

    private double layerX(List<D> layer, double maxLayerWidth) {
        double smallestLayerX = anchorX - maxLayerWidth / 2;
        return smallestLayerX + (maxLayerWidth - layerWidth(layer)) / 2;
    }

    private double nodeWidth(D dd) {
        return nodeWidth + dd.toString().length() * nodeWidthPerCharacter;
    }
}
