package info.scce.addlib.viewer;

import info.scce.addlib.codegenerator.DotGenerator;
import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.RegularDD;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static java.lang.Math.*;

public class DotViewerFrame<D extends RegularDD<?, D>> extends JFrame {

    public static final int DEFAULT_CANVAS_WIDTH = 1024;
    public static final int DEFAULT_CANVAS_HEIGHT = DEFAULT_CANVAS_WIDTH / 2;

    public static final String FILE_EXTENSION_DOT = "dot";
    public static final String FILE_EXTENSION_PNG = "png";

    public static final double ZOOM_BASE = 1.2;

    public static final Color ERROR_CIRCLES_COLOR = new Color(255, 0, 0, 64);
    public static final int ERROR_CIRCLES_N = 8;
    public static final int ERROR_LINE_HEIGHT = 16;
    public static final String ERROR_MESSAGE_L0 = "Failed to execute dot command";
    public static final String ERROR_MESSAGE_L1 = "https://www.graphviz.org/";

    private JLabel label;
    private JScrollPane scrollPane;
    private JMenuBar menuBar;

    private List<LabelledRegularDD<D>> roots;
    private BufferedImage image;
    private double zoomExp = 0.0;

    public DotViewerFrame(String title) {
        super(title);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        buildGUI();
    }

    private void buildGUI() {

        /* Build menu bar */
        JMenuItem exportPngItem = new JMenuItem("Export as *.png");
        exportPngItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
        exportPngItem.addActionListener(this::onExportPng);
        JMenuItem exportDotItem = new JMenuItem("Export as *.dot");
        exportDotItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        exportDotItem.addActionListener(this::onExportDot);
        JMenuItem zoomInItem = new JMenuItem("Zoom in");
        zoomInItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));
        zoomInItem.addActionListener(this::onZoomIn);
        JMenuItem zoomOutItem = new JMenuItem("Zoom out");
        zoomOutItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        zoomOutItem.addActionListener(this::onZoomOut);
        JMenu menu = new JMenu("Menu");
        menu.add(exportDotItem);
        menu.add(exportPngItem);
        menu.add(zoomInItem);
        menu.add(zoomOutItem);
        menuBar = new JMenuBar();
        menuBar.add(menu);
        menuBar.setBorder(BorderFactory.createEmptyBorder());

        /* Build canvas */
        label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        scrollPane = new JScrollPane(label);
        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        scrollPane.setPreferredSize(new Dimension(DEFAULT_CANVAS_WIDTH, DEFAULT_CANVAS_HEIGHT));

        /* Compose GUI */
        getContentPane().add(menuBar, BorderLayout.NORTH);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        pack();
    }

    public void view(List<LabelledRegularDD<D>> roots) {
        this.roots = roots;
        image = createImage(roots);
        updateCanvas();
    }

    public BufferedImage createImage(List<LabelledRegularDD<D>> roots) {
        try {

            /* Setup sub process */
            ProcessBuilder pb = new ProcessBuilder("dot", "-Tpng")
                    .redirectError(ProcessBuilder.Redirect.INHERIT);
            Process p = pb.start();

            /* Generate and feed dot */
            DotGenerator<D> g = new DotGenerator<>();
            g.generate(p.getOutputStream(), roots);
            p.getOutputStream().close();

            /* Read image */
            InputStream in = p.getInputStream();
            return ImageIO.read(in);

        } catch (IOException e) {
            return createErrorImage();
        }
    }

    private BufferedImage createErrorImage() {
        BufferedImage img = new BufferedImage(DEFAULT_CANVAS_WIDTH, DEFAULT_CANVAS_HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics g = img.getGraphics();

        /* Draw white background */
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, DEFAULT_CANVAS_WIDTH, DEFAULT_CANVAS_HEIGHT);

        /* Draw random circles */
        g.setColor(ERROR_CIRCLES_COLOR);
        for (int i = 1; i <= ERROR_CIRCLES_N; i++) {
            int d = min(DEFAULT_CANVAS_WIDTH, DEFAULT_CANVAS_HEIGHT) / ERROR_CIRCLES_N * i;
            int x = (int) (random() * (DEFAULT_CANVAS_WIDTH - d));
            int y = (int) (random() * (DEFAULT_CANVAS_HEIGHT - d));
            g.fillOval(x, y, d, d);
        }

        /* Print text */
        g.setColor(Color.BLACK);
        g.setFont(new Font(g.getFont().getName(), g.getFont().getStyle(), ERROR_LINE_HEIGHT));
        int y = (DEFAULT_CANVAS_HEIGHT - 2 * ERROR_LINE_HEIGHT) / 2;
        g.drawString(ERROR_MESSAGE_L0, ERROR_LINE_HEIGHT, y += ERROR_LINE_HEIGHT);
        g.drawString(ERROR_MESSAGE_L1, ERROR_LINE_HEIGHT, y += ERROR_LINE_HEIGHT);

        return img;
    }

    private void onZoomIn(ActionEvent e) {
        zoomExp += 1.0;
        updateCanvas();
    }

    private void onZoomOut(ActionEvent e) {
        zoomExp -= 1.0;
        updateCanvas();
    }

    private void updateCanvas() {

        /* Update displayed image according to current zoom level */
        double zoomFactor = pow(ZOOM_BASE, zoomExp);
        int width = (int) (image.getWidth() * zoomFactor);
        int height = (int) (image.getHeight() * zoomFactor);
        Image scaledImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);

        JViewport viewport = scrollPane.getViewport();

        scrollPane.setViewportView(new JLabel(new ImageIcon(scaledImage)));
        scrollPane.setViewport(viewport);
    }

    private void onExportDot(ActionEvent e) {
        File targetFile = promptTargetFile(FILE_EXTENSION_DOT);
        if (targetFile != null) {

            /* Use code generator to export dot format */
            DotGenerator<D> g = new DotGenerator<>();
            try {
                g.generateToFileSystem(targetFile, roots);
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void onExportPng(ActionEvent e) {
        File targetFile = promptTargetFile(FILE_EXTENSION_PNG);
        if (targetFile != null) {

            /* Save original image as png (zoom level has no effect on this) */
            try {
                ImageIO.write(image, "png", targetFile);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private File promptTargetFile(String ensureExt) {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File targetFile = chooser.getSelectedFile();
            if (targetFile != null) {

                /* Ensure the expected file extension (case insensitive) */
                boolean validExt = targetFile.getName().toLowerCase().endsWith(ensureExt.toLowerCase());
                if (!validExt) {
                    File parent = targetFile.getParentFile();
                    String extendedName = targetFile.getName() + "." + ensureExt;
                    targetFile = new File(parent, extendedName);
                }
                return targetFile;
            }
        }
        return null;
    }
}
