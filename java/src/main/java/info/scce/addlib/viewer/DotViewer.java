package info.scce.addlib.viewer;

import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.RegularDD;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.latticedd.example.KleenePriestLogicDDManager;
import info.scce.addlib.dd.xdd.latticedd.example.KleenePriestValue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DotViewer<D extends RegularDD<?, D>> {

    private static final String DEFAULT_VIEWER_NAME = "ADD-Lib Viewer";

    private final String viewerName;

    private Lock lock = new ReentrantLock();
    private Condition allFramesClosed = lock.newCondition();
    private List<DotViewerFrame<D>> frames = new ArrayList<>();

    public DotViewer() {
        this(DEFAULT_VIEWER_NAME);
    }

    public DotViewer(String viewerName) {
        this.viewerName = viewerName;
    }

    public void view(D root, String label) {
        view(new LabelledRegularDD<D>(root, label));
    }

    public void view(LabelledRegularDD<D> root) {
        view(Collections.singletonList(root));
    }

    public void view(List<LabelledRegularDD<D>> roots) {
        DotViewerFrame f = new DotViewerFrame<D>(viewerName);
        f.view(roots);
        f.setVisible(true);
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                lock.lock();
                try {
                    frames.remove(e.getWindow());
                    if (frames.isEmpty())
                        allFramesClosed.signal();
                } finally {
                    lock.unlock();
                }
            }
        });
        lock.lock();
        try {
            frames.add(f);
        } finally {
            lock.unlock();
        }
    }

    public void waitUntilAllClosed() {
        lock.lock();
        try {
            if (!frames.isEmpty())
                allFramesClosed.await();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void closeAll() {
        lock.lock();
        try {
            for (Frame f : frames) {
                SwingUtilities.invokeLater(() -> {
                    WindowEvent e = new WindowEvent(f, WindowEvent.WINDOW_CLOSING);
                    f.dispatchEvent(e);
                });
            }
        } finally {
            lock.unlock();
        }
    }

    /* Auxiliary main method useful for debugging */
    public static void main(String... args) {

        /* Initialize DDManager with default values */
        KleenePriestLogicDDManager ddManager = new KleenePriestLogicDDManager();

        /* Init common viewer */
        DotViewer<XDD<KleenePriestValue>> viewer = new DotViewer<>("ADD-Lib Viewer Test");

        /* Build email example */
        XDD<KleenePriestValue> recent = ddManager.namedVar("recentlyReceived", KleenePriestValue.TRUE, KleenePriestValue.UNKNOWN);
        XDD<KleenePriestValue> addrBook = ddManager.namedVar("senderInAddressBook", KleenePriestValue.TRUE, KleenePriestValue.FALSE);
        XDD<KleenePriestValue> urgentMail = recent.and(addrBook);
        recent.recursiveDeref();
        addrBook.recursiveDeref();
        viewer.view(urgentMail, "urgentMail");

        /* Build Wigner's friend example */
        XDD<KleenePriestValue> friendHappy = ddManager.namedVar("FriendIsHappy", KleenePriestValue.TRUE, KleenePriestValue.FALSE);
        XDD<KleenePriestValue> opened = ddManager.namedVar("BoxOpened", KleenePriestValue.TRUE, KleenePriestValue.UNKNOWN);
        XDD<KleenePriestValue> catAlive = opened.and(friendHappy).or(opened.not());
        opened.recursiveDeref();
        friendHappy.recursiveDeref();
        viewer.view(catAlive, "wignersFriend");

        /* Show examples in shared diagram */
        LabelledRegularDD<XDD<KleenePriestValue>> example0 = new LabelledRegularDD<>(urgentMail, "urgentMail");
        LabelledRegularDD<XDD<KleenePriestValue>> example1 = new LabelledRegularDD<>(catAlive, "wignersFriend");
        viewer.view(Arrays.asList(example0, example1));
        urgentMail.recursiveDeref();
        catAlive.recursiveDeref();

        /* Wait */
        System.out.println("windows open...");
        viewer.waitUntilAllClosed();
        System.out.print("all windows closed.");

        /* Release memory */
        ddManager.quit();
    }
}


