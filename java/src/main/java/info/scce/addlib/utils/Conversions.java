package info.scce.addlib.utils;

import info.scce.addlib.dd.DD;

public class Conversions {

    public static final long NULL = 0;

    public static int[] asInts(boolean... booleans) {
        int[] ints = new int[booleans.length];
        for (int i = 0; i < ints.length; i++)
            ints[i] = asInt(booleans[i]);
        return ints;
    }

    public static int asInt(boolean b) {
        return b ? 1 : 0;
    }

    public static double[] asDoubles(boolean... booleans) {
        double[] doubles = new double[booleans.length];
        for (int i = 0; i < doubles.length; i++)
            doubles[i] = asDouble(booleans[i]);
        return doubles;
    }

    public static double asDouble(boolean b) {
        return b ? 1.0 : 0.0;
    }

    public static boolean[] asBooleans(int... ints) {
        boolean[] booleans = new boolean[ints.length];
        for (int i = 0; i < booleans.length; i++)
            booleans[i] = asBoolean(ints[i]);
        return booleans;
    }

    public static boolean asBoolean(int i) {
        return i > 0;
    }

    public static boolean[] asBooleans(double... doubles) {
        boolean[] booleans = new boolean[doubles.length];
        for (int i = 0; i < booleans.length; i++)
            booleans[i] = asBoolean(doubles[i]);
        return booleans;
    }

    public static boolean asBoolean(double d) {
        return d > 0;
    }

    public static long[] ptrs(DD<?, ?>... fs) {
        long[] ptrs = new long[fs.length];
        for (int i = 0; i < ptrs.length; i++)
            ptrs[i] = fs[i].ptr();
        return ptrs;
    }
}
