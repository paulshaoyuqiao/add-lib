#include "info_scce_addlib_cudd_Cudd.h"
#include "cudd.h"

JNIEXPORT jlong JNICALL Java_info_scce_addlib_cudd_Cudd_native_1Cudd_1bddVectorCompose
	(JNIEnv *env, jclass, jlong ddManager, jlong f, jlongArray vector) {

	jlong *vectorElements = env->GetLongArrayElements(vector, 0);
	jsize vectorLength = env->GetArrayLength(vector);
	DdNode **ddVector = new DdNode *[vectorLength];
	jlong *src = vectorElements;
	jlong *end = vectorElements + vectorLength;
	DdNode **dest = ddVector;
	while (src < end)
		*(dest++) = (DdNode *) *(src++);
	DdNode *result = Cudd_bddVectorCompose((DdManager *) ddManager, (DdNode *) f, ddVector);
	delete [] ddVector;
	env->ReleaseLongArrayElements(vector, vectorElements, 0);
	return (jlong) result;
}

JNIEXPORT jlong JNICALL Java_info_scce_addlib_cudd_Cudd_native_1Cudd_1addVectorCompose
	(JNIEnv *env, jclass, jlong ddManager, jlong f, jlongArray vector) {
	
	jlong *vectorElements = env->GetLongArrayElements(vector, 0);
	jsize vectorLength = env->GetArrayLength(vector);
	DdNode **ddVector = new DdNode *[vectorLength];
	jlong *src = vectorElements;
	jlong *end = vectorElements + vectorLength;
	DdNode **dest = ddVector;
	while (src < end)
		*(dest++) = (DdNode *) *(src++);
	DdNode *result = Cudd_addVectorCompose((DdManager *) ddManager, (DdNode *) f, ddVector);
	delete [] ddVector;
	env->ReleaseLongArrayElements(vector, vectorElements, 0);
	return (jlong) result;
}
