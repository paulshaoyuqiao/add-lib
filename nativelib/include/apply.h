#ifndef ADDLIB_APPLY_H
#define ADDLIB_APPLY_H

#include <map>
#include "jni.h"
#include "cudd.h"
#include "applyAdapterFunctions.h"
#include "monadicApplyAdapterFunctions.h"

/* 
 * CUDD caches Cudd_addApply and Cudd_addmonadicApply results internally and identifies 
 * operations by their function pointer. For this reason every Java callback must be 
 * wrapped by a separate C function. We use a pool of N functions and clear CUDD's cache 
 * only once all of them are used. Clearing the cache may result in a different variable 
 * order. 
 */

/* Clears the CUDD cache through the CUDD API function Cudd_ReduceHeap */
void addlib_clearCache(DdManager *ddManager);

#endif /* ADDLIB_APPLY_H */
