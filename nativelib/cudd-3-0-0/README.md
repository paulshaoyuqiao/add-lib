# CUDD Dependency 

The native library of the ADD-Lib relies on CUDD 3.0.0 internally. Use one of the pre-build static libraries for your platform or build it yourself. 

## Use a Pre-build CUDD Library

The ADD-Lib comes with pre-build libraries for Linux, macOS, and Windows:

- ```linux/x64/libcudd.a```
- ```mac/x64/libcudd.a```
- ```windows/x64/libcudd.a```

## Build CUDD from Source

Download CUDD 3.0.0 from [vlsi.colorado.edu/~fabio](http://vlsi.colorado.edu/~fabio/) and compile it to a static library with position independent code.

### Linux and maxOS 

As explained in the CUDD readme file use the following commands: 

```sh
cd path/to/cudd-3.0.0
./configure --with-pic
make 
make check 
```

Find the static library in ```path/to/cudd-3.0.0/cudd/.libs/libcudd.a``` and copy it to the ADD-Lib's directory ```native-lib/cudd-3-0-0/linux/x64``` or ```native-lib/cudd-3-0-0/mac/x64``` depending on your platform. 

### Windows 

It's a journey: First, install MSYS2 on your 64 bit machine, open the MSYS2 64 bit terminal and update the software by running the update command **repeatedly**: 

```sh
pacman -Syuu
```

Now, install the needed build tools and other software:

```sh 
pacman -S --needed base-devel \
                   mingw-w64-x86_64-toolchain \
                   mingw-w64-x86_64-cmake
```

Now that you have the right compiler use the following commands as explained in the CUDD readme file:

```sh
cd path/to/cudd-3.0.0
./configure --with-pic --build=x86_64-w64-mingw32
make 
make check 
```

Find the static library in ```path/to/cudd-3.0.0/cudd/.libs/libcudd.a``` and copy it to the ADD-Lib's directory ```native-lib/cudd-3-0-0/windows/x64```. 



