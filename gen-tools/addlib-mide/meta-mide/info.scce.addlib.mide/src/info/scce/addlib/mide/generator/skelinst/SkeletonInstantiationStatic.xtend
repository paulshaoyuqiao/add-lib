package info.scce.addlib.mide.generator.skelinst

import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import java.io.IOException
import java.io.InputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import org.eclipse.core.resources.IFolder

class SkeletonInstantiationStatic {

	extension WorkspaceExtension = new WorkspaceExtension

	def instantiate(String skeletonZipResource, IFolder target) {

		/* Load skeleton from resources */
		val is = class.getResourceAsStream(skeletonZipResource)
		val zis = new ZipInputStream(is)

		/* Iterate over zip file entries */
		var e = null as ZipEntry;
		while ((e = zis.getNextEntry()) !== null) {

			/* This is to ensure that the ZipInputStream is not yet closed */
			val zisWrap = new InputStream() {
				override read() throws IOException {
					zis.read
				}
			}
			target.create.createFile(e.name, zisWrap, true)
		}

		zis.closeEntry
		zis.close
	}
}
