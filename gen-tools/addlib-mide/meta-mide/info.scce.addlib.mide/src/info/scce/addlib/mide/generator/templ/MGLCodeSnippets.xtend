package info.scce.addlib.mide.generator.templ

import info.scce.addlib.mide.algebraicStructure.ValueType

import static extension info.scce.addlib.mide.generator.templ.StringUtils.*

class MGLCodeSnippets {

	def static asMonadicOperationNodesMGL(Iterable<String> operationNames) '''
		«FOR name : operationNames»
			@style(operation, "«name»")
			@icon("as-icons-gen/operation1.png")
			@palette("Monadic Operations")
			node «name.capitalize» extends MonadicOperation {
			}
		«ENDFOR»
	'''

	def static asBinaryOperationNodesMGL(Iterable<String> operationNames) '''
		«FOR name : operationNames»
			@style(operation, "«name»")
			@icon("as-icons-gen/operation2.png")
			@palette("Binary Operations")
			node «name.capitalize» extends BinaryOperation {
			}
		«ENDFOR»
	'''

	def static asValueTypeMGL(ValueType t) {
		switch (t) {
			case BOOL: "EBoolean"
			case REAL: "EDouble"
			case INT: "EInt"
			case STR: "EString"
		}
	}

	def static asDefaultValueMGL(ValueType t) {
		switch (t) {
			case BOOL: "false"
			case REAL: "0.0"
			case INT: "0"
			case STR: ""
		}
	}
}
