package info.scce.addlib.mide.generator.skelinst

class PlaceholderValue extends PlaceholderFunction {

	String value

	new(String value) {
		this.value = value
	}

	new(CharSequence cs) {
		this(cs.toString)
	}

	override apply(String indentation, String... args) {
		return value.indent(indentation)
	}
}
