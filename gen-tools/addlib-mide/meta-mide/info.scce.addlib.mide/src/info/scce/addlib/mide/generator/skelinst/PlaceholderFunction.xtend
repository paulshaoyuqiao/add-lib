package info.scce.addlib.mide.generator.skelinst

abstract class PlaceholderFunction {

	abstract def String apply(String indentation, String... args)

	protected def indent(String str, String indentation) {
		val lines = str.split(System.lineSeparator)
		lines.map[ln|indentation + ln].join(System.lineSeparator)
	}
}
