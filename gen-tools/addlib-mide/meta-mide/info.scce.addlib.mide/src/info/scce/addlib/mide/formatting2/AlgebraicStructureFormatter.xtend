package info.scce.addlib.mide.formatting2

import com.google.inject.Inject
import info.scce.addlib.mide.algebraicStructure.AlgebraicStructure
import info.scce.addlib.mide.services.AlgebraicStructureGrammarAccess
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument

class AlgebraicStructureFormatter extends AbstractFormatter2 {

	@Inject extension AlgebraicStructureGrammarAccess

	def dispatch void format(AlgebraicStructure algebraicStructure, extension IFormattableDocument document) {

		algebraicStructure.allRegionsFor.keywords("algebraic").forEach[prepend[noSpace].append[oneSpace]]
		algebraicStructure.allRegionsFor.keywords("structure", ":=").forEach[surround[oneSpace]]
		algebraicStructure.allRegionsFor.keywords("{", "(").forEach[append[oneSpace]]
		algebraicStructure.allRegionsFor.keywords(",").forEach[prepend[noSpace].append[oneSpace]]
		algebraicStructure.allRegionsFor.keywords("}", ")").forEach[prepend[oneSpace]]
		algebraicStructure.allRegionsFor.keywords("implemented").forEach[prepend[newLine].append[oneSpace]]
		algebraicStructure.allRegionsFor.keywords("in").forEach[surround[oneSpace]]
		algebraicStructure.allRegionsFor.keywords(".").forEach[surround[noSpace]]
		algebraicStructure.append[newLines = 2]
	}
}
