package info.scce.addlib.mide.generator.templ

import info.scce.addlib.mide.algebraicStructure.FullyQualifiedClassName
import info.scce.addlib.mide.algebraicStructure.ValueType

import static extension info.scce.addlib.mide.generator.templ.StringUtils.*

class JavaCodeSnippets {

	def static asPackageName(String asName) {
		asName.toLowerCase
	}

	def static asOperationImplClassName(FullyQualifiedClassName fqClassName) {
		fqClassName.packageName.join(".") + "." + fqClassName.className
	}

	def static asXDDCreateFromBinaryOperationDispatchJava(Iterable<String> operationNames) '''
		«FOR b : operationNames»
			if (node instanceof «b.capitalize»)
				return xddCreateFromBinaryOperation((«b.capitalize») node, operationImpl::«b»);
		«ENDFOR»
	'''

	def static asXDDCreateFromMonadicOperationDispatchJava(Iterable<String> operationNames) '''
		«FOR u : operationNames»
			if (node instanceof «u.capitalize»)
				return xddCreateFromMonadicOperation((«u.capitalize») node, operationImpl::«u»);
		«ENDFOR»
	'''

	def static asValueGetterJava(ValueType t) {
		switch (t) {
			case BOOL: "isValue"
			case REAL: "getValue"
			case INT: "getValue"
			case STR: "getValue"
		}
	}

	def static asValueTypeJava(ValueType t) {
		switch (t) {
			case BOOL: "Boolean"
			case REAL: "Double"
			case INT: "Integer"
			case STR: "String"
		}
	}

	def static asValueTypeJavaNative(ValueType t) {
		switch (t) {
			case BOOL: "boolean"
			case REAL: "double"
			case INT: "int"
			case STR: "String"
		}
	}
}
