package info.scce.addlib.mide.generator.skelinst

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.regex.Pattern
import java.util.stream.Collectors
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import org.eclipse.core.resources.IFolder
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension

class SkeletonInstantiationDynamic {

	val PLACEHOLDER_REGEX = "(?<indent>[\\t ]*)@(?<name>[A-Za-z]+)(\\((?<args>\\s*[A-Za-z]+\\s*(,\\s*[A-Za-z]+\\s*)*)\\))?"

	Pattern placeholderPattern

	extension WorkspaceExtension = new WorkspaceExtension

	new() {
		this.placeholderPattern = Pattern.compile(PLACEHOLDER_REGEX)
	}

	def instantiate(String skeletonZipResource, PlaceholderConfiguration conf, IFolder target) {

		/* Delete old files because file names may change */
		target.delete(false, null)

		/* Load skeleton from resources */
		val is = class.getResourceAsStream(skeletonZipResource)
		val zis = new ZipInputStream(is)

		/* Iterate over zip file entries */
		var e = null as ZipEntry;
		while ((e = zis.getNextEntry()) !== null) {

			/* Read file content */
			val content = new BufferedReader(new InputStreamReader(zis)).lines().collect(
				Collectors.joining(System.lineSeparator));

			/* Instantiate file and replace placeholder in path and content */
			val concreteName = e.name.instantiatePlaceholders(conf)
			val concreteContent = content.instantiatePlaceholders(conf)
			target.create.createFile(concreteName, concreteContent, true)
		}
		zis.closeEntry
		zis.close
	}

	private def instantiatePlaceholders(String str, PlaceholderConfiguration conf) {
		val result = new StringBuilder
		var idxStr = 0
		val m = placeholderPattern.matcher(str)

		/* Iterate over placeholder candidates */
		while (m.find) {

			/* Maintain content without placeholder candidates */
			result.append(str.substring(idxStr, m.start))

			/* Determine placeholder replacement if exists */
			val indentation = m.group("indent")
			val placeholder = m.group("name")
			val arglist = m.group("args")
			val replacement = if (arglist !== null) {
					val args = arglist.split(",").map[trim]
					conf.apply(indentation, placeholder, args)
				} else {
					conf.apply(indentation, placeholder)
				}

			/* Collect replacement or copy raw match */
			result.append(if(replacement !== null) replacement else m.group)

			/* Move on */
			idxStr = m.end
		}

		/* Copy remainder */
		result.append(str.substring(idxStr))

		result.toString
	}
}
