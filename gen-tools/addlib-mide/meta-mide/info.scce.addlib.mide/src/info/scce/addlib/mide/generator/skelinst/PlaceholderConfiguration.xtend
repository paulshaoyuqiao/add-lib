package info.scce.addlib.mide.generator.skelinst

import java.util.Map
import java.util.HashMap

class PlaceholderConfiguration {

	Map<String, PlaceholderFunction> cfg = new HashMap

	def put(String placeholder, PlaceholderFunction f) {
		cfg.put(placeholder, f)
	}

	def with(String placeholder, PlaceholderFunction f) {
		put(placeholder, f)
		this
	}

	def String apply(String indentation, String placeholder, String... args) {
		val placeholderFunction = cfg.get(placeholder)
		placeholderFunction?.apply(indentation, args)
	}
}
