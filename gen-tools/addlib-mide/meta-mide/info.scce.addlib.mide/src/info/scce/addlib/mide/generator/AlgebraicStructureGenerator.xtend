package info.scce.addlib.mide.generator

import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import info.scce.addlib.mide.algebraicStructure.AlgebraicStructure
import info.scce.addlib.mide.generator.skelinst.PlaceholderConfiguration
import info.scce.addlib.mide.generator.skelinst.PlaceholderValue
import info.scce.addlib.mide.generator.skelinst.SkeletonInstantiationDynamic
import info.scce.addlib.mide.generator.skelinst.SkeletonInstantiationStatic
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

import static extension info.scce.addlib.mide.generator.templ.JavaCodeSnippets.*
import static extension info.scce.addlib.mide.generator.templ.MGLCodeSnippets.*

class AlgebraicStructureGenerator extends AbstractGenerator {

	val LIB_DIR = "as-lib-gen"
	val ICONS_DIR = "as-icons-gen"
	val SRC_GEN_DIR = "as-src-gen"
	val MODEL_GEN_DIR = "as-model-gen"

	val LIB_SKELETON_RESOURCE = "/info/scce/addlib/mide/generator/skelinst/as-lib.zip"
	val ICONS_SKELETON_RESOURCE = "/info/scce/addlib/mide/generator/skelinst/as-icons.zip"
	val SRC_SKELETON_RESOURCE = "/info/scce/addlib/mide/generator/skelinst/as-src-skeleton.zip"
	val MODEL_SKELETON_RESOURCE = "/info/scce/addlib/mide/generator/skelinst/as-model-skeleton.zip"

	SkeletonInstantiationStatic skeletonInstantiationStatic = new SkeletonInstantiationStatic
	SkeletonInstantiationDynamic skeletonInstantiationDynamic = new SkeletonInstantiationDynamic

	extension ResourceExtension = new ResourceExtension

	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {

		/* Copy lib files */
		val libTarget = resource.project.getFolder(LIB_DIR)
		if (!libTarget.exists)
			skeletonInstantiationStatic.instantiate(LIB_SKELETON_RESOURCE, libTarget)

		/* Copy icons */
		val iconsTarget = resource.project.getFolder(ICONS_DIR)
		if (!iconsTarget.exists)
			skeletonInstantiationStatic.instantiate(ICONS_SKELETON_RESOURCE, iconsTarget)

		/* Generate Java files */
		val ast = resource.allContents.filter(AlgebraicStructure).head
		val conf = ast.placeholderConf
		val srcTarget = resource.project.getFolder(SRC_GEN_DIR)
		skeletonInstantiationDynamic.instantiate(SRC_SKELETON_RESOURCE, conf, srcTarget)

		/* Generate model files */
		val modelTarget = resource.project.getFolder(MODEL_GEN_DIR)
		skeletonInstantiationDynamic.instantiate(MODEL_SKELETON_RESOURCE, conf, modelTarget)
	}

	private def placeholderConf(AlgebraicStructure algebraicStructure) {
		val conf = new PlaceholderConfiguration

		/* Configure MGL placeholders */
		conf.put("asMonadicOperationNodesMGL", new PlaceholderValue(
			algebraicStructure.monadicOperations.names.asMonadicOperationNodesMGL))
		conf.put("asBinaryOperationNodesMGL",
			new PlaceholderValue(algebraicStructure.binaryOperations.names.asBinaryOperationNodesMGL))
		conf.put("asValueTypeMGL", new PlaceholderValue(algebraicStructure.carrierSet.asValueTypeMGL))
		conf.put("asDefaultValueMGL", new PlaceholderValue(algebraicStructure.carrierSet.asDefaultValueMGL))

		/* Configure Java placeholders */
		conf.put("asPackageName", new PlaceholderValue(algebraicStructure.name.asPackageName))
		conf.put("asOperationImplClassName",
			new PlaceholderValue(algebraicStructure.implementationClassName.asOperationImplClassName))
		conf.put("asXDDCreateFromBinaryOperationDispatchJava",
			new PlaceholderValue(algebraicStructure.binaryOperations.names.asXDDCreateFromBinaryOperationDispatchJava))
		conf.put("asXDDCreateFromMonadicOperationDispatchJava",
			new PlaceholderValue(
				algebraicStructure.monadicOperations.names.asXDDCreateFromMonadicOperationDispatchJava))
		conf.put("asValueGetterJava", new PlaceholderValue(algebraicStructure.carrierSet.asValueGetterJava))
		conf.put("asValueTypeJava", new PlaceholderValue(algebraicStructure.carrierSet.asValueTypeJava))
		conf.put("asValueTypeJavaNative", new PlaceholderValue(algebraicStructure.carrierSet.asValueTypeJavaNative))

		conf
	}
}
