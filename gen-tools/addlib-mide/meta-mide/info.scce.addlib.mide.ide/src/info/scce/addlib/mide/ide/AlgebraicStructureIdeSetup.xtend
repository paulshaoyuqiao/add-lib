/*
 * generated by Xtext 2.14.0
 */
package info.scce.addlib.mide.ide

import com.google.inject.Guice
import info.scce.addlib.mide.AlgebraicStructureRuntimeModule
import info.scce.addlib.mide.AlgebraicStructureStandaloneSetup
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class AlgebraicStructureIdeSetup extends AlgebraicStructureStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new AlgebraicStructureRuntimeModule, new AlgebraicStructureIdeModule))
	}
}
