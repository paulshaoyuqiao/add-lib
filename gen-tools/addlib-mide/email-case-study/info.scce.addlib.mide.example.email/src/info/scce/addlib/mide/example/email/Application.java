package info.scce.addlib.mide.example.email;

public class Application {

	public static void main(String... args) {

		/* Create simulation */
		ServiceSimulation simulation = new ServiceSimulation();

		/* Add some exemplary Boolean decision services for comparison */
		simulation.withFrederiksBooleanDecisionServices();

		/* Add your own Boolean decision service */
		simulation.withBooleanDecisionService("My email classification service",
				new MyEmailClassificationService()::emailUrgent);

		/* Add some exemplary fuzzy decision services for comparison */
		// simulation.withFrederiksFuzzyDecisionServices();

		/* Add your own fuzzy decision service */
		// simulation.withFuzzyDecisionService("My fuzzy email classification service",
		//		new MyEmailClassificationService()::emailUrgent);

		/* Start the simulation */
		simulation.start();
	}
}
