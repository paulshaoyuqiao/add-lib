/* This file was generated with the ADD-Lib
 * http://add-lib.scce.info/ */

package info.scce.addlib.mide.example.email.baseline;

import info.scce.addlib.mide.example.email.EmailPredicates;

public class FrederiksFuzzyUnderstandingOfUrgentEmailsProbabilistic3Bins {

    public double emailUrgent(EmailPredicates predicates) {
        return eval140414048187712(predicates);
    }

    private double eval140414048187712(EmailPredicates predicates) {
        if (predicates.subjectContainsUrgent())
            return eval140414048187584(predicates);
        else
            return eval140414048187680(predicates);
    }

    private double eval140414048187680(EmailPredicates predicates) {
        if (predicates.subjectContainsDeadline())
            return eval140414048187648(predicates);
        else
            return eval140414048184224(predicates);
    }

    private double eval140414048187648(EmailPredicates predicates) {
        if (predicates.subjectContainsOverdue())
            return eval140414048187584(predicates);
        else
            return eval140414048187616(predicates);
    }

    private double eval140414048187616(EmailPredicates predicates) {
        if (predicates.subjectContainsNewsletter())
            return eval140414048184224(predicates);
        else
            return eval140414048187424(predicates);
    }

    private double eval140414048187424(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140414048187392(predicates);
        else
            return eval140414048184224(predicates);
    }

    private double eval140414048187392(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140414048184224(predicates);
        else
            return eval140414048187360(predicates);
    }

    private double eval140414048187360(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140414048184224(predicates);
        else
            return eval140414048187328(predicates);
    }

    private double eval140414048187584(EmailPredicates predicates) {
        if (predicates.subjectContainsNewsletter())
            return eval140414048187424(predicates);
        else
            return eval140414048187552(predicates);
    }

    private double eval140414048187552(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140414048187488(predicates);
        else
            return eval140414048187520(predicates);
    }

    private double eval140414048187520(EmailPredicates predicates) {
        if (predicates.inAddressBook())
            return eval140414048187392(predicates);
        else
            return eval140414048184224(predicates);
    }

    private double eval140414048187488(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140414048184224(predicates);
        else
            return eval140414048187456(predicates);
    }

    private double eval140414048187456(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140414048184224(predicates);
        else
            return eval140414048183008(predicates);
    }

    private double eval140414048184224(EmailPredicates predicates) {
        return 0.0;
    }

    private double eval140414048187328(EmailPredicates predicates) {
        return 0.5;
    }

    private double eval140414048183008(EmailPredicates predicates) {
        return 1.0;
    }
}
