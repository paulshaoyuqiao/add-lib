/* This file was generated with the ADD-Lib
 * http://add-lib.scce.info/ */

package info.scce.addlib.mide.example.email.baseline;

import info.scce.addlib.mide.example.email.EmailPredicates;

public class FrederiksFuzzyUnderstandingOfUrgentEmailsProbabilistic {

	public double emailUrgent(EmailPredicates predicates) {
		return eval140414030797216(predicates);
	}

	private double eval140414030797216(EmailPredicates predicates) {
		if (predicates.subjectContainsUrgent())
			return eval140414030795776(predicates);
		else
			return eval140414030797184(predicates);
	}

	private double eval140414030797184(EmailPredicates predicates) {
		if (predicates.subjectContainsDeadline())
			return eval140414030796480(predicates);
		else
			return eval140414030797152(predicates);
	}

	private double eval140414030797152(EmailPredicates predicates) {
		if (predicates.subjectContainsNewsletter())
			return eval140414030796800(predicates);
		else
			return eval140414030797120(predicates);
	}

	private double eval140414030797120(EmailPredicates predicates) {
		if (predicates.fromGov())
			return eval140414030796928(predicates);
		else
			return eval140414030797088(predicates);
	}

	private double eval140414030797088(EmailPredicates predicates) {
		if (predicates.inAddressBook())
			return eval140414030797056(predicates);
		else
			return eval140414030794144(predicates);
	}

	private double eval140414030797056(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030796960(predicates);
		else
			return eval140414030797024(predicates);
	}

	private double eval140414030797024(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030796960(predicates);
		else
			return eval140414030796992(predicates);
	}

	private double eval140414030796928(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030796832(predicates);
		else
			return eval140414030796896(predicates);
	}

	private double eval140414030796896(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030796832(predicates);
		else
			return eval140414030796864(predicates);
	}

	private double eval140414030796800(EmailPredicates predicates) {
		if (predicates.fromGov())
			return eval140414030796608(predicates);
		else
			return eval140414030796768(predicates);
	}

	private double eval140414030796768(EmailPredicates predicates) {
		if (predicates.inAddressBook())
			return eval140414030796736(predicates);
		else
			return eval140414030794144(predicates);
	}

	private double eval140414030796736(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030796640(predicates);
		else
			return eval140414030796704(predicates);
	}

	private double eval140414030796704(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030796640(predicates);
		else
			return eval140414030796672(predicates);
	}

	private double eval140414030796608(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030796512(predicates);
		else
			return eval140414030796576(predicates);
	}

	private double eval140414030796576(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030796512(predicates);
		else
			return eval140414030796544(predicates);
	}

	private double eval140414030796480(EmailPredicates predicates) {
		if (predicates.subjectContainsOverdue())
			return eval140414030795776(predicates);
		else
			return eval140414030796448(predicates);
	}

	private double eval140414030796448(EmailPredicates predicates) {
		if (predicates.subjectContainsNewsletter())
			return eval140414030796096(predicates);
		else
			return eval140414030796416(predicates);
	}

	private double eval140414030796416(EmailPredicates predicates) {
		if (predicates.fromGov())
			return eval140414030796224(predicates);
		else
			return eval140414030796384(predicates);
	}

	private double eval140414030796384(EmailPredicates predicates) {
		if (predicates.inAddressBook())
			return eval140414030796352(predicates);
		else
			return eval140414030794144(predicates);
	}

	private double eval140414030796352(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030796256(predicates);
		else
			return eval140414030796320(predicates);
	}

	private double eval140414030796320(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030796256(predicates);
		else
			return eval140414030796288(predicates);
	}

	private double eval140414030796224(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030796128(predicates);
		else
			return eval140414030796192(predicates);
	}

	private double eval140414030796192(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030796128(predicates);
		else
			return eval140414030796160(predicates);
	}

	private double eval140414030796096(EmailPredicates predicates) {
		if (predicates.fromGov())
			return eval140414030795904(predicates);
		else
			return eval140414030796064(predicates);
	}

	private double eval140414030796064(EmailPredicates predicates) {
		if (predicates.inAddressBook())
			return eval140414030796032(predicates);
		else
			return eval140414030794144(predicates);
	}

	private double eval140414030796032(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030795936(predicates);
		else
			return eval140414030796000(predicates);
	}

	private double eval140414030796000(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030795936(predicates);
		else
			return eval140414030795968(predicates);
	}

	private double eval140414030795904(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030795808(predicates);
		else
			return eval140414030795872(predicates);
	}

	private double eval140414030795872(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030795808(predicates);
		else
			return eval140414030795840(predicates);
	}

	private double eval140414030795776(EmailPredicates predicates) {
		if (predicates.subjectContainsNewsletter())
			return eval140414030795424(predicates);
		else
			return eval140414030795744(predicates);
	}

	private double eval140414030795744(EmailPredicates predicates) {
		if (predicates.fromGov())
			return eval140414030795552(predicates);
		else
			return eval140414030795712(predicates);
	}

	private double eval140414030795712(EmailPredicates predicates) {
		if (predicates.inAddressBook())
			return eval140414030795680(predicates);
		else
			return eval140414030794144(predicates);
	}

	private double eval140414030795680(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030795584(predicates);
		else
			return eval140414030795648(predicates);
	}

	private double eval140414030795648(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030795584(predicates);
		else
			return eval140414030795616(predicates);
	}

	private double eval140414030795552(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030795456(predicates);
		else
			return eval140414030795520(predicates);
	}

	private double eval140414030795520(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030795456(predicates);
		else
			return eval140414030795488(predicates);
	}

	private double eval140414030795424(EmailPredicates predicates) {
		if (predicates.fromGov())
			return eval140414030795232(predicates);
		else
			return eval140414030795392(predicates);
	}

	private double eval140414030795392(EmailPredicates predicates) {
		if (predicates.inAddressBook())
			return eval140414030795360(predicates);
		else
			return eval140414030794144(predicates);
	}

	private double eval140414030795360(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030795264(predicates);
		else
			return eval140414030795328(predicates);
	}

	private double eval140414030795328(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030795264(predicates);
		else
			return eval140414030795296(predicates);
	}

	private double eval140414030795232(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140414030795136(predicates);
		else
			return eval140414030795200(predicates);
	}

	private double eval140414030795200(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140414030795136(predicates);
		else
			return eval140414030795168(predicates);
	}

	private double eval140414030794144(EmailPredicates predicates) {
		return 0.0;
	}

	private double eval140414030796992(EmailPredicates predicates) {
		return 0.10080000000000001;
	}

	private double eval140414030796960(EmailPredicates predicates) {
		return 0.011199999999999995;
	}

	private double eval140414030796864(EmailPredicates predicates) {
		return 0.14400000000000004;
	}

	private double eval140414030796832(EmailPredicates predicates) {
		return 0.016;
	}

	private double eval140414030796672(EmailPredicates predicates) {
		return 0.018900000000000004;
	}

	private double eval140414030796640(EmailPredicates predicates) {
		return 0.002099999999999999;
	}

	private double eval140414030796544(EmailPredicates predicates) {
		return 0.108;
	}

	private double eval140414030796512(EmailPredicates predicates) {
		return 0.011999999999999997;
	}

	private double eval140414030796288(EmailPredicates predicates) {
		return 0.3024;
	}

	private double eval140414030796256(EmailPredicates predicates) {
		return 0.03359999999999998;
	}

	private double eval140414030796160(EmailPredicates predicates) {
		return 0.432;
	}

	private double eval140414030796128(EmailPredicates predicates) {
		return 0.04799999999999999;
	}

	private double eval140414030795968(EmailPredicates predicates) {
		return 0.05670000000000001;
	}

	private double eval140414030795936(EmailPredicates predicates) {
		return 0.006299999999999997;
	}

	private double eval140414030795840(EmailPredicates predicates) {
		return 0.324;
	}

	private double eval140414030795808(EmailPredicates predicates) {
		return 0.03599999999999999;
	}

	private double eval140414030795616(EmailPredicates predicates) {
		return 0.504;
	}

	private double eval140414030795584(EmailPredicates predicates) {
		return 0.055999999999999966;
	}

	private double eval140414030795488(EmailPredicates predicates) {
		return 0.7200000000000001;
	}

	private double eval140414030795456(EmailPredicates predicates) {
		return 0.07999999999999999;
	}

	private double eval140414030795296(EmailPredicates predicates) {
		return 0.09450000000000001;
	}

	private double eval140414030795264(EmailPredicates predicates) {
		return 0.010499999999999994;
	}

	private double eval140414030795168(EmailPredicates predicates) {
		return 0.54;
	}

	private double eval140414030795136(EmailPredicates predicates) {
		return 0.059999999999999984;
	}
}
