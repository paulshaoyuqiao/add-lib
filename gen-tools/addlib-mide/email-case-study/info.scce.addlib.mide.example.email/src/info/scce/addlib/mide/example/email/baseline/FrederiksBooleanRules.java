/* This file was generated with the ADD-Lib
 * http://add-lib.scce.info/ */

package info.scce.addlib.mide.example.email.baseline;

import info.scce.addlib.mide.example.email.EmailPredicates;

public class FrederiksBooleanRules {

    public boolean trustedSender(EmailPredicates predicates) {
        return eval140566071339904(predicates);
    }

    public boolean alertingSubject(EmailPredicates predicates) {
        return eval140566071340160(predicates);
    }

    public boolean irrelevantNewsletter(EmailPredicates predicates) {
        return eval140566071340320(predicates);
    }

    public boolean irrelevantFromShop(EmailPredicates predicates) {
        return eval140566071340512(predicates);
    }

    private boolean eval140566071339904(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140566071339744(predicates);
        else
            return eval140566071339872(predicates);
    }

    private boolean eval140566071339872(EmailPredicates predicates) {
        if (predicates.inAddressBook())
            return eval140566071339744(predicates);
        else
            return eval140566071339840(predicates);
    }

    private boolean eval140566071340160(EmailPredicates predicates) {
        if (predicates.subjectContainsUrgent())
            return eval140566071339744(predicates);
        else
            return eval140566071340128(predicates);
    }

    private boolean eval140566071340128(EmailPredicates predicates) {
        if (predicates.subjectContainsDeadline())
            return eval140566071339744(predicates);
        else
            return eval140566071339840(predicates);
    }

    private boolean eval140566071340320(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140566071339840(predicates);
        else
            return eval140566071340288(predicates);
    }

    private boolean eval140566071340288(EmailPredicates predicates) {
        if (predicates.subjectContainsNewsletter())
            return eval140566071339744(predicates);
        else
            return eval140566071339840(predicates);
    }

    private boolean eval140566071340512(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140566071339744(predicates);
        else
            return eval140566071340480(predicates);
    }

    private boolean eval140566071340480(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140566071339744(predicates);
        else
            return eval140566071339840(predicates);
    }

    private boolean eval140566071339840(EmailPredicates predicates) {
        return false;
    }

    private boolean eval140566071339744(EmailPredicates predicates) {
        return true;
    }
}
