/* This file was generated with the ADD-Lib
 * http://add-lib.scce.info/ */

package info.scce.addlib.mide.example.email.baseline;

import info.scce.addlib.mide.example.email.EmailPredicates;

public class FrederiksFuzzyUnderstandingOfUrgentEmailsProbabilistic2Bins {

    public double emailUrgent(EmailPredicates predicates) {
        return eval140414238907584(predicates);
    }

    private double eval140414238907584(EmailPredicates predicates) {
        if (predicates.subjectContainsUrgent())
            return eval140414238907488(predicates);
        else
            return eval140414238907552(predicates);
    }

    private double eval140414238907552(EmailPredicates predicates) {
        if (predicates.subjectContainsDeadline())
            return eval140414238907520(predicates);
        else
            return eval140414238904224(predicates);
    }

    private double eval140414238907520(EmailPredicates predicates) {
        if (predicates.subjectContainsOverdue())
            return eval140414238907488(predicates);
        else
            return eval140414238904224(predicates);
    }

    private double eval140414238907488(EmailPredicates predicates) {
        if (predicates.subjectContainsNewsletter())
            return eval140414238907392(predicates);
        else
            return eval140414238907456(predicates);
    }

    private double eval140414238907456(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140414238907360(predicates);
        else
            return eval140414238907424(predicates);
    }

    private double eval140414238907424(EmailPredicates predicates) {
        if (predicates.inAddressBook())
            return eval140414238907360(predicates);
        else
            return eval140414238904224(predicates);
    }

    private double eval140414238907360(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140414238904224(predicates);
        else
            return eval140414238907328(predicates);
    }

    private double eval140414238907328(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140414238904224(predicates);
        else
            return eval140414238903008(predicates);
    }

    private double eval140414238907392(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140414238907360(predicates);
        else
            return eval140414238904224(predicates);
    }

    private double eval140414238904224(EmailPredicates predicates) {
        return 0.0;
    }

    private double eval140414238903008(EmailPredicates predicates) {
        return 1.0;
    }
}
