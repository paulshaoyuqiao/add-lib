package info.scce.addlib.mide.example.email;

public class BooleanServiceDiagramPanel extends ServiceDiagramPanel<Boolean> {

	private static final long serialVersionUID = -6178718005026318976L;

	private static final String[] CATEGORY_NAMES = new String[] { "true", "false" };

	public BooleanServiceDiagramPanel(String serviceName) {
		super(serviceName, CATEGORY_NAMES);
	}

	@Override
	public int category(Boolean value) {
		return value ? 1 : 0;
	}
}
