package info.scce.addlib.mide.example.email;

public class Email implements EmailPredicates {

	private boolean fromShopA, fromShopB, subjectContainsUrgent, inAddressBook, subjectContainsDeadline, fromGov,
			subjectContainsOverdue, subjectContainsNewsletter;

	public Email(boolean fromShopA, boolean fromShopB, boolean subjectContainsUrgent, boolean inAddressBook,
			boolean subjectContainsDeadline, boolean fromGov, boolean subjectContainsOverdue,
			boolean subjectContainsNewsletter) {

		this.fromShopA = fromShopA;
		this.fromShopB = fromShopB;
		this.subjectContainsUrgent = subjectContainsUrgent;
		this.inAddressBook = inAddressBook;
		this.subjectContainsDeadline = subjectContainsDeadline;
		this.fromGov = fromGov;
		this.subjectContainsOverdue = subjectContainsOverdue;
		this.subjectContainsNewsletter = subjectContainsNewsletter;
	}

	@Override
	public boolean fromShopA() {
		return fromShopA;
	}

	@Override
	public boolean fromShopB() {
		return fromShopB;
	}

	@Override
	public boolean subjectContainsUrgent() {
		return subjectContainsUrgent;
	}

	@Override
	public boolean inAddressBook() {
		return inAddressBook;
	}

	@Override
	public boolean subjectContainsDeadline() {
		return subjectContainsDeadline;
	}

	@Override
	public boolean fromGov() {
		return fromGov;
	}

	@Override
	public boolean subjectContainsOverdue() {
		return subjectContainsOverdue;
	}

	@Override
	public boolean subjectContainsNewsletter() {
		return subjectContainsNewsletter;
	}

	@Override
	public String toString() {
		String nl = System.lineSeparator();
		return "Email:" + nl + "  fromShopA: " + fromShopA + nl + "  fromShopB: " + fromShopB + nl
				+ "  subjectContainsUrgent: " + subjectContainsUrgent + nl + "  inAddressBook: " + inAddressBook + nl
				+ "  subjectContainsDeadline: " + subjectContainsDeadline + nl + "  fromGov: " + fromGov + nl
				+ "  subjectContainsOverdue: " + subjectContainsOverdue + nl + "  subjectContainsNewsletter: "
				+ subjectContainsNewsletter;
	}
}
