/* This file was generated with the ADD-Lib
 * http://add-lib.scce.info/ */

package info.scce.addlib.mide.example.email.baseline;

import info.scce.addlib.mide.example.email.EmailPredicates;

public class FrederiksBooleanUnderstandingOfUrgentEmails {

    public boolean emailUrgent(EmailPredicates predicates) {
        return eval140566102090688(predicates);
    }

    private boolean eval140566102090688(EmailPredicates predicates) {
        if (predicates.subjectContainsUrgent())
            return eval140566102090624(predicates);
        else
            return eval140566102090656(predicates);
    }

    private boolean eval140566102090656(EmailPredicates predicates) {
        if (predicates.subjectContainsDeadline())
            return eval140566102090624(predicates);
        else
            return eval140566102089600(predicates);
    }

    private boolean eval140566102090624(EmailPredicates predicates) {
        if (predicates.subjectContainsNewsletter())
            return eval140566102090592(predicates);
        else
            return eval140566102090560(predicates);
    }

    private boolean eval140566102090560(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140566102090496(predicates);
        else
            return eval140566102090528(predicates);
    }

    private boolean eval140566102090528(EmailPredicates predicates) {
        if (predicates.inAddressBook())
            return eval140566102090496(predicates);
        else
            return eval140566102089600(predicates);
    }

    private boolean eval140566102090496(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140566102089600(predicates);
        else
            return eval140566102090464(predicates);
    }

    private boolean eval140566102090464(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140566102089600(predicates);
        else
            return eval140566102089440(predicates);
    }

    private boolean eval140566102090592(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140566102090496(predicates);
        else
            return eval140566102089600(predicates);
    }

    private boolean eval140566102089600(EmailPredicates predicates) {
        return false;
    }

    private boolean eval140566102089440(EmailPredicates predicates) {
        return true;
    }
}
