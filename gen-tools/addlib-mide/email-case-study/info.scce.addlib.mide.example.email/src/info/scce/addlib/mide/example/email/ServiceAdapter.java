package info.scce.addlib.mide.example.email;

import javax.swing.SwingUtilities;

public class ServiceAdapter<R> {

	private final Service<EmailPredicates, R> service;
	private final Service<EmailPredicates, R> referenceService;
	private final ServiceDiagramPanel<R> diagram;

	public ServiceAdapter(Service<EmailPredicates, R> service, Service<EmailPredicates, R> referenceService,
			ServiceDiagramPanel<R> diagram) {

		this.service = service;
		this.referenceService = referenceService;
		this.diagram = diagram;
	}

	public void post(Email email) {
		R result = service.function().apply(email);
		R referenceResult = referenceService.function().apply(email);
		System.out.println(service.name() + " = " + result);
		SwingUtilities.invokeLater(() -> diagram.post(result, referenceResult));
	}
}
