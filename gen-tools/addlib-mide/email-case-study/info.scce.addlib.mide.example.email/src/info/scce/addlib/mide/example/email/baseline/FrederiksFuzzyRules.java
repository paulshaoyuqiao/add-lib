/* This file was generated with the ADD-Lib
 * http://add-lib.scce.info/ */

package info.scce.addlib.mide.example.email.baseline;

import info.scce.addlib.mide.example.email.EmailPredicates;

public class FrederiksFuzzyRules {

    public double irrelevantNewsletter(EmailPredicates predicates) {
        return eval140710792677792(predicates);
    }

    public double trustedSender(EmailPredicates predicates) {
        return eval140710792678016(predicates);
    }

    public double irrelevantFromShop(EmailPredicates predicates) {
        return eval140710792678272(predicates);
    }

    public double alertingSubject(EmailPredicates predicates) {
        return eval140710792678592(predicates);
    }

    private double eval140710792677792(EmailPredicates predicates) {
        if (predicates.subjectContainsNewsletter())
            return eval140710792677728(predicates);
        else
            return eval140710792677760(predicates);
    }

    private double eval140710792677728(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140710792677664(predicates);
        else
            return eval140710792677696(predicates);
    }

    private double eval140710792678016(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140710792677824(predicates);
        else
            return eval140710792677984(predicates);
    }

    private double eval140710792677984(EmailPredicates predicates) {
        if (predicates.inAddressBook())
            return eval140710792677920(predicates);
        else
            return eval140710792677952(predicates);
    }

    private double eval140710792678272(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140710792678112(predicates);
        else
            return eval140710792678240(predicates);
    }

    private double eval140710792678240(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140710792678112(predicates);
        else
            return eval140710792678208(predicates);
    }

    private double eval140710792678592(EmailPredicates predicates) {
        if (predicates.subjectContainsUrgent())
            return eval140710792677824(predicates);
        else
            return eval140710792678560(predicates);
    }

    private double eval140710792678560(EmailPredicates predicates) {
        if (predicates.subjectContainsDeadline())
            return eval140710792678528(predicates);
        else
            return eval140710792677760(predicates);
    }

    private double eval140710792678528(EmailPredicates predicates) {
        if (predicates.subjectContainsOverdue())
            return eval140710792677824(predicates);
        else
            return eval140710792678496(predicates);
    }

    private double eval140710792677760(EmailPredicates predicates) {
        return 0.2;
    }

    private double eval140710792677696(EmailPredicates predicates) {
        return 0.85;
    }

    private double eval140710792677664(EmailPredicates predicates) {
        return 0.4;
    }

    private double eval140710792677952(EmailPredicates predicates) {
        return 0.0;
    }

    private double eval140710792677920(EmailPredicates predicates) {
        return 0.7;
    }

    private double eval140710792677824(EmailPredicates predicates) {
        return 1.0;
    }

    private double eval140710792678208(EmailPredicates predicates) {
        return 0.1;
    }

    private double eval140710792678112(EmailPredicates predicates) {
        return 0.9;
    }

    private double eval140710792678496(EmailPredicates predicates) {
        return 0.6;
    }
}
