package info.scce.addlib.mide.probabilisticlogic;

public class ProbabilisticLogic {

	public double and(double a, double b) {
		return a * b;
	}

	public double or(double a, double b) {
		return 1.0 - (1.0 - a) * (1.0 - b);
	}

	public double not(double a) {
		return 1.0 - a;
	}

	public double d2(double a) {
		if (a < 1.0 / 2)
			return 0.0;
		else
			return 1.0;
	}

	public double d3(double a) {
		if (a < 1.0 / 3)
			return 0.0;
		else if (a < 2.0 / 3)
			return 0.5;
		else
			return 1.0;
	}
}
