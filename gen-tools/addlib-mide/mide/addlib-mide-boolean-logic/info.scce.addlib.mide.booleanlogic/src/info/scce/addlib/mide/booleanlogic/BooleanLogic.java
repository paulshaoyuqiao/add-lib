package info.scce.addlib.mide.booleanlogic;

public class BooleanLogic {

	public boolean and(boolean a, boolean b) {
		return a && b;
	}

	public boolean or(boolean a, boolean b) {
		return a || b;
	}

	public boolean not(boolean a) {
		return !a;
	}
}