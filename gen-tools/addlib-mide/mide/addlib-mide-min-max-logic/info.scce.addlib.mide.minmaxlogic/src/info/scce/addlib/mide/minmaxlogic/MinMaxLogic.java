package info.scce.addlib.mide.minmaxlogic;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class MinMaxLogic {

	public double and(double a, double b) {
		return min(a, b);
	}

	public double or(double a, double b) {
		return max(a, b);
	}

	public double not(double a) {
		return 1.0 - a;
	}

	public double d2(double a) {
		if (a < 1.0 / 2)
			return 0.0;
		else
			return 1.0;
	}

	public double d3(double a) {
		if (a < 1.0 / 3)
			return 0.0;
		else if (a < 2.0 / 3)
			return 0.5;
		else
			return 1.0;
	}
}
