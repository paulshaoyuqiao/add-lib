#!/bin/bash

# Clean JNI header file for native library 
if [[ "$1" = "" ]] || [[ "$1" = "jniheader" ]]; then
  echo ">>>>>> Cleaning JNI header file for native library..."
  rm ./nativelib/include/info_scce_addlib_cudd_Cudd.h
fi

# Clean native library 
if [[ "$1" = "" ]] || [[ "$1" = "nativelib" ]]; then
  echo ">>>>>> Cleaning native library..."
  rm -r ./nativelib/build
fi

# Remove compiled native library from java library resources 
if [[ $1 == "" ]] || [[ $1 == "copynativelib" ]]; then 
  echo ">>>>>> Removing native library from java library resources..."
  os="$(uname -s)"
  if [[ $os = "Darwin" ]]; then os=mac; sharedlibext=dylib; 
  elif [[ $os = "Linux" ]]; then os=linux; sharedlibext=so; 
  else echo "Operating system name '$os' not yet supported. Please copy the native library manually." 
  fi
  arch="$(uname -m)"
  if [[ $arch = "x86_64" ]]; then arch=x64
  else echo "Machine hardware name '$arch' not yet supported. Please copy the native library manually." 
  fi
  targetdir="./java/src/main/resources/info/scce/addlib/cudd/nativelib/$os/$arch"
  rm $targetdir/*.$sharedlibext
fi

# Remove licence file from resources 
if [[ "$1" = "" ]] || [[ "$1" = "copylicence" ]]; then
  echo ">>>>>> Removing licence from Java library resources..."
  rm java/src/main/resources/info/scce/addlib/LICENCE.md
fi

# Clean Java library 
if [[ "$1" = "" ]] || [[ "$1" = "java" ]]; then
  echo ">>>>>> Cleaning Java library..."
  rm -r ./java/target
fi

