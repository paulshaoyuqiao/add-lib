# Usage

To use the latest release of the ADD-Lib in your Maven project, simply include the following dependency in your `pom.xml`.

```xml
<dependencies>
    ...
    <dependency>
        <groupId>info.scce</groupId>
        <artifactId>addlib</artifactId>
        <version>2.0.0-BETA</version>
    </dependency>
</dependencies>
```

# Build Instructions

The following instructions describe the entire build process for the ADD-Lib. However, in most cases it will be sufficient to build only the Java part of the library in `./java`. Treat it like any other Maven project `mvn clean install`. 

## Requirements

-   OS 
    -   Linux x64, 16.04.1 or newer
    -   macOS x64, 10.13.4 or newer
    -   Windows x64, 10 or newer 
-   CMake 3.5 or newer 
-   JDK 1.8.0 or newer 
-   C and C++ compiler with **32 bit integers** and **64 bit doubles** 
-   Maven 3.3.9 or newer
-   Graphviz DOT 2.38.0 or newer 

## Linux and macOS

For these two operating systems we have scripts set up to build and to clean the entire project. 

-   Use `./make.sh jniheader` to generate the JNI header files that declare native functions expected by the Java library. These files will appear in the native library subproject in `nativelib/include`. 
-   Use `./make.sh nativelib` to build the native library subproject. The compiled library will appear in `nativelib/build`. 
-   Use `./make.sh copynativelib` to copy the compiled native library to the resource folder of the Java subproject that corresponds to your platform. The exact location `java/src/main/resources/info/scce/addlib/nativelib/...` depends your platform. Note, that this update only the the native libraries for your very platform. 
-   Use `./make.sh java` to build the Java subproject. The result will be in `./java/target`. 
-   Use `./make.sh test` to run all test cases. 
-   Use `./make.sh` to perform all of the above steps at once and in order of their dependencies. 
-   Use `./clean.sh jniheader` to clean the generated JNI header files.
-   Use `./clean.sh nativelib` to clean the native library subproject build. 
-   Use `./clean.sh copynativelib` to clean the generated JNI header files.
-   Use `./clean.sh java` to clean the Java subproject build. 
-   Use `./clean.sh` to perform all of the above steps at once and in order of their dependencies. 

## Windows 10

It's a journey: For simplicity we will only cover the steps to build Windows-specific parts of the ADD-Lib. First, install MSYS2 on your 64 bit machine, open the **MSYS2 MinGW 64-bit** terminal and update software by running the update command **repeatedly**: 

```sh
pacman -Syuu
```

Now, install the needed build tools for the native library:

```sh
pacman -S --needed \
    base-devel \
    mingw-w64-x86_64-toolchain \
    mingw-w64-x86_64-cmake
```

Compile the native library and place the DLL in the Java library's resource folder for Windows: 

```sh
mkdir -p ./java/src/main/resources/info/scce/addlib/cudd/nativelib/windows/x64
x86_64-w64-mingw32-g++ \
    -static \
    -I"$JAVA_HOME/include" \
    -I"$JAVA_HOME/include/win32" \
    -I"./nativelib/include" \
    -I"./nativelib/cudd-3-0-0/include" \
    ./nativelib/source/*.cpp \
    ./nativelib/cudd-3-0-0/windows/x64/libcudd.a \
    -shared \
    -o ./java/src/main/resources/info/scce/addlib/cudd/nativelib/windows/x64/addlib.dll
```

Finally, you can build and test the Java library. Do this in the **Command Prompt** where only Windows DLLs are available. Extend the path to use Java, Maven, and DOT and run all tests: 

    set PATH=%PATH%;%JAVA_HOME%/bin;%M2_HOME%/bin;%GRAPHVIZ_HOME%/bin
    cd ./java 
    mvn test 
