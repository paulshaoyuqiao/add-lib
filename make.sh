#!/bin/bash

# Generate JNI header file for native library 
if [[ $1 = "" ]] || [[ $1 = "jniheader" ]]; then
  echo ">>>>>> Generating JNI header file for native library..."
  mkdir ./tmp
  javac -d ./tmp -h ./nativelib/include ./java/src/main/java/info/scce/addlib/cudd/*.java
  rm -r ./tmp
fi

# Make native library 
if [[ $1 == "" ]] || [[ $1 == "nativelib" ]]; then
  echo ">>>>>> Building native library..."
  cd ./nativelib 
  mkdir build 
  cd build 
  cmake ..
  make
  cd ../..
fi

# Copy compiled native library to Java library resources 
if [[ $1 == "" ]] || [[ $1 == "copynativelib" ]]; then 
  echo ">>>>>> Copying native library to Java library resources..."
  os="$(uname -s)"
  if [[ $os = Darwin ]]; then os=mac; sharedlibext=dylib; 
  elif [[ $os = Linux ]]; then os=linux; sharedlibext=so; 
  else echo "Operating system name '$os' not yet supported. Please copy the native library manually." 
  fi
  arch="$(uname -m)"
  if [[ $arch = x86_64 ]]; then arch=x64
  else echo "Machine hardware name '$arch' not yet supported. Please copy the native library manually." 
  fi
  targetdir="./java/src/main/resources/info/scce/addlib/cudd/nativelib/$os/$arch"
  mkdir -p $targetdir
  cp ./nativelib/build/*.$sharedlibext $targetdir 
fi

# Copy licence file to Java library resources 
if [[ $1 == "" ]] || [[ $1 == "copylicence" ]]; then
  echo ">>>>>> Copying licence to Java library resources..."
  cp LICENCE.md java/src/main/resources/info/scce/addlib/LICENCE.md
fi

# Make and test Java library 
if [[ $1 == "" ]] || [[ $1 == "java" ]]; then
  echo ">>>>>> Building and testing Java library..."
  cd ./java
  mvn test
  cd ..
fi
